<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CEstadoCivil;
use App\Services\DTO\CEstadosCiviles\CEstadoCivilRequest;
use App\Services\DTO\CEstadosCiviles\CEstadoCivilResponse;
use App\Services\Mapper\Interfaces\ICEstadoCivilMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CEstadoCivilMapper implements ICEstadoCivilMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(CEstadoCivil::class, CEstadoCivilResponse::class);

        /*CREATE*/
        $config->registerMapping(CEstadoCivilRequest::class, CEstadoCivil::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt());

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
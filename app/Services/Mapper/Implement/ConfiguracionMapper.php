<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Configuracion;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\Configuraciones\ConfiguracionRequest;
use App\Services\DTO\Configuraciones\ConfiguracionResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IConfiguracionMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class ConfiguracionMapper implements IConfiguracionMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Mapea configuraciones */
        $config->registerMapping(Configuracion::class,ConfiguracionResponse::class)
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class));


        /* Ignora Hijos */
        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('usuario', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember("cliente", Operation::ignore());

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        /* Create/Update */
        $config->registerMapping(ConfiguracionRequest::class, Configuracion::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('sucursal', Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, "empresa_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Empleado;
use App\Domain\Cliente;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Domain\Departamento;
use App\Domain\RegistroChecador;
use App\Domain\RelojChecador;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorRequest;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorResponse;
use App\Services\DTO\RelojChecador\RelojChecadorResponse;
use App\Services\Mapper\Interfaces\IRegistroChecadorMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class RegistroChecadorMapper implements IRegistroChecadorMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(RegistroChecador::class, RegistroChecadorResponse::class)
            ->forMember('fecha', Operation::mapAsDate())
            ->forMember('hora', Operation::mapAsTime())
            ->forMember('empleado', Operation::mapInstanceTo(EmpleadoResponse::class))
            ->forMember('reloj_checador', Operation::mapInstanceTo(RelojChecadorResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class))
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('departamento', Operation::mapInstanceTo(DepartamentoResponse::class));

        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('fecha_ingreso', Operation::ignore())
            ->forMember('fecha_baja', Operation::ignore())
            ->forMember('horario', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('incidencias', Operation::ignore())
            ->forMember('documentos', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('relojes_checador', Operation::ignore())
            ->forMember('estado_civil', Operation::ignore());

        $config->registerMapping(RelojChecador::class, RelojChecadorResponse::class)
            ->forMember('empleados', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());
        
        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember('usuario', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(RegistroChecadorRequest::class, RegistroChecador::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('hora', Operation::mapStringToTime())
            ->forMember('empleado', Operation::mapAsEntity(Empleado::class, "empleado_id"))
            ->forMember("reloj_checador", Operation::mapAsEntity(RelojChecador::class, "reloj_checador_id"))
            ->forMember("sucursal", Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember("departamento", Operation::mapAsEntity(Departamento::class, "departamento_id"))
            ->forMember('cliente', Operation::mapAsEntity(Cliente::class, 'cliente_id'))
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, 'empresa_id'));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
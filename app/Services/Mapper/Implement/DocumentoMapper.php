<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CTipoDocumento;
use App\Domain\Documento;
use App\Domain\Empleado;
use App\Services\DTO\CTiposDocumentos\CTipoDocumentoResponse;
use App\Services\DTO\Documentos\DocumentoRequest;
use App\Services\DTO\Documentos\DocumentoResponse;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\Mapper\Interfaces\IDocumentoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class DocumentoMapper implements IDocumentoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(Documento::class, DocumentoResponse::class)
            ->forMember('tipo', Operation::mapInstanceTo(CTipoDocumentoResponse::class))
            ->forMember('empleado', Operation::mapInstanceTo(EmpleadoResponse::class));

        $config->registerMapping(CTipoDocumento::class, CTipoDocumentoResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('fecha_ingreso', Operation::ignore())
            ->forMember('fecha_baja', Operation::ignore())
            ->forMember('horario', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('incidencias', Operation::ignore())
            ->forMember('documentos', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('relojes_checador', Operation::ignore())
            ->forMember('estado_civil', Operation::ignore());

        $config->registerMapping(DocumentoRequest::class, Documento::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("tipo", Operation::mapAsEntity(CTipoDocumento::class, "tipo_id"))
            ->forMember("empleado", Operation::mapAsEntity(Empleado::class, "empleado_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
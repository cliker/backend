<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\CEstadoCivil;
use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Documento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Horario;
use App\Domain\Incidencia;
use App\Domain\RegistroChecador;
use App\Domain\RelojChecador;
use App\Domain\Sucursal;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\DTO\CEstadosCiviles\CEstadoCivilResponse;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\Documentos\DocumentoResponse;
use App\Services\DTO\Empleados\EmpleadoRequest;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Horarios\HorarioResponse;
use App\Services\DTO\Incidencias\IncidenciaResponse;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorResponse;
use App\Services\DTO\RelojChecador\RelojChecadorResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IEmpleadoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class EmpleadoMapper implements IEmpleadoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::mapAsDate())
            ->forMember('estado', Operation::mapInstanceTo(CEstadoResponse::class))
            ->forMember('ciudad', Operation::mapInstanceTo(CCiudadResponse::class))
            ->forMember('fecha_ingreso', Operation::mapAsDate())
            ->forMember('fecha_baja', Operation::mapAsDate())
            ->forMember('horario', Operation::mapInstanceTo(HorarioResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('departamento', Operation::mapInstanceTo(DepartamentoResponse::class))
            ->forMember('incidencias', Operation::mapInstanceTo(IncidenciaResponse::class))
            ->forMember('documentos', Operation::mapInstanceTo(DocumentoResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class))
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('registros_checador', Operation::mapInstanceTo(RegistroChecadorResponse::class))
            ->forMember('relojes_checador', Operation::mapInstanceTo(RelojChecadorResponse::class))
            ->forMember('estado_civil', Operation::mapInstanceTo(CEstadoCivilResponse::class));

        $config->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember('ciudades', Operation::ignore());

        $config->registerMapping(CCiudad::class, CCiudadResponse::class)
            ->forMember('estado', Operation::ignore());

        $config->registerMapping(CEstadoCivil::class, CEstadoCivilResponse::class);

        $config->registerMapping(Horario::class, HorarioResponse::class)
            ->forMember('detalles_horarios', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore());

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Incidencia::class, IncidenciaResponse::class)
            ->forMember('tipo', Operation::ignore())
            ->forMember('fecha_inicio', Operation::mapAsDate())
            ->forMember('fecha_final', Operation::mapAsDate())
            ->forMember('empleado', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('estatus', Operation::ignore());

        $config->registerMapping(Documento::class, DocumentoResponse::class)
            ->forMember('tipo', Operation::ignore())
            ->forMember('empleado', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember('usuario', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(RegistroChecador::class, RegistroChecadorResponse::class)
            ->forMember('fecha', Operation::ignore())
            ->forMember('hora', Operation::ignore())
            ->forMember('empleado', Operation::ignore())
            ->forMember('reloj_checador', Operation::ignore())
            ->forMember('empresa', Operation::ignore());

        $config->registerMapping(RelojChecador::class, RelojChecadorResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empleados', Operation::ignore());

        $config->registerMapping(EmpleadoRequest::class, Empleado::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("documentos", Operation::ignore())
            ->forMember("estado", Operation::mapAsEntity(CEstado::class, "estado_id"))
            ->forMember("ciudad", Operation::mapAsEntity(CCiudad::class, "ciudad_id"))
            ->forMember("horario", Operation::mapAsEntity(Horario::class, "horario_id"))
            ->forMember("sucursal", Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember("departamento", Operation::mapAsEntity(Departamento::class, "departamento_id"))
            ->forMember('cliente', Operation::mapAsEntity(Cliente::class, 'cliente_id'))
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, 'empresa_id'))
            ->forMember("estado_civil", Operation::mapAsEntity(CEstadoCivil::class, "estado_civil_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
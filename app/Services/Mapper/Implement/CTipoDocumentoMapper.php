<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Cliente;
use App\Domain\CTipoDocumento;
use App\Domain\Empresa;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\CTiposDocumentos\CTipoDocumentoRequest;
use App\Services\DTO\CTiposDocumentos\CTipoDocumentoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\Mapper\Interfaces\ICTipoDocumentoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CTipoDocumentoMapper implements ICTipoDocumentoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config
            ->registerMapping(CTipoDocumento::class, CTipoDocumentoResponse::class)
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class));

        $config
            ->registerMapping(Empresa::class, EmpresaResponse::class)
            ->forMember("estado", Operation::ignore())
            ->forMember("ciudad", Operation::ignore())
            ->forMember("usuario", Operation::ignore())
            ->forMember("cliente", Operation::ignore())
            ->forMember("sucursales", Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        /*CREATE/UPDATE*/
        $config->registerMapping(CTipoDocumentoRequest::class, CTipoDocumento::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('cliente', Operation::mapAsEntity(Cliente::class, 'cliente_id'))
            ->forMember("empresa", Operation::mapAsEntity(Empresa::class, "empresa_id"));


        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
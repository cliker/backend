<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\RelojChecador;
use App\Domain\Sucursal;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\RelojChecador\RelojChecadorResponse;
use App\Services\DTO\RelojChecador\RelojChecadorRequest;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IRelojChecadorMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class RelojChecadorMapper implements IRelojChecadorMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Reloj Checador */
        $config->registerMapping(RelojChecador::class, RelojChecadorResponse::class)
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('departamento',Operation::mapInstanceTo(DepartamentoResponse::class))
            ->forMember('cliente',Operation::mapInstanceTo(ClienteResponse::class))
            ->forMember('empleados',Operation::mapInstanceTo(EmpleadoResponse::class));

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        /* Ignora Hijos */
        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('usuario', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember("cliente", Operation::ignore());

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('sucursal', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('fecha_ingreso', Operation::ignore())
            ->forMember('fecha_baja', Operation::ignore())
            ->forMember('horario', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('incidencias', Operation::ignore())
            ->forMember('documentos', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('relojes_checador', Operation::ignore())
            ->forMember('estado_civil', Operation::ignore());

        /* Create/Update */
        $config->registerMapping(RelojChecadorRequest::class, RelojChecador::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('empleados', Operation::ignore())
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, "empresa_id"))
            ->forMember('sucursal', Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember("departamento", Operation::mapAsEntity(Departamento::class,"departamento_id"))
            ->forMember("cliente", Operation::mapAsEntity(Cliente::class,"cliente_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
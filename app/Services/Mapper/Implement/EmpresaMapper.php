<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\Cliente;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Domain\Usuario;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Empresas\EmpresaRequest;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Mapper\Interfaces\IEmpresaMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class EmpresaMapper implements IEmpresaMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Empresa */
        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('estado', Operation::mapInstanceTo(CEstadoResponse::class))
            ->forMember('ciudad', Operation::mapInstanceTo(CCiudadResponse::class))
            ->forMember('sucursales', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('usuario', Operation::mapInstanceTo(UsuarioResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class));

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());


        /* Ignora lo relacionado a estado*/
        $config->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember('ciudades', Operation::ignore());

        /* Ciudades */
        $config->registerMapping(CCiudad::class,CCiudadResponse::class)
            ->forMember('estado', Operation::ignore());

        /* Usuario */
        $config->registerMapping(Usuario::class, UsuarioResponse::class)
            ->forMember('cliente', Operation::ignore())
            ->forMember('rol', Operation::ignore())
            ->forMember('fecha_nacimiento',   Operation::mapAsDate())
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore());

        /* Create/Update */
        $config->registerMapping(EmpresaRequest::class, Empresa::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('estado', Operation::mapAsEntity(CEstado::class, "estado_id"))
            ->forMember("ciudad", Operation::mapAsEntity(CCiudad::class, "ciudad_id"))
            ->forMember("cliente", Operation::mapAsEntity(Cliente::class,"cliente_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
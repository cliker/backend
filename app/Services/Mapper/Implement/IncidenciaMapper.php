<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CEstatusIncidencia;
use App\Domain\Cliente;
use App\Domain\CTipoIncidencia;
use App\Domain\Departamento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Incidencia;
use App\Domain\Sucursal;
use App\Services\DTO\CEstatusIncidencias\CEstatusIncidenciaResponse;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\CTiposIncidencias\CTipoIncidenciaResponse;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Incidencias\IncidenciaRequest;
use App\Services\DTO\Incidencias\IncidenciaResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IIncidenciaMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class IncidenciaMapper implements IIncidenciaMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(Incidencia::class, IncidenciaResponse::class)
            ->forMember('tipo', Operation::mapInstanceTo(CTipoIncidenciaResponse::class))
            ->forMember('fecha_inicio', Operation::mapAsDate())
            ->forMember('fecha_final', Operation::mapAsDate())
            ->forMember('empleado', Operation::mapInstanceTo(EmpleadoResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class))
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('departamento', Operation::mapInstanceTo(DepartamentoResponse::class))
            ->forMember('estatus', Operation::mapInstanceTo(CEstatusIncidenciaResponse::class));

        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('fecha_ingreso', Operation::ignore())
            ->forMember('fecha_baja', Operation::ignore())
            ->forMember('horario', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('incidencias', Operation::ignore())
            ->forMember('documentos', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('relojes_checador', Operation::ignore())
            ->forMember('estado_civil', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('usuario', Operation::ignore())
            ->forMember("cliente", Operation::ignore())
            ->forMember('sucursales', Operation::mapInstanceTo(SucursalResponse::class));

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(CTipoIncidencia::class, CTipoIncidenciaResponse::class);
        $config->registerMapping(CEstatusIncidencia::class, CEstatusIncidenciaResponse::class);

        $config->registerMapping(IncidenciaRequest::class, Incidencia::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('tipo', Operation::mapAsEntity(CTipoIncidencia::class, 'tipo_id'))
            ->forMember('empleado', Operation::mapAsEntity(Empleado::class, 'empleado_id'))
            ->forMember('cliente', Operation::mapAsEntity(Cliente::class, 'cliente_id'))
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, 'empresa_id'))
            ->forMember('sucursal', Operation::mapAsEntity(Sucursal::class, 'sucursal_id'))
            ->forMember('departamento', Operation::mapAsEntity(Departamento::class, 'departamento_id'))
            ->forMember('estatus', Operation::mapAsEntity(CEstatusIncidencia::class, 'estatus_id'));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
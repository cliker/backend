<?php
namespace App\Services\Mapper\Implement;

use App\Domain\DetalleHorario;
use App\Domain\Horario;
use App\Services\DTO\DetallesHorarios\DetalleHorarioRequest;
use App\Services\DTO\DetallesHorarios\DetalleHorarioResponse;
use App\Services\DTO\Horarios\HorarioResponse;
use App\Services\Mapper\Interfaces\IDetalleHorarioMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class DetalleHorarioMapper implements IDetalleHorarioMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(DetalleHorario::class, DetalleHorarioResponse::class)
            ->forMember('hora_entrada', Operation::mapAsTime())
            ->forMember('hora_salida', Operation::mapAsTime())
            ->forMember('horario', Operation::mapInstanceTo(HorarioResponse::class));

        $config->registerMapping(Horario::class, HorarioResponse::class)
            ->forMember('detalles_horarios', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore());

        /*CREATE/UPDATE*/
        $config->registerMapping(DetalleHorarioRequest::class, DetalleHorario::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("hora_entrada", Operation::mapStringToTime())
            ->forMember("hora_salida", Operation::mapStringToTime())
            ->forMember('horario', Operation::mapAsEntity(Horario::class, 'horario_id'));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
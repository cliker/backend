<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Empleado;
use App\Domain\HistoricoEmpleado;
use App\Domain\Usuario;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\HistoricoEmpleados\HistoricoEmpleadoRequest;
use App\Services\DTO\HistoricoEmpleados\HistoricoEmpleadoResponse;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Mapper\Interfaces\IHistoricoEmpleadoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class HistoricoEmpleadoMapper implements IHistoricoEmpleadoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Historico */
        $config->registerMapping(HistoricoEmpleado::class, HistoricoEmpleadoResponse::class)
            ->forMember('fecha', Operation::mapAsDate())
            ->forMember('empleado', Operation::mapInstanceTo(EmpleadoResponse::class))
            ->forMember('responsable', Operation::mapInstanceTo(UsuarioResponse::class));

        /* Empleado */
        $config->registerMapping(Empleado::class, EmpleadoResponse::class)
            ->forMember('fecha_nacimiento', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('fecha_ingreso', Operation::ignore())
            ->forMember('fecha_baja', Operation::ignore())
            ->forMember('horario', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore())
            ->forMember('incidencias', Operation::ignore())
            ->forMember('documentos', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('registros_checador', Operation::ignore())
            ->forMember('relojes_checador', Operation::ignore())
            ->forMember('estado_civil', Operation::ignore());

        /* Usuario */
        $config->registerMapping(Usuario::class, UsuarioResponse::class)
            ->forMember('cliente', Operation::ignore())
            ->forMember('rol', Operation::ignore())
            ->forMember('fecha_nacimiento',   Operation::mapAsDate())
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('departamento', Operation::ignore());

        /* Create/Update */
        $config->registerMapping(HistoricoEmpleadoRequest::class, HistoricoEmpleado::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('responsable', Operation::mapAsEntity(Usuario::class, "responsable_id"))
            ->forMember("empleado", Operation::mapAsEntity(Empleado::class, "empleado_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
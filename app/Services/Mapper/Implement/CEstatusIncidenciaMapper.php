<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CEstatusIncidencia;
use App\Services\DTO\CEstatusIncidencias\CEstatusIncidenciaRequest;
use App\Services\DTO\CEstatusIncidencias\CEstatusIncidenciaResponse;
use App\Services\Mapper\Interfaces\ICEstatusIncidenciaMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CEstatusIncidenciaMapper implements ICEstatusIncidenciaMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(CEstatusIncidencia::class, CEstatusIncidenciaResponse::class);

        /*CREATE*/
        $config->registerMapping(CEstatusIncidenciaRequest::class, CEstatusIncidencia::class)
        ->withDefaultOperation(Operation::mapIfRequestHasIt());

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
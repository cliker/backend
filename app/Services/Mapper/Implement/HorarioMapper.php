<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\DetalleHorario;
use App\Domain\Empresa;
use App\Domain\Horario;
use App\Domain\Sucursal;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\DetallesHorarios\DetalleHorarioResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Horarios\HorarioRequest;
use App\Services\DTO\Horarios\HorarioResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IHorarioMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class HorarioMapper implements IHorarioMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config->registerMapping(Horario::class, HorarioResponse::class)
            ->forMember('detalles_horarios', Operation::mapInstanceTo(DetalleHorarioResponse::class))
            ->forMember('departamento', Operation::mapInstanceTo(DepartamentoResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class))
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class));

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember('usuario', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(DetalleHorario::class, DetalleHorarioResponse::class)
            ->forMember('hora_entrada', Operation::mapAsTime())
            ->forMember('hora_salida', Operation::mapAsTime())
            ->forMember('horario', Operation::ignore());

        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('empresa', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(HorarioRequest::class, Horario::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("detalles_horarios", Operation::ignore())
            ->forMember("departamento", Operation::mapAsEntity(Departamento::class, "departamento_id"))
            ->forMember("sucursal", Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember('cliente', Operation::mapAsEntity(Cliente::class, 'cliente_id'))
            ->forMember('empresa', Operation::mapAsEntity(Empresa::class, 'empresa_id'));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\Cliente;
use App\Domain\CEstado;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\Clientes\ClienteRequest;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\Mapper\Interfaces\IClienteMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class ClienteMapper implements IClienteMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /*EMPRESA*/
        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::mapInstanceTo(CEstadoResponse::class))
            ->forMember('ciudad', Operation::mapInstanceTo(CCiudadResponse::class));

        /*ESTADO*/
        $config
            ->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember("ciudades", Operation::ignore());

        /*CIUDAD*/
        $config
            ->registerMapping(CCiudad::class, CCiudadResponse::class)
            ->forMember('estado', Operation::ignore());
        
        /*CREATE/UPDATE*/
        $config->registerMapping(ClienteRequest::class, Cliente::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("estado", Operation::mapAsEntity(CEstado::class, "estado_id"))
            ->forMember("ciudad", Operation::mapAsEntity(CCiudad::class, "ciudad_id"));


        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
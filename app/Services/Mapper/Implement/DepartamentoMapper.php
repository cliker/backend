<?php
namespace App\Services\Mapper\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\DiaFestivo;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Departamentos\DepartamentoRequest;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\DiasFestivos\DiaFestivoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\IDepartamentoMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class DepartamentoMapper implements IDepartamentoMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Departamento */
        $config->registerMapping(Departamento::class, DepartamentoResponse::class)
            ->forMember('empresa', Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember('sucursal', Operation::mapInstanceTo(SucursalResponse::class))
            ->forMember('dias_festivos', Operation::mapInstanceTo(DiaFestivoResponse::class))
            ->forMember('cliente', Operation::mapInstanceTo(ClienteResponse::class));

        /* Ignora Hijos */
        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember('usuario', Operation::ignore())
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('sucursales', Operation::ignore())
            ->forMember("cliente", Operation::ignore());

        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore())
            ->forMember('empresa', Operation::ignore())
            ->forMember('dias_festivos', Operation::ignore())
            ->forMember('cliente', Operation::ignore());

        $config->registerMapping(DiaFestivo::class, DiaFestivoResponse::class)
            ->forMember('fecha', Operation::mapAsDate())
            ->forMember('departamentos', Operation::ignore())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember('estado', Operation::ignore())
            ->forMember('ciudad', Operation::ignore());

        /* Create/update */
        $config->registerMapping(DepartamentoRequest::class, Departamento::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember('sucursal', Operation::mapAsEntity(Sucursal::class, "sucursal_id"))
            ->forMember("empresa", Operation::mapAsEntity(Empresa::class, "empresa_id"))
            ->forMember("dias_festivos", Operation::ignore())
            ->forMember("cliente", Operation::mapAsEntity(Cliente::class,"cliente_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\Cliente;
use App\Domain\DiaFestivo;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CEstados\CEstadoResponse;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\DiasFestivos\DiaFestivoResponse;
use App\Services\DTO\Empresas\EmpresaResponse;
use App\Services\DTO\Sucursales\SucursalRequest;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Mapper\Interfaces\ISucursalMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class SucursalMapper implements ISucursalMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        /* Sucursal */
        $config->registerMapping(Sucursal::class, SucursalResponse::class)
            ->forMember("estado", Operation::mapInstanceTo(CEstadoResponse::class))
            ->forMember("ciudad", Operation::mapInstanceTo(CCiudadResponse::class))
            ->forMember("empresa", Operation::mapInstanceTo(EmpresaResponse::class))
            ->forMember("dias_festivos", Operation::mapInstanceTo(DiaFestivoResponse::class))
            ->forMember("cliente", Operation::mapInstanceTo(ClienteResponse::class));


        /* Ignora relaciones Hijos*/
        $config->registerMapping(CEstado::class, CEstadoResponse::class)
            ->forMember("ciudades", Operation::ignore());

        $config->registerMapping(CCiudad::class,CCiudadResponse::class)
            ->forMember("estado", Operation::ignore());

        $config->registerMapping(Empresa::class,EmpresaResponse::class)
            ->forMember("usuario", Operation::ignore())
            ->forMember("estado", Operation::ignore())
            ->forMember("ciudad", Operation::ignore())
            ->forMember("dias_festivos",Operation::ignore())
            ->forMember("sucursales", Operation::ignore())
            ->forMember("cliente", Operation::ignore());

        $config->registerMapping(DiaFestivo::class, DiaFestivoResponse::class)
            ->forMember("departamento", Operation::ignore())
            ->forMember('fecha', Operation::mapAsDate())
            ->forMember('sucursal', Operation::ignore())
            ->forMember('cliente', Operation::ignore())
            ->forMember('empresa', Operation::ignore());

        $config->registerMapping(Cliente::class, ClienteResponse::class)
            ->forMember("estado", Operation::ignore())
            ->forMember("ciudad", Operation::ignore());

        /* Create/Update */
        $config->registerMapping(SucursalRequest::class, Sucursal::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt())
            ->forMember("estado", Operation::mapAsEntity(CEstado::class, "estado_id"))
            ->forMember("ciudad", Operation::mapAsEntity(CCiudad::class, "ciudad_id"))
            ->forMember("empresa", Operation::mapAsEntity(Empresa::class, "empresa_id"))
            ->forMember("cliente", Operation::mapAsEntity(Cliente::class,"cliente_id"));

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
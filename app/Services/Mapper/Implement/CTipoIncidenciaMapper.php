<?php
namespace App\Services\Mapper\Implement;

use App\Domain\CTipoIncidencia;
use App\Services\DTO\CTiposIncidencias\CTipoIncidenciaRequest;
use App\Services\DTO\CTiposIncidencias\CTipoIncidenciaResponse;
use App\Services\Mapper\Interfaces\ICTipoIncidenciaMapper;
use App\Services\Mapper\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use App\Services\Mapper\Operations\Operation;
use App\Infrastructure\MapperExtensions;

class CTipoIncidenciaMapper implements ICTipoIncidenciaMapper
{

    /**
     * @var AutoMapper
     */
    public $mapper;
    //public $enumMapper;

    public function __construct()
    {
        //$this->enumMapper = new EnumMapper();
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();

        $config
            ->registerMapping(CTipoIncidencia::class, CTipoIncidenciaResponse::class);

        /*CREATE*/
        $config->registerMapping(CTipoIncidenciaRequest::class, CTipoIncidencia::class)
            ->withDefaultOperation(Operation::mapIfRequestHasIt());

        $config = MapperExtensions::registerMappingsForProxies($config);

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
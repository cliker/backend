<?php
/**
 * Created by PhpStorm.
 * User: erichuerta
 * Date: 23/05/18
 * Time: 18:20
 */

namespace App\Services\Mapper\Implement;

use App\Exceptions\ApiValidationException;
use App\Services\DTO\Base\Exceptions\DeveloperExceptionInfoResponse;
use App\Services\DTO\Base\Exceptions\ExceptionResponse;
use App\Services\DTO\Base\Exceptions\TraceExceptionResponse;
use App\Services\Mapper\AutoMapper;
use App\Services\Mapper\Interfaces\IExceptionMapper;
use App\Services\Mapper\Operations\Operation;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use Exception;
use stdClass;
use function class_basename;
use function config;
use function get_class;

class ExceptionMapper implements IExceptionMapper
{
    /**
     * @var AutoMapper
     */
    public $mapper;

    public function __construct()
    {
        $this->initialize();
    }

    public function initialize()
    {
        $config = new AutoMapperConfig();
        $config
            ->registerMapping(Exception::class, ExceptionResponse::class)
            ->forMember("type", function (Exception $exception) {
                return class_basename($exception);
            })
            ->forMember("status_code", function (Exception $exception) {
                return $exception->getCode();
            })
            ->forMember("message", function (Exception $exception) {
                return $exception->getMessage();
            })
            ->forMember("developer_info", function (Exception $exception) {
                if(config("app.debug")| request()->getHost() == "localhost"){
                    $this->mapper->getConfiguration()->registerMapping(get_class($exception), DeveloperExceptionInfoResponse::class)
                        ->forMember("exception_class", function(Exception $exception){
                            return get_class($exception);
                        })
                        ->forMember("trace", function(Exception $exception){
                            $data = collect();
                            foreach ($exception->getTrace() as $trace){
                                $data->push($this->mapper->map((object) $trace, TraceExceptionResponse::class));
                            }
                            return $data;
                        });
                    return $this->mapper->map($exception, DeveloperExceptionInfoResponse::class);
                }
            });

        $config
            ->registerMapping(stdClass::class, TraceExceptionResponse::class)
            ->forMember("args", Operation::ignore());

        /*
        |--------------------------------------------------------------------------
        | ApiValidationException
        |--------------------------------------------------------------------------
        */
        $config->registerMapping(ApiValidationException::class, ExceptionResponse::class)
            ->copyFrom(Exception::class, ExceptionResponse::class)
            ->forMember("data", function (ApiValidationException $exception) {
                return [
                    "errors"=>$exception->getErrors()
                ];
            });

        $this->mapper = new AutoMapper($config);
    }

    /**
     * @return AutoMapper
     */
    public function getMapper()
    {
        return $this->mapper;
    }
}
<?php

namespace App\Services\Mapper\Operations;
use App\Services\Mapper\Operations\Implement\MapAsBoolean;
use App\Services\Mapper\Operations\Implement\MapAsDate;
use App\Services\Mapper\Operations\Implement\MapAsDateTime;
use App\Services\Mapper\Operations\Implement\MapAsEntity;
use App\Services\Mapper\Operations\Implement\MapAsInteger;
use App\Services\Mapper\Operations\Implement\MapAsTime;
use App\Services\Mapper\Operations\Implement\MapIfRequestHasIt;
use App\Services\Mapper\Operations\Implement\MapInstanceTo;
use App\Services\Mapper\Operations\Implement\MapIfNotEmpty;
use App\Services\Mapper\Operations\Implement\MapRound;
use App\Services\Mapper\Operations\Implement\MapSourceTo;
use App\Services\Mapper\Operations\Implement\MapStringToTime;

class Operation extends \AutoMapperPlus\MappingOperation\Operation
{
    public static function mapAsDate(string $format = 'Y-m-d'){
        return new MapAsDate($format);
    }
    public static function mapAsDateTime(string $format = 'Y-m-d H:i:s'){
        return new MapAsDateTime($format);
    }
    public static function mapAsTime(string $format = 'H:i:s'){
        return new MapAsTime($format);
    }
    public static function mapStringToTime(){
        return new MapStringToTime();
    }
    public static function mapAsFloat(int $precision = 2){
        return new MapRound($precision);
    }
    public static function mapAsBoolean(){
        return new MapAsBoolean();
    }
    public static function mapAsInteger(){
        return new MapAsInteger();
    }
    public static function mapInstanceTo($destinationClass, bool $initializeProxy = false){
        return new MapInstanceTo($destinationClass, $initializeProxy);
    }
    public static function mapIfNotEmpty(){
        return new MapIfNotEmpty();
    }
    public static function mapIfRequestHasIt(){
        return new MapIfRequestHasIt();
    }
    public static function mapSourceTo($destinationClass){
        return new MapSourceTo($destinationClass);
    }

    public static function mapAsEntity($destinationClass, $sourcePropertyName = null){
        return new MapAsEntity($destinationClass, $sourcePropertyName);
    }
}
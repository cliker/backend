<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IExportarDatosService extends IBaseService
{
    public function exportData($nombreArchivo, $titulosEncabezado, $datos, $cellsBgColor, $numAutoFilter, $arrayFormat);
    public function layoutEmpleado($nombreArchivo, $titulosEncabezado, $datos, $limite_filas, $arrayFormat, $tipo, $rol, $arrayGeneral );
}
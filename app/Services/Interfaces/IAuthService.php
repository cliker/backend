<?php
namespace App\Services\Interfaces;


use Illuminate\Http\Request;

interface IAuthService
{
    public function login(Request $request);
    public function logout(Request $request);
}
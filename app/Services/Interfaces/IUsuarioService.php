<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IUsuarioService extends IBaseService
{
    public function exportLayout($request);
    public function password($request);
}
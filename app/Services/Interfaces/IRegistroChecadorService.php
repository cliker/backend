<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IRegistroChecadorService extends IBaseService
{
    public function createFromAsp($request, $tipo);
    public function getLastIdAsp($empresa_id);
    public function updateChecadas();
    public function historico($id);
}
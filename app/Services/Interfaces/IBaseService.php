<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 14/02/18
 * Time: 16:14
 */

namespace App\Services\Interfaces;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

interface IBaseService
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request);

    public function get($id);

    /**
     * @param Request $request
     */
    public function create($request);

    /**
     * @param Request $request
     */
    public function update($request);

    /**
     * @param Request $request
     */
    public function delete($id);
}
<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IRelojChecadorService extends IBaseService
{
    public function drop($id);
    public function removeEmployee($request);
}
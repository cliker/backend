<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IArchivoService extends IBaseService
{
    public function documentoEmpleadoStore($request);
    public function documentoEmpleadoUpdate($request);
    public function documentoEmpleadoDelete($id);
}
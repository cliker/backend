<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IImportarDatosService
{
    public function importarEmpleados($request);
}
<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IEmpleadoService extends IBaseService
{
    public function findChecked($request);
    public function creaUsuario($request);
    public function dashboard($request);
    public function reingreso($request);
    public function asignacion($request);
    public function rotacion($request);
    public function incidencias($request);
}
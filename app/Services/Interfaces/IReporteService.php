<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IReporteService extends IBaseService
{
    public function asistencia($request);
    public function incidencias($request);
    
}
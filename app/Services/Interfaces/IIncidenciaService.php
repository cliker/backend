<?php
namespace App\Services\Interfaces;

use Illuminate\Http\Request;

interface IIncidenciaService extends IBaseService
{
    public function documento($request);
    public function tiposIncidencia($request);
}
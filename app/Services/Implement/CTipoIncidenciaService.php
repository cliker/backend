<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICTipoIncidenciaQuery;
use App\DataAccess\Repositories\Interfaces\ICTipoIncidenciaRepository;
use App\Domain\CTipoIncidencia;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\CTiposIncidencias\CTipoIncidenciaRequest;
use App\Services\DTO\CTiposIncidencias\CTipoIncidenciaResponse;
use App\Services\DTO\CTiposIncidencias\FindCTiposIncidenciasRequest;
use App\Services\Interfaces\ICTipoIncidenciaService;
use App\Services\Mapper\Interfaces\ICTipoIncidenciaMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class CTipoIncidenciaService implements ICTipoIncidenciaService
{
    protected $unitOfWork;
    protected $tipoIncidenciaRepository;
    protected $tipoIncidenciaQuery;
    protected $tipoIncidenciaMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICTipoIncidenciaRepository $tipoIncidenciaRepository,
        ICTipoIncidenciaQuery $tipoIncidenciaQuery,
        ICTipoIncidenciaMapper $tipoIncidenciaMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->tipoIncidenciaRepository    = $tipoIncidenciaRepository;
        $this->tipoIncidenciaQuery         = $tipoIncidenciaQuery;
        $this->tipoIncidenciaMapper        = $tipoIncidenciaMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findTipoIncidenciaRequest = new FindCTiposIncidenciasRequest($request->all());
        //Query
        $this->tipoIncidenciaQuery->init();
        $this->tipoIncidenciaQuery->withNombre($findTipoIncidenciaRequest->getNombre());
        $this->tipoIncidenciaQuery->withActivo($findTipoIncidenciaRequest->getActivo());

        //Orden
        $this->tipoIncidenciaQuery->sort($findTipoIncidenciaRequest->getSortBy(), $findTipoIncidenciaRequest->getSort());

        $totalCount = $this->tipoIncidenciaQuery->totalCount();

        //Paginacion
        $this->tipoIncidenciaQuery->paginate(
            $findTipoIncidenciaRequest->getPaginate(),
            $findTipoIncidenciaRequest->getItemsToShow(),
            $findTipoIncidenciaRequest->getPage()
        );

        //Ejecutar query
        $tiposIncidencias = $this->tipoIncidenciaQuery->execute();

        //Mapear query
        $tiposIncidenciasResponse = $this->tipoIncidenciaMapper->getMapper()->mapMultiple($tiposIncidencias, CTipoIncidenciaResponse::class);

        //Find data response
        $findTiposIncidenciasDataResponse = new FindDataResponse();
        $findTiposIncidenciasDataResponse->setResults($tiposIncidenciasResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findTipoIncidenciaRequest->getPage());
        $paginationResponse->setPageCount(count($tiposIncidenciasResponse));
        $paginationResponse->setPageSize($findTipoIncidenciaRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin responseº
        $findTiposIncidenciasResponse = new  FindResponse();
        $findTiposIncidenciasResponse->setData($findTiposIncidenciasDataResponse);
        $findTiposIncidenciasResponse->setPagination($paginationResponse);

        return response()->json($findTiposIncidenciasResponse);

    }

    public function get($id)
    {
        /* @var $tipoIncidencia CTipoIncidencia*/
        $tipoIncidencia = $this->tipoIncidenciaRepository->get($id);
        $tipoIncidenciaResponse = $this->tipoIncidenciaMapper->getMapper()
            ->map($tipoIncidencia, CTipoIncidenciaResponse::class);
        return response()->json($tipoIncidenciaResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $tipoIncidencia CTipoIncidencia*/
        $tipoIncidenciaRequest = new CTipoIncidenciaRequest($request->all());
        $tipoIncidencia = $this->tipoIncidenciaMapper->getMapper()->map($tipoIncidenciaRequest, CTipoIncidencia::class);
        $tipoIncidencia->setActivo(true);
        $this->tipoIncidenciaRepository->add($tipoIncidencia);

        $createResponse = new CreateResponse();
        $createResponse->setId($tipoIncidencia->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $tipoIncidencia CTipoIncidencia*/
            $tipoIncidenciaRequest = new CTipoIncidenciaRequest($request->all());
            $tipoIncidencia = $this->tipoIncidenciaRepository->get($tipoIncidenciaRequest->getId());
            $this->tipoIncidenciaMapper->getMapper()->mapToObject($tipoIncidenciaRequest, $tipoIncidencia);
            $this->tipoIncidenciaRepository->update($tipoIncidencia);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $tipoIncidencia CTipoIncidencia*/

        //Eliminado logico
        try{
            $tipoIncidencia = $this->tipoIncidenciaRepository->get($id);
            $tipoIncidencia->setActivo(false);

            $this->tipoIncidenciaRepository->update($tipoIncidencia);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

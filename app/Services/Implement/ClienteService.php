<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IClienteQuery;
use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\Domain\Cliente;
use App\Services\DTO\Base\CreateResponse;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Clientes\ClienteRequest;
use App\Services\DTO\Clientes\ClienteResponse;
use App\Services\DTO\Clientes\FindClientesRequest;
use App\Services\Interfaces\IClienteService;
use App\Services\Mapper\Interfaces\IClienteMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;


class ClienteService implements IClienteService
{
    protected $unitOfWork;
    protected $clienteRepository;
    protected $clienteQuery;
    protected $clienteMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IClienteRepository $clienteRepository,
        IClienteQuery $clienteQuery,
        IClienteMapper $clienteMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->clienteRepository    = $clienteRepository;
        $this->clienteQuery         = $clienteQuery;
        $this->clienteMapper        = $clienteMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findClienteRequest = new FindClientesRequest($request->all());

        //Query
        $this->clienteQuery->init();
        $this->clienteQuery->withNombre($findClienteRequest->getNombre());
        $this->clienteQuery->withCorreo($findClienteRequest->getCorreo());
        $this->clienteQuery->withActivo($findClienteRequest->getActivo());
        $this->clienteQuery->withEstadoId($findClienteRequest->getEstadoId());
        $this->clienteQuery->includeEstado($findClienteRequest->isIncludeEstado());
        $this->clienteQuery->withCiudadId($findClienteRequest->getCiudadId());
        $this->clienteQuery->includeCiudad($findClienteRequest->isIncludeCiudad());
        //Orden
        $this->clienteQuery->sort($findClienteRequest->getSortBy(), $findClienteRequest->getSort());
        //Total registros
        $totalCount = $this->clienteQuery->totalCount();

        //Paginacion
        $this->clienteQuery->paginate(
            $findClienteRequest->getPaginate(),
            $findClienteRequest->getItemsToShow(),
            $findClienteRequest->getPage()
        );

        //Ejecutar query
        $clientes = $this->clienteQuery->execute();

        /*dd($clientes);*/

        //Mapear query
        $clientesResponse = $this->clienteMapper->getMapper()->mapMultiple($clientes, ClienteResponse::class);

        //Find data response
        $findClientesDataResponse = new FindDataResponse();
        $findClientesDataResponse->setResults($clientesResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findClienteRequest->getPage());
        $paginationResponse->setPageCount(count($clientesResponse));
        $paginationResponse->setPageSize($findClienteRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findClientesResponse = new  FindResponse();
        $findClientesResponse->setData($findClientesDataResponse);
        $findClientesResponse->setPagination($paginationResponse);

        return response()->json($findClientesResponse);

    }

    public function get($id)
    {
        /* @var $cliente Cliente*/
        $cliente = $this->clienteRepository->get($id);
        $clienteResponse = $this->clienteMapper->getMapper()->map($cliente, ClienteResponse::class);
        return response()->json($clienteResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $cliente Cliente*/
        $clienteRequest = new ClienteRequest($request->all());
        $cliente = $this->clienteMapper->getMapper()
            ->map($clienteRequest, Cliente::class);
        $cliente->setActivo(true);
        $this->clienteRepository->add($cliente);
        $createResponse = new CreateResponse();
        $createResponse->setId($cliente->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $cliente Cliente*/
            $clienteRequest = new ClienteRequest($request->all());
            $cliente = $this->clienteRepository->get($clienteRequest->getId());
            $this->clienteMapper->getMapper()->mapToObject($clienteRequest, $cliente);
            $this->clienteRepository->update($cliente);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $cliente Cliente*/

        //Eliminado logico
        try{
            $cliente = $this->clienteRepository->get($id);
            $cliente->setActivo(false);
            $this->clienteRepository->update($cliente);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICEstatusIncidenciaQuery;
use App\DataAccess\Repositories\Interfaces\ICEstatusIncidenciaRepository;
use App\Domain\CEstatusIncidencia;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\CEstatusIncidencias\CEstatusIncidenciaRequest;
use App\Services\DTO\CEstatusIncidencias\CEstatusIncidenciaResponse;
use App\Services\DTO\CEstatusIncidencias\FindCEstatusIncidenciasRequest;
use App\Services\Interfaces\ICEstatusIncidenciaService;
use App\Services\Mapper\Interfaces\ICEstatusIncidenciaMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class CEstatusIncidenciaService implements ICEstatusIncidenciaService
{
    protected $unitOfWork;
    protected $estatusIncidenciaRepository;
    protected $estatusIncidenciaQuery;
    protected $estatusIncidenciaMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICEstatusIncidenciaRepository $estatusIncidenciaRepository,
        ICEstatusIncidenciaQuery $estatusIncidenciaQuery,
        ICEstatusIncidenciaMapper $estatusIncidenciaMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->estatusIncidenciaRepository    = $estatusIncidenciaRepository;
        $this->estatusIncidenciaQuery         = $estatusIncidenciaQuery;
        $this->estatusIncidenciaMapper        = $estatusIncidenciaMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findEstatusIncidenciaRequest = new FindCEstatusIncidenciasRequest($request->all());
        //Query
        $this->estatusIncidenciaQuery->init();
        $this->estatusIncidenciaQuery->withNombre($findEstatusIncidenciaRequest->getNombre());
        $this->estatusIncidenciaQuery->withActivo($findEstatusIncidenciaRequest->getActivo());

        //Orden
        $this->estatusIncidenciaQuery->sort($findEstatusIncidenciaRequest->getSortBy(), $findEstatusIncidenciaRequest->getSort());

        $totalCount = $this->estatusIncidenciaQuery->totalCount();

        //Paginacion
        $this->estatusIncidenciaQuery->paginate(
            $findEstatusIncidenciaRequest->getPaginate(),
            $findEstatusIncidenciaRequest->getItemsToShow(),
            $findEstatusIncidenciaRequest->getPage()
        );

        //Ejecutar query
        $estatusIncidencias = $this->estatusIncidenciaQuery->execute();

        //Mapear query
        $estatusIncidenciasResponse = $this->estatusIncidenciaMapper->getMapper()->mapMultiple($estatusIncidencias, CEstatusIncidenciaResponse::class);

        //Find data response
        $findEstatusIncidenciasDataResponse = new FindDataResponse();
        $findEstatusIncidenciasDataResponse->setResults($estatusIncidenciasResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findEstatusIncidenciaRequest->getPage());
        $paginationResponse->setPageCount(count($estatusIncidenciasResponse));
        $paginationResponse->setPageSize($findEstatusIncidenciaRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin responseº
        $findEstatusIncidenciasResponse = new  FindResponse();
        $findEstatusIncidenciasResponse->setData($findEstatusIncidenciasDataResponse);
        $findEstatusIncidenciasResponse->setPagination($paginationResponse);

        return response()->json($findEstatusIncidenciasResponse);

    }

    public function get($id)
    {
        /* @var $estatusIncidencia CEstatusIncidencia*/
        $estatusIncidencia = $this->estatusIncidenciaRepository->get($id);
        $estatusIncidenciaResponse = $this->estatusIncidenciaMapper->getMapper()
            ->map($estatusIncidencia, CEstatusIncidenciaResponse::class);
        return response()->json($estatusIncidenciaResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $estatusIncidencia CEstatusIncidencia*/
        $estatusIncidenciaRequest = new CEstatusIncidenciaRequest($request->all());
        $estatusIncidencia = $this->estatusIncidenciaMapper->getMapper()->map($estatusIncidenciaRequest, CEstatusIncidencia::class);
        $estatusIncidencia->setActivo(true);
        $this->estatusIncidenciaRepository->add($estatusIncidencia);
        $createResponse = new CreateResponse();
        $createResponse->setId($estatusIncidencia->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $estatusIncidencia CEstatusIncidencia*/
            $estatusIncidenciaRequest = new CEstatusIncidenciaRequest($request->all());
            $estatusIncidencia = $this->estatusIncidenciaRepository->get($estatusIncidenciaRequest->getId());
            $this->estatusIncidenciaMapper->getMapper()->mapToObject($estatusIncidenciaRequest, $estatusIncidencia);
            $this->estatusIncidenciaRepository->update($estatusIncidencia);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;
        /* @var $estatusIncidencia CEstatusIncidencia*/
        //Eliminado logico
        try{
            $estatusIncidencia = $this->estatusIncidenciaRepository->get($id);
            $estatusIncidencia->setActivo(false);

            $this->estatusIncidenciaRepository->update($estatusIncidencia);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IHorarioQuery;
use App\DataAccess\Repositories\Interfaces\IDetalleHorarioRepository;
use App\DataAccess\Repositories\Interfaces\IHorarioRepository;
use App\Domain\DetalleHorario;
use App\Domain\Horario;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\DetallesHorarios\DetalleHorarioRequest;
use App\Services\DTO\Horarios\FindHorariosRequest;
use App\Services\DTO\Horarios\HorarioRequest;
use App\Services\DTO\Horarios\HorarioResponse;
use App\Services\Interfaces\IHorarioService;
use App\Services\Mapper\Interfaces\IDetalleHorarioMapper;
use App\Services\Mapper\Interfaces\IHorarioMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class HorarioService implements IHorarioService
{
    protected $unitOfWork;
    protected $horarioRepository;
    protected $horarioQuery;
    protected $horarioMapper;
    protected $detalleHorarioMapper;
    protected $detalleHorarioRepository;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IHorarioRepository $horarioRepository,
        IHorarioQuery $horarioQuery,
        IHorarioMapper $horarioMapper,
        IDetalleHorarioMapper $detalleHorarioMapper,
        IDetalleHorarioRepository $detalleHorarioRepository
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->horarioRepository    = $horarioRepository;
        $this->horarioQuery         = $horarioQuery;
        $this->horarioMapper        = $horarioMapper;
        $this->detalleHorarioMapper = $detalleHorarioMapper;
        $this->detalleHorarioRepository = $detalleHorarioRepository;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findHorarioRequest = new FindHorariosRequest($request->all());

        //Query
        $this->horarioQuery->init();
        $this->horarioQuery->withNombre($findHorarioRequest->getNombre());
        $this->horarioQuery->includeDetallesHorario($findHorarioRequest->isIncludeDetallesHorario());
        $this->horarioQuery->withMinutosTolerancia($findHorarioRequest->getMinutosTolerancia());
        $this->horarioQuery->withMinMinutosTolerancia($findHorarioRequest->getMinMinutosTolerancia());
        $this->horarioQuery->withMaxMinutosTolerancia($findHorarioRequest->getMaxMinutosTolerancia());
        $this->horarioQuery->withDepartamentoId($findHorarioRequest->getDepartamentoId());
        $this->horarioQuery->includeDepartamento($findHorarioRequest->isIncludeDepartamento());
        $this->horarioQuery->withActivo($findHorarioRequest->getActivo());
        $this->horarioQuery->includeEmpresa($findHorarioRequest->isIncludeEmpresa());
        $this->horarioQuery->withEmpresaId($findHorarioRequest->getEmpresaId());
        $this->horarioQuery->includeCliente($findHorarioRequest->isIncludeCliente());
        $this->horarioQuery->withClienteId($findHorarioRequest->getClienteId());
        $this->horarioQuery->withSucursalId($findHorarioRequest->getSucursalId());
        $this->horarioQuery->includeSucursal($findHorarioRequest->isIncludeSucursal());

        //Orden
        $this->horarioQuery->sort($findHorarioRequest->getSortBy(), $findHorarioRequest->getSort());
        //Total registros
        $totalCount = $this->horarioQuery->totalCount();

        //Paginacion
        $this->horarioQuery->paginate(
            $findHorarioRequest->getPaginate(),
            $findHorarioRequest->getItemsToShow(),
            $findHorarioRequest->getPage()
        );

        //Ejecutar query
        $horarios = $this->horarioQuery->execute();

        //Mapear query
        $horariosResponse = $this->horarioMapper->getMapper()->mapMultiple($horarios, HorarioResponse::class);

        if($findHorarioRequest->isIncludeDetallesHorario() === true){
            $horariosResponse = $this->filterFindHorariosDetalles($horariosResponse);
        }

        //Find data response
        $findHorariosDataResponse = new FindDataResponse();
        $findHorariosDataResponse->setResults($horariosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findHorarioRequest->getPage());
        $paginationResponse->setPageCount(count($horariosResponse));
        $paginationResponse->setPageSize($findHorarioRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findHorariosResponse = new  FindResponse();
        $findHorariosResponse->setData($findHorariosDataResponse);
        $findHorariosResponse->setPagination($paginationResponse);

        return response()->json($findHorariosResponse);

    }

    private function filterFindHorariosDetalles($horarios){
        /* @var $horario Horario*/
        /* @var $detalle DetalleHorario*/
        $horariosObj = ArrayExtensions::cloneArray($horarios);
        foreach ($horarios as $key => $horario){
            unset($horariosObj[$key]->detalles_horarios);
            $detallesHorarios = [];
            foreach ($horario->getDetallesHorarios() as $detalle){
                if($detalle->getActivo() === true){
                    array_push($detallesHorarios, $detalle);
                }
            }
            $horariosObj[$key]->detalles_horarios = $detallesHorarios;
        }
        return $horariosObj;
    }

    public function get($id)
    {
        /* @var $horario Horario*/
        $horario = $this->horarioRepository->get($id);
        ProxyExtensions::toArray($horario->getDetallesHorarios());
        ProxyExtensions::load($horario->getDepartamento());
        $horarioResponse = $this->horarioMapper->getMapper()->map($horario, HorarioResponse::class);
        $horarioResponse = $this->filterDetallesHorario($horarioResponse);
        return response()->json($horarioResponse);
    }

    private function filterDetallesHorario($horario){
        $horarioNew = clone($horario);
        unset($horarioNew->detalles_horarios);
        $detallesHorarios = [];
        /* @var $horario Horario*/
        /* @var $detalleHorario DetalleHorario*/
        foreach ($horario->getDetallesHorarios() as $detalleHorario){
            if($detalleHorario->getActivo() === true){
                array_push($detallesHorarios, $detalleHorario);
            }
        }
        $horarioNew->detalles_horarios = $detallesHorarios;
        return $horarioNew;
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $horario Horario*/
        $horarioRequest = new HorarioRequest($request->all());
        $horario = $this->horarioMapper->getMapper()
            ->map($horarioRequest, Horario::class);
        $horario->setActivo(true);
        if(ArrayExtensions::isNotNullOrEmpty($horarioRequest->getDetallesHorario())){
            $detallesHorarios = $this->createDetallesHorario($horarioRequest->getDetallesHorario(), $horario);
            $horario->setDetallesHorarios($detallesHorarios);
        }

        $this->horarioRepository->add($horario);
        $createResponse = new CreateResponse();
        $createResponse->setId($horario->getId());
        return response()->json($createResponse);
    }

    private function createDetallesHorario($detallesHorarioRequest, $horario){
        $detallesHorario = array();
        foreach ($detallesHorarioRequest as $detalleHorarioRequest){

            /* @var $detalleHorario DetalleHorario*/
            $detalleHorarioRequestObj = new DetalleHorarioRequest($detalleHorarioRequest);
            $detalleHorario = $this->detalleHorarioMapper->getMapper()->map($detalleHorarioRequestObj, DetalleHorario::class);
            $detalleHorario->setActivo(true);
            $detalleHorario->setHorario($horario);
            array_push($detallesHorario, $detalleHorario);
        }
        return $detallesHorario;
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $horario Horario*/
            $horarioRequest = new HorarioRequest($request->all());
            $horario = $this->horarioRepository->get($horarioRequest->getId());
            $this->horarioMapper->getMapper()->mapToObject($horarioRequest, $horario);

            if(ArrayExtensions::isNotNullOrEmpty($horarioRequest->getDetallesEliminados())){
                $this->deleteDetallesHorario($horarioRequest->getDetallesEliminados());
            }

            if(ArrayExtensions::isNotNullOrEmpty($horarioRequest->getDetallesHorario())){
                $this->updateDetallesHorario($horarioRequest->getDetallesHorario(), $horario);
            }

            $this->horarioRepository->update($horario);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    private function updateDetallesHorario($detallesHorarioRequest, $horario){
        foreach ($detallesHorarioRequest as $detalleHorarioRequest){
            if(isset($detalleHorarioRequest['id']) && $detalleHorarioRequest['id'] !== 0){
                $detalleHorarioRequestObj = new DetalleHorarioRequest($detalleHorarioRequest);
                $detalleHorario = $this->detalleHorarioRepository->get($detalleHorarioRequest['id']);
                $this->detalleHorarioMapper->getMapper()->mapToObject($detalleHorarioRequestObj, $detalleHorario);
                $this->detalleHorarioRepository->update($detalleHorario);
            }else{

                $detalleHorarioRequestObj = new DetalleHorarioRequest($detalleHorarioRequest);
                /* @var $detalleHorario DetalleHorario*/
                $detalleHorario = $this->detalleHorarioMapper->getMapper()->map($detalleHorarioRequestObj, DetalleHorario::class);
                $detalleHorario->setActivo(true);
                $detalleHorario->setHorario($horario);
                $this->detalleHorarioRepository->add($detalleHorario);
            }
        }
    }

    private function deleteDetallesHorario($detallesEliminados){
        foreach ($detallesEliminados as $detalleEliminado){
            /* @var $detalleEliminado DetalleHorario*/
            try{
                $detalleEliminado = $this->detalleHorarioRepository->get($detalleEliminado['id']);
                $detalleEliminado->setActivo(false);
                $this->detalleHorarioRepository->update($detalleEliminado);
            }
            catch(Exception $exception){
                throw $exception;
            }
        }
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $horario Horario*/
        /* @var $detalleHorarioObj DetalleHorario*/

        //Eliminado logico
        try{
            $horario = $this->horarioRepository->get($id);
            foreach ($horario->getDetallesHorarios() as $detalleHorario){
                $detalleHorarioObj = $this->detalleHorarioRepository->get($detalleHorario->getId());
                $detalleHorarioObj->setActivo(false);
            }
            $horario->setActivo(false);
            $this->horarioRepository->update($horario);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

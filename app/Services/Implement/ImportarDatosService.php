<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\Services\Interfaces\IHistoricoEmpleadoService;
use App\Services\Interfaces\IImportarDatosService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use Excel;
use Illuminate\Support\Facades\DB;
use DateTime;
use Exception;

class ImportarDatosService implements IImportarDatosService
{
    protected $unitOfWork;
    protected $data;
    protected $error = array();
    protected $historicoService;

    public function __construct( IUnitOfWork $unitOfWork, IHistoricoEmpleadoService $historicoService)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->historicoService = $historicoService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function importarEmpleados($request){
        $this->data = $request;
        Excel::load($request->file('archivo'), function ($reader) {
            $results = $reader->get()->toArray();
           
            foreach ($results[0] as $key => $value) {
                $usuarios = $this->creaUsuario($value, $this->data);
                
                if($usuarios !== true){
                    array_push($this->error,$usuarios);
                }
                else{
                    $empleados = $this->creaEmpleado($value, $this->data);
                    if($empleados !== true){
                        $this->eliminaUsuario($empleados);
                        array_push($this->error,$empleados);
                    }
                }
            }
            if(count($this->error) > 0){
                $this->generaErrores($this->error);
            }
        });
        return true;
    }

    private function creaUsuario($excel, $datos){
        $response = false;
        $sucursal = $datos->sucursal_id;
        if($datos->rol_name == "admin-empresa" ){
            $sucursal = $excel["sucursal"];
        }
        try {
            $response = DB::table("usuarios")->insert([
                'cliente_id' => $datos->cliente_id,
                'rol_id' => 6,
                'nombre' => $excel["nombres"],
                'apellido_paterno' => $excel["apellido_paterno"],
                'apellido_materno' => $excel["apellido_materno"],
                'puesto' => null,
                'telefono' => $excel["telefono"],
                'email' => $excel["correo"],
                'fecha_nacimiento' => $excel["fecha_nacimiento"],
                'password' => bcrypt('123456'),
                'primer_inicio_sesion' => true,
                'empresa_id' => $datos->empresa_id,
                'sucursal_id' => $sucursal,
                'created_at' => new DateTime('now'),
                'departamento_id' => $excel["departamento"],
                'activo' => true

            ]);
            return $response;
        }
        catch(Exception $e) {
            array_push($excel, "No se puede registrar el correo, intentar con otro.");
            return $excel;
        }
    }

    private function creaEmpleado($excel, $datos){
        $response = false;
        $sucursal = $datos->sucursal_id;
        if($datos->rol_name == "admin-empresa" ){
            $sucursal = $excel["sucursal"];
        }
        $clave_empleado = $this->claveEmpleado($datos->empresa_id);
        try {
            $tiene_hijos = false;
            $variable =strtoupper($excel["tiene_hijos"]);
            
            if($variable == "SI"){
                $tiene_hijos = true;
            }
            $response = DB::table("empleados")->insert([
                'estado_id' => $excel["estado"],
                'ciudad_id' => $excel["ciudad"],
                'horario_id' => $excel["horario"],
                'sucursal_id' => $sucursal,
                'departamento_id' => $excel["departamento"],
                'nombre' => $excel["nombres"],
                'apellido_paterno' => $excel["apellido_paterno"],
                'apellido_materno' => $excel["apellido_materno"],
                'fecha_nacimiento' => $excel["fecha_nacimiento"],
                'sexo' => $excel["sexo"],
                'direccion' => $excel["direccion"],
                'codigo_postal' => $excel["codigo_postal"],
                'telefono' => $excel["telefono"],
                'correo' => $excel["correo"],
                'fecha_ingreso' => $excel["fecha_de_ingreso"],
                'activo' => true,
                'created_date' => new DateTime('now'),
                'cliente_id' => $datos->cliente_id,
                'empresa_id' => $datos->empresa_id,
                'clave' => $clave_empleado,
                'estado_civil_id' => $excel["estado_civil"],
                'dia_descanso' => "NA",
                'puesto' => $excel["puesto"],
                'clave_interna' => $excel["clave_interna"],
                'tiene_hijos' => $tiene_hijos,
                'estado_civil_id' => $excel["estado_civil"],
            ]);

            $ultimo = DB::table('empleados')->get()->last();
            DB::table('historico_empleados')->insert([
                "accion" => 'Ingreso',
                "fecha"  => new DateTime('now'),
                "empleado_id" => $ultimo->id,
                "responsable_id" => $datos->usuario_id,
                "activo" => true,
                "created_date" => new DateTime('now'),
            ]);

            
            return $response;
        }
        catch(Exception $e) {
            array_push($excel, "Algun dato ingresado es invalido, favor de guardar el empleado manualmente.");
            return $excel;
        }
        return $response;
    }

    private function generaErrores($errores){
        $nombreArchivo = "Respuesta".'_'.date('d_m_y');
        $cellsBgColor = 'A1:P1';
        $arrayFormat = array();
        $titulosEncabezado = [
            'Nombres', 'Apellido Paterno', 'Apellido Materno', 'Fecha Nacimiento', 'Sexo', 'Direccion', 'Estado', 'Ciudad',
            'Codigo Postal', 'Telefono', 'Correo', 'Fecha de Ingreso', 'Horario', "Estado Civil", "Departamento"
        ];
        
        if(count($this->error) > 0){
            if(count($errores[0]) > 15){
                array_push($titulosEncabezado, 'Sucursal', 'Obseraciones');
                $cellsBgColor = 'A1:Q1';
            }
            else{
                array_push($titulosEncabezado, 'Obseraciones');
            }
        }

        Excel::create($nombreArchivo, function($excel) use ($errores, $nombreArchivo, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
            $excel->setTitle($nombreArchivo);
            $excel->setCreator('PBM') ->setCompany('PBM');
            $excel->setDescription('Listado de '. $nombreArchivo);
            $excel->sheet('Hoja 1', function($sheet) use ($errores, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
                //Estilo de la hoja
                $sheet->setFontSize(16);
                $sheet->setColumnFormat($arrayFormat);
                $sheet->freezeFirstRow();
                //Estilo de las celdas
                $sheet->cells($cellsBgColor, function($cells) {
                    $cells->setBackground('#B3EFFF');
                    //$cells->setColor('#000000');

                });
                //Encabezado
                $sheet->row(1, $titulosEncabezado);

                //LLenado de celdas
                foreach($errores as $index => $dato) {
                    $sheet->row($index+2, $dato);
                }
            });
        })->export('xlsx', ['Access-Control-Allow-Origin'=>'*']);
    }

    private function eliminaUsuario($datos){
        DB::table('usuarios')->where('email', '=', $datos["correo"])->delete();
        return;
    }

    private function claveEmpleado($empresa_id){
        $empresa = DB::table('empresa')->where('id', $empresa_id)->first();
        $clave = (int)($empresa->clave_empleado_auto + 1);
        DB::table('empresa')->where('id', $empresa_id)->update(['clave_empleado_auto' => $clave]);
        return $clave;
    }
}

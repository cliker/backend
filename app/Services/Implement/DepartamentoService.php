<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IDepartamentoQuery;
use App\DataAccess\Repositories\Interfaces\IDepartamentoRepository;
use App\Domain\Departamento;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Departamentos\DepartamentoRequest;
use App\Services\DTO\Departamentos\DepartamentoResponse;
use App\Services\DTO\Departamentos\FindDepartamentosRequest;
use App\Services\Interfaces\IDepartamentoService;
use App\Services\Mapper\Interfaces\IDepartamentoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class DepartamentoService implements IDepartamentoService
{
    protected $unitOfWork;
    protected $departamentoRepository;
    protected $departamentoQuery;
    protected $departamentoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IDepartamentoRepository $departamentoRepository,
        IDepartamentoQuery $departamentoQuery,
        IDepartamentoMapper $departamentoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->departamentoRepository    = $departamentoRepository;
        $this->departamentoQuery         = $departamentoQuery;
        $this->departamentoMapper        = $departamentoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findDeptoRequest = new FindDepartamentosRequest($request->all());

        //Query
        $this->departamentoQuery->init();
        $this->departamentoQuery->withNombre($findDeptoRequest->getNombre());
        $this->departamentoQuery->withEncargado($findDeptoRequest->getEncargado());
        $this->departamentoQuery->withCorreo($findDeptoRequest->getCorreo());
        $this->departamentoQuery->withDescripcion($findDeptoRequest->getDescripcion());
        $this->departamentoQuery->withSucursalId($findDeptoRequest->getSucursalId());
        $this->departamentoQuery->includeSucursal($findDeptoRequest->isIncludeSucursal());
        $this->departamentoQuery->withEmpresaId($findDeptoRequest->getEmpresaId());
        $this->departamentoQuery->includeEmpresa($findDeptoRequest->isIncludeEmpresa());
        $this->departamentoQuery->withActivo($findDeptoRequest->getActivo());
        $this->departamentoQuery->includeDiasFestivos($findDeptoRequest->isIncludeDiasFestivos());
        $this->departamentoQuery->withClienteId($findDeptoRequest->getClienteId());
        $this->departamentoQuery->includeCliente($findDeptoRequest->isIncludeCliente());

        //Orden
        $this->departamentoQuery->sort($findDeptoRequest->getSortBy(), $findDeptoRequest->getSort());

        //Total registros
        $totalCount = $this->departamentoQuery->totalCount();

        //Paginacion
        $this->departamentoQuery->paginate(
            $findDeptoRequest->getPaginate(),
            $findDeptoRequest->getItemsToShow(),
            $findDeptoRequest->getPage()
        );

        //Ejecutar query
        $departamentos = $this->departamentoQuery->execute();

        //Mapear query
        $deptoResponse = $this->departamentoMapper->getMapper()
            ->mapMultiple($departamentos, DepartamentoResponse::class);

        //Find data response
        $findDataDepto = new FindDataResponse();
        $findDataDepto->setResults($deptoResponse);

        //Pagination response
        $paginateResponse = new PaginationResponse();
        $paginateResponse->setPage($findDeptoRequest->getPage());
        $paginateResponse->setPageCount(count($deptoResponse));
        $paginateResponse->setPageSize($findDeptoRequest->getItemsToShow());
        $paginateResponse->setTotalCount($totalCount);

        //Fin response
        $findDeptoResponse = new FindResponse();
        $findDeptoResponse->setData($findDataDepto);
        $findDeptoResponse->setPagination($paginateResponse);

        return response()->json($findDeptoResponse);

    }

    public function get($id)
    {
        /* @var $departamento Departamento */
        $departamento = $this->departamentoRepository->get($id);
        ProxyExtensions::load($departamento->getSucursal());
        ProxyExtensions::load($departamento->getEmpresa());
        ProxyExtensions::toArray($departamento->getDiasFestivos());
        $deptoResponse = $this->departamentoMapper->getMapper()->map($departamento, DepartamentoResponse::class);

        return response()->json($deptoResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $departamento Departamento */
        $deptoRequest = new DepartamentoRequest($request->all());
        $departamento = $this->departamentoMapper->getMapper()->map($deptoRequest, Departamento::class);
        $departamento->setActivo(true);
        $this->departamentoRepository->add($departamento);
        $createResponse = new CreateResponse();
        $createResponse->setId($departamento->getId());

        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $departamento Departamento */
        $succes = false;

        try{
            $deptoRequest = new DepartamentoRequest($request->all());
            $departamento = $this->departamentoRepository->get($deptoRequest->getId());
            $this->departamentoMapper->getMapper()->mapToObject($deptoRequest, $departamento);
            $this->departamentoRepository->update($departamento);
            $succes = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($succes);

        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        /* @var $departamento Departamento */
        $succes = false;

        try{
            $departamento = $this->departamentoRepository->get($id);
            $departamento->setActivo(false);
            $this->departamentoRepository->update($departamento);
            $succes = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($succes);

        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IConfiguracionQuery;
use App\DataAccess\Repositories\Interfaces\IConfiguracionRepository;
use App\Domain\Configuracion;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Configuraciones\ConfiguracionRequest;
use App\Services\DTO\Configuraciones\ConfiguracionResponse;
use App\Services\DTO\Configuraciones\FindConfiguracionesRequest;
use App\Services\Interfaces\IConfiguracionService;
use App\Services\Mapper\Interfaces\IConfiguracionMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class ConfiguracionService implements IConfiguracionService
{
    protected $unitOfWork;
    protected $configuracionRepository;
    protected $configuracionQuery;
    protected $configuracionMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IConfiguracionRepository $configuracionRepository,
        IConfiguracionQuery $configuracionQuery,
        IConfiguracionMapper $configuracionMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->configuracionRepository    = $configuracionRepository;
        $this->configuracionQuery         = $configuracionQuery;
        $this->configuracionMapper        = $configuracionMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findConfiguracionRequest = new FindConfiguracionesRequest($request->all());

        //Query
        $this->configuracionQuery->init();
        $this->configuracionQuery->withNombre($findConfiguracionRequest->getNombre());
        $this->configuracionQuery->withClave($findConfiguracionRequest->getClave());
        $this->configuracionQuery->withValor($findConfiguracionRequest->getValor());
        $this->configuracionQuery->withDescripcion($findConfiguracionRequest->getDescripcion());
        $this->configuracionQuery->withActivo($findConfiguracionRequest->getActivo());
        $this->configuracionQuery->withSucursalId($findConfiguracionRequest->getSucursalId());
        $this->configuracionQuery->includeSucursal($findConfiguracionRequest->isIncludeSucursal());
        $this->configuracionQuery->withEmpresaId($findConfiguracionRequest->getEmpresaId());
        $this->configuracionQuery->includeEmpresa($findConfiguracionRequest->isIncludeEmpresa());

        //Orden
        $this->configuracionQuery->sort($findConfiguracionRequest->getSortBy(), $findConfiguracionRequest->getSort());

        //Total registros
        $totalCount = $this->configuracionQuery->totalCount();

        //Paginacion
        $this->configuracionQuery->paginate(
            $findConfiguracionRequest->getPaginate(),
            $findConfiguracionRequest->getItemsToShow(),
            $findConfiguracionRequest->getPage()
        );

        //Ejecutar query
        $configuraciones = $this->configuracionQuery->execute();

        //Mapear query
        $configuracionResponse = $this->configuracionMapper->getMapper()->mapMultiple($configuraciones, ConfiguracionResponse::class);

        //Find data response
        $findConfiguracionDataResponse = new FindDataResponse();
        $findConfiguracionDataResponse->setResults($configuracionResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findConfiguracionRequest->getPage());
        $paginationResponse->setPageCount(count($configuracionResponse));
        $paginationResponse->setPageSize($findConfiguracionRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findConfiguracionResponse = new FindResponse();
        $findConfiguracionResponse->setData($findConfiguracionDataResponse);
        $findConfiguracionResponse->setPagination($paginationResponse);

        return response()->json($findConfiguracionResponse);

    }

    public function get($id)
    {
        /* @var $configuracion Configuracion */
        $configuracion = $this->configuracionRepository->get($id);
        $configuracionResponse = $this->configuracionMapper->getMapper()->map($configuracion, ConfiguracionResponse::class);
        return response()->json($configuracionResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $configuracion Configuracion */
        $configuracionRequest = new ConfiguracionRequest($request->all());
        $configuracion = $this->configuracionMapper->getMapper()
            ->map($configuracionRequest,Configuracion::class);
        $configuracion->setActivo(true);
        $this->configuracionRepository->add($configuracion);
        $createResponse = new CreateResponse();
        $createResponse->setId($configuracion->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $configuracion Configuracion */
            $configuracionRequest = new ConfiguracionRequest($request->all());
            $configuracion = $this->configuracionRepository->get($configuracionRequest->getId());
            $this->configuracionMapper->getMapper()->mapToObject($configuracionRequest, $configuracion);
            $this->configuracionRepository->update($configuracion);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;
        try{
            /* @var $configuracion Configuracion */
            $configuracion = $this->configuracionRepository->get($id);
            $configuracion->setActivo(false);
            $this->configuracionRepository->update($configuracion);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IRelojChecadorQuery;
use App\DataAccess\Repositories\Interfaces\IEmpleadoRepository;
use App\DataAccess\Repositories\Interfaces\IRelojChecadorRepository;
use App\Domain\RelojChecador;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\RelojChecador\FindRelojChecadoresRequest;
use App\Services\DTO\RelojChecador\RelojChecadorResponse;
use App\Services\DTO\RelojChecador\RelojChecadorRequest;
use App\Services\DTO\RelojChecador\RemoveEmpleadoRelojChecadorRequest;
use App\Services\Interfaces\IRelojChecadorService;
use App\Services\Mapper\Interfaces\IRelojChecadorMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class RelojChecadorService implements IRelojChecadorService
{
    protected $unitOfWork;
    protected $relojRepository;
    protected $relojQuery;
    protected $relojMapper;
    protected $empleadoRepository;
    protected $final;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IRelojChecadorRepository $relojRepository,
        IRelojChecadorQuery $relojQuery,
        IRelojChecadorMapper $relojMapper,
        IEmpleadoRepository $empleadoRepository

    )
    {
        $this->unitOfWork         = $unitOfWork;
        $this->relojRepository    = $relojRepository;
        $this->relojQuery         = $relojQuery;
        $this->relojMapper        = $relojMapper;
        $this->empleadoRepository = $empleadoRepository;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findRelojRequest = new FindRelojChecadoresRequest($request->all());

        //Query
        $this->relojQuery->init();
        $this->relojQuery->withSerial($findRelojRequest->getSerial());
        $this->relojQuery->withEmpresa($findRelojRequest->getEmpresaId());
        $this->relojQuery->includeEmpresa($findRelojRequest->isIncludeEmpresa());
        $this->relojQuery->withSucursal($findRelojRequest->getSucursalId());
        $this->relojQuery->includeSucursal($findRelojRequest->isIncludeSucursal());
        $this->relojQuery->withDepartamento($findRelojRequest->getDepartamentoId());
        $this->relojQuery->includeDepartamento($findRelojRequest->isIncludeDepartamento());
        $this->relojQuery->withActivo($findRelojRequest->getActivo());
        $this->relojQuery->withClienteId($findRelojRequest->getClienteId());
        $this->relojQuery->includeCliente($findRelojRequest->isIncludeCliente());
        $this->relojQuery->withEmpleadoId($findRelojRequest->getEmpleadoId());
        $this->relojQuery->includeEmpleados($findRelojRequest->isIncludeEmpleados());

        //Orden
        $this->relojQuery->sort($findRelojRequest->getSortBy(), $findRelojRequest->getSort());

        //Total registros
        $totalCount = $this->relojQuery->totalCount();

        //Paginacion
        $this->relojQuery->paginate(
            $findRelojRequest->getPaginate(),
            $findRelojRequest->getItemsToShow(),
            $findRelojRequest->getPage()
        );

        //Ejecutar query
        $relojes = $this->relojQuery->execute();

        //Mapear query
        $relojResponse = $this->relojMapper->getMapper()
            ->mapMultiple($relojes, RelojChecadorResponse::class);


        //Find data response
        $findDataReloj = new FindDataResponse();
        $findDataReloj->setResults($relojResponse);

        //Pagination response
        $paginateResponse = new PaginationResponse();
        $paginateResponse->setPage($findRelojRequest->getPage());
        $paginateResponse->setPageCount(count($relojResponse));
        $paginateResponse->setPageSize($findRelojRequest->getItemsToShow());
        $paginateResponse->setTotalCount($totalCount);

        //Fin response
        $findRejojResponse = new FindResponse();
        $findRejojResponse->setData($findDataReloj);
        $findRejojResponse->setPagination($paginateResponse);

        return response()->json($findRejojResponse);

    }

    public function get($id)
    {
        /* @var $reloj RelojChecador */
        $reloj = $this->relojRepository->get($id);
        ProxyExtensions::load($reloj->getEmpresa());
        ProxyExtensions::load($reloj->getSucursal());
        ProxyExtensions::toArray($reloj->getEmpleados());
        $relojResponse = $this->relojMapper->getMapper()->map($reloj, RelojChecadorResponse::class);
        return response()->json($relojResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $reloj RelojChecador */
        $relojRequest = new RelojChecadorRequest($request->all());
        $empresa = DB::table('empresa')->where('id', "=", $request->empresa_id)->get();
        $reloj = $this->relojMapper->getMapper()->map($relojRequest, RelojChecador::class);
        $reloj->setActivo(true);
        $this->relojRepository->add($reloj);
        $createResponse = new CreateResponse();
        $createResponse->setId($reloj->getId());
        $asp =  $this->checadorAsp($request, $empresa[0]->url_server_asp, $reloj->getId());
        if($asp != true){
            DB::table('reloj_checador')->where('id', '=', $reloj->getId())->delete();
            return response()->json("No se pudo conectar al servidor del checador", 401);
        }
        return response()->json($createResponse);
    }

    private function checadorAsp($data, $url, $id_gdth){
        $success = false;
        try{
            $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
            $param = array( 'usuario' => 'admin','password' => 'Chec4dor$2019.');
            $login = $cliente->request('POST', 'login/login', array('form_params' => $param ));
            $token_asp = json_decode($login->getBody()->getContents());
            
            $datos = [ 'idgdth' => $id_gdth, 'nombre' => $data->descripcion, 'ip' => $data->ip ];
            $datos = (object) $datos;
            $headers =  [ 'Authorization' => 'Bearer ' . $token_asp,'Accept'        => 'application/json' ];
            $response = $cliente->request('POST', 'dispositivos', array('form_params' => $datos , 'headers' => $headers ));
            $success = true;
        }
        catch(Exception $ex){
            
        }
        return $success;
    }

    private function getEmpleadoToReloj($empleados){
        $empleadosResponse = array();
        foreach ($empleados as $item){
            $empleado = $this->empleadoRepository->get($item['id']);
            array_push($empleadosResponse, $empleado);
        }
        return $empleadosResponse;
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $reloj RelojChecador */
        if(ArrayExtensions::isMultidimensional($request->all())){
            $requestArray = $request->all();
            $success = false;

            try{
                foreach ($requestArray as $requestItem){
                    $relojRequest = new  RelojChecadorRequest($requestItem);
                    $reloj = $this->relojRepository->get($relojRequest->getId());
                    $this->relojMapper->getMapper()->mapToObject($relojRequest, $reloj);
                    $empleados = $this->getEmpleadoToReloj($relojRequest->getEmpleados());
                    $reloj->addEmpleados($empleados);
                    $this->relojRepository->update($reloj);
                    $success = true;
                }
            }
            catch(Exception $exception){
                throw $exception;
            }

            $isSuccess = new SuccessResponse();
            $isSuccess->setIsSuccess($success);
            return response()->json($isSuccess);


        }else{
            $success = false;
            try{
                $empresa = DB::table('empresa')->where('id', "=", $request->empresa_id)->get();
                $update = $this->updateAsp($request, $empresa[0]->url_server_asp, "update");
                if($update == true){
                    $relojRequest = new  RelojChecadorRequest($request->all());
                    $reloj = $this->relojRepository->get($relojRequest->getId());
                    $this->relojMapper->getMapper()->mapToObject($relojRequest, $reloj);
                    $empleados = $this->getEmpleadoToReloj($relojRequest->getEmpleados());
                    $reloj->addEmpleados($empleados);
                    $this->relojRepository->update($reloj);
                    $success = true;
                }
            }catch(Exception $exception){
                throw $exception;
            }
            $isSuccess = new SuccessResponse();
            $isSuccess->setIsSuccess($success);
            return response()->json($isSuccess);
        }
    }

    public function updateAsp($request,$url, $accion){
        $success = false;
       
        try{
            $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
            $param = array( 'usuario' => 'admin','password' => 'Chec4dor$2019.');
            $login = $cliente->request('POST', 'login/login', array('form_params' => $param ));
            $token_asp = json_decode($login->getBody()->getContents());
            if($accion == "delete"){
                $headers =  [ 'Authorization' => 'Bearer ' . $token_asp,'Accept' => 'application/json' ];
                $response = $cliente->request('DELETE', 'dispositivos?id='.$request->id, array( 'headers' => $headers ));
            }
            else{
                $datos = ["id" => 0, "idgdth" => $request->id, "nombre" => $request->descripcion, "ip" => $request->ip ];
                $datos = (object) $datos;
                $headers =  [ 'Authorization' => 'Bearer ' . $token_asp,'Accept' => 'application/json' ];
                $response = $cliente->request('PUT', 'dispositivos', array('form_params' => $datos , 'headers' => $headers ));
            }
            
            $success = true;
        }
        catch(Exception $ex){
            
        }
        return $success;
    }

    /**
     * @param Request $request
     */
    public function delete($request)
    {
        /* @var $reloj RelojChecador */
        $success = false;
        try{
            $reloj = $this->relojRepository->get($request->id);
            $reloj->setActivo(false);
            $empresa = DB::table('empresa')->where('id', "=", $request->empresa_id)->get();
            $delete = $this->updateAsp($request, $empresa[0]->url_server_asp, "delete");
            if($delete == true){
                $this->relojRepository->update($reloj);
            }
            $success = true;
        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    public function drop($id)
    {
        /* @var $reloj RelojChecador */
        $success = false;
        try{
            $reloj = $this->relojRepository->get($id);
            $this->relojRepository->remove($reloj);
            $success = true;
        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function removeEmployee($request)
    {
        
        $success = false;
        try{
            $reloj = $this->relojRepository->get($request->id);
            $empleado = $this->empleadoRepository->get($request->empleado_id);
            $url = $empleado->getEmpresa()->getUrlServerAsp();
            $e = $this->eliminaAsp($request, $url);
            $reloj->removeEmpleado($empleado);
            $this->relojRepository->update($reloj);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    private function eliminaAsp($request, $url){
        $success = false;
        try{
            $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
            $param = array( 'usuario' => 'admin','password' => 'Chec4dor$2019.');
            $login = $cliente->request('POST', 'login/login', array('form_params' => $param ));
            $token_asp = json_decode($login->getBody()->getContents());
            $datos = $request->all();
            
            $c = (object) $datos['checadores'][0];
            $e = (object)$datos['empleados'][0];
            $this->final = (object)[ "checadores" => [$c], "empleados" => [$e]];
            
            $headers =  [ 'Authorization' => 'Bearer ' . $token_asp,'Accept' => 'application/json' ];
            $response = $cliente->request('POST', 'empleados/eliminar', array('form_params' => $this->final , 'headers' => $headers ));
            $success = true;
        }
        catch(Exception $exception){
        }
        return $success;
    }
}

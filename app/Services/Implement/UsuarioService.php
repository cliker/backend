<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IUsuarioQuery;
use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Domain\Usuario;
use App\Infrastructure\ProxyExtensions;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Usuarios\FindUsuariosRequest;
use App\Services\DTO\Usuarios\LayoutUsuariosRequest;
use App\Services\DTO\Usuarios\UsuarioRequest;
use App\Services\DTO\Usuarios\UsuarioResponse;
use App\Services\Interfaces\ICCiudadService;
use App\Services\Interfaces\ICEstadoCivilService;
use App\Services\Interfaces\ICEstadoService;
use App\Services\Interfaces\IExportarDatosService;
use App\Services\Interfaces\IUsuarioService;
use App\Services\Mapper\Interfaces\IUsuarioMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;

use Illuminate\Support\Facades\DB;


class UsuarioService implements IUsuarioService
{
    protected $unitOfWork;
    protected $usuarioRepository;
    protected $usuarioQuery;
    protected $usuarioMapper;


    protected $expotLayout;
    protected $estados;
    protected $ciudades;
    protected $estadoCivil;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IUsuarioRepository $usuarioRepository,
        IUsuarioQuery $usuarioQuery,
        IUsuarioMapper $usuarioMapper,

        IExportarDatosService $exportarDatosService,
        ICEstadoService $estadoService,
        ICCiudadService $ciudadService,
        ICEstadoCivilService $estadoCivilService
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->usuarioRepository    = $usuarioRepository;
        $this->usuarioQuery         = $usuarioQuery;
        $this->usuarioMapper        = $usuarioMapper;

        $this->exportLayout         = $exportarDatosService;
        $this->estados              = $estadoService;
        $this->ciudades             = $ciudadService;
        $this->estadoCivil          = $estadoCivilService;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findUsuarioRequest = new FindUsuariosRequest($request->all());
        //Query
        $this->usuarioQuery->init();
        $this->usuarioQuery->withRolNombre($findUsuarioRequest->getRolNombre());
        $this->usuarioQuery->withNombre($findUsuarioRequest->getNombre());
        $this->usuarioQuery->withNombreCompleto($findUsuarioRequest->getNombreCompleto());
        $this->usuarioQuery->withApellidoPaterno($findUsuarioRequest->getApellidoPaterno());
        $this->usuarioQuery->withApellidoMaterno($findUsuarioRequest->getApellidoMaterno());
        $this->usuarioQuery->withTitulo($findUsuarioRequest->getTitulo());
        $this->usuarioQuery->withPuesto($findUsuarioRequest->getPuesto());
        $this->usuarioQuery->withFechaNacimiento($findUsuarioRequest->getFechaNacimiento());
        $this->usuarioQuery->withMinFechaNacimiento($findUsuarioRequest->getMinFechaNacimiento());
        $this->usuarioQuery->withMaxFechaNacimiento($findUsuarioRequest->getMaxFechaNacimiento());
        $this->usuarioQuery->withTelefono($findUsuarioRequest->getTelefono());
        $this->usuarioQuery->withTelefonoOficina($findUsuarioRequest->getTelefonoOficina());
        $this->usuarioQuery->withExtension($findUsuarioRequest->getExtension());
        $this->usuarioQuery->withCelular($findUsuarioRequest->getCelular());
        $this->usuarioQuery->withEmail($findUsuarioRequest->getEmail());
        $this->usuarioQuery->includeCliente($findUsuarioRequest->isIncludeCliente());
        $this->usuarioQuery->withClienteId($findUsuarioRequest->getClienteId());
        $this->usuarioQuery->withActivo($findUsuarioRequest->getActivo());
        $this->usuarioQuery->includeRol($findUsuarioRequest->isIncludeRol());
        $this->usuarioQuery->withRolId($findUsuarioRequest->getRolId());
        $this->usuarioQuery->includeEmpresa($findUsuarioRequest->isIncludeEmpresa());
        $this->usuarioQuery->withEmpresaId($findUsuarioRequest->getEmpresaId());
        $this->usuarioQuery->includeSucursal($findUsuarioRequest->isIncludeSucursal());
        $this->usuarioQuery->withSucursalId($findUsuarioRequest->getSucursalId());

        $this->usuarioQuery->includeDepartamento($findUsuarioRequest->isIncludeDepartamento());
        $this->usuarioQuery->withDepartamentoId($findUsuarioRequest->getDepartamentoId());
        //Orden
        $this->usuarioQuery->sort($findUsuarioRequest->getSortBy(), $findUsuarioRequest->getSort());
        //Total registros
        $totalCount = $this->usuarioQuery->totalCount();
        //Paginacion
        $this->usuarioQuery->paginate(
            $findUsuarioRequest->getPaginate(),
            $findUsuarioRequest->getItemsToShow(),
            $findUsuarioRequest->getPage()
        );

        //Ejecutar query
        $usuarios = $this->usuarioQuery->execute();

        //Mapear query
        $usuariosResponse = $this->usuarioMapper->getMapper()->mapMultiple($usuarios, UsuarioResponse::class);

        //Find data response
        $findUsuariosDataResponse = new FindDataResponse();
        $findUsuariosDataResponse->setResults($usuariosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findUsuarioRequest->getPage());
        $paginationResponse->setPageCount(count($usuariosResponse));
        $paginationResponse->setPageSize($findUsuarioRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findUsuariosResponse = new  FindResponse();
        $findUsuariosResponse->setData($findUsuariosDataResponse);
        $findUsuariosResponse->setPagination($paginationResponse);

        return response()->json($findUsuariosResponse);

    }

    public function get($id)
    {
        /* @var $usuario Usuario*/
        $usuario = $this->usuarioRepository->get($id);
        ProxyExtensions::load($usuario->getRol());
        ProxyExtensions::toArray($usuario->getRol()->getPermisos());
        ProxyExtensions::load($usuario->getCliente());
        $usuarioResponse = $this->usuarioMapper->getMapper()->map($usuario, UsuarioResponse::class);
        return response()->json($usuarioResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $usuario Usuario*/
        $usuarioRequest = new UsuarioRequest($request->all());
        $password = bcrypt($request->password);
        $fechaNacimiento = new \DateTime($request->fecha_nacimiento);
        $usuario = $this->usuarioMapper->getMapper()->map($usuarioRequest, Usuario::class);
        $usuario->setActivo(true);
        $usuario->setPassword($password);
        $usuario->setFechaNacimiento($fechaNacimiento);
        $this->usuarioRepository->add($usuario);
        $createResponse = new CreateResponse();
        $createResponse->setId($usuario->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $usuario Usuario*/
            $usuarioRequest = new UsuarioRequest($request->all());
            $usuario = $this->usuarioRepository->get($usuarioRequest->getId());
            $this->usuarioMapper->getMapper()->mapToObject($usuarioRequest, $usuario);
            if(!is_null($usuarioRequest->password)){
                $password = bcrypt($usuarioRequest->password);
                $usuario->setPassword($password);
            }
            $this->usuarioRepository->update($usuario);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $usuario Usuario*/

        //Eliminado logico
        try{
            $usuario = $this->usuarioRepository->get($id);
            $usuario->setActivo(false);
            $this->usuarioRepository->update($usuario);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function exportLayout($request){
        $findUsuario = new LayoutUsuariosRequest($request->all());
        if(StringExtensions::isNotNullOrEmpty($findUsuario->getRolName()) &&
            StringExtensions::isNotNullOrEmpty($findUsuario->getTipo())){
            $layout = $this->generaLayout($findUsuario->getTipo(), $findUsuario->getRolName(), $findUsuario->getEmpresaId(), $findUsuario->getSucursalId());
            return response()->json($layout);
        }
        else{
            return response()->json(array(), 422);
        } 
    }

    private function generaLayout($tipo, $rol, $empresa_id= 0, $sucursal_id= 0){
        $titulosEncabezado = [];
        $datos = [];
        $nombre = "Layout de " .$tipo. " para el rol de " .$rol;
        $encabezado_base = ["Valor", "Nombre"];

        // Layout para el alta de los empleados
        if($tipo === "empleado"){
            $formatColumn = [ 'D' => 'yyyy-mm-dd', 'L' => 'yyyy-mm-dd'];
            $titulosEncabezado = [
                'Nombres', 'Apellido Paterno', 'Apellido Materno', 'Fecha Nacimiento', 'Sexo', 'Direccion', 'Estado', 'Ciudad', 
                'Codigo Postal', 'Telefono', 'Correo', 'Fecha de Ingreso', 'Tiene Hijos', 'Puesto', 'Clave Interna' ,'Horario', "Estado Civil", "Departamento"
            ];

            $hojas = ["Estados","Ciudades", "Horarios", "Estado civil", "Departamentos"];
            $estados = $this->getEstados("estados");
            $ciudades = $this->getCiudades();
            $estados_civil= $this->getEstados("estados_civil");
            $encabezado_ciudad = ["Valor", "Nombre", "Estado"];

            if($rol == "admin-empresa"){
                $datos = [['Manuel Jesus','Euan', 'Batun', 2019-01-01, 'M', 'Calle 15 # 101 * 28 y 30', 1, 1, '97390', '9999999999', 'manuel@gmail.com', 2019-01-01, "SI", "Desarrollador", "1010 (No obligatorio)", 1, 1, 1,1]];
                array_push($titulosEncabezado, 'Sucursal');
                array_push($hojas, 'Sucursales');
                $limite_filas= 'A1:S1';
                $departamentos = $this->getDepartamentos($empresa_id,$rol);
                $sucursales = $this->getSucursales($empresa_id);
                $horarios = $this->getHorario($empresa_id,$rol);

                $arrayGeneral= array('Hojas' => $hojas, 'Estados' => $estados, 'Ciudades' => $ciudades, "Civiles" => $estados_civil, 
                                "Departamentos" => $departamentos, "Sucursales" => $sucursales, "Horarios" => $horarios, "encabezado_base" => $encabezado_base, "encabezado_ciudad" => $encabezado_ciudad );
            }
            else{
                $limite_filas= 'A1:R1';
                $datos = [['Manuel Jesus','Euan', 'Batun', 2019-01-01, 'M', 'Calle 15 # 101 * 28 y 30', 1, 1, '97390', '9999999999', 'manuel@gmail.com', 2019-01-01, "SI", "Desarrollador", "1010 (No obligatorio)", 1, 1,1]];
                $departamentos = $this->getDepartamentos($sucursal_id,$rol);
                $horarios = $this->getHorario($sucursal_id,$rol);
                $arrayGeneral= array('Hojas' => $hojas, 'Estados' => $estados, 'Ciudades' => $ciudades, "Civiles" => $estados_civil, 
                                "Departamentos" => $departamentos, "Horarios" => $horarios, "encabezado_base" => $encabezado_base, "encabezado_ciudad" => $encabezado_ciudad );
            }
        }
        
        elseif($tipo == "dia_festivo"){
            $formatColumn = [ 'B' => 'yyyy-mm-dd' ];
            $titulosEncabezado = [ 'Nombre', 'Fecha', 'Descripción',"Departamento" ];

            $hojas = [ "Departamentos"];
            $estados = $this->getEstados("estados");

            if($rol == "admin-empresa"){
                $datos = [['Navidad',2019-12-24, 'Felicidades a todos en estas fiestas.', 1, 1]];
                array_push($titulosEncabezado, 'Sucursal');
                array_push($hojas, 'Sucursales');
                $limite_filas= 'A1:E1';
                $departamentos = $this->getDepartamentos($empresa_id,$rol);
                $sucursales = $this->getSucursales($empresa_id);

                $arrayGeneral= array('Hojas' => $hojas, "Departamentos" => $departamentos, "Sucursales" => $sucursales, "encabezado_base" => $encabezado_base );
            }
            else{
                $limite_filas= 'A1:D1';
                $datos = [['Navidad',2019-12-24, 'Felicidades a todos en estas fiestas.', 1 ]];
                $departamentos = $this->getDepartamentos($sucursal_id,$rol);
                $arrayGeneral= array('Hojas' => $hojas,  "Departamentos" => $departamentos, "encabezado_base" => $encabezado_base);
            }       
        }
        return $this->exportLayout->layoutEmpleado($nombre, $titulosEncabezado, $datos, $limite_filas, $formatColumn, $tipo, $rol, $arrayGeneral );
    }

    private function getEstados($opcion){
        $query = DB::table($opcion);
        $query->where("activo", true);
        $estados = $query->get();
        
        $arrayRegistros= array();
        foreach ($estados as $index => $registro) {
            $arrayRegistros[$index] = [
                isset($registro->id) ? $registro->id : "",
                isset($registro->nombre) ? $registro->nombre : "",
            ];
        }
        return $arrayRegistros;
    }

    private function getCiudades(){
        $query = DB::table("ciudades");
        $query->join("estados", "ciudades.estado_id", "=", "estados.id");
        $query->where("ciudades.activo", true);
        $query->select("ciudades.*", "estados.nombre as estado");
        $estados = $query->get();
        
        $arrayRegistros= array();
        foreach ($estados as $index => $registro) {
            $arrayRegistros[$index] = [
                isset($registro->id) ? $registro->id : "",
                isset($registro->nombre) ? $registro->nombre : "",
                isset($registro->estado) ? $registro->estado : "",
            ];
        }
        return $arrayRegistros;
    }

    private function getDepartamentos($value,$rol){
        $query = DB::table("departamento");

        if($rol == "admin-empresa"){
            $query->join("sucursal", "departamento.sucursal_id", "=", "sucursal.id");
            $query->where("departamento.activo", true);
            $query->where("departamento.empresa_id", "=", $value);
            $query->select("departamento.*", "sucursal.nombre as sucursal");
       }
       else{
            $query->where("departamento.sucursal_id", "=", $value);
            $query->where("departamento.activo", true);
       }
       $departamentos = $query->get();
       
       $arrayRegistros= array();
        foreach ($departamentos as $index => $registro) {
            $nombre = $registro->nombre;
            if(isset($registro->sucursal)){
                $nombre = $registro->nombre.", sucursal: ". $registro->sucursal;
            }
        
            $arrayRegistros[$index] = [
                isset($registro->id) ? $registro->id : "",
                isset($nombre) ? $nombre : "",
            ];
        }
        return $arrayRegistros;
    }

    private function getSucursales($empresa_id){
        $query = DB::table("sucursal");
        $query->where("sucursal.activo", true);
        $query->where("empresa_id", "=", $empresa_id);
        $sucursales = $query->get();

       $arrayRegistros= array();
        foreach ($sucursales as $index => $registro) {
            $arrayRegistros[$index] = [
                isset($registro->id) ? $registro->id : "",
                isset($registro->nombre) ? $registro->nombre : "",
            ];
        }
        return $arrayRegistros;
    }

    private function getHorario($value, $rol){
        $query = DB::table("horarios");
        if($rol == "admin-empresa"){
            $query->join("sucursal", "horarios.sucursal_id", "=", "sucursal.id");
            $query->where("horarios.activo", true);
            $query->where("horarios.empresa_id", "=", $value);
            $query->select("horarios.*", "sucursal.nombre as sucursal");
       }
       else{
            $query->where("horarios.sucursal_id", "=", $value);
            $query->where("horarios.activo", true);
       }
       $horarios = $query->get();
       $arrayRegistros= array();

        foreach ($horarios as $index => $registro) {
            $nombre = $registro->nombre;
            if(isset($registro->sucursal)){
                $nombre = $registro->nombre.", sucursal: ". $registro->sucursal;
            }
        
            $arrayRegistros[$index] = [
                isset($registro->id) ? $registro->id : "",
                isset($nombre) ? $nombre : "",
            ];
        }
        return $arrayRegistros;
    }

    public function password($request){
        $password = bcrypt($request->password);
        DB::table('usuarios')->where('id', $request->id)
            ->update([ 'password' => $password, 
                        'primer_inicio_sesion' => $request->primer_inicio_sesion 
            ]);
        return response()->json(true, 200 );
    }
}
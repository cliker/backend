<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Implement\ChecadaQuery;
use App\DataAccess\Queries\Interfaces\IEmpleadoQuery;
use App\DataAccess\Repositories\Interfaces\IDocumentoRepository;
use App\DataAccess\Repositories\Interfaces\IEmpleadoRepository;
use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\Domain\Documento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Incidencia;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\ProxyExtensions;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Documentos\DocumentoRequest;
use App\Services\DTO\Empleados\CreateEmpleadoResponse;
use App\Services\DTO\Empleados\EmpleadoRequest;
use App\Services\DTO\Empleados\EmpleadoResponse;
use App\Services\DTO\Empleados\FindCheckedEmpleadosRequest;
use App\Services\DTO\Empleados\FindEmpleadosRequest;
use App\Services\Interfaces\IEmpleadoService;
use App\Services\Interfaces\IHistoricoEmpleadoService;
use App\Services\Interfaces\IIncidenciaService;
use App\Services\Mapper\Interfaces\IDocumentoMapper;
use App\Services\Mapper\Interfaces\IEmpleadoMapper;
use function Functional\true;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use DateTime;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class EmpleadoService implements IEmpleadoService
{
    protected $unitOfWork;
    protected $empleadoRepository;
    protected $empleadoQuery;
    protected $empleadoMapper;
    protected $documentoMapper;
    protected $documentoRepository;
    protected $empresaRepository;
    protected $incidenciaService;
    protected $historicoService;
    public function __construct(
        IUnitOfWork               $unitOfWork,
        IEmpleadoRepository $empleadoRepository,
        IEmpleadoQuery $empleadoQuery,
        IEmpleadoMapper $empleadoMapper,
        IDocumentoMapper $documentoMapper,
        IDocumentoRepository $documentoRepository,
        IEmpresaRepository $empresaRepository,
        IIncidenciaService $incidenciaService,
        IHistoricoEmpleadoService $historicoService
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->empleadoRepository    = $empleadoRepository;
        $this->empleadoQuery         = $empleadoQuery;
        $this->empleadoMapper        = $empleadoMapper;
        $this->documentoMapper = $documentoMapper;
        $this->documentoRepository = $documentoRepository;
        $this->empresaRepository = $empresaRepository;
        $this->incidenciaService = $incidenciaService;
        $this->historicoService = $historicoService;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request){

        $findEmpleadoRequest = new FindEmpleadosRequest($request->all());

        //Query
        $this->empleadoQuery->init();
        $this->empleadoQuery->withNombre($findEmpleadoRequest->getNombre());
        $this->empleadoQuery->withApellidoPaterno($findEmpleadoRequest->getApellidoPaterno());
        $this->empleadoQuery->withApellidoMaterno($findEmpleadoRequest->getApellidoMaterno());
        $this->empleadoQuery->withFechaNacimiento($findEmpleadoRequest->getFechaNacimiento());
        $this->empleadoQuery->withMinFechaNacimiento($findEmpleadoRequest->getMinFechaNacimiento());
        $this->empleadoQuery->withMaxFechaNacimiento($findEmpleadoRequest->getMaxFechaNacimiento());
        $this->empleadoQuery->withSexo($findEmpleadoRequest->getSexo());
        $this->empleadoQuery->withEstadoId($findEmpleadoRequest->getEstadoId());
        $this->empleadoQuery->includeEstado($findEmpleadoRequest->isIncludeEstado());
        $this->empleadoQuery->withCiudadId($findEmpleadoRequest->getCiudadId());
        $this->empleadoQuery->includeCiudad($findEmpleadoRequest->isIncludeCiudad());
        $this->empleadoQuery->withCodigoPostal($findEmpleadoRequest->getCodigoPostal());
        $this->empleadoQuery->withTelefono($findEmpleadoRequest->getTelefono());
        $this->empleadoQuery->withCorreo($findEmpleadoRequest->getCorreo());
        $this->empleadoQuery->withEstadoCivilId($findEmpleadoRequest->getEstadoCivilId());
        $this->empleadoQuery->includeEstadoCivil($findEmpleadoRequest->isIncludeEstadoCivil());
        $this->empleadoQuery->withFechaIngreso($findEmpleadoRequest->getFechaIngreso());
        $this->empleadoQuery->withMinFechaIngreso($findEmpleadoRequest->getMinFechaIngreso());
        $this->empleadoQuery->withMaxFechaIngreso($findEmpleadoRequest->getMaxFechaIngreso());
        $this->empleadoQuery->withFechaBaja($findEmpleadoRequest->getFechaBaja());
        $this->empleadoQuery->withMinFechaBaja($findEmpleadoRequest->getMinFechaBaja());
        $this->empleadoQuery->withMaxFechaBaja($findEmpleadoRequest->getMaxFechaBaja());
        $this->empleadoQuery->withDiaDescanso($findEmpleadoRequest->getDiaDescanso());
        $this->empleadoQuery->withHorarioId($findEmpleadoRequest->getHorarioId());
        $this->empleadoQuery->includeHorario($findEmpleadoRequest->isIncludeHorario());
        $this->empleadoQuery->withSucursalId($findEmpleadoRequest->getSucursalId());
        $this->empleadoQuery->includeSucursal($findEmpleadoRequest->isIncludeSucursal());
        $this->empleadoQuery->withDepartamentoId($findEmpleadoRequest->getDepartamentoId());
        $this->empleadoQuery->includeDepartamento($findEmpleadoRequest->isIncludeDepartamento());
        $this->empleadoQuery->withActivo($findEmpleadoRequest->getActivo());
        $this->empleadoQuery->includeEmpresa($findEmpleadoRequest->isIncludeEmpresa());
        $this->empleadoQuery->withEmpresaId($findEmpleadoRequest->getEmpresaId());
        $this->empleadoQuery->includeCliente($findEmpleadoRequest->isIncludeCliente());
        $this->empleadoQuery->withClienteId($findEmpleadoRequest->getClienteId());
        $this->empleadoQuery->includeRelojesChecador($findEmpleadoRequest->isIncludeRelojesChecador());

        //Orden
        $this->empleadoQuery->sort($findEmpleadoRequest->getSortBy(), $findEmpleadoRequest->getSort());
        //Total registros
        $totalCount = $this->empleadoQuery->totalCount();

        //Paginacion
        $this->empleadoQuery->paginate(
            $findEmpleadoRequest->getPaginate(),
            $findEmpleadoRequest->getItemsToShow(),
            $findEmpleadoRequest->getPage()
        );

        //Ejecutar query
        $empleados = $this->empleadoQuery->execute();

        //Mapear query
        $empleadosResponse = $this->empleadoMapper->getMapper()->mapMultiple($empleados, EmpleadoResponse::class);
        $antiguedad = $this->agregaAntiguedad($empleadosResponse);

        //Find data response
        $findEmpleadosDataResponse = new FindDataResponse();
        $findEmpleadosDataResponse->setResults($antiguedad);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findEmpleadoRequest->getPage());
        $paginationResponse->setPageCount(count($empleadosResponse));
        $paginationResponse->setPageSize($findEmpleadoRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findEmpleadosResponse = new  FindResponse();
        $findEmpleadosResponse->setData($findEmpleadosDataResponse);
        $findEmpleadosResponse->setPagination($paginationResponse);
        return response()->json($findEmpleadosResponse);
    }

    private function agregaAntiguedad($datos){
        $nuevo = [];
        $hoy = date("Y-m-d");
        $año = date("Y");

        foreach ($datos as $valor){
            $fecha_ingreso = explode("-",$valor->fecha_ingreso);
            $ing_mod = $año."-".$fecha_ingreso[1]."-".$fecha_ingreso[2];
            $antiguedad = $año - $fecha_ingreso[0];
            
            if($antiguedad != 0){
                if($hoy < $ing_mod){
                    $antiguedad = $antiguedad - 1;
                }
            }
            $valor->dia_descanso = (string)$antiguedad;
            array_push($nuevo,$valor);
        }
        return $nuevo;
    }

    public function get($id){
        /* @var $empleado Empleado*/
        $empleado = $this->empleadoRepository->get($id);
        ProxyExtensions::load($empleado->getEstado());
        ProxyExtensions::load($empleado->getCiudad());
        ProxyExtensions::load($empleado->getHorario());
        ProxyExtensions::load($empleado->getSucursal());
        ProxyExtensions::load($empleado->getEstadoCivil());
        ProxyExtensions::load($empleado->getDepartamento());
        ProxyExtensions::toArray($empleado->getIncidencias());
        ProxyExtensions::toArray($empleado->getDocumentos());
        ProxyExtensions::toArray($empleado->getRegistrosChecador());
        ProxyExtensions::toArray($empleado->getRelojesChecador());
        $empleadoResponse = $this->empleadoMapper->getMapper()->map($empleado, EmpleadoResponse::class);
        $empleadoResponse = $this->filterData($empleadoResponse);
        return response()->json($empleadoResponse);
    }

    private function filterData($empleado){
        $empleadoNew = clone($empleado);
        /* @var $empleado Empleado*/
        unset($empleadoNew->incidencias);
        unset($empleadoNew->documentos);
        $incidencias = $this->filterIncidencias($empleado->getIncidencias());
        $documentos = $this->filterDocumentos($empleado->getDocumentos());
        $empleadoNew->incidencias = $incidencias;
        $empleadoNew->documentos = $documentos;
        return $empleadoNew;
    }

    private function filterIncidencias($incidencias){
        $incidenciasObj = [];
        /* @var $incidencia Incidencia*/
        foreach ($incidencias as $incidencia){
            if($incidencia->getActivo() === true){
                array_push($incidenciasObj, $incidencia);
            }
        }
        return $incidenciasObj;
    }

    private function filterDocumentos($documentos){
        $documentosObj = [];
        /* @var $documento Documento*/
        foreach ($documentos as $documento){
            if($documento->getActivo() === true){
                array_push($documentosObj, $documento);
            }
        }
        return $documentosObj;
    }

    /**
     * @param Request $request
     */
    public function create($request){
        /* @var $empleado Empleado*/
        $empleadoRequest = new EmpleadoRequest($request->all());
        $claveEmpleado = $this->getClaveAuto($empleadoRequest->getEmpresaId());

        if($claveEmpleado !== false){
            $empleado = $this->empleadoMapper->getMapper()
                ->map($empleadoRequest, Empleado::class);
            $empleado->setActivo(true);
            $empleado->setClave($claveEmpleado);
            if(ArrayExtensions::isNotNullOrEmpty($empleadoRequest->getDocumentos())){
                $documentos = $this->createDocumentos($empleadoRequest->getDocumentos(), $empleado);
                $empleado->setDocumentos($documentos);
            }
            $this->empleadoRepository->add($empleado);
            $this->updateClaveAuto($empleadoRequest->getEmpresaId(), $claveEmpleado);
            $createResponse = new CreateEmpleadoResponse();
            $createResponse->setId($empleado->getId());
            $createResponse->setClave($empleado->getClave());
            $empleado->usuario_id = $request->usuario_id;
            $empleado->accion = "Ingreso";
            $this->historicoService->create($empleado);

            return response()->json($createResponse);

        }else{
            $isSuccess = new SuccessResponse();
            $isSuccess->setIsSuccess(false);
            return response()->json($isSuccess);
        }
    }

    private function getClaveAuto($empresaId){
        $clave = false;
        /* @var $empresa Empresa*/
        $empresa = $this->empresaRepository->get($empresaId);
        $claveEmpresa = $empresa->getClaveEmpleadoAuto();
        $clave =  ++$claveEmpresa;
        return $clave;
    }

    private function updateClaveAuto($empresaId, $claveEmpleado){
        /* @var $empresa Empresa*/
        $empresa = $this->empresaRepository->get($empresaId);
        $empresa->setClaveEmpleadoAuto($claveEmpleado);
        $this->empresaRepository->update($empresa);
    }

    private function createDocumentos($documentosRequest, $empleado){
        $documentos = array();
        foreach ($documentosRequest as $documentoRequest){
            /* @var $documento Documento*/
            $documentoRequestObj = new DocumentoRequest($documentoRequest);
            $documento = $this->documentoMapper->getMapper()->map($documentoRequestObj, Documento::class);
            $documento->setActivo(true);
            $documento->setEmpleado($empleado);
            array_push($documentos, $documento);
        }
        return $documentos;
    }

    /**
     * @param Request $request
     */
    public function update($request){
        $success = false;
        try{
            /* @var $empleado Empleado*/
            $empleadoRequest = new EmpleadoRequest($request->all());
            $empleado = $this->empleadoRepository->get($empleadoRequest->getId());
            $this->empleadoMapper->getMapper()->mapToObject($empleadoRequest, $empleado);

            if(ArrayExtensions::isNotNullOrEmpty($empleadoRequest->getDocumentosEliminados())){
                $this->deleteDocumentos($empleadoRequest->getDocumentosEliminados());
            }

            if(ArrayExtensions::isNotNullOrEmpty($empleadoRequest->getDocumentos())){
                $this->updateDocumentos($empleadoRequest->getDocumentos(), $empleado);
            }
            $this->updateUsuario($request);
            $this->empleadoRepository->update($empleado);
            $this->update_ingreso_historico($empleadoRequest);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    private function update_ingreso_historico($request){
        DB::table('historico_empleados')
            ->where('empleado_id', "=" ,$request->id)
            ->where('accion', "=", "Ingreso")
            ->update(['fecha' => $request->fecha_ingreso ]);
        return true;
    }

    private function updateUsuario($request){
        DB::table('usuarios')->where('email', $request->email_anterior)
            ->update([
                'nombre' => $request->nombre,
                'apellido_paterno' => $request->apellido_paterno,
                'apellido_materno' => $request->apellido_materno,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'telefono' => $request->telefono,
                'puesto' => $request->puesto,
            ]);
    }

    private function updateDocumentos($documentosRequest, $empleado){

        foreach ($documentosRequest as $documentoRequest){

            if(isset($documentoRequest['id']) && $documentoRequest['id'] !== 0){
                $documentoRequestObj = new DocumentoRequest($documentoRequest);
                $documento = $this->documentoRepository->get($documentoRequest['id']);
                $this->documentoMapper->getMapper()->mapToObject($documentoRequestObj, $documento);
                $this->documentoRepository->update($documento);
            }else{

                $documentoRequestObj = new DocumentoRequest($documentoRequest);
                /* @var $documento Documento*/
                $documento = $this->documentoMapper->getMapper()->map($documentoRequestObj, Documento::class);
                $documento->setActivo(true);
                $documento->setEmpleado($empleado);
                $this->documentoRepository->add($documento);
            }
        }
    }

    private function deleteDocumentos($documentosEliminados){
        foreach ($documentosEliminados as $documentoEliminado){
            /* @var $documentoEliminado Documento*/
            try{
                $documentoEliminado = $this->documentoRepository->get($documentoEliminado['id']);
                $documentoEliminado->setActivo(false);
                $this->documentoRepository->update($documentoEliminado);
            }
            catch(Exception $exception){
                throw $exception;
            }
        }
    }

    /**
     * @param Request $request
     */
    public function delete($request){
        $success = false;

        /* @var $empleado Empleado*/
        //Eliminado logico
        try{
            $empleado = $this->empleadoRepository->get($request->id);
            $empleado->setActivo(false);
            $empleado->setFechaBaja(new DateTime('now'));
            $this->empleadoRepository->update($empleado);
            $this->activaUsuario($request, false);
            $this->historicoService->create($request);
            $success = true;
            
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function findChecked($request){
        $findCheckedEmpleadoRequest = new FindCheckedEmpleadosRequest($request->all());

        if(StringExtensions::isNotNullOrEmpty($findCheckedEmpleadoRequest->getMinFechaChecada()) &&
            StringExtensions::isNotNullOrEmpty($findCheckedEmpleadoRequest->getMaxFechaChecada())) {
            $fechas = $this->generateDatesCheked($findCheckedEmpleadoRequest->getMinFechaChecada(), $findCheckedEmpleadoRequest->getMaxFechaChecada());
            $empleadosObj = $this->generateEmpleadosObj($findCheckedEmpleadoRequest, $fechas);
            return response()->json($empleadosObj);
        }else{
            return response()->json(array(), 422);
        }
    }

    private function generateDatesCheked($minFecha, $maxFecha){

        $fechas = array();

        while (strtotime($minFecha) <= strtotime($maxFecha)){
            array_push($fechas, $minFecha);
            $minFecha = strtotime( '+1 day' , strtotime($minFecha));
            $minFecha = date('Y-m-j',$minFecha);
        }
        return $fechas;
    }

    /**
     * @param FindCheckedEmpleadosRequest $findCheckedEmpleadoRequest
     * @return array
     */
    private function generateEmpleadosObj($findCheckedEmpleadoRequest, $fechas){
        $empleadosObj = array();

        foreach ($fechas as $fecha){
            $queryChecada = new ChecadaQuery();
            $queryChecada->getHoras($fecha);
            $queryChecada->includeHorario($findCheckedEmpleadoRequest->isIncludeHorario());
            $queryChecada->includeSucursal($findCheckedEmpleadoRequest->isIncludeSucursal());
            $queryChecada->includeDepartamento($findCheckedEmpleadoRequest->isIncludeDepartamento());
            $queryChecada->includeCliente($findCheckedEmpleadoRequest->isIncludeCliente());
            $queryChecada->includeEmpresa($findCheckedEmpleadoRequest->isIncludeEmpresa());
            $queryChecada->from();
            $queryChecada->leftJoinHorario($findCheckedEmpleadoRequest->isIncludeHorario());
            $queryChecada->leftJoinSucursal($findCheckedEmpleadoRequest->isIncludeSucursal());
            $queryChecada->leftJoinDepartamento($findCheckedEmpleadoRequest->isIncludeDepartamento());
            $queryChecada->leftJoinCliente($findCheckedEmpleadoRequest->isIncludeCliente());
            $queryChecada->leftJoinEmpresa($findCheckedEmpleadoRequest->isIncludeEmpresa());
            $queryChecada->where($findCheckedEmpleadoRequest->getEmpleadoId());
            $queryChecada->whitHorarioId($findCheckedEmpleadoRequest->getHorarioId());
            $queryChecada->whitSucursalId($findCheckedEmpleadoRequest->getSucursalId());
            $queryChecada->whitDepartamentoId($findCheckedEmpleadoRequest->getDepartamentoId());
            $queryChecada->whitClienteId($findCheckedEmpleadoRequest->getClienteId());
            $queryChecada->whitEmpresaId($findCheckedEmpleadoRequest->getEmpresaId());
            $queryChecada->withActivo($findCheckedEmpleadoRequest->getActivo());
            $checadas = $queryChecada->execute($findCheckedEmpleadoRequest->isIncludeHorario(), $findCheckedEmpleadoRequest->isIncludeSucursal(), $findCheckedEmpleadoRequest->isIncludeDepartamento(), $findCheckedEmpleadoRequest->isIncludeCliente(), $findCheckedEmpleadoRequest->isIncludeEmpresa());
            $queryChecada->closeConnection();

            foreach ($checadas as $checada){
                $checada['retardo'] = null;
                $checada['anticipado'] = null;
                $checada['falta'] = null;

                $detalle = $checada['horario']['detalles_horario'];
                $tolerancia = $checada['horario']['minutos_tolerancia'];
                $registro_entrada =  new DateTime($checada['hora_entrada']);
                $registro_salida=  new DateTime($checada['hora_salida']);
                $incidencia = $this->validaIncidencias($checada);
                $total = count($incidencia);

                if(!is_null($checada['horario']) && !is_null($checada['hora_entrada'])){
                    foreach($detalle as $det){
                        if($checada['dia'] === $det['dia']){
                            if(!is_null($registro_entrada)){
                                $entrada = new DateTime($det['hora_entrada']);
                                $entrada->modify('+'.($tolerancia +1).'minutes');
                                $salida =  new DateTime($det['hora_salida']);
                                
                                if($registro_entrada >= $entrada){
                                    if($total === 0){
                                        if($checada['dia'] === $det['dia']){
                                            $checada['retardo'] = "SI";
                                        }
                                    }
                                    else{
                                        $checada['retardo'] = $incidencia[0]->nombre;
                                    }
                                }
                                
                                if($registro_salida < $salida){
                                    if($total === 0){
                                        if($checada['dia'] === $det['dia']){
                                            $checada['anticipado'] = "SI";
                                        }
                                    }
                                    else{
                                        $checada['anticipado'] = $incidencia[0]->nombre;
                                    }
                                }
                            }
                        }
                    }
                }
                else{
                    foreach($detalle as $det){
                        if($checada['dia'] === $det['dia']){
                            if(!is_null($registro_entrada)){
                                if($total === 0){
                                    if($checada['dia'] === $det['dia']){
                                        $checada['falta'] = "SI";
                                    }
                                }
                                else{
                                    $checada['falta'] = $incidencia[0]->nombre;
                                }
                            }
                        }
                    }
                }
               
                array_push($empleadosObj, $checada);
            }
        }
        return $empleadosObj;
    }

    private function validaIncidencias($request){
        try{
            $incidencia = DB::table('incidencias')
                        ->join('tipos_incidencias', 'incidencias.tipo_id', '=', 'tipos_incidencias.id')
                        ->select('tipos_incidencias.*', 'incidencias.fecha_inicio', 'incidencias.fecha_final')
                        ->where('incidencias.estatus_id', '=', 3)
                        ->where('incidencias.activo', '=', true)
                        ->where('incidencias.fecha_inicio','<=', $request['fecha'])
                        ->where('incidencias.fecha_final','>=', $request['fecha'])
                        ->where('incidencias.empleado_id','=', $request["id"])
                        ->get();
        }
        catch(Exception $ex){
            throw $ex;
        }
        
        return $incidencia;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function creaUsuario($request){
        $response = false;
        try {
            $response = DB::table("usuarios")->insert([
                'cliente_id' => $request->cliente_id,
                'rol_id' => 6,
                'nombre' => $request->nombre,
                'apellido_paterno' => $request->apellido_paterno,
                'apellido_materno' => $request->apellido_materno,
                'puesto' => $request->puesto,
                'telefono' => $request->telefono,
                'email' => $request->correo,
                'fecha_nacimiento' => $request->fecha_ingreso,
                'password' => bcrypt('123456'),
                'primer_inicio_sesion' => true,
                'empresa_id' => $request->empresa_id,
                'sucursal_id' => $request->sucursal_id,
                'created_at' => new DateTime('now'),
                'departamento_id' =>$request->departamento_id,
                'activo' => true
    
            ]);
            return $response;
        }
        catch(Exception $e) {
            return $response;
        }
    }

    public function dashboard($request){
        $array_empleados = [];
        $array_cumple = [];
        $array_aniversarios = [];

        $query = DB::table('empleados');
        if($request->rol_name == 'admin'){
            $query->where('empleados.cliente_id', '=', $request->cliente_id);
        }
        elseif($request->rol_name == 'admin-empresa'){
            $query->where('empleados.empresa_id', '=', $request->empresa_id);
        }
        elseif($request->rol_name == 'admin-sucursal' || $request->rol_name == 'admin-depto' || $request->rol_name == 'empleado'){
            $query->where('empleados.sucursal_id', '=', $request->sucursal_id);
        }
        else{
            return response()->json("No tienes accesso.", 402);;
        }
        $query->join('departamento', 'empleados.departamento_id', '=', 'departamento.id')
                ->select('empleados.*', 'departamento.nombre as nombre_depto');
        $cumpleanios = $query->get();
        $inicio = explode("-",$request->min_fecha);
        $final = explode("-",$request->max_fecha);
        
        $fecha_inicio = "2019-".$inicio[1]."-".$inicio[2];
        $fecha_final = "2019-".$final[1]."-".$final[2];

        foreach($cumpleanios as $valor){
            $cumple = explode("-",$valor->fecha_nacimiento);
            $cumple_mod = "2019-".$cumple[1]."-".$cumple[2];
            $ingreso = explode("-", $valor->fecha_ingreso);
            $ingreso_mod = "2019-".$ingreso[1]."-".$cumple[2];

            if($cumple_mod >= $fecha_inicio && $cumple_mod <= $fecha_final){
                array_push($array_cumple, $valor);
            }

            if($ingreso_mod >= $fecha_inicio && $ingreso_mod <= $fecha_final){
                $ingreso = explode("-",$valor->fecha_ingreso);
                $anio_ingreso= (int)($ingreso[0]);
                $año_actual = date ("Y");
                $antiguedad = $año_actual - $anio_ingreso;
                if($antiguedad > 0){
                    $valor->antiguedad = $antiguedad;
                    array_push($array_aniversarios, $valor);
                }
            }
        }

        $incidencias= $this->incidenciaService->tiposIncidencia($request);;
        $rotacion = $this->rotacion($request);
        $repuesta=array("cumpleanios"=> $array_cumple, "aniversarios"=> $array_aniversarios, "incidencias" => $incidencias, "rotacion" => $rotacion );
        return $repuesta;
    }

    public function reingreso($request){
        $success = false;

        /* @var $empleado Empleado*/
        try{
            $empleado = $this->empleadoRepository->get($request->id);
            $empleado->setActivo(true);
            $empleado->setFechaIngreso(new DateTime('now'));
            $empleado->setFechaBaja(null);
            $this->empleadoRepository->update($empleado);
            $this->activaUsuario($request, true);
            $this->historicoService->create($request);
            $success = true;

        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    private function activaUsuario($data, $accion){
        DB::table('usuarios')->where('email', $data->email)
            ->update([ 'activo' => $accion ]);
        return;
    }

    public function asignacion($request){
        $data = $request->all();
        $checadores= [];
        $empleados= [];
        $errores = [];

        for ($x=0; $x < count($data['empleados']); $x++){
            $e = (object) $data['empleados'][$x];
            array_push($empleados,$e);
        }

        for ($x=0; $x < count($data['checadores']); $x++){
            $r = (object) $data['checadores'][$x];
            
            $final = [ "checadores" => [$r], "empleados" => $empleados];
            $final = (object)$final;
            $asp = $this->createAsp($final, $request->url_server_asp);
           
            if($asp == true){
                $this->guardadoLocal($final);
            }
            else{
               array_push($errores, $r);
            }
        }
        if(count($errores) > 0){
            return $errores; 
        }
        return response()->json("Se guardo con exito", 200);;
    }

    private  function guardadoLocal($request){
        $checador_id = $request->checadores[0]->id;
        $empleados = $request->empleados;
        
        foreach($empleados as $value){
            $e = DB::table('empleados_reloj_checador')->where('reloj_checador_id', '=' , $checador_id)
                    ->where('empleado_id', '=' , $value->id)->get();

            if(count($e) == 0){
                DB::table("empleados_reloj_checador")->insert([
                    'reloj_checador_id' => $checador_id,
                    'empleado_id' => $value->id
                ]);
            }
        }
        return true;
    }

    private function createAsp($datos, $url){
        $success = false;
        try{
            $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
            $param = array( 'usuario' => 'admin','password' => 'Chec4dor$2019.');
            $login = $cliente->request('POST', 'login/login', array('form_params' => $param ));
            $token_asp = json_decode($login->getBody()->getContents());
           
            $headers =  [ 'Authorization' => 'Bearer ' . $token_asp,'Accept' => 'application/json' ];
            $response = $cliente->request('POST', 'empleados', array('form_params' => $datos , 'headers' => $headers ));
            $success = true;
        }
        catch(Exception $exception){
            return $success;
        }
        return $success;
    }

    public function rotacion($request){
        $incio = date($request->min_fecha);
        $fecha_final = date("Y-m-d",strtotime($incio." - 90 days"));
        $hoy = date("Y-m-d");
        $final = array();
        $niveles = array();

        $query = DB::table('empleados')
            ->where('empleados.activo', '=', false)
            ->whereBetween('empleados.fecha_baja', [$fecha_final, $hoy]);

        if($request->rol_name == "admin-empresa"){
            $niveles = DB::table('sucursal')->where("empresa_id","=", $request->empresa_id)->get();
            $query->where("empleados.empresa_id","=", $request->empresa_id)
                ->join("sucursal", "empleados.sucursal_id", "=", "sucursal.id")
                ->select('sucursal.nombre', 'sucursal.id', 'empleados.id as empleado_id');
            
        } 
       elseif ($request->rol_name == "admin-sucursal"){
            $niveles = DB::table('departamento')->where("sucursal_id","=", $request->sucursal_id)->get();
            
            $query->where("empleados.sucursal_id","=", $request->sucursal_id)
                ->join("departamento", "empleados.departamento_id", "=", "departamento.id")
                ->select('departamento.nombre', 'departamento.id', 'empleados.id as empleado_id');
        }
        elseif ($request->rol_name == "admin"){
            $niveles = DB::table('empresa')->where("cliente_id","=", $request->cliente_id)->get();
            
            $query->where("empleados.cliente_id","=", $request->cliente_id)
                ->join("empresa", "empleados.empresa_id", "=", "empresa.id")
                ->select('empresa.nombre', 'empresa.id', 'empleados.id as empleado_id');
        }
        else{
            return [];
        }

        $e= $query->get();

        foreach($niveles as $nivel){
            $n = [];
            foreach($e as $empleado){
                if($empleado->id == $nivel->id){
                    array_push($n,$empleado);
                }
            }
            if(count($n) > 0){
                array_push($final, $n);
            }
        }
        return $final;
    }

    public function incidencias($request){
        $final = array();
        
        $query = DB::table('incidencias')
            ->where('incidencias.fecha_inicio','>=', $request->min_fecha)
            ->where('incidencias.fecha_inicio','<=', $request->max_fecha)
            ->where('incidencias.activo', '=', true)
            ->where('incidencias.estatus_id', '=', 3)
            ->where('incidencias.tipo_id', '=', $request->tipo_incidencia);

        if($request->mostrar == "empresa"){
            $niveles = DB::table('empresa')->where("cliente_id","=", $request->cliente_id)->get();
            
            $query->where('incidencias.cliente_id', '=', $request->cliente_id)
                ->join("empresa", "incidencias.empresa_id", "=", "empresa.id")
                ->select('empresa.nombre', 'empresa.id', 'incidencias.id as incidencia_id');
        }
        elseif($request->mostrar == "sucursal"){
            $niveles = DB::table('sucursal')->where("empresa_id","=", $request->empresa_id)->get();
            
            $query->where('incidencias.empresa_id', '=', $request->empresa_id)
                ->join("sucursal", "incidencias.sucursal_id", "=", "sucursal.id")
                ->select('sucursal.nombre', 'sucursal.id', 'incidencias.id as incidencia_id');
        }
        elseif($request->mostrar == "departamento"){
            $niveles = DB::table('departamento')->where("sucursal_id","=", $request->sucursal_id)->get();
            
            $query->where('incidencias.sucursal_id', '=', $request->sucursal_id)
                ->join("departamento", "incidencias.departamento_id", "=", "departamento.id")
                ->select('departamento.nombre', 'departamento.id', 'incidencias.id as incidencia_id');
        }
        $i= $query->get();



        /* elseif ($request->mostrar == "suc-dep") {
            $niveles = DB::table('departamento')->where("empresa_id","=", $request->empresa_id)->get();

            $query->where('incidencias.empresa_id', '=', $request->empresa_id)
                ->join("sucursal", "incidencias.sucursal_id", "=", "sucursal.id")
                ->select('sucursal.nombre', 'sucursal.id', 'incidencias.id as incidencia_id');
        }
        $i= $query->get();

        if($request->mostrar == "suc-dep"){
            $i = $this->muestraDepto($i);
        } */

        foreach($niveles as $nivel){
            $n = [];
            foreach($i as $incidencia){
                if($incidencia->id == $nivel->id){
                    array_push($n,$incidencia);
                }
            }
            if(count($n) > 0){
                array_push($final, $n);
            }
        }
        return $final;
    }

    public function muestraDepto($datos){
        $deptos = array();
        foreach($datos as $depto){
            $r= DB::table('incidencias')->where("incidencias.id",'=', $depto->incidencia_id)
                    ->join("departamento", "incidencias.departamento_id", "=", "departamento.id")
                    ->select('departamento.nombre', 'departamento.id', 'incidencias.id as incidencia_id')->get();
                    //dd($r[0]);
            array_push($deptos,$r[0]);
        }
        // dd($deptos);
       return $deptos;
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IIncidenciaQuery;
use App\DataAccess\Repositories\Interfaces\IIncidenciaRepository;
use App\Domain\Incidencia;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Incidencias\FindIncidenciasRequest;
use App\Services\DTO\Incidencias\IncidenciaRequest;
use App\Services\DTO\Incidencias\IncidenciaResponse;
use App\Services\Interfaces\IIncidenciaService;
use App\Services\Mapper\Interfaces\IIncidenciaMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use DateTime;

class IncidenciaService implements IIncidenciaService
{   protected $unitOfWork;
    protected $incidenciaRepository;
    protected $incidenciaQuery;
    protected $incidenciaMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IIncidenciaRepository $incidenciaRepository,
        IIncidenciaQuery $incidenciaQuery,
        IIncidenciaMapper $incidenciaMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->incidenciaRepository    = $incidenciaRepository;
        $this->incidenciaQuery         = $incidenciaQuery;
        $this->incidenciaMapper        = $incidenciaMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request){
        $findIncidenciaRequest = new FindIncidenciasRequest($request->all());

        //Query
        $this->incidenciaQuery->init();
        $this->incidenciaQuery->withTipoId($findIncidenciaRequest->getTipoId());
        $this->incidenciaQuery->includeTipo($findIncidenciaRequest->isIncludeTipo());
        $this->incidenciaQuery->withFechaInicio($findIncidenciaRequest->getFechaInicio());
        $this->incidenciaQuery->withMinFechaInicio($findIncidenciaRequest->getMinFechaInicio());
        $this->incidenciaQuery->withMaxFechaInicio($findIncidenciaRequest->getMaxFechaInicio());
        $this->incidenciaQuery->withFechaFinal($findIncidenciaRequest->getFechaFinal());
        $this->incidenciaQuery->withMinFechaFinal($findIncidenciaRequest->getMinFechaFinal());
        $this->incidenciaQuery->withMaxFechaFinal($findIncidenciaRequest->getMaxFechaFinal());
        $this->incidenciaQuery->withEmpleadoId($findIncidenciaRequest->getEmpleadoId());
        $this->incidenciaQuery->includeEmpleado($findIncidenciaRequest->isIncludeEmpleado());
        $this->incidenciaQuery->withActivo($findIncidenciaRequest->getActivo());
        $this->incidenciaQuery->includeEmpresa($findIncidenciaRequest->isIncludeEmpresa());
        $this->incidenciaQuery->withEmpresaId($findIncidenciaRequest->getEmpresaId());
        $this->incidenciaQuery->includeSucursal($findIncidenciaRequest->isIncludeSucursal());
        $this->incidenciaQuery->withSucursalId($findIncidenciaRequest->getSucursalId());
        $this->incidenciaQuery->includeCliente($findIncidenciaRequest->isIncludeCliente());
        $this->incidenciaQuery->withClienteId($findIncidenciaRequest->getClienteId());
        $this->incidenciaQuery->includeDepartamento($findIncidenciaRequest->isIncludeDepartamento());
        $this->incidenciaQuery->withDepartamentoId($findIncidenciaRequest->getDepartamentoId());
        $this->incidenciaQuery->withEstatusId($findIncidenciaRequest->getEstatusId());
        $this->incidenciaQuery->includeEstatus($findIncidenciaRequest->isIncludeEstatus());
        $this->incidenciaQuery->withEstatusValor($findIncidenciaRequest->getEstatusValor());

        //Orden
        $this->incidenciaQuery->sort($findIncidenciaRequest->getSortBy(), $findIncidenciaRequest->getSort());
        //Total registros
        $totalCount = $this->incidenciaQuery->totalCount();

        //Paginacion
        $this->incidenciaQuery->paginate(
            $findIncidenciaRequest->getPaginate(),
            $findIncidenciaRequest->getItemsToShow(),
            $findIncidenciaRequest->getPage()
        );

        //Ejecutar query
        $incidencias = $this->incidenciaQuery->execute();

        //Mapear query
        $incidenciasResponse = $this->incidenciaMapper->getMapper()->mapMultiple($incidencias, IncidenciaResponse::class);

        //Find data response
        $findIncidenciasDataResponse = new FindDataResponse();
        $findIncidenciasDataResponse->setResults($incidenciasResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findIncidenciaRequest->getPage());
        $paginationResponse->setPageCount(count($incidenciasResponse));
        $paginationResponse->setPageSize($findIncidenciaRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findIncidenciasResponse = new  FindResponse();
        $findIncidenciasResponse->setData($findIncidenciasDataResponse);
        $findIncidenciasResponse->setPagination($paginationResponse);

        return response()->json($findIncidenciasResponse);
    }

    public function get($id){
        /* @var $incidencia Incidencia*/
        $incidencia = $this->incidenciaRepository->get($id);
        ProxyExtensions::load($incidencia->getEmpleado());
        ProxyExtensions::load($incidencia->getEstatus());
        $incidenciaResponse = $this->incidenciaMapper->getMapper()->map($incidencia, IncidenciaResponse::class);
        return response()->json($incidenciaResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request){
        /* @var $incidencia Incidencia*/
        $incidenciaRequest = new IncidenciaRequest($request->all());
        $this->SendMail($incidenciaRequest);
        $incidencia = $this->incidenciaMapper->getMapper()
            ->map($incidenciaRequest, Incidencia::class);
        $incidencia->setActivo(true);
        $this->incidenciaRepository->add($incidencia);
        $createResponse = new CreateResponse();
        $createResponse->setId($incidencia->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request){
        $success = false;
        try{
            /* @var $incidencia Incidencia*/
            $incidenciaRequest = new IncidenciaRequest($request->all());
            $incidencia = $this->incidenciaRepository->get($incidenciaRequest->getId());
            $this->incidenciaMapper->getMapper()->mapToObject($incidenciaRequest, $incidencia);
            $this->incidenciaRepository->update($incidencia);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    public function tiposIncidencia($request)
    {
        $tipos = DB::table('tipos_incidencias')
                        ->where('activo', '=', true)->get();
        $numero_incidencias = $this->getTipoIncidencias($tipos, $request);
        return $numero_incidencias;
    }

    private function getTipoIncidencias($datos, $request){
        $array = array();

        foreach ($datos as $incidencias){
            $query = DB::table('incidencias')->where('tipo_id', "=", $incidencias->id);
        
            if($request->rol_name == "admin-empresa"){
                $query->where("incidencias.empresa_id","=", $request->empresa_id);
            }
            elseif ($request->rol_name == "admin-sucursal"){
                $query->where("incidencias.sucursal_id","=", $request->sucursal_id);
            }
            elseif ($request->rol_name == "admin-depto" || $request->rol_name == "empleado"){
                $query->where("incidencias.departamento_id","=", $request->departamento_id);
            }
            elseif ($request->rol_name == "admin"){
                $query->where("incidencias.cliente_id","=", $request->cliente_id);
            }
            else{
                return [];
            }
            
            $query->where('incidencias.fecha_inicio','>=', $request->min_fecha);
            $query->where('incidencias.fecha_inicio','<=', $request->max_fecha);
            $query->where('incidencias.activo', '=', true);
            $query->where('estatus_incidencias.valor','=', "estatus_aprobado");
            $query->join("estatus_incidencias", "incidencias.estatus_id", "=", "estatus_incidencias.id");
            $query->select("incidencias.*");
            $total = $query->count();
           
            $incidencia=array("nombre"=> $incidencias->nombre, "total"=> $total, "tipo_id" => $incidencias->id );
            array_push($array, $incidencia);
        }
        return $array;
    }

    /**
     * @param Request $request
     */
    public function delete($id){
        $success = false;

        /* @var $incidencia Incidencia*/

        //Eliminado logico
        try{
            $incidencia = $this->incidenciaRepository->get($id);
            $incidencia->setActivo(false);
            $this->incidenciaRepository->update($incidencia);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function documento($request){
        $response = false;
        $path = storage_path()."/incidencias/C_".$request->cliente_id."_E_".$request->empresa_id."_S_".$request->sucursal_id."_EMPL_".$request->empleado_id;
        $response = array(
            "nombre" => "",
            "extension" => "",
            "url" => "",
            "success" => false,
            "message" => ""
        );
        if(!File::isDirectory($path)){
            if(!File::makeDirectory($path, $mode = 0777, true, true)){

            }
        }
        if($request->update){
            $anterior = storage_path()."/incidencias/".$request->url;
            File::delete($anterior);
        }
        if($request->hasFile('archivo')){
            $archivo = $request->archivo;
            $extension = $archivo->getClientOriginalExtension();
            $nombre_archivo = "incidencia_".time().".".$extension;
            $url = "C_".$request->cliente_id."_E_".$request->empresa_id."_S_".$request->sucursal_id."_EMPL_".$request->empleado_id."/".$nombre_archivo;

            if($archivo->move($path,$nombre_archivo)){
                $response = array(
                    "nombre" => $nombre_archivo,
                    "extension" => $extension,
                    "url" => $url,
                    "success" => true,
                    "message" => "Ok"
                );
            }
        }
        else{
            $response["message"] = "No es un archivo valido";
        }
        return response()->json($response);
    }

    private function SendMail($datos){
        $departamento = DB::table('departamento')->where('id', $datos->departamento_id)->get();
        $configuraciones = DB::table('configuraciones')->where('sucursal_id', $datos->sucursal_id)->get();
        $empleado = DB::table('empleados')->where('id', $datos->empleado_id)->get();
        $tipo_incidencia = DB::table("tipos_incidencias")->where('id', $datos->tipo_id)->get();
        $incidencia = $tipo_incidencia[0]->nombre;
        $nombre_empleado = $empleado[0]->nombre." ".$empleado[0]->apellido_paterno;
        $emisor = $departamento[0]->correo;
        $destinatario = null;
        $contenido = null;
        $asunto = "Registro de incidencia del empleado ".$nombre_empleado;

        foreach ($configuraciones as $clave => $valor){
            /* if($valor->clave == 'correo_usado_para_envio'){
                $destinatario = $valor->valor;
            } */
            if($valor->clave == 'correo_incidencia'){
                $contenido = $valor->valor;
            }
        }

        $contenido = str_replace('{{empleado}}', $nombre_empleado, $contenido);
        $contenido = str_replace('{{tipo_incidencia}}', $incidencia, $contenido);
        $data = array(
            'contenido' => $contenido,
            'titulo' => $asunto
        );
        try{
            if($emisor != null){
                Mail::send('email', $data, function ($message) use ($emisor, $asunto){
                    $message->from("sistema.gdth@gmail.com.mx", 'Reporte de Incidencias.');
                    $message->to($emisor)->subject($asunto);
                });
            }

        }catch (Exception $exception){
            throw $exception;
        }
    }
}

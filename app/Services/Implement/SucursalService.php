<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ISucursalQuery;
use App\DataAccess\Repositories\Interfaces\ISucursalRepository;
use App\Domain\Configuracion;
use App\Domain\Sucursal;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Sucursales\FindSucursalRequest;
use App\Services\DTO\Sucursales\SucursalRequest;
use App\Services\DTO\Sucursales\SucursalResponse;
use App\Services\Interfaces\ISucursalService;
use App\Services\Mapper\Interfaces\ISucursalMapper;
use function Functional\true;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use DateTime;
use Illuminate\Support\Facades\DB;

class SucursalService implements ISucursalService
{
    protected $unitOfWork;
    protected $sucursalRepository;
    protected $sucursalQuery;
    protected $sucursalMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ISucursalRepository $sucursalRepository,
        ISucursalQuery $sucursalQuery,
        ISucursalMapper $sucursalMapper)
    {
        $this->unitOfWork           = $unitOfWork;
        $this->sucursalRepository   = $sucursalRepository;
        $this->sucursalQuery        = $sucursalQuery;
        $this->sucursalMapper       = $sucursalMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findSucursalRequest = new FindSucursalRequest($request->all());

        //Query
        $this->sucursalQuery->init();
        $this->sucursalQuery->withNombre($findSucursalRequest->getNombre());
        $this->sucursalQuery->withDireccion($findSucursalRequest->getDireccion());
        $this->sucursalQuery->withCodigoPostal($findSucursalRequest->getCodigoPostal());
        $this->sucursalQuery->withDescripcion($findSucursalRequest->getDescripcion());
        $this->sucursalQuery->withActivo($findSucursalRequest->getActivo());
        $this->sucursalQuery->withEstadoId($findSucursalRequest->getEstadoId());
        $this->sucursalQuery->includeEstado($findSucursalRequest->isIncludeEstado());
        $this->sucursalQuery->withCiudadId($findSucursalRequest->getCiudadId());
        $this->sucursalQuery->includeCiudad($findSucursalRequest->isIncludeCiudad());
        $this->sucursalQuery->withEmpresaId($findSucursalRequest->getEmpresaId());
        $this->sucursalQuery->includeEmpresa($findSucursalRequest->isIncludeEmpresa());
        $this->sucursalQuery->withDiaFestivoId($findSucursalRequest->getDiaFestivoId());
        $this->sucursalQuery->includeDiaFestivo($findSucursalRequest->isIncludeDiaFestivo());
        $this->sucursalQuery->withClienteId($findSucursalRequest->getClienteId());
        $this->sucursalQuery->includeCliente($findSucursalRequest->isIncludeCliente());


        //Orden
        $this->sucursalQuery->sort($findSucursalRequest->getSortBy(), $findSucursalRequest->getSort());

        //Total registros
        $totalCount = $this->sucursalQuery->totalCount();

        //Paginacion
        $this->sucursalQuery->paginate(
            $findSucursalRequest->getPaginate(),
            $findSucursalRequest->getItemsToShow(),
            $findSucursalRequest->getPage()
        );

        //Ejecutar query
        $sucursales = $this->sucursalQuery->execute();

        //Mapear query
        $sucursalResponse = $this->sucursalMapper->getMapper()
            ->mapMultiple($sucursales, SucursalResponse::class);

        //Find data response
        $findDataSucursal = new FindDataResponse();
        $findDataSucursal->setResults($sucursalResponse);

        //Pagination response
        $paginateResponse = new PaginationResponse();
        $paginateResponse->setPage($findSucursalRequest->getPage());
        $paginateResponse->setPageCount(count($sucursalResponse));
        $paginateResponse->setPageSize($findSucursalRequest->getItemsToShow());
        $paginateResponse->setTotalCount($totalCount);

        //Fin response
        $findSucursalResponse = new FindResponse();
        $findSucursalResponse->setData($findDataSucursal);
        $findSucursalResponse->setPagination($paginateResponse);

        return response()->json($findSucursalResponse);

    }

    public function get($id)
    {
        /* @var $sucursal Sucursal */
        $sucursal = $this->sucursalRepository->get($id);
        ProxyExtensions::load($sucursal->getEmpresa());
        $sucursalResponse = $this->sucursalMapper->getMapper()->map($sucursal, SucursalResponse::class);
        return response()->json($sucursalResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $sucursal Sucursal */
        $sucursalRequest = new SucursalRequest($request->all());
        $sucursal = $this->sucursalMapper->getMapper()->map($sucursalRequest, Sucursal::class);
        $sucursal->setActivo(true);
        $this->sucursalRepository->add($sucursal);
        $createResponse = new CreateResponse();
        $createResponse->setId($sucursal->getId());
        $this->configuraciones($createResponse, $request->empresa_id);
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $sucursal Sucursal */
        $success = false;

        try{
            $sucursalRequest = new SucursalRequest($request->all());
            $sucursal = $this->sucursalRepository->get($sucursalRequest->getId());
            $this->sucursalMapper->getMapper()->mapToObject($sucursalRequest, $sucursal);
            $this->sucursalRepository->update($sucursal);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);

        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        /* @var $sucursal Sucursal */
        $success = false;

        try{
            $sucursal = $this->sucursalRepository->get($id);
            $sucursal->setActivo(false);
            $this->sucursalRepository->update($sucursal);
            $success = true;
        } catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);

        return response()->json($isSuccess);
    }

    private function configuraciones($sucursal_id, $empresa_id){
        $string = file_get_contents(__DIR__."/datos/configuraciones-base.json");
        $json_a = json_decode($string, true);

        foreach ($json_a as $configuraciones => $config) {
            try{
                $configuracion = DB::table("configuraciones")->insert([
                    'nombre' => $config['nombre'],
                    'clave' => $config['clave'],
                    'valor' => $config['valor'],
                    'descripcion' => $config['descripcion'],
                    'empresa_id' => (int)$empresa_id,
                    'sucursal_id' => (int)$sucursal_id->id,
                    'created_date' => new DateTime('now'),
                    'activo' => $config['activo']
                ]);
                /* if ($configuraciones == "dias_de_vacaciones") {
                    $anos= $config['anos'];
                    foreach ($anos as $vacaciones) {
                    }
                } */
            }
            catch(Exception $exception){
                throw $exception;
            }
        }
    }
}
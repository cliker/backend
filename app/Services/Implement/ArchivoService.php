<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\IDocumentoRepository;
use App\Domain\Documento;
use App\Services\DTO\Archivos\ArchivoRequest;
use App\Services\DTO\Archivos\ArchivoResponse;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\Interfaces\IArchivoService;
use function Functional\concat;
use function Functional\true;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class ArchivoService implements IArchivoService
{
    protected $unitOfWork;
    protected $documentoRepository;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IDocumentoRepository $documentoRepository
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->documentoRepository = $documentoRepository;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        //Query


        //Orden

        //Total registros

        //Paginacion

        //Ejecutar query

        //Mapear query

        //Find data response

        //Pagination response

        //Fin response


        //return response()->json($findJobsResponse);

    }

    public function get($id)
    {
        //return response()->json($jobResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function documentoEmpleadoStore($request)
    {
        $documentoRequest = new ArchivoRequest($request->all());
        $nameFolder = "C".$documentoRequest->getClienteId()."-E".$documentoRequest->getEmpresaId()."-S".$documentoRequest->getSucursalId()."-D".$documentoRequest->getDepartamentoId()."-EM".$documentoRequest->getEmpleadoId();
        $path = storage_path().'/documentos/' .$nameFolder;
        $responseUpload = array(
            "nombre" => "",
            "extension" => "",
            "url" => "",
            "success" => false,
            "message" => ''

        );
        if($this->createFolderEmpleado($path)){
            /* @var $archivo UploadedFile*/
            if($request->hasFile('archivo')){
                $archivo = $documentoRequest->getArchivo();
                $responseUpload = $this->uploadArchivo($archivo, $nameFolder, $path);
            }else{
                $responseUpload['message'] = 'No es un archivo';
            }
        }
        $archivoResponse = new ArchivoResponse($responseUpload);
        return response()->json($archivoResponse);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function documentoEmpleadoUpdate($request)
    {
        $documentoRequest = new ArchivoRequest($request->all());
        /* @var $documentoObj Documento*/
        $documentoObj = $this->documentoRepository->get($documentoRequest->getDocumentoId());
        $archivoOriginal = storage_path().$documentoObj->getUrl();
        File::delete($archivoOriginal);
        $nameFolder = "C".$documentoRequest->getClienteId()."-E".$documentoRequest->getEmpresaId()."-S".$documentoRequest->getSucursalId()."-D".$documentoRequest->getDepartamentoId()."-EM".$documentoRequest->getEmpleadoId();
        $path = storage_path().'/documentos/' .$nameFolder;
        $responseUpload = array(
            "nombre" => "",
            "extension" => "",
            "url" => "",
            "success" => false,
            "message" => ''
        );
        /* @var $archivo UploadedFile*/
        if($request->hasFile('archivo')){
            $archivo = $documentoRequest->getArchivo();
            $responseUpload = $this->uploadArchivo($archivo, $nameFolder, $path);
        }else{
            $responseUpload['message'] = 'No es un archivo';
        }
        $archivoResponse = new ArchivoResponse($responseUpload);
        return response()->json($archivoResponse);
    }

    public function documentoEmpleadoDelete($id)
    {

        $success = false;
        try{
            /* @var $documentoObj Documento*/
            $documentoObj = $this->documentoRepository->get($id);
            $archivoOriginal = storage_path().$documentoObj->getUrl();
            File::delete($archivoOriginal);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @return bool
     */
    private function createFolderEmpleado($path){
        $response = true;
        if(!File::isDirectory($path)){
            if(!File::makeDirectory($path, $mode = 0777, true, true)){
                $response = false;
            }
        }
        return $response;
    }

    private function uploadArchivo($archivo, $directorioDestino, $pathDirectorio){
        $response = array(
            "nombre" => "",
            "extension" => "",
            "url" => "",
            "success" => false
        );
        /* @var $archivo UploadedFile*/
        $extension = $archivo->getClientOriginalExtension();
        $phpName = $archivo->getFilename();
        $nombreArchivo = time().$phpName.'docEmp.'.$extension;
        $url = '/documentos/' .$directorioDestino.'/'.$nombreArchivo;
        if($archivo->move($pathDirectorio, $nombreArchivo)){
            $response = array(
                "nombre" => $nombreArchivo,
                "extension" => $extension,
                "url" => $url,
                "success" => true
            );

        }
        return $response;
    }
}

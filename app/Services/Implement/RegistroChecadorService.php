<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IRegistroChecadorQuery;
use App\DataAccess\Repositories\Interfaces\IEmpleadoRepository;
use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\DataAccess\Repositories\Interfaces\ISucursalRepository;
use App\DataAccess\Repositories\Interfaces\IDepartamentoRepository;
use App\DataAccess\Repositories\Interfaces\IRegistroChecadorRepository;
use App\Domain\RegistroChecador;
use App\Infrastructure\ArrayExtensions;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\RegistrosChecadores\FindRegistrosChecadores;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorAspRequest;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorRequest;
use App\Services\DTO\RegistrosChecadores\RegistroChecadorResponse;
use App\Services\Interfaces\IRegistroChecadorService;
use App\Services\Mapper\Interfaces\IRegistroChecadorMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class RegistroChecadorService implements IRegistroChecadorService
{
    protected $unitOfWork;
    protected $registroChecadorRepository;
    protected $registroChecadorQuery;
    protected $registroChecadorMapper;
    protected $clienteRepository;
    protected $empresaRepository;
    protected $sucursalRepository;
    protected $departamentoRepository;
    protected $empleadoRepository;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IRegistroChecadorRepository $registroChecadorRepository,
        IRegistroChecadorQuery $registroChecadorQuery,
        IRegistroChecadorMapper $registroChecadorMapper,
        IClienteRepository $clienteRepository,
        IEmpresaRepository $empresaRepository,
        ISucursalRepository $sucursalRepository,
        IDepartamentoRepository $departamentoRepository,
        IEmpleadoRepository $empleadoRepository
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->registroChecadorRepository    = $registroChecadorRepository;
        $this->registroChecadorQuery         = $registroChecadorQuery;
        $this->registroChecadorMapper        = $registroChecadorMapper;
        $this->clienteRepository = $clienteRepository;
        $this->empresaRepository = $empresaRepository;
        $this->sucursalRepository = $sucursalRepository;
        $this->departamentoRepository = $departamentoRepository;
        $this->empleadoRepository = $empleadoRepository;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findRegistroChecadorRequest = new FindRegistrosChecadores($request->all());
        //Query
        $this->registroChecadorQuery->init();
        $this->registroChecadorQuery->withFecha($findRegistroChecadorRequest->getFecha());
        $this->registroChecadorQuery->withMinFecha($findRegistroChecadorRequest->getMinFecha());
        $this->registroChecadorQuery->withMaxFecha($findRegistroChecadorRequest->getMaxFecha());
        $this->registroChecadorQuery->withHora($findRegistroChecadorRequest->getHora());
        $this->registroChecadorQuery->withMinHora($findRegistroChecadorRequest->getMinHora());
        $this->registroChecadorQuery->withMaxHora($findRegistroChecadorRequest->getMaxHora());
        $this->registroChecadorQuery->withEmpleadoId($findRegistroChecadorRequest->getEmpleadoId());
        $this->registroChecadorQuery->includeEmpleado($findRegistroChecadorRequest->isIncludeEmpleado());
        $this->registroChecadorQuery->withRelojChecadorId($findRegistroChecadorRequest->getRelojChecadorId());
        $this->registroChecadorQuery->includeRelojChecador($findRegistroChecadorRequest->isIncludeRelojChecador());
        $this->registroChecadorQuery->withClienteId($findRegistroChecadorRequest->getClienteId());
        $this->registroChecadorQuery->includeCliente($findRegistroChecadorRequest->isIncludeCliente());
        $this->registroChecadorQuery->withEmpresaId($findRegistroChecadorRequest->getEmpresaId());
        $this->registroChecadorQuery->includeEmpresa($findRegistroChecadorRequest->isIncludeEmpresa());
        $this->registroChecadorQuery->withSucursalId($findRegistroChecadorRequest->getSucursalId());
        $this->registroChecadorQuery->includeSucursal($findRegistroChecadorRequest->isIncludeSucursal());
        $this->registroChecadorQuery->withDepartamentoId($findRegistroChecadorRequest->getDepartamentoId());
        $this->registroChecadorQuery->includeDepartamento($findRegistroChecadorRequest->isIncludeDepartamento());
        $this->registroChecadorQuery->withActivo($findRegistroChecadorRequest->getActivo());


        //Orden

        $this->registroChecadorQuery->sort($findRegistroChecadorRequest->getSortBy(), $findRegistroChecadorRequest->getSort());
        //Total registros
        $totalCount = $this->registroChecadorQuery->totalCount();

        //Paginacion
        $this->registroChecadorQuery->paginate(
            $findRegistroChecadorRequest->getPaginate(),
            $findRegistroChecadorRequest->getItemsToShow(),
            $findRegistroChecadorRequest->getPage()
        );

        //Ejecutar query
        $registrosChecadores = $this->registroChecadorQuery->execute();

        /*dd($registrosChecadores);*/

        //Mapear query
        $registrosChecadoresResponse = $this->registroChecadorMapper->getMapper()->mapMultiple($registrosChecadores, RegistroChecadorResponse::class);

        //Find data response
        $findRegistrosChecadoresDataResponse = new FindDataResponse();
        $findRegistrosChecadoresDataResponse->setResults($registrosChecadoresResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findRegistroChecadorRequest->getPage());
        $paginationResponse->setPageCount(count($registrosChecadoresResponse));
        $paginationResponse->setPageSize($findRegistroChecadorRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findRegistrosChecadoresResponse = new  FindResponse();
        $findRegistrosChecadoresResponse->setData($findRegistrosChecadoresDataResponse);
        $findRegistrosChecadoresResponse->setPagination($paginationResponse);

        return response()->json($findRegistrosChecadoresResponse);

    }

    public function get($id)
    {
        /* @var $registroChecador RegistroChecador*/
        $registroChecador = $this->registroChecadorRepository->get($id);
        ProxyExtensions::load($registroChecador->getEmpleado());
        ProxyExtensions::load($registroChecador->getEmpresa());
        $registroChecadorResponse = $this->registroChecadorMapper->getMapper()->map($registroChecador, RegistroChecadorResponse::class);
        return response()->json($registroChecadorResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        if(!ArrayExtensions::isMultidimensional($request->all())) {
            /* @var $registroChecador RegistroChecador*/
            $registroChecadorRequest = new RegistroChecadorRequest($request->all());
            $registroChecador = $this->registroChecadorMapper->getMapper()
                ->map($registroChecadorRequest, RegistroChecador::class);
            $registroChecador->setActivo(true);
            $this->registroChecadorRepository->add($registroChecador);
            $createResponse = new CreateResponse();
            $createResponse->setId($registroChecador->getId());
            return response()->json($createResponse);
        }else{
            $registrosChecador = array();
            $registrosChecadorRequest = $request->all();
            foreach ($registrosChecadorRequest as $registroChecadorRequestObj) {
                $registroChecadorRequest = new RegistroChecadorRequest($registroChecadorRequestObj);
                $registroChecador = $this->registroChecadorMapper->getMapper()
                    ->map($registroChecadorRequest, RegistroChecador::class);
                $registroChecador->setActivo(true);
                $this->registroChecadorRepository->add($registroChecador);
                $createResponse = new CreateResponse();
                $createResponse->setId($registroChecador->getId());
                array_push($registrosChecador, $createResponse);
            }
            return response()->json($registrosChecador);
        }
    }

    /**
     * @param Request $request
     */
    public function createFromAsp($request, $tipo){
        $data = $request->all();
        $sucursal = null;
        $departamento = null;
        $cliente = null;
        $empresa = null;
        
        if(ArrayExtensions::isMultidimensional($data['registros'])) {
            //Obtengo la empresa
            $empresa = $this->empresaRepository->get($data['empresa_id']);
            $cliente_id = $empresa->getCliente()->getId();
            if($cliente_id != null){
                $cliente = $this->clienteRepository->get($cliente_id);
            }
            
            $registrosChecadorRequest = $data['registros'];
            $registrosChecador = array();

            foreach ($registrosChecadorRequest as $registroChecadorRequestObj) {
                if($tipo === "automatico"){
                    $registroChecadorRequestObj = (array) $registroChecadorRequestObj;
                }

                $registroChecadorRequest = new RegistroChecadorAspRequest($registroChecadorRequestObj);
                //Obtengo el empleado
                $empleado = $this->empleadoRepository->getByEmpresaClave($data['empresa_id'], $registroChecadorRequest->getClaveUsuario());
                if($empleado != null){
                    $sucursal_id = $empleado->getSucursal()->getId();
                    $sucursal = $this->sucursalRepository->get($sucursal_id);
                    $departamento_id = $empleado->getDepartamento()->getId();
                    $departamento = $this->departamentoRepository->get($departamento_id);

                    //Divido fecha y hora
                    $dateTime = explode("T", $registroChecadorRequest->getRegistro());
                    $fecha = $dateTime[0];
                    $hora = $dateTime[1];
                    $registroChecador = new RegistroChecador();
                    $registroChecador->setIdAsp($registroChecadorRequest->getId());
                    $registroChecador->setFecha(new \DateTime($fecha));
                    $registroChecador->setHora(new \DateTime($hora));
                    $registroChecador->setEmpleado($empleado);
                    $registroChecador->setCliente($cliente);
                    $registroChecador->setEmpresa($empresa);
                    $registroChecador->setSucursal($sucursal);
                    $registroChecador->setDepartamento($departamento);
                    $registroChecador->setActivo(true);
                    $this->registroChecadorRepository->add($registroChecador);
                    $createResponse = new CreateResponse();
                    $createResponse->setId($registroChecador->getId());
                    array_push($registrosChecador, $createResponse);
                }
                
            }
            return response()->json($registrosChecador);
        }
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $registroChecador RegistroChecador*/
        if (!ArrayExtensions::isMultidimensional($request->all())) {
            $success = false;
            try{
                $registroChecadorRequest = new RegistroChecadorRequest($request->all());
                $registroChecador = $this->registroChecadorRepository->get($registroChecadorRequest->getId());
                $this->registroChecadorMapper->getMapper()->mapToObject($registroChecadorRequest, $registroChecador);
                $this->registroChecadorRepository->update($registroChecador);
                $success = true;

            }catch (Exception $exception){
                throw $exception;
            }
            $isSuccess = new SuccessResponse();
            $isSuccess->setIsSuccess($success);
            return response()->json($isSuccess);
        }else{
            $registrosChecadorRequest = $request->all();
            $success = false;
            try{
                foreach ($registrosChecadorRequest as $registroChecadorRequestObj) {
                    $success = false;
                    $registroChecadorRequest = new RegistroChecadorRequest($registroChecadorRequestObj);
                    $registroChecador = $this->registroChecadorRepository->get($registroChecadorRequest->getId());
                    $this->registroChecadorMapper->getMapper()->mapToObject($registroChecadorRequest, $registroChecador);
                    $this->registroChecadorRepository->update($registroChecador);
                    $success = true;
                }
            }catch (Exception $exception){
                throw $exception;
            }

            $isSuccess = new SuccessResponse();
            $isSuccess->setIsSuccess($success);
            return response()->json($isSuccess);
        }
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;
        /* @var $registroChecador RegistroChecador*/
        try{
            $registroChecador = $this->registroChecadorRepository->get($id);
            $registroChecador->setActivo(false);
            $this->registroChecadorRepository->update($registroChecador);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    public function getLastIdAsp($empresa_id)
    {
        $lastId = $this->registroChecadorRepository->getLastIdAsp($empresa_id);
        $array = array(
            'last_id' => $lastId
        );
        return response()->json($array);
    }

    public function updateChecadas(){
        $empresas = DB::table('empresa')->where("activo", "=", true)->get();

        foreach ($empresas as $empresa){
            $lastId = $this->registroChecadorRepository->getLastIdAsp($empresa->id);
            $url = $empresa->url_server_asp;
            
            try{
                $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
                /* $param = array( 'usuario' => 'admin','password' => 'Chec4dor$2019.');
                $login = $cliente->request('POST', 'login/login', array('form_params' => $param ));
                $token_asp = json_decode($login->getBody()->getContents()); */

                $get_registros = $cliente->request('GET', 'registros?id='.$lastId);
                $registros = json_decode($get_registros->getBody()->getContents());
                $datos = ['empresa_id' => $empresa->id, 'registros' => $registros];
                $request = new Request($datos);
                $this->createFromAsp($request, "automatico");
            }
            catch(Exception $ex){
               
            }
        }
    }

    public function historico($id){
        $lastId = $this->registroChecadorRepository->getLastIdAsp($id);
        $empresa = DB::table('empresa')->where("id", "=", $id)->get();
        $url = $empresa[0]->url_server_asp;

        try{
            $cliente = new Client(['base_uri' => $url.'/Checador/api/' ]);
            $get_registros = $cliente->request('GET', 'registros?id='.$lastId);
            $registros = json_decode($get_registros->getBody()->getContents());
            $datos = ['empresa_id' => $id, 'registros' => $registros];
            $request = new Request($datos);
            return $this->createFromAsp($request, "automatico");
        }
        catch(Exception $ex){
            return response()->json("No se pudo conectar al servidor.");;
        }
    }
}

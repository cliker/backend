<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IDetalleHorarioQuery;
use App\DataAccess\Repositories\Interfaces\IDetalleHorarioRepository;
use App\Domain\DetalleHorario;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\DetallesHorarios\DetalleHorarioRequest;
use App\Services\DTO\DetallesHorarios\DetalleHorarioResponse;
use App\Services\DTO\DetallesHorarios\FindDetallesHorariosRequest;
use App\Services\Interfaces\IDetalleHorarioService;
use App\Services\Mapper\Interfaces\IDetalleHorarioMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class DetalleHorarioService implements IDetalleHorarioService
{
    protected $unitOfWork;
    protected $detalleHorarioRepository;
    protected $detalleHorarioQuery;
    protected $detalleHorarioMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IDetalleHorarioRepository $detalleHorarioRepository,
        IDetalleHorarioQuery $detalleHorarioQuery,
        IDetalleHorarioMapper $detalleHorarioMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->detalleHorarioRepository    = $detalleHorarioRepository;
        $this->detalleHorarioQuery         = $detalleHorarioQuery;
        $this->detalleHorarioMapper        = $detalleHorarioMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findDetalleHorarioRequest = new FindDetallesHorariosRequest($request->all());

        //Query
        $this->detalleHorarioQuery->init();
        $this->detalleHorarioQuery->withDia($findDetalleHorarioRequest->getDia());
        $this->detalleHorarioQuery->withHoraEntrada($findDetalleHorarioRequest->getHoraEntrada());
        $this->detalleHorarioQuery->withMinHoraEntrada($findDetalleHorarioRequest->getMinHoraEntrada());
        $this->detalleHorarioQuery->withMaxHoraEntrada($findDetalleHorarioRequest->getMaxHoraEntrada());
        $this->detalleHorarioQuery->withHoraSalida($findDetalleHorarioRequest->getHoraSalida());
        $this->detalleHorarioQuery->withMinHoraSalida($findDetalleHorarioRequest->getMinHoraSalida());
        $this->detalleHorarioQuery->withMaxHoraSalida($findDetalleHorarioRequest->getMaxHoraSalida());
        $this->detalleHorarioQuery->withHorarioId($findDetalleHorarioRequest->getHorarioId());
        $this->detalleHorarioQuery->includeHorario($findDetalleHorarioRequest->isIncludeHorario());
        $this->detalleHorarioQuery->withActivo($findDetalleHorarioRequest->getActivo());


        //Orden
        $this->detalleHorarioQuery->sort($findDetalleHorarioRequest->getSortBy(), $findDetalleHorarioRequest->getSort());
        //Total registros
        $totalCount = $this->detalleHorarioQuery->totalCount();

        //Paginacion
        $this->detalleHorarioQuery->paginate(
            $findDetalleHorarioRequest->getPaginate(),
            $findDetalleHorarioRequest->getItemsToShow(),
            $findDetalleHorarioRequest->getPage()
        );

        //Ejecutar query
        $detallesHorarios = $this->detalleHorarioQuery->execute();

        //Mapear query
        $detallesHorariosResponse = $this->detalleHorarioMapper->getMapper()->mapMultiple($detallesHorarios, DetalleHorarioResponse::class);

        //Find data response
        $findDetallesHorariosDataResponse = new FindDataResponse();
        $findDetallesHorariosDataResponse->setResults($detallesHorariosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findDetalleHorarioRequest->getPage());
        $paginationResponse->setPageCount(count($detallesHorariosResponse));
        $paginationResponse->setPageSize($findDetalleHorarioRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findDetallesHorariosResponse = new  FindResponse();
        $findDetallesHorariosResponse->setData($findDetallesHorariosDataResponse);
        $findDetallesHorariosResponse->setPagination($paginationResponse);

        return response()->json($findDetallesHorariosResponse);

    }

    public function get($id)
    {
        /* @var $detalleHorario DetalleHorario*/
        $detalleHorario = $this->detalleHorarioRepository->get($id);
        ProxyExtensions::load($detalleHorario->getHorario());
        $detalleHorarioResponse = $this->detalleHorarioMapper->getMapper()->map($detalleHorario, DetalleHorarioResponse::class);
        return response()->json($detalleHorarioResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $detalleHorario DetalleHorario*/
        $detalleHorarioRequest = new DetalleHorarioRequest($request->all());
        $detalleHorario = $this->detalleHorarioMapper->getMapper()
            ->map($detalleHorarioRequest, DetalleHorario::class);
        $detalleHorario->setActivo(true);
        $this->detalleHorarioRepository->add($detalleHorario);
        $createResponse = new CreateResponse();
        $createResponse->setId($detalleHorario->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $detalleHorario DetalleHorario*/
            $detalleHorarioRequest = new DetalleHorarioRequest($request->all());
            $detalleHorario = $this->detalleHorarioRepository->get($detalleHorarioRequest->getId());
            $this->detalleHorarioMapper->getMapper()->mapToObject($detalleHorarioRequest, $detalleHorario);
            $this->detalleHorarioRepository->update($detalleHorario);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $detalleHorario DetalleHorario*/

        //Eliminado logico
        try{
            $detalleHorario = $this->detalleHorarioRepository->get($id);
            $detalleHorario->setActivo(false);
            $this->detalleHorarioRepository->update($detalleHorario);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

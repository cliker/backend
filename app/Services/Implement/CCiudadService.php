<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICCiudadQuery;
use App\DataAccess\Repositories\Interfaces\ICCiudadRepository;
use App\DataAccess\Repositories\Interfaces\IEstadoRepository;
use App\Domain\CCiudad;
use App\Services\DTO\Base\CreateResponse;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\CCiudades\CCiudadRequest;
use App\Services\DTO\CCiudades\CCiudadResponse;
use App\Services\DTO\CCiudades\FindCCiudadesRequest;
use App\Services\Interfaces\ICCiudadService;
use App\Services\Mapper\Interfaces\ICCiudadMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;


class CCiudadService implements ICCiudadService
{
    protected $unitOfWork;
    protected $ciudadRepository;
    protected $ciudadQuery;
    protected $ciudadMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICCiudadRepository $ciudadRepository,
        ICCiudadQuery $ciudadQuery,
        ICCiudadMapper $ciudadMapper
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->ciudadRepository    = $ciudadRepository;
        $this->ciudadQuery         = $ciudadQuery;
        $this->ciudadMapper        = $ciudadMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findCiudadRequest = new FindCCiudadesRequest($request->all());

        //Query
        $this->ciudadQuery->init();
        $this->ciudadQuery->withNombre($findCiudadRequest->getNombre());
        $this->ciudadQuery->withActivo($findCiudadRequest->getActivo());
        $this->ciudadQuery->withEstadoId($findCiudadRequest->getEstadoId());
        $this->ciudadQuery->includeEstado($findCiudadRequest->isIncludeEstado());

        //Orden
        $this->ciudadQuery->sort($findCiudadRequest->getSortBy(), $findCiudadRequest->getSort());
        //Total registros

        $totalCount = $this->ciudadQuery->totalCount();

        //Paginacion
        $this->ciudadQuery->paginate(
            $findCiudadRequest->getPaginate(),
            $findCiudadRequest->getItemsToShow(),
            $findCiudadRequest->getPage()
        );

        //Ejecutar query
        $ciudades = $this->ciudadQuery->execute();

        //Mapear query
        $ciudadesResponse = $this->ciudadMapper->getMapper()->mapMultiple($ciudades, CCiudadResponse::class);

        //Find data response
        $findCiudadesDataResponse = new FindDataResponse();
        $findCiudadesDataResponse->setResults($ciudadesResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findCiudadRequest->getPage());
        $paginationResponse->setPageCount(count($ciudadesResponse));
        $paginationResponse->setPageSize($findCiudadRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findCiudadesResponse = new  FindResponse();
        $findCiudadesResponse->setData($findCiudadesDataResponse);
        $findCiudadesResponse->setPagination($paginationResponse);

        return response()->json($findCiudadesResponse);

    }

    public function get($id)
    {
        /* @var $ciudad CCiudad*/
        $ciudad = $this->ciudadRepository->get($id);
        $ciudadResponse = $this->ciudadMapper->getMapper()->map($ciudad, CCiudadResponse::class);
        return response()->json($ciudadResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $ciudad CCiudad*/
        $ciudadRequest = new CCiudadRequest($request->all());
        $ciudad = $this->ciudadMapper->getMapper()
            ->map($ciudadRequest, CCiudad::class);
        $ciudad->setActivo(true);
        $this->ciudadRepository->add($ciudad);
        $createResponse = new CreateResponse();
        $createResponse->setId($ciudad->getId());
        return response()->json($createResponse);

    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $ciudad CCiudad*/
            $ciudadRequest = new CCiudadRequest($request->all());
            $ciudad = $this->ciudadRepository->get($ciudadRequest->getId());
            $this->ciudadMapper->getMapper()->mapToObject($ciudadRequest, $ciudad);
            $this->ciudadRepository->update($ciudad);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $ciudad CCiudad*/

        //Eliminado logico
        try{
            $ciudad = $this->ciudadRepository->get($id);
            $ciudad->setActivo(false);
            $this->ciudadRepository->update($ciudad);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IDocumentoQuery;
use App\DataAccess\Repositories\Interfaces\IDocumentoRepository;
use App\Domain\Documento;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\Documentos\DocumentoRequest;
use App\Services\DTO\Documentos\DocumentoResponse;
use App\Services\DTO\Documentos\FindDocumentosRequest;
use App\Services\Interfaces\IDocumentoService;
use App\Services\Mapper\Interfaces\IDocumentoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class DocumentoService implements IDocumentoService
{
    protected $unitOfWork;
    protected $documentoRepository;
    protected $documentoQuery;
    protected $documentoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IDocumentoRepository $documentoRepository,
        IDocumentoQuery $documentoQuery,
        IDocumentoMapper $documentoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->documentoRepository    = $documentoRepository;
        $this->documentoQuery         = $documentoQuery;
        $this->documentoMapper        = $documentoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findDocumentoRequest = new FindDocumentosRequest($request->all());

        //Query
        $this->documentoQuery->init();
        $this->documentoQuery->withTipoId($findDocumentoRequest->getTipoId());
        $this->documentoQuery->includeTipo($findDocumentoRequest->isIncludeTipo());
        $this->documentoQuery->withEmpledoId($findDocumentoRequest->getEmpleadoId());
        $this->documentoQuery->includeEmpleado($findDocumentoRequest->isIncludeEmpleado());
        $this->documentoQuery->withActivo($findDocumentoRequest->getActivo());


        //Orden
        $this->documentoQuery->sort($findDocumentoRequest->getSortBy(), $findDocumentoRequest->getSort());
        //Total registros
        $totalCount = $this->documentoQuery->totalCount();

        //Paginacion
        $this->documentoQuery->paginate(
            $findDocumentoRequest->getPaginate(),
            $findDocumentoRequest->getItemsToShow(),
            $findDocumentoRequest->getPage()
        );

        //Ejecutar query
        $documentos = $this->documentoQuery->execute();

        //Mapear query
        $documentosResponse = $this->documentoMapper->getMapper()->mapMultiple($documentos, DocumentoResponse::class);

        //Find data response
        $findDocumentosDataResponse = new FindDataResponse();
        $findDocumentosDataResponse->setResults($documentosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findDocumentoRequest->getPage());
        $paginationResponse->setPageCount(count($documentosResponse));
        $paginationResponse->setPageSize($findDocumentoRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin response
        $findDocumentosResponse = new  FindResponse();
        $findDocumentosResponse->setData($findDocumentosDataResponse);
        $findDocumentosResponse->setPagination($paginationResponse);

        return response()->json($findDocumentosResponse);

    }

    public function get($id)
    {
        /* @var $documento Documento*/
        $documento = $this->documentoRepository->get($id);
        ProxyExtensions::load($documento->getTipo());
        ProxyExtensions::load($documento->getEmpleado());
        $documentoResponse = $this->documentoMapper->getMapper()->map($documento, DocumentoResponse::class);
        return response()->json($documentoResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $documento Documento*/
        $documentoRequest = new DocumentoRequest($request->all());
        $documento = $this->documentoMapper->getMapper()
            ->map($documentoRequest, Documento::class);
        $documento->setActivo(true);
        $this->documentoRepository->add($documento);
        $createResponse = new CreateResponse();
        $createResponse->setId($documento->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $documento Documento*/
            $documentoRequest = new DocumentoRequest($request->all());
            $documento = $this->documentoRepository->get($documentoRequest->getId());
            $this->documentoMapper->getMapper()->mapToObject($documentoRequest, $documento);
            $this->documentoRepository->update($documento);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $documento Documento*/

        //Eliminado logico
        try{
            $documento = $this->documentoRepository->get($id);
            $documento->setActivo(false);
            $this->documentoRepository->update($documento);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

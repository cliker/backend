<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\Interfaces\IExportarDatosService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;

use Maatwebsite\Excel\Facades\Excel;

class ExportarDatosService implements IExportarDatosService
{
    protected  $unitOfWork;
    public function __construct(IUnitOfWork $unitOfWork)
    {
        $this->unitOfWork = $unitOfWork;
    }


    public function exportData($nombreArchivo = 'Datos', $titulosEncabezado = array(), $datos = array(), $cellsBgColor = 'A1:N1', $numAutoFilter = 13, $arrayFormat = array()){
        $nombreArchivo = $nombreArchivo.'_'.date('d_m_y').'_'.date('G_i_s');

        Excel::create($nombreArchivo, function($excel) use ($datos, $nombreArchivo, $numAutoFilter, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
            $excel->setTitle($nombreArchivo);
            $excel->setCreator('PBM') ->setCompany('PBM');
            $excel->setDescription('Listado de '. $nombreArchivo);
            $excel->sheet('Hoja 1', function($sheet) use ($datos, $numAutoFilter, $cellsBgColor, $titulosEncabezado, $arrayFormat) {
                //Estilo de la hoja
                $sheet->setFontSize(16);
                $sheet->setColumnFormat($arrayFormat);
                $sheet->freezeFirstRow();
                $sheet->setAutoFilterByColumnAndRow($numAutoFilter);
                //Estilo de las celdas
                $sheet->cells($cellsBgColor, function($cells) {
                    $cells->setBackground('#B3EFFF');
                    //$cells->setColor('#000000');

                });
                //Encabezado
                $sheet->row(1, $titulosEncabezado);

                //LLenado de celdas
               foreach($datos as $index => $dato) {
                    $sheet->row($index+2, $dato);
                }
            });
        })->export('xlsx', ['Access-Control-Allow-Origin'=>'*']);
    }


    public function layoutEmpleado($nombreArchivo, $titulosEncabezado = array(), $datos = array(),$limite_filas, $arrayFormat = array(), $tipo, $rol, $arrayGeneral){
        
        Excel::create($nombreArchivo, function($excel) use ($datos, $nombreArchivo, $limite_filas, $titulosEncabezado, $arrayFormat, $tipo, $rol, $arrayGeneral) {
            $excel->setTitle($nombreArchivo);
            $excel->setCreator('PBM') ->setCompany('PBM');
            $excel->setDescription('Listado de '. $nombreArchivo);
            $excel->sheet('Hoja 1', function($sheet) use ( $datos, $limite_filas, $titulosEncabezado, $arrayFormat) {
                //Estilo de la hoja
                $sheet->setFontSize(16);
                $sheet->setColumnFormat($arrayFormat);
                $sheet->freezeFirstRow();

                //Estilo de las celdas
                $sheet->cells($limite_filas, function($cells) {
                    $cells->setBackground('#B3EFFF');
                });
                //Encabezado
                $sheet->row(1, $titulosEncabezado);

                //LLenado de celdas
                foreach($datos as $index => $dato) {
                    
                    $sheet->row($index+2, $dato);
                }
            });
            
            for($i = 0; $i < count($arrayGeneral["Hojas"]); ++$i) {
                $tipo_hoja = $arrayGeneral["Hojas"][$i];
                $limite_filas = "A1:B1";
                if($tipo_hoja == "Ciudades"){
                    $limite_filas = "A1:C1";
                }
                $excel->sheet($tipo_hoja, function($sheet) use ($limite_filas, $titulosEncabezado, $arrayFormat, $arrayGeneral, $tipo_hoja) {
                    //Estilo de la hoja
                    $sheet->setFontSize(16);
                    $sheet->setColumnFormat($arrayFormat);
                    $sheet->freezeFirstRow();
   
                    //Estilo de las celdas
                    $sheet->cells($limite_filas, function($cells) {
                        $cells->setBackground('#B3EFFF');
                    });
                    
                    //Encabezado
                    $titulosEncabezado = $arrayGeneral["encabezado_base"];
                    if($tipo_hoja == "Ciudades"){
                        $titulosEncabezado = $arrayGeneral["encabezado_ciudad"];
                    }
                    
                    $sheet->row(1, $titulosEncabezado);

                    //LLenado de celdas
                    $datos= [];
                    if($tipo_hoja == "Estados"){
                        $datos =  $arrayGeneral["Estados"];
                    }
                    elseif($tipo_hoja == "Estado civil"){
                        $datos =  $arrayGeneral["Civiles"];
                    }
                    elseif($tipo_hoja == "Ciudades"){
                        $datos =  $arrayGeneral["Ciudades"];
                    }
                    elseif($tipo_hoja == "Horarios"){
                        $datos =  $arrayGeneral["Horarios"];
                    }
                    elseif($tipo_hoja == "Departamentos"){
                        $datos =  $arrayGeneral["Departamentos"];
                    }
                    elseif($tipo_hoja == "Sucursales"){
                        $datos =  $arrayGeneral["Sucursales"];
                    }

                    foreach($datos as $index => $dato) {
                        $sheet->row($index+2, $dato);
                    }
                });
            }
        })->export('xlsx', ['Access-Control-Allow-Origin'=>'*']);
    }

    



    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        //Query


        //Orden

        //Total registros

        //Paginacion

        //Ejecutar query

        //Mapear query

        //Find data response

        //Pagination response

        //Fin response


        //return response()->json($findJobsResponse);

    }

    public function get($id)
    {
        //return response()->json($jobResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}

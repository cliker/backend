<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICEstadoCivilQuery;
use App\DataAccess\Repositories\Interfaces\ICEstadoCivilRepository;
use App\Domain\CEstadoCivil;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\CEstadosCiviles\CEstadoCivilRequest;
use App\Services\DTO\CEstadosCiviles\CEstadoCivilResponse;
use App\Services\DTO\CEstadosCiviles\FindCEstadosCivilesRequest;
use App\Services\Interfaces\ICEstadoCivilService;
use App\Services\Mapper\Interfaces\ICEstadoCivilMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class CEstadoCivilService implements ICEstadoCivilService
{
    protected $unitOfWork;
    protected $estadoCivilRepository;
    protected $estadoCivilQuery;
    protected $estadoCivilMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICEstadoCivilRepository $estadoCivilRepository,
        ICEstadoCivilQuery $estadoCivilQuery,
        ICEstadoCivilMapper $estadoCivilMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->estadoCivilRepository    = $estadoCivilRepository;
        $this->estadoCivilQuery         = $estadoCivilQuery;
        $this->estadoCivilMapper        = $estadoCivilMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {

        $findEstadoCivilRequest = new FindCEstadosCivilesRequest($request->all());
        //Query
        $this->estadoCivilQuery->init();
        $this->estadoCivilQuery->withNombre($findEstadoCivilRequest->getNombre());
        $this->estadoCivilQuery->withActivo($findEstadoCivilRequest->getActivo());

        //Orden
        $this->estadoCivilQuery->sort($findEstadoCivilRequest->getSortBy(), $findEstadoCivilRequest->getSort());

        $totalCount = $this->estadoCivilQuery->totalCount();

        //Paginacion
        $this->estadoCivilQuery->paginate(
            $findEstadoCivilRequest->getPaginate(),
            $findEstadoCivilRequest->getItemsToShow(),
            $findEstadoCivilRequest->getPage()
        );

        //Ejecutar query
        $estadosCiviles = $this->estadoCivilQuery->execute();

        //Mapear query
        $estadosCivilesResponse = $this->estadoCivilMapper->getMapper()->mapMultiple($estadosCiviles, CEstadoCivilResponse::class);

        //Find data response
        $findEstadosCivilesDataResponse = new FindDataResponse();
        $findEstadosCivilesDataResponse->setResults($estadosCivilesResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findEstadoCivilRequest->getPage());
        $paginationResponse->setPageCount(count($estadosCivilesResponse));
        $paginationResponse->setPageSize($findEstadoCivilRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin responseº
        $findEstadosCivilesResponse = new  FindResponse();
        $findEstadosCivilesResponse->setData($findEstadosCivilesDataResponse);
        $findEstadosCivilesResponse->setPagination($paginationResponse);

        return response()->json($findEstadosCivilesResponse);

    }

    public function get($id)
    {
        /* @var $estadoCivil CEstadoCivil*/
        $estadoCivil = $this->estadoCivilRepository->get($id);
        $estadoCivilResponse = $this->estadoCivilMapper->getMapper()
            ->map($estadoCivil, CEstadoCivilResponse::class);
        return response()->json($estadoCivilResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $estadoCivil CEstadoCivil*/
        $estadoCivilRequest = new CEstadoCivilRequest($request->all());
        $estadoCivil = $this->estadoCivilMapper->getMapper()->map($estadoCivilRequest, CEstadoCivil::class);
        $estadoCivil->setActivo(true);
        $this->estadoCivilRepository->add($estadoCivil);
        $createResponse = new CreateResponse();
        $createResponse->setId($estadoCivil->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $estadoCivil CEstadoCivil*/
            $estadoCivilRequest = new CEstadoCivilRequest($request->all());
            $estadoCivil = $this->estadoCivilRepository->get($estadoCivilRequest->getId());
            $this->estadoCivilMapper->getMapper()->mapToObject($estadoCivilRequest, $estadoCivil);
            $this->estadoCivilRepository->update($estadoCivil);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;
        /* @var $estadoCivil CEstadoCivil*/
        //Eliminado logico
        try{
            $estadoCivil = $this->estadoCivilRepository->get($id);
            $estadoCivil->setActivo(false);
            $this->estadoCivilRepository->update($estadoCivil);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

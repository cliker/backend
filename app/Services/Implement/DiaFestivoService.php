<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IDiaFestivoQuery;
use App\DataAccess\Repositories\Interfaces\IDepartamentoRepository;
use App\DataAccess\Repositories\Interfaces\IDiaFestivoRepository;
use App\Domain\Departamento;
use App\Domain\DiaFestivo;
use App\Infrastructure\ProxyExtensions;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\DiasFestivos\DiaFestivoRequest;
use App\Services\DTO\DiasFestivos\DiaFestivoResponse;
use App\Services\DTO\DiasFestivos\FindDiasFestivosRequest;
use App\Services\Interfaces\IDiaFestivoService;
use App\Services\Mapper\Interfaces\IDepartamentoMapper;
use App\Services\Mapper\Interfaces\IDiaFestivoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class DiaFestivoService implements IDiaFestivoService
{
    protected $unitOfWork;
    protected $diaFestivoRepository;
    protected $diaFestivoQuery;
    protected $diaFestivoMapper;
    protected $departamentoRepository;
    protected $departamentoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IDiaFestivoRepository $diaFestivoRepository,
        IDiaFestivoQuery $diaFestivoQuery,
        IDiaFestivoMapper $diaFestivoMapper,
        IDepartamentoRepository $departamentoRepository,
        IDepartamentoMapper $departamentoMapper
    )
    {
        $this->unitOfWork       = $unitOfWork;
        $this->diaFestivoRepository    = $diaFestivoRepository;
        $this->diaFestivoQuery         = $diaFestivoQuery;
        $this->diaFestivoMapper        = $diaFestivoMapper;
        $this->departamentoRepository = $departamentoRepository;
        $this->departamentoMapper = $departamentoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findDiaRequest = new FindDiasFestivosRequest($request->all());

        //Query
        $this->diaFestivoQuery->init();
        $this->diaFestivoQuery->withNombre($findDiaRequest->getNombre());
        $this->diaFestivoQuery->withFecha($findDiaRequest->getFecha());
        $this->diaFestivoQuery->withMaxFecha($findDiaRequest->getMaxFecha());
        $this->diaFestivoQuery->withMinFecha($findDiaRequest->getMinFecha());
        $this->diaFestivoQuery->withDescripcion($findDiaRequest->getDescripcion());
        $this->diaFestivoQuery->withActivo($findDiaRequest->getActivo());
        $this->diaFestivoQuery->withDepartamentoId($findDiaRequest->getDepartamentolId());
        $this->diaFestivoQuery->includeDepartamentos($findDiaRequest->isIncludeDepartamentos());
        $this->diaFestivoQuery->includeEmpresa($findDiaRequest->isIncludeEmpresa());
        $this->diaFestivoQuery->withEmpresaId($findDiaRequest->getEmpresaId());
        $this->diaFestivoQuery->includeCliente($findDiaRequest->isIncludeCliente());
        $this->diaFestivoQuery->withClienteId($findDiaRequest->getClienteId());
        $this->diaFestivoQuery->withSucursalId($findDiaRequest->getSucursalId());
        $this->diaFestivoQuery->includeSucursal($findDiaRequest->isIncludeSucursal());

        //Orden
        $this->diaFestivoQuery->sort($findDiaRequest->getSortBy(), $findDiaRequest->getSort());

        //Total registros
        $totalCount = $this->diaFestivoQuery->totalCount();

        //Paginacion
        $this->diaFestivoQuery->paginate(
            $findDiaRequest->getPaginate(),
            $findDiaRequest->getItemsToShow(),
            $findDiaRequest->getPage()
        );

        //Ejecutar query
        $diasFestivos = $this->diaFestivoQuery->execute();

        //Mapear query
        $diasFestivosResponse = $this->diaFestivoMapper->getMapper()->mapMultiple($diasFestivos, DiaFestivoResponse::class);

        //Find data response
        $findDiasDataResponse = new FindDataResponse();
        $findDiasDataResponse->setResults($diasFestivosResponse);

        //Pagination response
        $paginateResponse = new PaginationResponse();
        $paginateResponse->setPage($findDiaRequest->getPage());
        $paginateResponse->setPageCount(count($diasFestivosResponse));
        $paginateResponse->setPageSize($findDiaRequest->getItemsToShow());
        $paginateResponse->setTotalCount($totalCount);

        //Fin response
        $findDiasResponse = new FindResponse();
        $findDiasResponse->setData($findDiasDataResponse);
        $findDiasResponse->setPagination($paginateResponse);


        return response()->json($findDiasResponse);

    }

    public function get($id)
    {
        /* @var $diaFestivo DiaFestivo */
        $diaFestivo = $this->diaFestivoRepository->get($id);
        ProxyExtensions::toArray($diaFestivo->getDepartamentos());
        $diaFestivoResponse = $this->diaFestivoMapper->getMapper()->map($diaFestivo, DiaFestivoResponse::class);

        return response()->json($diaFestivoResponse);

    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        $departamentos = array();
        /* @var $diaFestivo DiaFestivo */
        $diaFestivoRequest = new DiaFestivoRequest($request->all());
        $diaFestivo = $this->diaFestivoMapper->getMapper()
            ->map($diaFestivoRequest, DiaFestivo::class);
        $departamentos = $this->setDepartamentoToDiaFestivo($diaFestivoRequest->getDepartamentos());
        $diaFestivo->setDepartamentos($departamentos);
        $diaFestivo->setActivo(true);
        $this->diaFestivoRepository->add($diaFestivo);
        $createResponse = new CreateResponse();
        $createResponse->setId($diaFestivo->getId());
        return response()->json($createResponse);
    }

    private function setDepartamentoToDiaFestivo($departamentos){
        $departamentosResoponse = array();
        foreach ($departamentos as $depto){
            $departamento = $this->departamentoRepository->get($depto['id']);
            array_push($departamentosResoponse, $departamento);
        }
        return $departamentosResoponse;
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $diaFestivo DiaFestivo */
        $success = false;

        try{
            $diaFestivoRequest = new DiaFestivoRequest($request->all());
            $diaFestivo = $this->diaFestivoRepository->get($diaFestivoRequest->getId());
            $this->diaFestivoMapper->getMapper()->mapToObject($diaFestivoRequest, $diaFestivo);
            $departamentos = $this->setDepartamentoToDiaFestivo($diaFestivoRequest->getDepartamentos());
            $diaFestivo->setDepartamentos($departamentos);
            $this->diaFestivoRepository->update($diaFestivo);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);

        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        /* @var $diaFestivo DiaFestivo */
        $success = false;

        try{
            $diaFestivo = $this->diaFestivoRepository->get($id);
            $diaFestivo->setActivo(false);
            $this->diaFestivoRepository->update($diaFestivo);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);

        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\Interfaces\IReporteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;
use App\Infrastructure\HelperExtensions;

use App\Services\Interfaces\IExportarDatosService;

class ReporteService implements IReporteService
{
    protected $unitOfWork;
    protected $exportarDatosService;
    public function __construct(
        IUnitOfWork $unitOfWork,
        IExportarDatosService  $exportarDatosService
    )
    {
        $this->unitOfWork = $unitOfWork;
        $this->exportarDatosService = $exportarDatosService;
    } 

    public function asistencia($request){
        $registros = $this->arrayChecadas($request->getData());

        $titulosEncabezado = [
            'Empleado',
            'Fecha',
            'Hora Entrada',
            'Hora Salida',
            'Retardo',
            'Anticipado',
            'Falta'
        ];
        $formatColumn = [
            'B' => '"$"#,##0.00_-'

        ];
        return $this->exportarDatosService->exportData('Registro_de_asistencias', $titulosEncabezado, $registros, 'A1:G1', 6, $formatColumn);
    }

    private function arrayChecadas($registros){
        $arrayRegistros= array();

        foreach ($registros as $index => $registro) {
            $arrayRegistros[$index] = [
                isset($registro->nombre) ? $registro->nombre : "",
                isset($registro->fecha) ? HelperExtensions::formatDate($registro->fecha) : "",
                isset($registro->hora_entrada) ? $registro->hora_entrada : "",
                isset($registro->hora_salida) ? $registro->hora_salida : "",
                isset($registro->retardo) ? $registro->retardo : "",
                isset($registro->anticipado) ? $registro->anticipado : "",
                isset($registro->falta) ? $registro->falta : ""
            ];
        }
        return $arrayRegistros;
    }

    public function incidencias($request){
        $registros = $this->getIncidencias($request->getData());
        $titulosEncabezado = [
            'Departamento',
            'Empleado',
            'Descripcion',
            'Fecha Inicio',
            'Fecha Final',
            'Tipo',
            'Estatus'
        ];
        $formatColumn = [
        ];
        return $this->exportarDatosService->exportData('Incidencias', $titulosEncabezado, $registros, 'A1:H1', 7, $formatColumn);
    }

    private function getIncidencias($registros){
        $reg = $registros->data->results;
        $arrayRegistros= array();
    
        foreach ($reg as $index => $registro) {
            $arrayRegistros[$index] = [
                isset($registro->departamento) ? $registro->departamento->nombre : "",
                isset($registro->empleado) ? $registro->empleado->nombre. " ".$registro->empleado->apellido_paterno. " ".$registro->empleado->apellido_materno : "",
                isset($registro->descripcion) ? $registro->descripcion : "",
                isset($registro->fecha_inicio) ? HelperExtensions::formatDate($registro->fecha_inicio) : "",
                isset($registro->fecha_final) ? HelperExtensions::formatDate($registro->fecha_final) : "",
                isset($registro->tipo) ? $registro->tipo->nombre : "",
                isset($registro->estatus) ? $registro->estatus->nombre : ""
            ];
        }
        return $arrayRegistros;
    }











    
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
    }

    public function get($id)
    {
        //return response()->json($jobResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        // TODO: Implement create() method.
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}

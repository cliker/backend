<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\ICTipoDocumentoQuery;
use App\DataAccess\Repositories\Interfaces\ICTipoDocumentoRepository;
use App\Domain\CTipoDocumento;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\CTiposDocumentos\CTipoDocumentoRequest;
use App\Services\DTO\CTiposDocumentos\CTipoDocumentoResponse;
use App\Services\DTO\CTiposDocumentos\FindCTiposDocumentosRequest;
use App\Services\Interfaces\ICTipoDocumentoService;
use App\Services\Mapper\Interfaces\ICTipoDocumentoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;


class CTipoDocumentoService implements ICTipoDocumentoService
{
    protected $unitOfWork;
    protected $tipoDocumentoRepository;
    protected $tipoDocumentoQuery;
    protected $tipoDocumentoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        ICTipoDocumentoRepository $tipoDocumentoRepository,
        ICTipoDocumentoQuery $tipoDocumentoQuery,
        ICTipoDocumentoMapper $tipoDocumentoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->tipoDocumentoRepository    = $tipoDocumentoRepository;
        $this->tipoDocumentoQuery         = $tipoDocumentoQuery;
        $this->tipoDocumentoMapper        = $tipoDocumentoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findTipoDocumentoRequest = new FindCTiposDocumentosRequest($request->all());
        //Query
        $this->tipoDocumentoQuery->init();
        $this->tipoDocumentoQuery->withNombre($findTipoDocumentoRequest->getNombre());
        $this->tipoDocumentoQuery->withActivo($findTipoDocumentoRequest->getActivo());
        $this->tipoDocumentoQuery->withEmpresaId($findTipoDocumentoRequest->getEmpresaId());
        $this->tipoDocumentoQuery->includeEmpresa($findTipoDocumentoRequest->isIncludeEmpresa());
        $this->tipoDocumentoQuery->includeCliente($findTipoDocumentoRequest->isIncludeCliente());
        $this->tipoDocumentoQuery->withClienteId($findTipoDocumentoRequest->getClienteId());

        //Orden
        $this->tipoDocumentoQuery->sort($findTipoDocumentoRequest->getSortBy(), $findTipoDocumentoRequest->getSort());

        $totalCount = $this->tipoDocumentoQuery->totalCount();

        //Paginacion
        $this->tipoDocumentoQuery->paginate(
            $findTipoDocumentoRequest->getPaginate(),
            $findTipoDocumentoRequest->getItemsToShow(),
            $findTipoDocumentoRequest->getPage()
        );

        //Ejecutar query
        $tiposDocumentos = $this->tipoDocumentoQuery->execute();

        //Mapear query
        $tiposDocumentosResponse = $this->tipoDocumentoMapper->getMapper()->mapMultiple($tiposDocumentos, CTipoDocumentoResponse::class);

        //Find data response
        $findTiposDocumentosDataResponse = new FindDataResponse();
        $findTiposDocumentosDataResponse->setResults($tiposDocumentosResponse);

        //Pagination response
        $paginationResponse = new PaginationResponse();
        $paginationResponse->setPage($findTipoDocumentoRequest->getPage());
        $paginationResponse->setPageCount(count($tiposDocumentosResponse));
        $paginationResponse->setPageSize($findTipoDocumentoRequest->getItemsToShow());
        $paginationResponse->setTotalCount($totalCount);

        //Fin responseº
        $findTiposDocumentosResponse = new  FindResponse();
        $findTiposDocumentosResponse->setData($findTiposDocumentosDataResponse);
        $findTiposDocumentosResponse->setPagination($paginationResponse);

        return response()->json($findTiposDocumentosResponse);

    }

    public function get($id)
    {
        /* @var $tipoDocumento CTipoDocumento*/
        $tipoDocumento = $this->tipoDocumentoRepository->get($id);
        $tipoDocumentoResponse = $this->tipoDocumentoMapper->getMapper()
            ->map($tipoDocumento, CTipoDocumentoResponse::class);
        return response()->json($tipoDocumentoResponse);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        /* @var $tipoDocumento CTipoDocumento*/
        $tipoDocumentoRequest = new CTipoDocumentoRequest($request->all());
        $tipoDocumento = $this->tipoDocumentoMapper->getMapper()->map($tipoDocumentoRequest, CTipoDocumento::class);
        $tipoDocumento->setActivo(true);
        $this->tipoDocumentoRepository->add($tipoDocumento);

        $createResponse = new CreateResponse();
        $createResponse->setId($tipoDocumento->getId());
        return response()->json($createResponse);
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        $success = false;
        try{
            /* @var $tipoDocumento CTipoDocumento*/
            $tipoDocumentoRequest = new CTipoDocumentoRequest($request->all());
            $tipoDocumento = $this->tipoDocumentoRepository->get($tipoDocumentoRequest->getId());
            $this->tipoDocumentoMapper->getMapper()->mapToObject($tipoDocumentoRequest, $tipoDocumento);
            $this->tipoDocumentoRepository->update($tipoDocumento);
            $success = true;

        }catch (Exception $exception){
            throw $exception;
        }
        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        $success = false;

        /* @var $tipoDocumento CTipoDocumento*/

        //Eliminado logico
        try{
            $tipoDocumento = $this->tipoDocumentoRepository->get($id);
            $tipoDocumento->setActivo(false);

            $this->tipoDocumentoRepository->update($tipoDocumento);
            $success = true;
        }
        catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

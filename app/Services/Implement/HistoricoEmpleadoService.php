<?php
namespace App\Services\Implement;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Interfaces\IHistoricoEmpleadoQuery;
use App\DataAccess\Repositories\Interfaces\IHistoricoEmpleadoRepository;
use App\Domain\HistoricoEmpleado;
use App\Services\DTO\Base\FindDataResponse;
use App\Services\DTO\Base\FindResponse;
use App\Services\DTO\Base\PaginationResponse;
use App\Services\DTO\HistoricoEmpleados\FindHistoricoEmpleadosRequest;
use App\Services\DTO\HistoricoEmpleados\HistoricoEmpleadoRequest;
use App\Services\DTO\HistoricoEmpleados\HistoricoEmpleadoResponse;
use App\Services\Interfaces\IHistoricoEmpleadoService;
use App\Services\Mapper\Interfaces\IHistoricoEmpleadoMapper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Exception;
use App\Services\DTO\Base\SuccessResponse;
use App\Services\DTO\Base\CreateResponse;

use Illuminate\Support\Facades\DB;
use DateTime;

class HistoricoEmpleadoService implements IHistoricoEmpleadoService
{
    protected $unitOfWork;
    protected $historicoRepository;
    protected $historicoQuery;
    protected $historicoMapper;

    public function __construct(
        IUnitOfWork               $unitOfWork,
        IHistoricoEmpleadoRepository $historicoRepository,
        IHistoricoEmpleadoQuery $historicoQuery,
        IHistoricoEmpleadoMapper $historicoMapper)
    {
        $this->unitOfWork       = $unitOfWork;
        $this->historicoRepository    = $historicoRepository;
        $this->historicoQuery         = $historicoQuery;
        $this->historicoMapper        = $historicoMapper;
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function find($request)
    {
        $findHistoricoRequest = new FindHistoricoEmpleadosRequest($request->all());

        //Query
        $this->historicoQuery->init();
        $this->historicoQuery->withAccion($findHistoricoRequest->getAccion());
        $this->historicoQuery->withFecha($findHistoricoRequest->getFecha());
        $this->historicoQuery->withMinFecha($findHistoricoRequest->getMinFecha());
        $this->historicoQuery->withMaxFecha($findHistoricoRequest->getMaxFecha());
        $this->historicoQuery->withEmpleadoId($findHistoricoRequest->getEmpleadoId());
        $this->historicoQuery->includeEmpleado($findHistoricoRequest->isIncludeEmpleado());
        $this->historicoQuery->withResponsableId($findHistoricoRequest->getResponsableId());
        $this->historicoQuery->includeResponsable($findHistoricoRequest->isIncludeResponsable());

        //Orden
        $this->historicoQuery->sort($findHistoricoRequest->getSortBy(), $findHistoricoRequest->getSort());

        //Total registros
        $totalCount = $this->historicoQuery->totalCount();

        //Paginacion
        $this->historicoQuery->paginate(
            $findHistoricoRequest->getPaginate(),
            $findHistoricoRequest->getItemsToShow(),
            $findHistoricoRequest->getPage()
        );

        //Ejecutar query
        $historico = $this->historicoQuery->execute();

        //Mapear query
        $historicoResponse = $this->historicoMapper->getMapper()
            ->mapMultiple($historico, HistoricoEmpleadoResponse::class);

        //Find data response
        $findHistoricoDataResponse = new FindDataResponse();
        $findHistoricoDataResponse->setResults($historicoResponse);

        //Pagination response
        $paginateResponse = new PaginationResponse();
        $paginateResponse->setPage($findHistoricoRequest->getPage());
        $paginateResponse->setPageCount(count($historicoResponse));
        $paginateResponse->setPageSize($findHistoricoRequest->getItemsToShow());
        $paginateResponse->setTotalCount($totalCount);

        //Fin response
        $findHistoricoResponse = new FindResponse();
        $findHistoricoResponse->setData($findHistoricoDataResponse);
        $findHistoricoResponse->setPagination($paginateResponse);

        return response()->json($findHistoricoResponse);

    }

    public function get($id)
    {
        /* @var $historico HistoricoEmpleado */
        $historico= DB::table('historico_empleados')->where('historico_empleados.empleado_id',"=", $id)
        ->join('empleados','historico_empleados.empleado_id','=', 'empleados.id')
        ->select('historico_empleados.accion', 'historico_empleados.fecha as fecha_de_accion', 'empleados.*')->orderBy('historico_empleados.id')->get();

        return response()->json($historico);
    }

    /**
     * @param Request $request
     */
    public function create($request)
    {
        $response = false;
        $empleado_id = 1;
        if($request->accion == "Ingreso"){
            $empleado_id = $request->getid();
        }
        else{
            $empleado_id = $request->id;
        }
        try{
            $historico = DB::table('historico_empleados')->insert([
                "accion" => $request->accion,
                "fecha"  => new DateTime('now'),
                "empleado_id" => $empleado_id,
                "responsable_id" => $request->usuario_id,
                "activo" => true,
                "created_date" => new DateTime('now'),
            ]);
        }
        catch(Exception $e) {
            return $response;
        }
    }

    /**
     * @param Request $request
     */
    public function update($request)
    {
        /* @var $historico HistoricoEmpleado */
        $success = false;

        try{
            $historicoRequest = new HistoricoEmpleadoRequest($request->all());
            $historico = $this->historicoRepository->get($historicoRequest->getId());
            $this->historicoMapper->getMapper()->mapToObject($historicoRequest, $historico);
            $this->historicoRepository->update($historico);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }

    /**
     * @param Request $request
     */
    public function delete($id)
    {
        /* @var $historico HistoricoEmpleado */
        $success = false;

        try{
            $historico = $this->historicoRepository->get($id);
            $historico->setActivo(false);
            $this->historicoRepository->update($historico);
            $success = true;
        }catch(Exception $exception){
            throw $exception;
        }

        $isSuccess = new SuccessResponse();
        $isSuccess->setIsSuccess($success);
        return response()->json($isSuccess);
    }
}

<?php
namespace App\Services\Validations\Interfaces;

use Illuminate\Http\Request;

interface IArchivoValidator extends IBaseValidator
{
    public function validateDocumentoEmpleadoStore(Request $request);
    public function validateDocumentoEmpleadoUpdate(Request $request);
    public function validateDocumentoEmpleadoDelete($id);
}

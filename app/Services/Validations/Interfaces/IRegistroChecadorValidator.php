<?php
namespace App\Services\Validations\Interfaces;
use Illuminate\Http\Request;
interface IRegistroChecadorValidator extends IBaseValidator
{
    public function validateStoreFromAsp(Request $request);
    public function empresa($id);
}

<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 11:40 AM
 */

namespace App\Services\Validations\Interfaces;
use Illuminate\Http\Request;


interface IAuthValidator
{
    public function validateLogin(Request $request);
}
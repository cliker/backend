<?php
namespace App\Services\Validations\Interfaces;

use Illuminate\Http\Request;

interface IEmpleadoValidator extends IBaseValidator
{
    public function importarDatos(Request $request);
    public function dashboard(Request $request);
    public function asignacion(Request $request);
    public function checadas(Request $request);
    public function incidencias(Request $request);
}

<?php
namespace App\Services\Validations\Interfaces;
use Illuminate\Http\Request;

interface IUsuarioValidator extends IBaseValidator
{
    public function exportarLayout(Request $request);
    public function password(Request $request);
}

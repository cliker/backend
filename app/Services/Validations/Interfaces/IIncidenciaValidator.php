<?php
namespace App\Services\Validations\Interfaces;
use Illuminate\Http\Request;

interface IIncidenciaValidator extends IBaseValidator
{
    public function upload_documento(Request $reqquest);
}

<?php
namespace App\Services\Validations\Interfaces;

use Illuminate\Http\Request;

interface IRelojChecadorValidator extends IBaseValidator
{
    public function validateDrop($id);
    public function validateRemoveEmployee(Request $request);
}

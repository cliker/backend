<?php
/**
 * Created by PhpStorm.
 * User: sunwise
 * Date: 4/06/18
 * Time: 03:58 PM
 */

namespace App\Services\Validations\Interfaces;


use Illuminate\Http\Request;

interface IBaseValidator
{
    public function validate($attributes, $rules, $messages = array());
    public function validateFind(Request $request);
    public function validateGet($id);
    public function validateStore(Request $request);
    public function validateUpdate(Request $request);
    public function validateDelete($id);
}
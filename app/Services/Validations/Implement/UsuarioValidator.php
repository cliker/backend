<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Empresa;
use App\Domain\Rol;
use App\Domain\Sucursal;
use App\Domain\Usuario;
use App\Services\DTO\Usuarios\FindUsuariosRequest;
use App\Services\Validations\Interfaces\IUsuarioValidator;
use Illuminate\Http\Request;

class UsuarioValidator implements IUsuarioValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindUsuariosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Usuario::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'apellido_paterno' => ['required', 'string'],
            'puesto' => ['string'],
            'fecha_nacimiento' => ['date'],
            'telefono' => ['required', 'string'],
            'email' => ['required', 'unique:'. Usuario::class, 'email'],
            'password' => ['required', 'string'],
            'empresa_id' => ['strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'rol_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Rol::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            "id" => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class],
            'nombre' => ['required', 'string'],
            'apellido_paterno' => ['required', 'string'],
            'puesto' => ['string'],
            'fecha_nacimiento' => ['date'],
            'telefono' => ['required', 'string'],
            'email' => ['required', 'email', 'unique_email'],
            'empresa_id' => ['strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'rol_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Rol::class],
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Usuario::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico",
            'no_exist' => "El elemento no existe",
        ];

        $this->validate($attributes, $rules, $messages);
    }

    public function exportarLayout(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            "tipo" => ['required', 'string'],
            'rol_name' => ['required', 'string'],
            'empresa_id' => ['integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['integer', 'positive'],
        ];
        $this->validate($attributes, $rules);
    }

    public function password(Request $request){
        $attributes = $request->all();
        $rules = [
            "id" => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class],
            'password' => ['required', 'string'],
            'primer_inicio_sesion' => ['required']
        ];
        $this->validate($attributes, $rules);
    }
}
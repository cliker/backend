<?php
namespace App\Services\Validations\Implement;

use App\Domain\DetalleHorario;
use App\Domain\Horario;
use App\Services\DTO\DetallesHorarios\FindDetallesHorariosRequest;
use App\Services\Validations\Interfaces\IDetalleHorarioValidator;
use Illuminate\Http\Request;

class DetalleHorarioValidator implements IDetalleHorarioValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindDetallesHorariosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".DetalleHorario::class
            ]
        ];
        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'dia' => ['required', 'string'],
            'hora_entrada' => ['required', 'date_format:H:i:s'],
            'hora_salida' => ['required', 'date_format:H:i:s', 'after:hora_entrada'],
            'horario_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Horario::class]
        ];
        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.DetalleHorario::class],
            'dia' => ['required', 'string'],
            'hora_entrada' => ['required', 'date_format:H:i:s'],
            'hora_salida' => ['required', 'date_format:H:i:s', 'after:hora_entrada'],
            'horario_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Horario::class]
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".DetalleHorario::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
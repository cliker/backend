<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\CTipoDocumento;
use App\Domain\Empresa;
use App\Services\DTO\CTiposDocumentos\FindCTiposDocumentosRequest;
use App\Services\Validations\Interfaces\ICTipoDocumentoValidator;
use Illuminate\Http\Request;

class CTipoDocumentoValidator implements ICTipoDocumentoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindCTiposDocumentosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".CTipoDocumento::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'descripcion' => ['string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoDocumento::class],
            'nombre' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'descripcion' => ['string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".CTipoDocumento::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
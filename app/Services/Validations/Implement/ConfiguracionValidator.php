<?php
namespace App\Services\Validations\Implement;

use App\Domain\Configuracion;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\Configuraciones\FindConfiguracionesRequest;
use App\Services\Validations\Interfaces\IConfiguracionValidator;
use Illuminate\Http\Request;

class ConfiguracionValidator implements IConfiguracionValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindConfiguracionesRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Configuracion::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'clave' => ['required', 'string'],
            'valor' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Configuracion::class],
            'nombre' => ['required', 'string'],
            'clave' => ['required', 'string'],
            'valor' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Configuracion::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
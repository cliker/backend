<?php
namespace App\Services\Validations\Implement;

use App\Domain\CEstadoCivil;
use App\Services\DTO\CEstadosCiviles\FindCEstadosCivilesRequest;
use App\Services\Validations\Interfaces\ICEstadoCivilValidator;
use Illuminate\Http\Request;

class CEstadoCivilValidator implements ICEstadoCivilValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindCEstadosCivilesRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".CEstadoCivil::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string']
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstadoCivil::class],
            'nombre' => ['required', 'string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".CEstadoCivil::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
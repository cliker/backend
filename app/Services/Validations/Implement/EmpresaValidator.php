<?php
namespace App\Services\Validations\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\Cliente;
use App\Domain\Empresa;
use App\Domain\Usuario;
use App\Services\DTO\Empresas\FindEmpresasRequest;
use App\Services\Validations\Interfaces\IEmpresaValidator;
use Illuminate\Http\Request;

class EmpresaValidator implements IEmpresaValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindEmpresasRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Empresa::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'razon_social' => ['required', 'string'],
            'rfc' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.CCiudad::class],
            'cliente_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.Cliente::class],
            'url_server_asp' => ['string', 'url'],

        ];
        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'nombre' => ['required', 'string'],
            'razon_social' => ['required', 'string'],
            'rfc' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.CCiudad::class],
            'cliente_id' => ['required', 'positive', 'strict_integer', 'exists_bd:'.Cliente::class],
            'url_server_asp' => ['string', 'url']
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Empresa::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico",
            'no_exist' => "El elemento no existe",
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
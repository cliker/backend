<?php
namespace App\Services\Validations\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Services\DTO\CCiudades\FindCCiudadesRequest;
use App\Services\Validations\Interfaces\ICCiudadValidator;
use Illuminate\Http\Request;

class CCiudadValidator implements ICCiudadValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindCCiudadesRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".CCiudad::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class],
            'nombre' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".CCiudad::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
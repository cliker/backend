<?php
namespace App\Services\Validations\Implement;

use App\Domain\CEstatusIncidencia;
use App\Services\DTO\CEstatusIncidencias\FindCEstatusIncidenciasRequest;
use App\Services\Validations\Interfaces\ICEstatusIncidenciaValidator;
use Illuminate\Http\Request;

class CEstatusIncidenciaValidator implements ICEstatusIncidenciaValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindCEstatusIncidenciasRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".CEstatusIncidencia::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'nombre' => ['required', 'string']
        ];
        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstatusIncidencia::class],
            'nombre' => ['required', 'string'],
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".CEstatusIncidencia::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
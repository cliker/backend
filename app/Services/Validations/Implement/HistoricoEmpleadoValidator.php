<?php
namespace App\Services\Validations\Implement;

use App\Domain\Empleado;
use App\Domain\HistoricoEmpleado;
use App\Domain\Usuario;
use App\Services\DTO\HistoricoEmpleados\FindHistoricoEmpleadosRequest;
use App\Services\Validations\Interfaces\IHistoricoEmpleadoValidator;
use Illuminate\Http\Request;

class HistoricoEmpleadoValidator implements IHistoricoEmpleadoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindHistoricoEmpleadosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Empleado::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'accion' => ['required', 'string'],
            'fecha' => ['required', 'date'],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'responsable_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.HistoricoEmpleado::class],
            'accion' => ['required', 'string'],
            'fecha' => ['required', 'date'],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'responsable_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".HistoricoEmpleado::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
<?php
namespace App\Services\Validations\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\Cliente;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\Sucursales\FindSucursalRequest;
use App\Services\Validations\Interfaces\ISucursalValidator;
use Illuminate\Http\Request;

class SucursalValidator implements ISucursalValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindSucursalRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Sucursal::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class],
            'codigo_postal' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'nombre' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class],
            'codigo_postal' => ['required', 'string'],
            'descripcion' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Sucursal::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
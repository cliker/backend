<?php
namespace App\Services\Validations\Implement;

use App\Domain\CTipoIncidencia;
use App\Services\DTO\CTiposIncidencias\FindCTiposIncidenciasRequest;
use App\Services\Validations\Interfaces\ICTipoIncidenciaValidator;
use Illuminate\Http\Request;

class CTipoIncidenciaValidator implements ICTipoIncidenciaValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindCTiposIncidenciasRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".CTipoIncidencia::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string']
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoIncidencia::class],
            'nombre' => ['required', 'string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".CTipoIncidencia::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Documento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\Validations\Interfaces\IArchivoValidator;
use Illuminate\Http\Request;

class ArchivoValidator implements IArchivoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        // TODO: Implement validateFind() method.
    }

    public function validateGet($id)
    {
        /*$attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Entity::class
            ]
        ];

        $this->validate($attributes, $rules);*/
    }

    public function validateStore(Request $request)
    {
        // TODO: Implement validateStore() method.
    }

    public function validateUpdate(Request $request)
    {
        // TODO: Implement validateUpdate() method.
    }

    public function validateDelete($id)
    {
        // TODO: Implement validateDelete() method.
    }

    public function validateDocumentoEmpleadoStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'cliente_id' => ['required', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'exists_bd:'.Departamento::class],
            'empleado_id' => ['required', 'exists_bd:'.Empleado::class],
            'archivo' => ['required', 'file', 'mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx'],
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDocumentoEmpleadoUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'documento_id' => ['required', 'exists_bd:'.Documento::class],
            'cliente_id' => ['required', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'exists_bd:'.Departamento::class],
            'empleado_id' => ['required', 'exists_bd:'.Empleado::class],
            'archivo' => ['required', 'file', 'mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx'],
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDocumentoEmpleadoDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Documento::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
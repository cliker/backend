<?php
namespace App\Services\Validations\Implement;

use App\Domain\Empleado;
use App\Domain\Cliente;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Domain\Departamento;
use App\Domain\RegistroChecador;
use App\Infrastructure\ArrayExtensions;
use App\Services\DTO\RegistrosChecadores\FindRegistrosChecadores;
use App\Services\Validations\Interfaces\IRegistroChecadorValidator;
use Illuminate\Http\Request;

class RegistroChecadorValidator implements IRegistroChecadorValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindRegistrosChecadores::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".RegistroChecador::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id_asp' => ['required', 'strict_integer', 'positive'],
            'fecha' => ['required', 'date'],
            'hora' => ['required'],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class]
        ];
        if(ArrayExtensions::isNotNullOrEmpty($attributes)){
            foreach ($attributes as $attribute){
                $this->validate($attribute, $rules);
            }
        }else{
            $this->validate($attributes, $rules);
        }
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.RegistroChecador::class],
            'id_asp' => ['required', 'strict_integer', 'positive'],
            'fecha' => ['required', 'date'],
            'hora' => ['required'],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class]
        ];

        if(ArrayExtensions::isNotNullOrEmpty($attributes)){
            if(ArrayExtensions::isNotNullOrEmpty($attributes)){
                foreach ($attributes as $attribute){
                    $this->validate($attribute, $rules);
                }
            }
        }else{
            $this->validate($attributes, $rules);
        }
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".RegistroChecador::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }

    public function validateStoreFromAsp(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'registros' => ['array']
        ];
        $this->validate($attributes, $rules);

        $rulesRegistros = [
            'id' => ['required', 'strict_integer', 'positive'],
            'clave_usuario' => ['required', 'strict_integer', 'positive'],
            'registro' => ['required']
        ];
        if(ArrayExtensions::isNotNullOrEmpty($attributes['registros'])){
            foreach ($attributes['registros'] as $registro){
                $this->validate($registro, $rulesRegistros);
            }
        }
    }

    public function empresa($id){
        
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Empresa::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
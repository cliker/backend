<?php
namespace App\Services\Validations\Implement;

use App\Exceptions\ApiValidationException;
use App\Infrastructure\ArrayExtensions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


trait ValidatorBase
{
    public function validate($attributes, $rules, $messages = array(), $data = null){
        if(!ArrayExtensions::isMultidimensional($attributes)){
            $validator = Validator::make($attributes, $rules, $messages);
            if($validator->fails()){
                throw new ApiValidationException($validator->errors()->messages());
            }
        }
        else{

            $invalidos = array();
            foreach ($attributes as $key=> $attribute){
                $validator = Validator::make($attribute, $rules, $messages);
                $valido = $validator->passes();
                if(!$valido){
                    $invalidos[] = [
                        "index"=>$key,
                        "errors" => $validator->errors()->getMessages()
                    ];
                }
            }

            if(isset($data["attribute"]) && !empty($data["attribute"]) &&!empty($invalidos)){
                $invalidos = [$data["attribute"]=>$invalidos];
            }

            if(isset($data["parentIndex"]) && !empty($data["parentIndex"]) &&!empty($invalidos)){
                $invalidos = [
                    "index"=>$data["parentIndex"],
                    "errors" => $invalidos
                ];
            }

            if(!empty($invalidos)){
                throw new ApiValidationException($invalidos);
            }
        }
    }

    public function _validateFind($attributes, $findClassRequest){
        foreach ($attributes as $key => $attribute){
            if(!property_exists($findClassRequest, $key)){
                throw new ApiValidationException([$key => "Este parámetro no es permitido"]);
            }
        }
    }

    public function cleanRequest(Request $request, $attributes){
        return $request->replace($attributes);
    }

    public function getAcceptedAttributes(Request $request, $requestAttributes){
        return $request->only(
            array_keys($requestAttributes)
        );
    }

}
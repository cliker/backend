<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Services\DTO\Departamentos\FindDepartamentosRequest;
use App\Services\Validations\Interfaces\IDepartamentoValidator;
use Illuminate\Http\Request;

class DepartamentoValidator implements IDepartamentoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindDepartamentosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Departamento::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'encargado' => ['required','string'],
            'correo' => ['required','string'],
            'descripcion' => ['required', 'string'],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'nombre' => ['required', 'string'],
            'encargado' => ['required','string'],
            'correo' => ['required','string'],
            'descripcion' => ['required', 'string'],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Departamento::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
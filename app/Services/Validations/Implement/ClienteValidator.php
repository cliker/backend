<?php
namespace App\Services\Validations\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\Cliente;
use App\Services\DTO\Clientes\FindClientesRequest;
use App\Services\Validations\Interfaces\IClienteValidator;
use Illuminate\Http\Request;

class ClienteValidator implements IClienteValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindClientesRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Cliente::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'telefono' => ['required', 'string'],
            'correo' => ['required', 'string', 'email'],
            'direccion' => ['string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'nombre' => ['required', 'string'],
            'telefono' => ['required', 'string'],
            'correo' => ['required', 'string', 'email'],
            'direccion' => ['string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Cliente::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
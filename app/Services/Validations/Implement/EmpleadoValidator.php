<?php
namespace App\Services\Validations\Implement;

use App\Domain\CCiudad;
use App\Domain\CEstado;
use App\Domain\CEstadoCivil;
use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Horario;
use App\Domain\Sucursal;
use App\Domain\Usuario;
use App\Domain\CTipoIncidencia;
use App\Services\DTO\Empleados\FindEmpleadosRequest;
use App\Services\Validations\Interfaces\IEmpleadoValidator;
use Illuminate\Http\Request;

class EmpleadoValidator implements IEmpleadoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindEmpleadosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Empleado::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'apellido_paterno' => ['required', 'string'],
            'apellido_materno' => ['required', 'string'],
            'fecha_nacimiento' => ['required', 'date'],
            'sexo' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class],
            'codigo_postal' => ['string'],
            'telefono' => ['required', 'string'],
            'puesto' => ['required', 'string', 'max:150'],
            'tiene_hijos' => ['required', 'boolean'],
            'correo' => ['email'],
            'estado_civil_id' => ['strict_integer', 'positive', 'exists_bd:'.CEstadoCivil::class],
            'fecha_ingreso' => ['required', 'date'],
            'dia_descanso' => ['required', 'string'],
            'horario_id' => ['strict_integer', 'positive', 'exists_bd:'.Horario::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'documentos' => ['array'],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'usuario_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'email_anterior' => ['required', 'string'],
            'nombre' => ['required', 'string'],
            'apellido_paterno' => ['required', 'string'],
            'apellido_materno' => ['required', 'string'],
            'fecha_nacimiento' => ['required', 'date'],
            'sexo' => ['required', 'string'],
            'direccion' => ['required', 'string'],
            'estado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstado::class],
            'ciudad_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CCiudad::class],
            'codigo_postal' => ['string'],
            'telefono' => ['required', 'string'],
            'puesto' => ['required', 'string', 'max:150'],
            'tiene_hijos' => ['required', 'boolean'],
            'correo' => ['email'],
            'estado_civil_id' => ['strict_integer', 'positive', 'exists_bd:'.CEstadoCivil::class],
            'fecha_ingreso' => ['required', 'date'],
            'dia_descanso' => ['required', 'string'],
            'horario_id' => ['strict_integer', 'positive', 'exists_bd:'.Horario::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'documentos' => ['array'],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Empleado::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }

    public function importarDatos(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'archivo' => ['required', 'file', 'mimes: xls,xlsx'],
            'rol_name' => ['required', 'string'],
            'sucursal_id' => ['required', 'integer', 'positive'],
            'cliente_id' => ['required', 'integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'integer', 'positive', 'exists_bd:'.Empresa::class],
            'usuario_id' => ['required', 'integer', 'positive', 'exists_bd:'.Usuario::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function eliminar(Request $request){
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'integer', 'positive', 'exists_bd:'.Empleado::class],
            'accion' => ['required', 'string'],
            'email' => ['required', 'string'],
            'usuario_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Usuario::class],
        ];

        $this->validate($attributes, $rules);
    }

    public function dashboard(Request $request){
        $attributes = $request->all();

        $rules = [
            'cliente_id' => 'integer|exists_bd:'.Cliente::class,
            'empresa_id' => 'integer|exists_bd:'.Empresa::class,
            'sucursal_id' => 'integer|exists_bd:'.Sucursal::class,
            'departamento_id' => 'integer|exists_bd:'.Departamento::class,
            'min_fecha' => 'required|date',
            'max_fecha' => 'required|date',
            'rol_name' => 'required|string|max:20'
        ];

        $this->validate($attributes, $rules);
    }

    public function asignacion(Request $request){
        $attributes = $request->all();

        $rules = [
            'empleados' => 'required',
            'checadores' => 'required',
            'url_server_asp' => 'required|string'
        ];

        $this->validate($attributes, $rules);
    }

    public function checadas(Request $request){
        $attributes = $request->all();

        $rules = [
            'min_fecha_checada' => 'required|date',
            'max_fecha_checada' => 'required|date',
        ];

        $this->validate($attributes, $rules);
    }

    public function incidencias(Request $request){
        $attributes = $request->all();

        $rules = [
            'cliente_id' => 'integer|exists_bd:'.Cliente::class,
            'empresa_id' => 'integer|exists_bd:'.Empresa::class,
            'sucursal_id' => 'integer|exists_bd:'.Sucursal::class,
            'departamento_id' => 'integer|exists_bd:'.Departamento::class,
            'tipo_incidencia' => 'required|integer|exists_bd:'.CTipoIncidencia::class,
            'min_fecha' => 'required|date',
            'max_fecha' => 'required|date',
            'mostrar' => 'required|string|max:20'
        ];

        $this->validate($attributes, $rules);
    }
}
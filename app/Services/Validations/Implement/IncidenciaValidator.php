<?php
namespace App\Services\Validations\Implement;

use App\Domain\CEstatusIncidencia;
use App\Domain\Cliente;
use App\Domain\CTipoIncidencia;
use App\Domain\Departamento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Incidencia;
use App\Domain\Sucursal;
use App\Services\DTO\Incidencias\FindIncidenciasRequest;
use App\Services\Validations\Interfaces\IIncidenciaValidator;
use Illuminate\Http\Request;

class IncidenciaValidator implements IIncidenciaValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindIncidenciasRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Incidencia::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'descripcion' => ['required', 'string'],
            'tipo_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoIncidencia::class],
            'fecha_inicio' => ['required', 'date'],
            'fecha_final' => ['required', 'date'],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'estatus_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstatusIncidencia::class]

        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Incidencia::class],
            'descripcion' => ['required', 'string'],
            'tipo_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoIncidencia::class],
            'fecha_inicio' => ['required', 'date'],
            'fecha_final' => ['required', 'date'],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'estatus_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CEstatusIncidencia::class]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Incidencia::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }

    public function upload_documento(Request $request){
        $attributes = $request->all();

        $rules = [
            'cliente_id' => ['required', 'integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'integer', 'positive', 'exists_bd:'.Sucursal::class],
            'empleado_id' => ['required', 'integer', 'positive', 'exists_bd:'.Empleado::class],
            'archivo' => ['required', 'file', 'mimes:jpeg,jpg,png,pdf,xls,xlsx,doc,docx'],
        ];

        $this->validate($attributes, $rules);
    }
}
<?php
namespace App\Services\Validations\Implement;

use App\Domain\Permiso;
use App\Services\DTO\Permisos\FindPermisosRequest;
use App\Services\Validations\Interfaces\IPermisoValidator;
use Illuminate\Http\Request;

class PermisoValidator implements IPermisoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindPermisosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Permiso::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'name' => ['required', 'string', 'unique:'. Permiso::class],
            'display_name' => ['required', 'string'],
            'description' => ['string']
        ];
        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            "id" => ['required', 'strict_integer', 'positive', 'exists_bd:'.Permiso::class],
            'name' => ['required', 'string'],
            'display_name' => ['required', 'string'],
            'description' => ['string']
        ];
        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Permiso::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico",
            'no_exist' => "El elemento no existe",
        ];
        $this->validate($attributes, $rules, $messages);
    }
}
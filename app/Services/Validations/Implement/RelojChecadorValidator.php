<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\RelojChecador;
use App\Domain\Sucursal;
use App\Infrastructure\ArrayExtensions;
use App\Services\DTO\RelojChecador\FindRelojChecadoresRequest;
use App\Services\Validations\Interfaces\IRelojChecadorValidator;
use Illuminate\Http\Request;

class RelojChecadorValidator implements IRelojChecadorValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindRelojChecadoresRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".RelojChecador::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'serial' => ['required', 'string'],
            'ip' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'descripcion' => ['string']
        ];
        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {


        if(ArrayExtensions::isMultidimensional($request->all())){
            $rules = $this->getRulesRelojChecadorUpdateArray();
            $requestArray = $request->all();
            foreach ($requestArray as $attributes){
                $this->validate($attributes, $rules);

                //Validar empleados
                if(isset($attributes['empleados']) && ArrayExtensions::isNotNullOrEmpty($attributes['empleados'])){
                    $rulesEmpleados = $this->getRulesEmpleados();
                    foreach ($attributes['empleados'] as $empleado){
                        $this->validate($empleado, $rulesEmpleados);
                    }
                }
            }
        }else{
            $rules = $this->getRulesRelojChecadorUpdate();
            $attributes = $request->all();
            $this->validate($attributes, $rules);

            //Validar empleados
            if(isset($attributes['empleados']) && ArrayExtensions::isNotNullOrEmpty($attributes['empleados'])){
                $rulesEmpleados = $this->getRulesEmpleados();
                foreach ($attributes['empleados'] as $empleado){
                    $this->validate($empleado, $rulesEmpleados);
                }
            }
        }
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".RelojChecador::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }

    public function validateDrop($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".RelojChecador::class
            ]
        ];
        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }

    public function validateRemoveEmployee(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id' => ['bail', 'integer', 'positive', 'required', "exists_bd:".RelojChecador::class],
            'empleado_id' => ['bail', 'integer', 'positive', 'required', "exists_bd:".Empleado::class],
            'empleados' => ['required'],
            'checadores' => ['required']
        ];
        $this->validate($attributes, $rules);
    }

    private function getRulesRelojChecadorUpdate(){
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.RelojChecador::class],
            'serial' => ['required', 'string'],
            'ip' => ['required', 'string'],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'descripcion' => ['string'],
            'empleados' => ['array']
        ];

        return $rules;
    }

    private function getRulesRelojChecadorUpdateArray(){
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.RelojChecador::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'departamento_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'descripcion' => ['string'],
            'empleados' => ['array']
        ];

        return $rules;
    }

    private function getRulesEmpleados(){
        $rulesEmpleados = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
        ];

        return $rulesEmpleados;
    }

    public function eliminar(Request $request){
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.RelojChecador::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
        ];
        $this->validate($attributes, $rules);
    }
}
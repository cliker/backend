<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\DetalleHorario;
use App\Domain\Empresa;
use App\Domain\Horario;
use App\Domain\Sucursal;
use App\Infrastructure\ArrayExtensions;
use App\Services\DTO\Horarios\FindHorariosRequest;
use App\Services\Validations\Interfaces\IHorarioValidator;
use Illuminate\Http\Request;

class HorarioValidator implements IHorarioValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindHorariosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Horario::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'nombre' => ['required', 'string'],
            'descripcion' => ['string'],
            'detalles_horario' => ['array'],
            'minutos_tolerancia' => ['required', 'integer'],
            'departamento_id' => [ 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class]
        ];
        $this->validate($attributes, $rules);

        //Validar detalles horario
        if(isset($attributes['detalles_horario']) && ArrayExtensions::isNotNullOrEmpty($attributes['detalles_horario'])){
            $rulesDetallesHorario = $this->getRulesStoreDetallesHorario();

            foreach ($attributes['detalles_horario'] as $detalleHorario){
                $this->validate($detalleHorario, $rulesDetallesHorario);
            }
        }
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();
        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Horario::class],
            'nombre' => ['required', 'string'],
            'descripcion' => ['string'],
            'detalles_horario' => ['array'],
            'minutos_tolerancia' => ['required', 'integer'],
            'departamento_id' => [ 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class],
            'detalles_eliminados' => ['array'],
        ];
        $this->validate($attributes, $rules);

        //Validar detalles horario
        if(isset($attributes['detalles_horario']) && ArrayExtensions::isNotNullOrEmpty($attributes['detalles_horario'])){
            foreach ($attributes['detalles_horario'] as $detalleHorario){
                if(isset($detalleHorario['id']) && $detalleHorario['id'] !== 0){
                    $rulesDetallesHorario = $this->getRulesUpdateDetallesHorario();
                }else{
                    $rulesDetallesHorario = $this->getRulesStoreDetallesHorario();
                }

                $this->validate($detalleHorario, $rulesDetallesHorario);
            }
        }

        //Validar detalles eliminados
        if(isset($attributes['detalles_eliminados']) && ArrayExtensions::isNotNullOrEmpty($attributes['detalles_eliminados'])){
            $rulesDetallesEliminados = [
                'id' => [
                    'bail',
                    'required',
                    'integer',
                    'positive',
                    "exists_bd:".DetalleHorario::class
                ]
            ];

            foreach ($attributes['detalles_eliminados'] as $detalleEliminado){
                $this->validate($detalleEliminado, $rulesDetallesEliminados);
            }
        }
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");
        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Horario::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];
        $this->validate($attributes, $rules, $messages);
    }

    private function getRulesStoreDetallesHorario(){
        $rulesDetallesHorario = [
            'dia' => ['required', 'string'],
            'hora_entrada' => ['required', 'date_format:H:i:s'],
            'hora_salida' => ['required', 'date_format:H:i:s', 'after:hora_entrada'],
        ];

        return $rulesDetallesHorario;
    }

    private function getRulesUpdateDetallesHorario(){
        $rulesDetallesHorario = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.DetalleHorario::class],
            'dia' => ['required', 'string'],
            'hora_entrada' => ['required', 'date_format:H:i:s'],
            'hora_salida' => ['required', 'date_format:H:i:s', 'after:hora_entrada'],
        ];

        return $rulesDetallesHorario;
    }
}
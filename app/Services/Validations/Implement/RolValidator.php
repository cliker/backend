<?php
namespace App\Services\Validations\Implement;

use App\Domain\Rol;
use App\Services\DTO\Roles\FindRolesRequest;
use App\Services\Validations\Interfaces\IRolValidator;
use Illuminate\Http\Request;

class RolValidator implements IRolValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindRolesRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Rol::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'name' => ['required', 'string'],
            'display_name' => ['required', 'string']
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            "id" => ['required', 'strict_integer', 'positive', 'exists_bd:'.Rol::class],
            'name' => ['required', 'string'],
            'display_name' => ['required', 'string']
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Rol::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico",
            'no_exist' => "El elemento no existe",
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
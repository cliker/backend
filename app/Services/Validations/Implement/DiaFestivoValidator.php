<?php
namespace App\Services\Validations\Implement;

use App\Domain\Cliente;
use App\Domain\Departamento;
use App\Domain\DiaFestivo;
use App\Domain\Empresa;
use App\Domain\Sucursal;
use App\Infrastructure\ArrayExtensions;
use App\Services\DTO\DiasFestivos\FindDiasFestivosRequest;
use App\Services\Validations\Interfaces\IDiaFestivoValidator;
use Illuminate\Http\Request;

class DiaFestivoValidator implements IDiaFestivoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindDiasFestivosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".DiaFestivo::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'fecha' => ['required', 'date'],
            'descripcion' => ['string'],
            'departamentos' => ['array'],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class]
        ];

        $this->validate($attributes, $rules);
        //Validar departamentos
        if(isset($attributes['departamentos']) && ArrayExtensions::isNotNullOrEmpty($attributes['departamentos'])){
            $rulesDepartamentos = [
                'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            ];

            foreach ($attributes['departamentos'] as $departamento){
                $this->validate($departamento, $rulesDepartamentos);
            }
        }
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.DiaFestivo::class],
            'nombre' => ['required', 'string'],
            'fecha' => ['required', 'date'],
            'descripcion' => ['string'],
            'departamentos' => ['array'],
            'cliente_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Cliente::class],
            'empresa_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empresa::class],
            'sucursal_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Sucursal::class]
        ];

        $this->validate($attributes, $rules);

        //Validar departamentos
        if(isset($attributes['departamentos']) && ArrayExtensions::isNotNullOrEmpty($attributes['departamentos'])){
            $rulesDepartamentos = [
                'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Departamento::class],
            ];

            foreach ($attributes['departamentos'] as $departamento){
                $this->validate($departamento, $rulesDepartamentos);
            }
        }
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".DiaFestivo::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
<?php
namespace App\Services\Validations\Implement;

use App\Domain\CTipoDocumento;
use App\Domain\Documento;
use App\Domain\Empleado;
use App\Services\DTO\Documentos\FindDocumentosRequest;
use App\Services\Validations\Interfaces\IDocumentoValidator;
use Illuminate\Http\Request;

class DocumentoValidator implements IDocumentoValidator
{
    use ValidatorBase;

    public function validateFind(Request $request)
    {
        $this->_validateFind($request->all(), FindDocumentosRequest::class);
    }

    public function validateGet($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'integer',
                'positive',
                'required',
                "exists_bd:".Documento::class
            ]
        ];

        $this->validate($attributes, $rules);
    }

    public function validateStore(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'nombre' => ['required', 'string'],
            'descripcion' => ['string'],
            'tipo_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoDocumento::class],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'extension' => ['required', 'string'],
            'url' => ['required', 'string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateUpdate(Request $request)
    {
        $attributes = $request->all();

        $rules = [
            'id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Documento::class],
            'nombre' => ['required', 'string'],
            'descripcion' => ['string'],
            'tipo_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.CTipoDocumento::class],
            'empleado_id' => ['required', 'strict_integer', 'positive', 'exists_bd:'.Empleado::class],
            'extension' => ['required', 'string'],
            'url' => ['required', 'string'],
        ];

        $this->validate($attributes, $rules);
    }

    public function validateDelete($id)
    {
        $attributes = compact("id");

        $rules = [
            'id' => [
                'bail',
                'required',
                'integer',
                'positive',
                "exists_bd:".Documento::class
            ]
        ];

        $messages = [
            'numeric' => "El id debe ser numérico"
        ];

        $this->validate($attributes, $rules, $messages);
    }
}
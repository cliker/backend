<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 3:35 PM
 */

namespace App\Services\DTO\DetallesHorarios;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindDetallesHorariosRequest extends DataTransferObject implements IFindDetallesHorariosRequest
{
    use TFindBaseRequest;

    protected $dia = '';
    protected $hora_entrada = '';
    protected $min_hora_entrada = '';
    protected $max_hora_entrada = '';

    protected $hora_salida = '';
    protected $min_hora_salida = '';
    protected $max_hora_salida = '';

    protected $horario_id = 0;
    protected $include_horario = false;

    protected $activo = '';

    /**
     * @return string|null
     */
    public function getDia(): ?string
    {
        return $this->dia;
    }

    /**
     * @param string|null $dia
     */
    public function setDia(?string $dia): void
    {
        $this->dia = $dia;
    }

    /**
     * @return string|null
     */
    public function getHoraEntrada(): ?string
    {
        return $this->hora_entrada;
    }

    /**
     * @param string|null $hora_entrada
     */
    public function setHoraEntrada(?string $hora_entrada): void
    {
        $this->hora_entrada = $hora_entrada;
    }

    /**
     * @return string|null
     */
    public function getMinHoraEntrada(): ?string
    {
        return $this->min_hora_entrada;
    }

    /**
     * @param string|null $min_hora_entrada
     */
    public function setMinHoraEntrada(?string $min_hora_entrada): void
    {
        $this->min_hora_entrada = $min_hora_entrada;
    }

    /**
     * @return string|null
     */
    public function getMaxHoraEntrada(): ?string
    {
        return $this->max_hora_entrada;
    }

    /**
     * @param string|null $max_hora_entrada
     */
    public function setMaxHoraEntrada(?string $max_hora_entrada): void
    {
        $this->max_hora_entrada = $max_hora_entrada;
    }

    /**
     * @return string|null
     */
    public function getHoraSalida(): ?string
    {
        return $this->hora_salida;
    }

    /**
     * @param string|null $hora_salida
     */
    public function setHoraSalida(?string $hora_salida): void
    {
        $this->hora_salida = $hora_salida;
    }

    /**
     * @return string|null
     */
    public function getMinHoraSalida(): ?string
    {
        return $this->min_hora_salida;
    }

    /**
     * @param string|null $min_hora_salida
     */
    public function setMinHoraSalida(?string $min_hora_salida): void
    {
        $this->min_hora_salida = $min_hora_salida;
    }

    /**
     * @return string|null
     */
    public function getMaxHoraSalida(): ?string
    {
        return $this->max_hora_salida;
    }

    /**
     * @param string|null $max_hora_salida
     */
    public function setMaxHoraSalida(?string $max_hora_salida): void
    {
        $this->max_hora_salida = $max_hora_salida;
    }

    /**
     * @return int|null
     */
    public function getHorarioId(): ?int
    {
        return $this->horario_id;
    }

    /**
     * @param string|null $horario_id
     */
    public function setHorarioId(?string $horario_id): void
    {
        $this->horario_id = $horario_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeHorario(): ?bool
    {
        return StringExtensions::toBoolean($this->include_horario);
    }

    /**
     * @param string|null $include_horario
     */
    public function setIncludeHorario(?string $include_horario): void
    {
        $this->include_horario = $include_horario;
    }

    /**
     * @return string|null
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }
}
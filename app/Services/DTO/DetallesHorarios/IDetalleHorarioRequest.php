<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 3:13 PM
 */

namespace App\Services\DTO\DetallesHorarios;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IDetalleHorarioRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getDia(): ?string;
    public function setDia(?string $dia);

    public function getHoraEntrada(): ?string;
    public function setHoraEntrada(?string $hora_entrada);

    public function getHoraSalida(): ?string;
    public function setHoraSalida(?string $hora_salida);

    public function getHorarioId(): ?int;
    public function setHorarioId(?int $horario_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 3:19 PM
 */

namespace App\Services\DTO\DetallesHorarios;
use App\Services\DTO\DataTransferObject;


class DetalleHorarioRequest extends DataTransferObject implements IDetalleHorarioRequest
{
    protected $id = 0;
    protected $dia = '';
    protected $hora_entrada = '';
    protected $hora_salida = '';
    protected $activo = true;
    protected $horario_id = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getDia(): ?string
    {
        return $this->dia;
    }

    /**
     * @param string|null $dia
     */
    public function setDia(?string $dia)
    {
        $this->dia = $dia;
    }

    /**
     * @return string|null
     */
    public function getHoraEntrada(): ?string
    {
        return $this->hora_entrada;
    }

    /**
     * @param string|null $hora_entrada
     */
    public function setHoraEntrada(?string $hora_entrada)
    {
        $this->hora_entrada = $hora_entrada;
    }

    /**
     * @return string|null
     */
    public function getHoraSalida(): ?string
    {
        return $this->hora_salida;
    }

    /**
     * @param string|null $hora_salida
     */
    public function setHoraSalida(?string $hora_salida)
    {
        $this->hora_salida = $hora_salida;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getHorarioId(): ?int
    {
        return $this->horario_id;
    }

    /**
     * @param int|null $horario_id
     */
    public function setHorarioId(?int $horario_id)
    {
        $this->horario_id = $horario_id;
    }
}
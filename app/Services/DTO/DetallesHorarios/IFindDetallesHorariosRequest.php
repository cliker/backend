<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 3:26 PM
 */

namespace App\Services\DTO\DetallesHorarios;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindDetallesHorariosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getDia(): ?string;
    public function setDia(?string $dia): void;

    public function getHoraEntrada(): ?string;
    public function setHoraEntrada(?string $hora_entrada): void;

    public function getMinHoraEntrada(): ?string;
    public function setMinHoraEntrada(?string $min_hora_entrada): void;

    public function getMaxHoraEntrada(): ?string;
    public function setMaxHoraEntrada(?string $max_hora_entrada): void;

    public function getHoraSalida(): ?string;
    public function setHoraSalida(?string $hora_salida): void;

    public function getMinHoraSalida(): ?string;
    public function setMinHoraSalida(?string $min_hora_salida): void;

    public function getMaxHoraSalida(): ?string;
    public function setMaxHoraSalida(?string $max_hora_salida): void;

    public function getHorarioId(): ?int;
    public function setHorarioId(?string $horario_id): void;
    public function isIncludeHorario(): ?bool;
    public function setIncludeHorario(?string $include_horario): void;

    public function getActivo():? string;
    public function setActivo(?string $activo): void;
}
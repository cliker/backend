<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\HistoricoEmpleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Usuarios\IUsuarioResponse;

interface IHistoricoEmpleadoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getAccion(): ?string;
    public function setAccion(?string $accion);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getResponsable(): ?IUsuarioResponse;
    public function setResponsable(?IUsuarioResponse $responsable_id);

    public function getEmpleado(): ?IEmpleadoResponse;
    public function setEmpleado(?IEmpleadoResponse $empleado_id);
}
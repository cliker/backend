<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\HistoricoEmpleados;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindHistoricoEmpleadosRequest extends DataTransferObject implements IFindHistoricoEmpleadosRequest
{
    use TFindBaseRequest;

    protected $id = 0;
    protected $accion= '';
    protected $fecha = '';
    protected $max_fecha = '';
    protected $min_fecha = '';
    protected $activo = true;
    protected $include_responsable= false;
    protected $responsable_id = null;
    protected $include_empleado= false;
    protected $empleado_id = null;

    /**
     * @return string
     */
    public function getAccion(): ?string
    {
        return $this->accion;
    }

    /**
     * @param string $accion
     */
    public function setAccion(?string $accion)
    {
        $this->accion = $accion;
    }

    /**
     * @return string
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string
     */
    public function getMaxFecha(): ?string
    {
        return $this->max_fecha;
    }

    /**
     * @param string $max_fecha
     */
    public function setMaxFecha(?string $max_fecha)
    {
        $this->max_fecha = $max_fecha;
    }

    /**
     * @return string
     */
    public function getMinFecha(): ?string
    {
        return $this->min_fecha;
    }

    /**
     * @param string $min_fecha
     */
    public function setMinFecha(?string $min_fecha)
    {
        $this->min_fecha = $min_fecha;
    }

    /**
     * @return int|null
     */
    public function getResponsableId(): ?int
    {
        return StringExtensions::toInt($this->responsable_id);
    }

    /**
     * @param null|string $responsable_id
     */
    public function setResponsableId(?string $responsable_id): void
    {
        $this->responsable_id = $responsable_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeResponsable(): ?bool
    {
        return StringExtensions::toBoolean($this->include_responsable);
    }

    /**
     * @param null|string $include_responsable
     */
    public function setIncludeResponsable(?string $include_responsable): void
    {
        $this->include_responsable = $include_responsable;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return StringExtensions::toInt($this->empleado_id);
    }

    /**
     * @param null|string $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpleado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empleado);
    }

    /**
     * @param null|string $include_empleado
     */
    public function setIncludeEmpleado(?string $include_empleado): void
    {
        $this->include_empleado = $include_empleado;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\HistoricoEmpleados;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Usuarios\IUsuarioResponse;


class HistoricoEmpleadoResponse extends DataTransferObject implements IHistoricoEmpleadoResponse
{
    protected $id = 0;
    protected $fecha = '';
    protected $accion = '';
    protected $activo = true;
    protected $responsable_id = null;
    protected $empleado_id = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAccion(): ?string
    {
        return $this->accion;
    }

    /**
     * @param string $accion
     */
    public function setAccion(?string $accion)
    {
        $this->accion = $accion;
    }

    /**
     * @return string
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return IUsuarioResponse|null
     */
    public function getResponsable(): ?IUsuarioResponse
    {
        return $this->responsable_id;
    }

    /**
     * @param IUsuarioResponse|null $responsable_id
     */
    public function setResponsable(?IUsuarioResponse $responsable_id)
    {
        $this->responsable_id = $responsable_id;
    }

    /**
     * @return IEmpleadoResponse|null
     */
    public function getEmpleado(): ?IEmpleadoResponse
    {
        return $this->empleado_id;
    }

    /**
     * @param IEmpleadoResponse|null $empleado_id
     */
    public function setEmpleado(?IEmpleadoResponse $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}
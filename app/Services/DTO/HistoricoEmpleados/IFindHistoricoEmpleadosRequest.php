<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\HistoricoEmpleados;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindHistoricoEmpleadosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getMaxFecha(): ?string;
    public function setMaxFecha(?string $max_fecha);

    public function getMinFecha(): ?string;
    public function setMinFecha(?string $min_fecha);

    public function getAccion(): ?string;
    public function setAccion(?string $accion);

    public function getActivo():? string;
    public function setActivo(?string $activo);

    public function getResponsableId(): ?int;
    public function setResponsableId(?string $responsable_id): void;
    public function isIncludeResponsable(): ?bool;
    public function setIncludeResponsable(?string $include_responsable): void;

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;
    public function isIncludeEmpleado(): ?bool;
    public function setIncludeEmpleado(?string $include_empleado): void;
}
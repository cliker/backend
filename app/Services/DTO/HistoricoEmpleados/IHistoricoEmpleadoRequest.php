<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\HistoricoEmpleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IHistoricoEmpleadoRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getAccion(): ?string;
    public function setAccion(?string $accion);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getResponsable(): ?int;
    public function setResponsable(?int $responsable_id);

    public function getEmpleado(): ?int ;
    public function setEmpleado(?int $empleado_id);
}
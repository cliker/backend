<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\HistoricoEmpleados;
use App\Services\DTO\DataTransferObject;


class HistoricoEmpleadoRequest extends DataTransferObject implements IHistoricoEmpleadoRequest
{
    protected $id = 0;
    protected $fecha = '';
    protected $accion = '';
    protected $activo = true;
    protected $responsable_id = null;
    protected $empleado_id = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAccion(): ?string
    {
        return $this->accion;
    }

    /**
     * @param string $accion
     */
    public function setAccion(?string $accion)
    {
        $this->accion = $accion;
    }

    /**
     * @return string
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return int|null
     */
    public function getEmpleado(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param int|null $empleado_id
     */
    public function setEmpleado(?int $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return int|null
     */
    public function getResponsable(): ?int
    {
        return $this->responsable_id;
    }

    /**
     * @param int|null $responsable_id
     */
    public function setResponsable(?int $responsable_id)
    {
        $this->responsable_id = $responsable_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 11:46 AM
 */

namespace App\Services\DTO\Permisos;
use App\Services\DTO\DataTransferObject;

class PermisoRequest extends DataTransferObject implements IPermisoRequest
{
    protected $id = 0;
    protected $name = '';
    protected $display_name = '';
    protected $description = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getDisplayName(): ?string
    {
        return $this->display_name;
    }

    /**
     * @param string|null $display_name
     */
    public function setDisplayName(?string $display_name)
    {
        $this->display_name = $display_name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description)
    {
        $this->description = $description;
    }
}
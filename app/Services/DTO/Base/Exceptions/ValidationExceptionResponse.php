<?php


namespace App\Services\DTO\Base\Exceptions;


use Aedart\DTO\DataTransferObject;

class ValidationExceptionResponse extends DataTransferObject implements IValidationExceptionResponse
{
    protected $message = "";
    protected $errors = array();

    /**
     * @return string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getErrors(): ?array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(?array $errors): void
    {
        $this->errors = $errors;
    }


}
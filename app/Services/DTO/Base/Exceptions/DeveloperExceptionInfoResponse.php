<?php


namespace App\Services\DTO\Base\Exceptions;


use Aedart\DTO\DataTransferObject;

class DeveloperExceptionInfoResponse extends DataTransferObject implements IDeveloperExceptionInfoResponse
{
    protected $exception_class = "";
    protected $trace;

    /**
     * @return string
     */
    public function getExceptionClass(): ?string
    {
        return $this->exception_class;
    }

    /**
     * @param string $exception_class
     */
    public function setExceptionClass(?string $exception_class): void
    {
        $this->exception_class = $exception_class;
    }

    /**
     * @return mixed
     */
    public function getTrace()
    {
        return $this->trace;
    }

    /**
     * @param mixed $trace
     */
    public function setTrace($trace): void
    {
        $this->trace = $trace;
    }


}
<?php


namespace App\Services\DTO\Base\Exceptions;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IValidationExceptionResponse extends DataTransferObjectInterface
{
    public function getMessage(): ?string;
    public function setMessage(?string $message): void;
    public function getErrors(): ?array;
    public function setErrors(?array $errors): void;

}
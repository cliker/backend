<?php
namespace App\Services\DTO\Base\Exceptions;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface ITraceExceptionResponse extends DataTransferObjectInterface
{
    public function getFile(): ?string;
    public function setFile(?string $file): void;
    public function getLine(): ?int;
    public function setLine(?int $line): void;
    public function getFunction(): ?string;
    public function setFunction(?string $function): void;
    public function getClass(): ?string;
    public function setClass(?string $class): void;
    public function getType(): ?string;
    public function setType(?string $type): void;

}
<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 18/05/18
 * Time: 18:49
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface ICreateArrayResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);
    public function getIdFrontend(): ?int;
    public function setIdFrontend(?int $id_frontend);
}
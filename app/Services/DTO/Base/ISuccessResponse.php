<?php

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface ISuccessResponse extends DataTransferObjectInterface
{

    public function setIsSuccess(?bool $isSuccess);
    public function getIsSuccess() : ?bool;
    public function getData(): ?array;
    public function setData(?array $data): void;


}
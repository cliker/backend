<?php


namespace App\Services\DTO\Base;


use Aedart\DTO\DataTransferObject;

class FindDataResponse extends DataTransferObject implements IFindDataResponse
{
    protected $results = null;

    /**
     * @return array
     */
    public function getResults(): ?array
    {
        return $this->results;
    }

    /**
     * @param array $results
     */
    public function setResults(?array $results): void
    {
        $this->results = $results;
    }



}
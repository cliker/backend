<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/11/18
 * Time: 12:52 AM
 */

namespace App\Services\DTO\Base;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IFindBaseRequest extends DataTransferObjectInterface
{
    public function getSort(): ?string;
    public function setSort(?string $sort);
    public function getSortBy(): ?string;
    public function setSortBy(?string $sort_by);
    public function getItemsToShow(): ?int;
    public function setItemsToShow(?string $items_to_show);
    public function getPage(): ?int;
    public function setPage(?string $page);
    public function getPaginate(): ?bool;
    public function setPaginate(?string $paginate);
}
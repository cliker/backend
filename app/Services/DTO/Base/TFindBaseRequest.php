<?php


namespace App\Services\DTO\Base;


use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;

trait TFindBaseRequest
{
    protected $sort = 'asc';
    protected $sort_by = 'id';
    protected $items_to_show = 10;
    protected $page = 1;
    protected $paginate = true;

    public function getSort(): ?string
    {
        return $this->sort;
    }

    public function setSort(?string $sort)
    {
        $this->sort = $sort;
    }

    public function getSortBy(): ?string
    {
        return $this->sort_by;
    }

    public function setSortBy(?string $sort_by)
    {
        $this->sort_by = $sort_by;
    }

    public function getItemsToShow(): ?int
    {
        return StringExtensions::toInt($this->items_to_show);
    }

    public function setItemsToShow(?string $items_to_show)
    {
        $_items_to_show = StringExtensions::toInt($items_to_show);

        if(NumberExtensions::isPositiveInteger($_items_to_show)){
            $this->items_to_show = $items_to_show;
        }
        else{
            throw new \Exception("El parámetro items_to_show debe ser un número positivo", 422);
        }

    }

    public function getPage(): ?int
    {
        return StringExtensions::toInt($this->page);
    }

    public function setPage(?string $page)
    {
        $_page = StringExtensions::toInt($page);

        if(NumberExtensions::isPositiveInteger($_page)){
            $this->page = $page;
        }
        else{
            throw new \Exception("El parámetro page debe ser un número positivo", 422);
        }
    }

    public function getPaginate(): ?bool
    {
        return StringExtensions::toBoolean($this->paginate);
    }

    public function setPaginate(?string $paginate)
    {
        $this->paginate = $paginate;
    }
}
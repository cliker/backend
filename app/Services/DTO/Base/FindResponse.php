<?php


namespace App\Services\DTO\Base;


use Aedart\DTO\DataTransferObject;

class FindResponse extends DataTransferObject implements IFindBaseResponse
{
    protected $data = null;
    protected $pagination = null;

    /**
     * @return FindDataResponse
     */
    public function getData(): ?FindDataResponse
    {
        return $this->data;
    }

    /**
     * @param FindDataResponse $data
     */
    public function setData(?FindDataResponse $data): void
    {
        $this->data = $data;
    }

    /**
     * @return PaginationResponse
     */
    public function getPagination(): ?PaginationResponse
    {
        return $this->pagination;
    }

    /**
     * @param PaginationResponse $pagination
     */
    public function setPagination(?PaginationResponse $pagination): void
    {
        $this->pagination = $pagination;
    }


}
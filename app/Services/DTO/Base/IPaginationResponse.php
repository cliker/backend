<?php


namespace App\Services\DTO\Base;


use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IPaginationResponse extends DataTransferObjectInterface
{
    public function getPage(): int;
    public function setPage(int $page): PaginationResponse;
    public function getPageSize(): int;
    public function setPageSize(int $page_size): PaginationResponse;
    public function getPageCount(): int;
    public function setPageCount(int $page_count): PaginationResponse;
    public function getTotalCount(): int;
    public function setTotalCount(int $total_count): PaginationResponse;
}
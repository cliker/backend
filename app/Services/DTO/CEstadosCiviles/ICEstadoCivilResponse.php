<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/26/19
 * Time: 4:56 PM
 */
namespace App\Services\DTO\CEstadosCiviles;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseResponse;

interface ICEstadoCivilResponse extends DataTransferObjectInterface, ICatalogoBaseResponse
{

}
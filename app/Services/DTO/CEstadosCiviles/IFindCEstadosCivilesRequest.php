<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 10:58 AM
 */

namespace App\Services\DTO\CEstadosCiviles;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;
use App\Services\DTO\CatalogosBase\IFindCatalogosBaseRequest;

interface IFindCEstadosCivilesRequest extends DataTransferObjectInterface, IFindBaseRequest, IFindCatalogosBaseRequest
{

}
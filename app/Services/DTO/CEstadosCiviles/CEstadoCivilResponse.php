<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 10:55 AM
 */

namespace App\Services\DTO\CEstadosCiviles;
use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;


class CEstadoCivilResponse extends CatalogoBaseResponse implements ICEstadoCivilResponse
{

}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\Empresas;
use App\Services\DTO\DataTransferObject;


class EmpresaRequest extends DataTransferObject implements IEmpresaRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $razon_social = '';
    protected $rfc = '';
    protected $direccion = '';
    protected $estado_id = 0;
    protected $ciudad_id = 0;
    protected $activo = true;
    protected $cliente_id = 0;
    protected $url_server_asp = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getUrlServerAsp(): ?string
    {
        return $this->url_server_asp;
    }


    /**
     * @param string|null $url_server_asp
     */
    public function setUrlServerAsp(?string $url_server_asp)
    {
        $this->url_server_asp = $url_server_asp;
    }

    /**
     * @return string|null
     */
    public function getRazonSocial(): ?string
    {
        return $this->razon_social;
    }

    /**
     * @param string|null $razon_social
     */
    public function setRazonSocial(?string $razon_social)
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return string|null
     */
    public function getRFC(): ?string
    {
        return $this->rfc;
    }

    /**
     * @param string|null $rfc
     */
    public function setRFC(?string $rfc)
    {
        $this->rfc = $rfc;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setEstadoId(?int $estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return $this->ciudad_id;
    }

    /**
     * @param int|null $ciudad_id
     */
    public function setCiudadId(?int $ciudad_id)
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }
}
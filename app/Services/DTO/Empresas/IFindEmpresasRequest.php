<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\Empresas;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindEmpresasRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getRazonSocial(): ?string;
    public function setRazonSocial(?string $razon_social);

    public function getRFC(): ?string;
    public function setRFC(?string $rfc);

    public function getEstadoId(): ?int;
    public function setEstadoId(?string $estado_id): void;
    public function isIncludeEstado(): ?bool;
    public function setIncludeEstado(?string $include_estado): void;

    public function getCiudadId(): ?int;
    public function setCiudadId(?string $ciudad_id): void;
    public function isIncludeCiudad(): ?bool;
    public function setIncludeCiudad(?string $include_ciudad): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;
    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;
}
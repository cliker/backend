<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\Empresas;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\CEstados\ICEstadoResponse;


class EmpresaResponse extends DataTransferObject implements IEmpresaResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $razon_social = '';
    protected $rfc = '';
    protected $direccion = '';
    protected $estado = null;
    protected $ciudad = null;
    protected $activo = true;
    protected $sucursales = null;
    protected $cliente = null;
    protected $url_server_asp = '';

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getUrlServerAsp(): ?string
    {
        return $this->url_server_asp;
    }

    /**
     * @param string $url_server_asp
     */
    public function setUrlServerAsp(?string $url_server_asp)
    {
        $this->url_server_asp = $url_server_asp;
    }

    /**
     * @return string
     */
    public function getRazonSocial(): ?string
    {
        return $this->razon_social;
    }

    /**
     * @param string $razon_social
     */
    public function setRazonSocial(?string $razon_social)
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return string
     */
    public function getRFC(): ?string
    {
        return $this->rfc;
    }

    /**
     * @param string $rfc
     */
    public function setRFC(?string $rfc)
    {
        $this->rfc = $rfc;
    }

    /**
     * @return string
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return ICEstadoResponse|null
     */
    public function getEstado(): ?ICEstadoResponse
    {
        return $this->estado;
    }

    /**
     * @param ICEstadoResponse|null $estado
     */
    public function setEstado(?ICEstadoResponse $estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return ICCiudadResponse|null
     */
    public function getCiudad(): ?ICCiudadResponse
    {
        return $this->ciudad;
    }

    /**
     * @param ICCiudadResponse|null $ciudad
     */
    public function setCiudad(?ICCiudadResponse $ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return array|null
     */
    public function getSucursales(): ?array
    {
        return $this->sucursales;
    }

    /**
     * @param array|null $sucursales
     */
    public function setSucursales(?array $sucursales)
    {
        $this->sucursales = $sucursales;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }
}
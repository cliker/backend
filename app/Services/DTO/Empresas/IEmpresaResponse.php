<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\Empresas;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\Clientes\IClienteResponse;

interface IEmpresaResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getUrlServerAsp(): ?string;
    public function setUrlServerAsp(?string $url_server_asp);

    public function getRazonSocial(): ?string;
    public function setRazonSocial(?string $razon_social);

    public function getRFC(): ?string;
    public function setRFC(?string $rfc);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstado(): ?ICEstadoResponse;
    public function setEstado(?ICEstadoResponse $estado);

    public function getCiudad(): ?ICCiudadResponse;
    public function setCiudad(?ICCiudadResponse $ciudad);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getSucursales(): ?array;
    public function setSucursales(?array $sucursales);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);
}
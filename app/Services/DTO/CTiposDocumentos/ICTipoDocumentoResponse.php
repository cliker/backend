<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 12:16 PM
 */
namespace App\Services\DTO\CTiposDocumentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;

interface ICTipoDocumentoResponse extends DataTransferObjectInterface, ICatalogoBaseResponse
{
    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);
}
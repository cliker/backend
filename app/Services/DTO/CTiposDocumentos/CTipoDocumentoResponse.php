<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 12:19 PM
 */

namespace App\Services\DTO\CTiposDocumentos;

use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;

class CTipoDocumentoResponse extends CatalogoBaseResponse implements ICTipoDocumentoResponse
{
    protected $empresa = null;
    protected $descripcion = '';
    protected $cliente = null;

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 12:24 PM
 */

namespace App\Services\DTO\CTiposDocumentos;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseRequest;

interface ICTipoDocumentoRequest extends DataTransferObjectInterface, ICatalogoBaseRequest
{
    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);
}
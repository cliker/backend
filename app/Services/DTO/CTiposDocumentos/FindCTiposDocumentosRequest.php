<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 12:32 PM
 */

namespace App\Services\DTO\CTiposDocumentos;

use App\Infrastructure\StringExtensions;
use App\Services\DTO\CatalogosBase\FindCatalogosBaseRequest;

class FindCTiposDocumentosRequest extends FindCatalogosBaseRequest implements IFindCTiposDocumentosRequest
{
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $cliente_id = 0;
    protected $include_cliente = false;

    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }
}
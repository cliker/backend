<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 03:24 PM
 */

namespace App\Services\DTO\Roles;
use App\Services\DTO\Base\IFindBaseRequest;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IFindRolesRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getName(): ?string;
    public function setName(?string $name);

    public function getDisplayName(): ?string;
    public function setDisplayName(?string $display_name);

    public function getIsDelete():? string;
    public function setIsDelete(?string $is_delete): void;
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 20/12/18
 * Time: 02:53 PM
 */

namespace App\Services\DTO\Roles;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IRolResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getName(): ?string;
    public function setName(?string $name);

    public function getDisplayName(): ?string;
    public function setDisplayName(?string $display_name);

    public function getDescription(): ?string;
    public function setDescription(?string $description);

    public function getIsDelete(): ?bool;
    public function setIsDelete(?bool $is_delete);

    public function getPermisos(): ?array;
    public function setPermisos(?array $permisos);
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 21/12/18
 * Time: 10:01 AM
 */

namespace App\Services\DTO\Roles;
use Aedart\DTO\DataTransferObject;

class CreateRolResponse extends DataTransferObject implements ICreateRolResponse
{
    protected $id           = 0;
    protected $permisos    = null;
    protected $success = false;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getPermisos(): ?array
    {
        return $this->permisos;
    }

    /**
     * @param null $permisos
     */
    public function setPermisos(?array $permisos)
    {
        $this->permisos = $permisos;
    }

    /**
     * @return null
     */
    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    /**
     * @param null $success
     */
    public function setSuccess(?bool $success)
    {
        $this->success = $success;
    }
}
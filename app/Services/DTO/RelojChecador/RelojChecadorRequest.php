<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\RelojChecador;
use App\Services\DTO\DataTransferObject;


class RelojChecadorRequest extends DataTransferObject implements IRelojChecadorRequest
{
    protected $id = 0;
    protected $serial = '';
    protected $ip = '';
    protected $empresa_id = 0;
    protected $sucursal_id = 0;
    protected $departamento_id = 0;
    protected $activo = true;
    protected $cliente_id = 0;
    protected $descripcion = "";
    protected $empleados = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getSerial(): ?string
    {
        return $this->serial;
    }


    /**
     * @param string|null $serial
     */
    public function setSerial(?string $serial)
    {
        $this->serial = $serial;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @param string|null $ip
     */
    public function setIp(?string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param int|null $departamento_id
     */
    public function setDepartamentoId(?int $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return array|null
     */
    public function getEmpleados(): ?array
    {
        return $this->empleados;
    }

    /**
     * @param array|null $empleados
     */
    public function setEmpleados(?array $empleados)
    {
        $this->empleados = $empleados;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\RelojChecador;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;


interface IRelojChecadorResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getSerial(): ?string;
    public function setSerial(?string $serial);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getIp(): ?string;
    public function setIp(?string $ip);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa_id);

    public function getSucursal(): ?ISucursalResponse;
    public function setSucursal(?ISucursalResponse $sucursal_id);

    public function getDepartamento(): ?IDepartamentoResponse;
    public function setDepartamento(?string $departamento_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getEmpleados(): ?array ;
    public function setEmpleados(?array $empleados);

}
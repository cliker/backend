<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\RelojChecador;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;
use App\Services\DTO\RelojChecador\IFindRelojChecadoresRequest;


class FindRelojChecadoresRequest extends DataTransferObject implements IFindRelojChecadoresRequest
{
    use TFindBaseRequest;

    protected $serial = '';
    protected $ip = '';
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $departamento_id = 0;
    protected $include_departamento = false;
    protected $activo = '';
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empleado_id = 0;
    protected $include_empleados = false;

    /**
     * @return string
     */
    public function getSerial(): ?string
    {
        return $this->serial;
    }

    /**
     * @param string $serial
     */
    public function setSerial(?string $serial)
    {
        $this->serial = $serial;
    }

    /**
     * @return string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(?string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return StringExtensions::toInt($this->empresa_id);
    }

    /**
     * @param null|string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param null|string $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return StringExtensions::toInt($this->sucursal_id);
    }

    /**
     * @param null|string $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param null|string $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return StringExtensions::toInt($this->departamento_id);
    }

    /**
     * @param null|string $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    /**
     * @param null|string $include_departamento
     */
    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpleados(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empleados);
    }

    /**
     * @param string|null $include_empleados
     */
    public function setIncludeEmpleados(?string $include_empleados): void
    {
        $this->include_empleados = $include_empleados;
    }
}
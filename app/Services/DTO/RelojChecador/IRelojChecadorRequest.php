<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\RelojChecador;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IRelojChecadorRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getSerial(): ?string;
    public function setSerial(?string $serial);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getIp(): ?string;
    public function setIp(?string $ip);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?int $departamento_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpleados(): ?array;
    public function setEmpleados(?array $empleados);
}
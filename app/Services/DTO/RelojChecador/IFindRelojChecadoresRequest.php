<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\RelojChecador;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindRelojChecadoresRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getSerial(): ?string;
    public function setSerial(?string $serial);

    public function getIp(): ?string;
    public function setIp(?string $ip);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;
    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;
    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;
    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;
    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;
    public function isIncludeEmpleados(): ?bool;
    public function setIncludeEmpleados(?string $include_empleados): void;
}
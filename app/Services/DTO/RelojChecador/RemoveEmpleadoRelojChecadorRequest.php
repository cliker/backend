<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\RelojChecador;
use App\Services\DTO\DataTransferObject;


class RemoveEmpleadoRelojChecadorRequest extends DataTransferObject implements IRemoveEmpleadoRelojChecadorRequest
{
    protected $id = 0;
    protected $empleado_id = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param int|null $empleado_id
     */
    public function setEmpleadoId(?int $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }
}
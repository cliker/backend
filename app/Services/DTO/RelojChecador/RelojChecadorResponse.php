<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\RelojChecador;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\RelojChecador\IRelojChecadorResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;


class RelojChecadorResponse extends DataTransferObject implements IRelojChecadorResponse
{
    protected $id = 0;
    protected $serial = '';
    protected $ip = '';
    protected $empresa = null;
    protected $sucursal = null;
    protected $departamento = null;
    protected $activo = true;
    protected $cliente = null;
    protected $descripcion = "";
    protected $empleados = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSerial(): ?string
    {
        return $this->serial;
    }

    /**
     * @param string $serial
     */
    public function setSerial(?string $serial)
    {
        $this->serial = $serial;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }


    /**
     * @param string|null $ip
     */
    public function setIp(?string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return IEmpresaResponse
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return IDepartamentoResponse
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param IDepartamentoResponse $departamento
     */
    public function setDepartamento(?string $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return array|null
     */
    public function getEmpleados(): ?array
    {
        return $this->empleados;
    }

    /**
     * @param array|null $empleados
     */
    public function setEmpleados(?array $empleados)
    {
        $this->empleados = $empleados;
    }
}
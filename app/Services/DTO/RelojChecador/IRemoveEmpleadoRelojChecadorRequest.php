<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/26/19
 * Time: 4:03 PM
 */

namespace App\Services\DTO\RelojChecador;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IRemoveEmpleadoRelojChecadorRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?int $empleado_id);

}
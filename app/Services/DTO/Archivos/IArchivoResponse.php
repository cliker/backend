<?php
namespace App\Services\DTO\Archivos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IArchivoResponse extends DataTransferObjectInterface
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre): void;
    public function getSuccess(): ?bool;
    public function setSuccess(?bool $success): void;
    public function getExtension(): ?string;
    public function setExtension(?string $extension): void;
    public function getUrl(): ?string;
    public function setUrl(?string $url): void;
    public function getMessage(): ?string;
    public function setMessage(?string $message): void;
}
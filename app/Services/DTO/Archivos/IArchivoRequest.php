<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/31/19
 * Time: 3:25 PM
 */
namespace App\Services\DTO\Archivos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IArchivoRequest extends DataTransferObjectInterface
{
    public function getDocumentoId(): ?int;
    public function setDocumentoId(?string $documento_id);

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id);

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id);

    public function getArchivo();
    public function setArchivo($archivo): void;
}
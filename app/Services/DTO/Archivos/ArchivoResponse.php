<?php
namespace App\Services\DTO\Archivos;

use App\Services\DTO\DataTransferObject;

class ArchivoResponse extends DataTransferObject implements IArchivoResponse
{
    protected $nombre;
    protected $extension;
    protected $url;
    protected $success = false;
    protected $message = '';

    /**
     * @return mixed
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre(?string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension(?string $extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getSuccess(): ?bool
    {
        return $this->success;
    }

    /**
     * @param mixed $success
     */
    public function setSuccess(?bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @return mixed
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
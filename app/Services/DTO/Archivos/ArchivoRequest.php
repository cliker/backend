<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:11 PM
 */

namespace App\Services\DTO\Archivos;
use App\Services\DTO\DataTransferObject;


class ArchivoRequest extends DataTransferObject implements IArchivoRequest
{
    protected $documento_id = 0;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;
    protected $departamento_id = 0;
    protected $empleado_id = 0;
    protected $archivo = null;

    /**
     * @return int|null
     */
    public function getDocumentoId(): ?int
    {
        return $this->documento_id;
    }

    /**
     * @param string|null $documento_id
     */
    public function setDocumentoId(?string $documento_id)
    {
        $this->documento_id =  $documento_id;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?string $cliente_id)
    {
        $this->cliente_id =  $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?string $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string|null $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return null
     */
    public function getArchivo()
    {
        return $this->archivo;
    }

    /**
     * @param null $archivo
     */
    public function setArchivo($archivo): void
    {
        $this->archivo = $archivo;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\Departamentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IDepartamentoRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getEncargado(): ?string;
    public function setEncargado(?string $telefono);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getDiasFestivos(): ?array;
    public function setDiasFestivos(?array $dias_festivos);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);
}
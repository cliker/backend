<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\Departamentos;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;


class DepartamentoResponse extends DataTransferObject implements IDepartamentoResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $encargado = '';
    protected $descripcion = '';
    protected $empresa = null;
    protected $sucursal = null;
    protected $activo = true;
    protected $dias_festivos = null;
    protected $cliente = null;
    protected $correo = '';

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getEncargado(): ?string
    {
        return $this->encargado;
    }

    /**
     * @param string $encargado
     */
    public function setEncargado(?string $encargado)
    {
        $this->encargado = $encargado;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return IEmpresaResponse
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return array|null
     */
    public function getDiasFestivos(): ?array
    {
        return $this->dias_festivos;
    }

    /**
     * @param array|null $dias_festivos
     */

    public function setDiasFestivos(?array $dias_festivos)
    {
        $this->dias_festivos = $dias_festivos;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }
}
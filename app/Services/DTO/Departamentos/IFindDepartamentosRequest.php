<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\Departamentos;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindDepartamentosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getEncargado(): ?string;
    public function setEncargado(?string $encargado);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);
    
    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;
    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;
    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function isIncludeDiasFestivos(): ?bool;
    public function setIncludeDiasFestivos(?string $include_dias_festivos): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;
    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;
}
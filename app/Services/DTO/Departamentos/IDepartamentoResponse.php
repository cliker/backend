<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\Departamentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;
use Illuminate\Contracts\Container\Container;

interface IDepartamentoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getEncargado(): ?string;
    public function setEncargado(?string $encargado);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function  getSucursal(): ?ISucursalResponse;
    public function  setSucursal(?ISucursalResponse $sucursal);

    public function  getEmpresa(): ?IEmpresaResponse;
    public function  setEmpresa(?IEmpresaResponse $empresa);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getDiasFestivos(): ?array;
    public function setDiasFestivos(?array $dias_festivos);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);
}
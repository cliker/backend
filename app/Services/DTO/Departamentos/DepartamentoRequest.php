<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\Departamentos;
use App\Services\DTO\DataTransferObject;


class DepartamentoRequest extends DataTransferObject implements IDepartamentoRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $encargado = '';
    protected $descripcion = '';
    protected $sucursal_id = 0;
    protected $empresa_id = 0;
    protected $activo = true;
    protected $dias_festivos = null;
    protected $cliente_id = 0;
    protected $correo = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getEncargado(): ?string
    {
        return $this->encargado;
    }

    /**
     * @param string|null $encargado
     */
    public function setEncargado(?string $encargado)
    {
        $this->encargado = $encargado;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return array|null
     */
    public function getDiasFestivos(): ?array
    {
        return $this->dias_festivos;
    }

    /**
     * @param array|null $dias_festivos
     */

    public function setDiasFestivos(?array $dias_festivos)
    {
        $this->dias_festivos = $dias_festivos;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Departamentos;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;
use function Functional\false;


class FindDepartamentosRequest extends DataTransferObject implements IFindDepartamentosRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $encargado = '';
    protected $descripcion = '';
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $activo = '';
    protected $include_dias_festivos = false;
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $correo = '';

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return string
     */
    public function getEncargado(): ?string
    {
        return $this->encargado;
    }

    /**
     * @param string $encargado
     */
    public function setEncargado(?string $encargado)
    {
        $this->encargado = $encargado;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return StringExtensions::toInt($this->sucursal_id);
    }

    /**
     * @param null|string $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param null|string $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return StringExtensions::toInt($this->empresa_id);
    }

    /**
     * @param null|string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param null|string $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDiasFestivos(): ?bool
    {
        return StringExtensions::toBoolean($this->include_dias_festivos);
    }

    /**
     * @param string|null $include_dias_festivos
     */
    public function setIncludeDiasFestivos(?string $include_dias_festivos): void
    {
        $this->include_dias_festivos = $include_dias_festivos;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return StringExtensions::toInt($this->cliente_id);
    }

    /**
     * @param null|string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param null|string $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }
}
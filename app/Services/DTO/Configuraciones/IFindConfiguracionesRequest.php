<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\Configuraciones;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindConfiguracionesRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getClave(): ?string;
    public function setClave(?string $clave);

    public function getValor(): ?string;
    public function setValor(?string $valor);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getActivo():? string;
    public function setActivo(?string $activo): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;
    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;
    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;
}
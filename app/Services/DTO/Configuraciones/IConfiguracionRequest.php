<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\Configuraciones;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IConfiguracionRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getClave(): ?string;
    public function setClave(?string $clave);

    public function getValor(): ?string;
    public function setValor(?string $valor);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);
}
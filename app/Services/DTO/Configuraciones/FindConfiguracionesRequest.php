<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Configuraciones;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindConfiguracionesRequest extends DataTransferObject implements IFindConfiguracionesRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $clave = '';
    protected $valor = '';
    protected $descripcion = '';
    protected $activo = '';
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;


    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getClave(): ?string
    {
        return $this->clave;
    }

    /**
     * @param string $clave
     */
    public function setClave(?string $clave)
    {
        $this->clave = $clave;
    }

    /**
     * @return string
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(?string $valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return StringExtensions::toInt($this->sucursal_id);
    }

    /**
     * @param null|string $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param null|string $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return StringExtensions::toInt($this->empresa_id);
    }

    /**
     * @param null|string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param null|string $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }
}
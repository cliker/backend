<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:02 AM
 */

namespace App\Services\DTO\RegistrosChecadores;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IRegistroChecadorRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getIdAsp(): ?int;
    public function setIdAsp(?int $id_asp);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getHora(): ?string;
    public function setHora(?string $hora);

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?int $empleado_id);

    public function getRelojChecadorId(): ?int;
    public function setRelojChecadorId(?int $reloj_checador_id);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?int $departamento_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);
}
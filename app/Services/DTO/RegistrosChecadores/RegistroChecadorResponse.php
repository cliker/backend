<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 10:41 AM
 */

namespace App\Services\DTO\RegistrosChecadores;

use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\RelojChecador\IRelojChecadorResponse;

class RegistroChecadorResponse extends DataTransferObject implements IRegistroChecadorResponse
{

    protected $id = 0;
    protected $id_asp = 0;
    protected $fecha = '';
    protected $hora = '';
    protected $empleado = null;
    protected $reloj_checador = null;
    protected $cliente = null;
    protected $empresa = null;
    protected $sucursal = null;
    protected $departamento = null;
    protected $activo = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdAsp(): ?int
    {
        return $this->id_asp;
    }

    /**
     * @param int|null $id_asp
     */
    public function setIdAsp(?int $id_asp)
    {
        $this->id_asp = $id_asp;
    }

    /**
     * @return string|null
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string|null $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string|null
     */
    public function getHora(): ?string
    {
        return $this->hora;
    }

    /**
     * @param string|null $hora
     */
    public function setHora(?string $hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return IEmpleadoResponse|null
     */
    public function getEmpleado(): ?IEmpleadoResponse
    {
        return $this->empleado;
    }

    /**
     * @param string|null $empleado
     */
    public function setEmpleado(?IEmpleadoResponse $empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return IRelojChecadorResponse|null
     */
    public function getRelojChecador(): ?IRelojChecadorResponse
    {
        return $this->reloj_checador;
    }

    /**
     * @param string|null $reloj_checador
     */
    public function setRelojChecador(?IRelojChecadorResponse $reloj_checador)
    {
        $this->reloj_checador = $reloj_checador;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param string|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param string|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param string|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return IDepartamentoResponse|null
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param string|null $departamento
     */
    public function setDepartamento(?IDepartamentoResponse $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:08 AM
 */

namespace App\Services\DTO\RegistrosChecadores;
use App\Services\DTO\DataTransferObject;

class RegistroChecadorAspRequest extends DataTransferObject implements IRegistroChecadorAspRequest
{
    protected $id = 0;
    protected $clave_usuario = 0;
    protected $empresa_id = 0;
    protected $registro = "";

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getClaveUsuario(): ?int
    {
        return $this->clave_usuario;
    }

    /**
     * @param int|null $id
     */
    public function setClaveUsuario(?int $clave_usuario)
    {
        $this->clave_usuario = $clave_usuario;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return string|null
     */
    public function getRegistro(): ?string
    {
        return $this->registro;
    }

    /**
     * @param string|null $registro
     */
    public function setRegistro(?string $registro)
    {
        $this->registro = $registro;
    }
}
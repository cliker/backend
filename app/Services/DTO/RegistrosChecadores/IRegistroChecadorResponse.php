<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 10:36 AM
 */
namespace App\Services\DTO\RegistrosChecadores;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\RelojChecador\IRelojChecadorResponse;

interface IRegistroChecadorResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getIdAsp(): ?int;
    public function setIdAsp(?int $id_asp);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getHora(): ?string;
    public function setHora(?string $hora);

    public function getEmpleado(): ?IEmpleadoResponse;
    public function setEmpleado(?IEmpleadoResponse $empleado);

    public function getRelojChecador(): ?IRelojChecadorResponse;
    public function setRelojChecador(?IRelojChecadorResponse $reloj_checador);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getSucursal(): ?ISucursalResponse;
    public function setSucursal(?ISucursalResponse $sucursal);

    public function getDepartamento(): ?IDepartamentoResponse;
    public function setDepartamento(?IDepartamentoResponse $departamento);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);
}
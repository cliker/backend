<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:25 AM
 */

namespace App\Services\DTO\RegistrosChecadores;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindRegistrosChecadores extends DataTransferObject implements IFindRegistrosChecadores
{
    use TFindBaseRequest;

    protected $fecha = '';
    protected $min_fecha = '';
    protected $max_fecha = '';
    protected $hora = '';
    protected $min_hora = '';
    protected $max_hora = '';
    protected $empleado_id = 0;
    protected $include_empleado = false;
    protected $reloj_checador_id = 0;
    protected $include_reloj_checador = false;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;
    protected $departamento_id = 0;
    protected $include_cliente = false;
    protected $include_empresa = false;
    protected $include_sucursal = false;
    protected $include_departamento = false;
    protected $activo = '';

    /**
     * @return string|null
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string|null $fecha
     */
    public function setFecha(?string $fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string|null
     */
    public function getMinFecha(): ?string
    {
        return $this->min_fecha;
    }

    /**
     * @param string|null $min_fecha
     */
    public function setMinFecha(?string $min_fecha): void
    {
        $this->min_fecha = $min_fecha;
    }

    /**
     * @return string|null
     */
    public function getMaxFecha(): ?string
    {
        return $this->max_fecha;
    }

    /**
     * @param string|null $max_fecha
     */
    public function setMaxFecha(?string $max_fecha): void
    {
        $this->max_fecha = $max_fecha;
    }

    /**
     * @return string|null
     */
    public function getHora(): ?string
    {
        return $this->hora;
    }

    /**
     * @param string|null $hora
     */
    public function setHora(?string $hora):void
    {
        $this->hora = $hora;
    }

    /**
     * @return string|null
     */
    public function getMinHora(): ?string
    {
        return $this->min_hora;
    }

    /**
     * @param string|null $min_hora
     */
    public function setMinHora(?string $min_hora):void
    {
        $this->min_hora = $min_hora;
    }

    /**
     * @return string|null
     */
    public function getMaxHora(): ?string
    {
        return $this->max_hora;
    }

    /**
     * @param string|null $max_hora
     */
    public function setMaxHora(?string $max_hora):void
    {
        $this->max_hora = $max_hora;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpleado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empleado);
    }

    /**
     * @param string|null $include_empleado
     */
    public function setIncludeEmpleado(?string $include_empleado): void
    {
        $this->include_empleado = $include_empleado;
    }

    /**
     * @return int|null
     */
    public function getRelojChecadorId(): ?int
    {
        return $this->reloj_checador_id;
    }

    /**
     * @param string|null $id
     */
    public function setRelojChecadorId(?string $reloj_checador_id): void
    {
        $this->reloj_checador_id = $reloj_checador_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeRelojChecador(): ?bool
    {
        return StringExtensions::toBoolean($this->include_reloj_checador);
    }

    /**
     * @param string|null $include_reloj_checador
     */
    public function setIncludeRelojChecador(?string $include_reloj_checador): void
    {
        $this->include_reloj_checador = $include_reloj_checador;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string|null $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return string|null
     */
    public function getActivo():?string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }
}
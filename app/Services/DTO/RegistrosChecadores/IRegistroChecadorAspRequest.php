<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:02 AM
 */

namespace App\Services\DTO\RegistrosChecadores;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IRegistroChecadorAspRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getClaveUsuario(): ?int;
    public function setClaveUsuario(?int $clave_usuario);

    public function getRegistro(): ?string;
    public function setRegistro(?string $registro);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:12 AM
 */

namespace App\Services\DTO\RegistrosChecadores;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindRegistrosChecadores extends DataTransferObjectInterface, IFindBaseRequest
{

    public function getFecha(): ?string;
    public function setFecha(?string $fecha): void;
    public function getMinFecha(): ?string;
    public function setMinFecha(?string $min_fecha): void;
    public function getMaxFecha(): ?string;
    public function setMaxFecha(?string $max_fecha): void;

    public function getHora(): ?string;
    public function setHora(?string $hora): void;
    public function getMinHora(): ?string;
    public function setMinHora(?string $min_hora): void;
    public function getMaxHora(): ?string;
    public function setMaxHora(?string $max_hora): void;

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;
    public function isIncludeEmpleado(): ?bool;
    public function setIncludeEmpleado(?string $include_empleado): void;

    public function getRelojChecadorId(): ?int;
    public function setRelojChecadorId(?string $reloj_checador_id): void;
    public function isIncludeRelojChecador(): ?bool;
    public function setIncludeRelojChecador(?string $include_reloj_checador): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;
    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;
    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;
    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;
    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;
}
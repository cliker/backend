<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 11:08 AM
 */

namespace App\Services\DTO\RegistrosChecadores;
use App\Services\DTO\DataTransferObject;

class RegistroChecadorRequest extends DataTransferObject implements IRegistroChecadorRequest
{
    protected $id = 0;
    protected $fecha = '';
    protected $hora = '';
    protected $empleado_id = 0;
    protected $reloj_checador_id = 0;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;
    protected $departamento_id = 0;
    protected $activo = false;
    protected $id_asp = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getIdAsp(): ?int
    {
        return $this->id_asp;
    }

    /**
     * @param int|null $id_asp
     */
    public function setIdAsp(?int $id_asp)
    {
        $this->id_asp = $id_asp;
    }

    /**
     * @return string|null
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string|null $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string|null
     */
    public function getHora(): ?string
    {
        return $this->hora;
    }

    /**
     * @param string|null $hora
     */
    public function setHora(?string $hora)
    {
        $this->hora = $hora;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param int|null $empleado_id
     */
    public function setEmpleadoId(?int $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return int|null
     */
    public function getRelojChecadorId(): ?int
    {
        return $this->reloj_checador_id;
    }

    /**
     * @param int|null $reloj_checador_id
     */
    public function setRelojChecadorId(?int $reloj_checador_id)
    {
        $this->reloj_checador_id = $reloj_checador_id;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param int|null $departamento_id
     */
    public function setDepartamentoId(?int $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/28/19
 * Time: 10:58 AM
 */

namespace App\Services\DTO\CEstatusIncidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseRequest;

interface ICEstatusIncidenciaRequest extends DataTransferObjectInterface, ICatalogoBaseRequest
{

}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/28/19
 * Time: 10:57 AM
 */

namespace App\Services\DTO\CEstatusIncidencias;
use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;

class CEstatusIncidenciaResponse extends CatalogoBaseResponse implements ICEstatusIncidenciaResponse
{

}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/28/19
 * Time: 11:03 AM
 */

namespace App\Services\DTO\CEstatusIncidencias;
use App\Services\DTO\CatalogosBase\FindCatalogosBaseRequest;

class FindCEstatusIncidenciasRequest extends FindCatalogosBaseRequest implements IFindCEstatusIncidenciasRequest
{

}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/28/19
 * Time: 11:02 AM
 */

namespace App\Services\DTO\CEstatusIncidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;
use App\Services\DTO\CatalogosBase\IFindCatalogosBaseRequest;

interface IFindCEstatusIncidenciasRequest extends DataTransferObjectInterface, IFindBaseRequest, IFindCatalogosBaseRequest
{

}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\Sucursales;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface ISucursalRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstadoId(): ?int;
    public function setEstadoId(?int $estado_id);

    public function getCiudadId(): ?int;
    public function setCiudadId(?int $ciudad_id);

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);
}
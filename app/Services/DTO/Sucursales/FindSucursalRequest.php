<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Sucursales;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindSucursalRequest extends DataTransferObject implements IFindSucursalesRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $direccion = '';
    protected $estado_id = 0;
    protected $include_estado = false;
    protected $ciudad_id = 0;
    protected $include_ciudad = false;
    protected $codigo_postal = "";
    protected $descripcion = "";
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $diaFestivo_id = 0;
    protected $include_dia_festivo = false;
    protected $activo = '';
    protected $cliente_id = 0;
    protected $include_cliente = false;


    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return StringExtensions::toInt($this->estado_id);
    }

    /**
     * @param null|string $estado_id
     */
    public function setEstadoId(?string $estado_id): void
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estado);
    }

    /**
     * @param null|string $include_estado
     */
    public function setIncludeEstado(?string $include_estado): void
    {
        $this->include_estado = $include_estado;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return StringExtensions::toInt($this->ciudad_id);
    }

    /**
     * @param null|string $ciudad_id
     */
    public function setCiudadId(?string $ciudad_id): void
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCiudad(): ?bool
    {
        return StringExtensions::toBoolean($this->include_ciudad);
    }

    /**
     * @param null|string $include_ciudad
     */
    public function setIncludeCiudad(?string $include_ciudad): void
    {
        $this->include_ciudad = $include_ciudad;
    }

    /**
     * @return string
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return StringExtensions::toInt($this->empresa_id);
    }

    /**
     * @param null|string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param null|string $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getDiaFestivoId(): ?int
    {
        return StringExtensions::toInt($this->diaFestivo_id);
    }

    /**
     * @param null|string $diaFestivo_id
     */
    public function setDiaFestivoId(?string $diaFestivo_id): void
    {
        $this->diaFestivo_id = $diaFestivo_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDiaFestivo(): ?bool
    {
        return StringExtensions::toBoolean($this->include_dia_festivo);
    }

    /**
     * @param null|string $include_dia_festivo
     */
    public function setIncludeDiaFestivo(?string $include_dia_festivo): void
    {
        $this->include_dia_festivo = $include_dia_festivo;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return StringExtensions::toInt($this->cliente_id);
    }

    /**
     * @param null|string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param null|string $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }
}
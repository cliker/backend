<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\Sucursales;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;


class SucursalResponse extends DataTransferObject implements ISucursalResponse
{
    protected $id = 0;
    protected $nombre = "";
    protected $direccion = "";
    protected $estado = null;
    protected $ciudad = null;
    protected $codigo_postal = "";
    protected $descripcion = "";
    protected $empresa = null;
    protected $dias_festivos = null;
    protected $activo = true;
    protected $cliente = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return ICEstadoResponse|null
     */
    public function getEstado(): ?ICEstadoResponse
    {
        return $this->estado;
    }

    /**
     * @param ICEstadoResponse|null $estado
     */
    public function setEstado(?ICEstadoResponse $estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return ICCiudadResponse|null
     */
    public function getCiudad(): ?ICCiudadResponse
    {
        return $this->ciudad;
    }

    /**
     * @param ICCiudadResponse|null $ciudad
     */
    public function setCiudad(?ICCiudadResponse $ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return string
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return array|null
     */
    public function getDiasFestivos(): ?array
    {
        return $this->empresa;
    }

    /**
     * @param array|null $dias_festivos
     */
    public function setDiasFestivos(?array $dias_festivos)
    {
        $this->dias_festivos = $dias_festivos;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }
}
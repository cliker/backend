<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:14 AM
 */

namespace App\Services\DTO\Sucursales;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;

interface ISucursalResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstado(): ?ICEstadoResponse;
    public function setEstado(?ICEstadoResponse $estado);

    public function getCiudad(): ?ICCiudadResponse;
    public function setCiudad(?ICCiudadResponse $ciudad);

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getDiasFestivos(): ?array ;
    public function setDiasFestivos(?array $dias_festivos);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

}
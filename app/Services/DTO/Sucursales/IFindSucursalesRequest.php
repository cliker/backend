<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\Sucursales;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindSucursalesRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstadoId(): ?int;
    public function setEstadoId(?string $estado_id): void;
    public function isIncludeEstado(): ?bool;
    public function setIncludeEstado(?string $include_estado): void;

    public function getCiudadId(): ?int;
    public function setCiudadId(?string $ciudad_id): void;
    public function isIncludeCiudad(): ?bool;
    public function setIncludeCiudad(?string $include_ciudad): void;

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;
    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getDiaFestivoId(): ?int;
    public function setDiaFestivoId(?string $diaFestivo_id): void;
    public function isIncludeDiaFestivo(): ?bool;
    public function setIncludeDiaFestivo(?string $include_dia_festivo): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;
    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\Sucursales;
use App\Services\DTO\DataTransferObject;


class SucursalRequest extends DataTransferObject implements ISucursalRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $direccion = '';
    protected $estado_id = 0;
    protected $ciudad_id = 0;
    protected $codigo_postal = '';
    protected $descripcion = '';
    protected $empresa_id = 0;
    protected $activo = true;
    protected $cliente_id= 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }


    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setEstadoId(?int $estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return $this->ciudad_id;
    }

    /**
     * @param int|null $ciudad_id
     */
    public function setCiudadId(?int $ciudad_id)
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return string|null
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string|null $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getDiaFestivoId(): ?int
    {
        return $this->diaFestivo_id;
    }

    /**
     * @param int|null $diaFestivo_id
     */
    public function setDiaFestivoId(?int $diaFestivo_id)
    {
        $this->diaFestivo_id = $diaFestivo_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:21 PM
 */

namespace App\Services\DTO\Documentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindDocumentosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getTipoId(): ?int;
    public function setTipoId(?string $tipo_id): void;

    public function isIncludeTipo(): ?bool;
    public function setIncludeTipo(?string $include_tipo): void;

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;

    public function isIncludeEmpleado(): ?bool;
    public function setIncludeEmpleado(?string $include_empleado): void;

    public function getActivo(): ?string ;
    public function setActivo(?string $activo): void;
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:08 PM
 */

namespace App\Services\DTO\Documentos;
use App\Services\DTO\CTiposDocumentos\ICTipoDocumentoResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Empleados\IEmpleadoResponse;

class DocumentoResponse extends DataTransferObject implements IDocumentoResponse
{

    protected $id = 0;
    protected $nombre = '';
    protected $extension = '';
    protected $url = '';
    protected $descripcion = '';
    protected $tipo = null;
    protected $activo = false;
    protected $empleado = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return ICTipoDocumentoResponse|null
     */
    public function getTipo(): ?ICTipoDocumentoResponse
    {
        return $this->tipo;
    }

    /**
     * @param ICTipoDocumentoResponse|null $tipo
     */
    public function setTipo(?ICTipoDocumentoResponse $tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IEmpleadoResponse|null
     */
    public function getEmpleado(): ?IEmpleadoResponse
    {
        return $this->empleado;
    }

    /**
     * @param IEmpleadoResponse|null $activo
     */
    public function setEmpleado(?IEmpleadoResponse $empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string|null $extension
     */
    public function setExtension(?string $extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;
    }
}
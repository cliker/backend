<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:03 PM
 */
namespace App\Services\DTO\Documentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CTiposDocumentos\ICTipoDocumentoResponse;
use App\Services\DTO\Empleados\IEmpleadoResponse;

interface IDocumentoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getExtension(): ?string;
    public function setExtension(?string $extension);

    public function getUrl(): ?string;
    public function setUrl(?string $url);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getTipo(): ?ICTipoDocumentoResponse;
    public function setTipo(?ICTipoDocumentoResponse $tipo);

    public function getEmpleado(): ?IEmpleadoResponse;
    public function setEmpleado(?IEmpleadoResponse $empleado);

    public function getActivo(): ?bool;
    public function setActivo(?bool $activo);
}
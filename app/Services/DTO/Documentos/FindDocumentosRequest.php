<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:26 PM
 */

namespace App\Services\DTO\Documentos;
use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindDocumentosRequest extends DataTransferObject implements IFindDocumentosRequest
{
    use TFindBaseRequest;

    protected $tipo_id = 0;
    protected $include_tipo = false;
    protected $empleado_id = 0;
    protected $include_empleado = false;
    protected $activo = '';

    /**
     * @return int|null
     */
    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    /**
     * @param string|null $tipo_id
     */
    public function setTipoId(?string $tipo_id): void
    {
        $this->tipo_id = $tipo_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeTipo(): ?bool
    {
        return StringExtensions::toBoolean($this->include_tipo);
    }

    /**
     * @param string|null $include_tipo
     */
    public function setIncludeTipo(?string $include_tipo): void
    {
        $this->include_tipo = $include_tipo;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpleado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empleado);
    }

    /**
     * @param string|null $empleado_id
     */
    public function setIncludeEmpleado(?string $include_empleado): void
    {
        $this->include_empleado = $include_empleado;
    }

    /**
     * @return string|null
     */
    public function getActivo(): ?string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }
}
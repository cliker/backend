<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:15 PM
 */

namespace App\Services\DTO\Documentos;
use App\Services\DTO\DataTransferObject;

class DocumentoRequest extends DataTransferObject implements IDocumentoRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $extension = '';
    protected $url = '';
    protected $descripcion = '';
    protected $tipo_id = 0;
    protected $empleado_id = 0;
    protected $activo = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    /**
     * @param int|null $tipo_id
     */
    public function setTipoId(?int $tipo_id)
    {
        $this->tipo_id = $tipo_id;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param int|null $empleado_id
     */
    public function setEmpleadoId(?int $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return string|null
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string|null $extension
     */
    public function setExtension(?string $extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     */
    public function setUrl(?string $url)
    {
        $this->url = $url;
    }
}
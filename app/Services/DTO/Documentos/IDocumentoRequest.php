<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/4/19
 * Time: 5:14 PM
 */

namespace App\Services\DTO\Documentos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IDocumentoRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getExtension(): ?string;
    public function setExtension(?string $extension);

    public function getUrl(): ?string;
    public function setUrl(?string $url);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getTipoId(): ?int;
    public function setTipoId(?int $tipo_id);

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?int $empleado_id);

    public function getActivo(): ?bool;
    public function setActivo(?bool $activo);
}
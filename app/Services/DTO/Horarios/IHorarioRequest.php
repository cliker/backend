<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 10:40 AM
 */

namespace App\Services\DTO\Horarios;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IHorarioRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getDetallesHorario(): ?array;
    public function setDetallesHorario(?array $detalles_horario);

    public function getMinutosTolerancia(): ?int;
    public function setMinutosTolerancia(?int $minutos_tolerancia);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?int $departamento_id);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getDetallesEliminados(): ?array;
    public function setDetallesEliminados(?array $detalles_eliminados);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);
}
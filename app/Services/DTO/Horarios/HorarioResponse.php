<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 10:33 AM
 */

namespace App\Services\DTO\Horarios;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;


class HorarioResponse extends DataTransferObject implements IHorarioResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $descripcion = '';
    protected $detalles_horarios = null;
    protected $minutos_tolerancia = 0;
    protected $activo = false;
    protected $departamento = null;
    protected $cliente = null;
    protected $empresa = null;
    protected $sucursal = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return array|null
     */
    public function getDetallesHorarios(): ?array
    {
        return $this->detalles_horarios;
    }

    /**
     * @param array|null $detalles_horarios
     */
    public function setDetallesHorarios(?array $detalles_horarios)
    {
        $this->detalles_horarios = $detalles_horarios;
    }

    /**
     * @return int|null
     */
    public function getMinutosTolerancia(): ?int
    {
        return $this->minutos_tolerancia;
    }

    /**
     * @param int|null $minutos_tolerancia
     */
    public function setMinutosTolerancia(?int $minutos_tolerancia)
    {
        $this->minutos_tolerancia = $minutos_tolerancia;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IDepartamentoResponse|null
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param IDepartamentoResponse|null $departamento
     */
    public function setDepartamento(?IDepartamentoResponse $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }
}
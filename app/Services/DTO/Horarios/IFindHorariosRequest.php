<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 1:13 PM
 */

namespace App\Services\DTO\Horarios;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindHorariosRequest extends DataTransferObjectInterface, IFindBaseRequest
{

    public function getNombre(): ?string;
    public function setNombre(?string $nombre): void;

    public function isIncludeDetallesHorario(): ?bool;
    public function setIncludeDetallesHorario(?string $include_detalles_horario): void;

    public function getMinutosTolerancia(): ?int;
    public function setMinutosTolerancia(?string $minutos_tolerancia): void;

    public function getMinMinutosTolerancia(): ?int;
    public function setMinMinutosTolerancia(?string $min_minutos_tolerancia): void;

    public function getMaxMinutosTolerancia(): ?int;
    public function setMaxMinutosTolerancia(?string $max_minutos_tolerancia): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;

    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    public function getActivo():? string;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;

    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;

    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;
}
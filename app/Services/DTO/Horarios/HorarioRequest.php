<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 1:05 PM
 */

namespace App\Services\DTO\Horarios;

use App\Services\DTO\DataTransferObject;

class HorarioRequest extends DataTransferObject implements IHorarioRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $descripcion = '';
    protected $detalles_horario = null;
    protected $minutos_tolerancia = 0;
    protected $activo = false;
    protected $departamento_id = 0;
    protected $detalles_eliminados = null;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return array|null
     */
    public function getDetallesHorario(): ?array
    {
        return $this->detalles_horario;
    }

    /**
     * @param array|null $detalles_horario
     */
    public function setDetallesHorario(?array $detalles_horario)
    {
        $this->detalles_horario = $detalles_horario;
    }

    /**
     * @return int|null
     */
    public function getMinutosTolerancia(): ?int
    {
        return $this->minutos_tolerancia;
    }

    /**
     * @param int|null $minutos_tolerancia
     */
    public function setMinutosTolerancia(?int $minutos_tolerancia)
    {
        $this->minutos_tolerancia = $minutos_tolerancia;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param bool|null $activo
     */
    public function setDepartamentoId(?int $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return array|null
     */
    public function getDetallesEliminados(): ?array
    {
        return $this->detalles_eliminados;
    }

    /**
     * @param array|null $detalles_eliminados
     */
    public function setDetallesEliminados(?array $detalles_eliminados)
    {
        $this->detalles_eliminados = $detalles_eliminados;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }
}
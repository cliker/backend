<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 10:22 AM
 */

namespace App\Services\DTO\Horarios;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;


interface IHorarioResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getDetallesHorarios(): ?array;
    public function setDetallesHorarios(?array $detalles_horarios);

    public function getMinutosTolerancia(): ?int;
    public function setMinutosTolerancia(?int $minutos_tolerancia);

    public function getDepartamento(): ?IDepartamentoResponse;
    public function setDepartamento(?IDepartamentoResponse $departamento);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getSucursal(): ?ISucursalResponse;
    public function setSucursal(?ISucursalResponse $sucursal);
}
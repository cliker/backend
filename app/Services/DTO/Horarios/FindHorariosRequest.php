<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/2/19
 * Time: 2:55 PM
 */

namespace App\Services\DTO\Horarios;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindHorariosRequest extends DataTransferObject implements IFindHorariosRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $include_detalles_horario = false;
    protected $minutos_tolerancia = 0;
    protected $min_minutos_tolerancia = 0;
    protected $max_minutos_tolerancia = 0;
    protected $departamento_id = 0;
    protected $include_departamento = false;
    protected $activo = '';
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDetallesHorario(): ?bool
    {
        return StringExtensions::toBoolean($this->include_detalles_horario);
    }

    /**
     * @param string|null $include_detalles_horario
     */
    public function setIncludeDetallesHorario(?string $include_detalles_horario): void
    {
        $this->include_detalles_horario = $include_detalles_horario;
    }

    /**
     * @return int|null
     */
    public function getMinutosTolerancia(): ?int
    {
        return $this->minutos_tolerancia;
    }

    /**
     * @param string|null $minutos_tolerancia
     */
    public function setMinutosTolerancia(?string $minutos_tolerancia): void
    {
        $this->minutos_tolerancia = $minutos_tolerancia;
    }

    /**
     * @return int|null
     */
    public function getMinMinutosTolerancia(): ?int
    {
        return $this->min_minutos_tolerancia;
    }

    /**
     * @param string|null $min_minutos_tolerancia
     */
    public function setMinMinutosTolerancia(?string $min_minutos_tolerancia): void
    {
        $this->min_minutos_tolerancia = $min_minutos_tolerancia;
    }

    /**
     * @return int|null
     */
    public function getMaxMinutosTolerancia(): ?int
    {
        return $this->max_minutos_tolerancia;
    }

    /**
     * @param string|null $max_minutos_tolerancia
     */
    public function setMaxMinutosTolerancia(?string $max_minutos_tolerancia): void
    {
        $this->max_minutos_tolerancia = $max_minutos_tolerancia;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string|null $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    /**
     * @param string|null $include_departamento
     */
    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return string|null
     */
    public function getActivo(): ?string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param string|null $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }
}
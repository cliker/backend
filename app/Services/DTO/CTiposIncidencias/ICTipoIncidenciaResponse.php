<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 10:53 AM
 */

namespace App\Services\DTO\CTiposIncidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseResponse;

interface ICTipoIncidenciaResponse extends DataTransferObjectInterface, ICatalogoBaseResponse
{

}
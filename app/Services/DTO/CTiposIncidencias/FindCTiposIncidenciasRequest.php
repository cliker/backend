<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 10:59 AM
 */

namespace App\Services\DTO\CTiposIncidencias;

use App\Services\DTO\CatalogosBase\FindCatalogosBaseRequest;

class FindCTiposIncidenciasRequest extends FindCatalogosBaseRequest implements IFindCTiposIncidenciasRequest
{

}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 10:55 AM
 */

namespace App\Services\DTO\CTiposIncidencias;
use App\Services\DTO\CatalogosBase\CatalogoBaseResponse;


class CTipoIncidenciaResponse extends CatalogoBaseResponse implements ICTipoIncidenciaResponse
{

}
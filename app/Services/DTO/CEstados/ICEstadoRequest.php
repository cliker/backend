<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 11/12/18
 * Time: 01:04 PM
 */

namespace App\Services\DTO\CEstados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CatalogosBase\ICatalogoBaseRequest;

interface ICEstadoRequest extends DataTransferObjectInterface, ICatalogoBaseRequest
{
    public function getCiudades(): ?array;
    public function setCiudades(?array $ciudades);
}
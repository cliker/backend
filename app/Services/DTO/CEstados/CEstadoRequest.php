<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 18/12/18
 * Time: 10:06 AM
 */

namespace App\Services\DTO\CEstados;
use App\Services\DTO\CatalogosBase\CatalogoBaseRequest;


class CEstadoRequest extends CatalogoBaseRequest implements ICEstadoRequest
{
    protected $ciudades = null;

    /**
     * @return array|null
     */
    public function getCiudades(): ?array
    {
        return $this->ciudades;
    }

    /**
     * @param array|null $ciudades
     */
    public function setCiudades(?array $ciudades)
    {
        $this->ciudades = $ciudades;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 3:25 PM
 */

namespace App\Services\DTO\Incidencias;
use App\Services\DTO\DataTransferObject;

class IncidenciaRequest extends DataTransferObject implements IIncidenciaRequest
{
    protected $id = 0;
    protected $descripcion = '';
    protected $tipo_id = 0;
    protected $fecha_inicio = '';
    protected $fecha_final = '';
    protected $activo = false;
    protected $empleado_id = 0;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;
    protected $departamento_id = 0;
    protected $estatus_id = 0;
    protected $url_archivo = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    /**
     * @param int|null $tipo_id
     */
    public function setTipoId(?int $tipo_id)
    {
        $this->tipo_id = $tipo_id;
    }

    /**
     * @return string|null
     */
    public function getFechaInicio(): ?string
    {
        return $this->fecha_inicio;
    }

    /**
     * @param string|null $fecha_inicio
     */
    public function setFechaInicio(?string $fecha_inicio)
    {
        $this->fecha_inicio = $fecha_inicio;
    }

    /**
     * @return string|null
     */
    public function getFechaFinal(): ?string
    {
        return $this->fecha_final;
    }

    /**
     * @param string|null $fecha_final
     */
    public function setFechaFinal(?string $fecha_final)
    {
        $this->fecha_final = $fecha_final;
    }

    /**
     * @return string|null
     */
    public function getUrlArchivo():? string
    {
        return $this->url_archivo;
    }

    /**
     * @param string|null $url_archivo
     */
    public function setUrlArchivo(?string $url_archivo)
    {
        $this->url_archivo = $url_archivo;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param int|null $empleado_id
     */
    public function setEmpleadoId(?int $empleado_id)
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param int|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param int|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param int|null $departamento_id
     */
    public function setDepartamentoId(?int $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return int|null
     */
    public function getEstatusId(): ?int
    {
        return $this->estatus_id;
    }

    /**
     * @param int|null $estatus_id
     */
    public function setEstatusId(?int $estatus_id)
    {
        $this->estatus_id = $estatus_id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 3:33 PM
 */

namespace App\Services\DTO\Incidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindIncidenciasRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getTipoId(): ?int;
    public function setTipoId(?string $tipo_id);

    public function isIncludeTipo(): ?bool;
    public function setIncludeTipo(?string $include_tipo): void;

    public function getFechaInicio(): ?string;
    public function setFechaInicio(?string $fecha_inicio): void;

    public function getMinFechaInicio(): ?string;
    public function setMinFechaInicio(?string $fecha_inicio): void;

    public function getMaxFechaInicio(): ?string;
    public function setMaxFechaInicio(?string $fecha_inicio): void;

    public function getFechaFinal(): ?string;
    public function setFechaFinal(?string $fecha_final): void;

    public function getMinFechaFinal(): ?string;
    public function setMinFechaFinal(?string $fecha_final): void;

    public function getMaxFechaFinal(): ?string;
    public function setMaxFechaFinal(?string $fecha_final): void;

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;

    public function isIncludeEmpleado(): ?bool;
    public function setIncludeEmpleado(?string $include_empleado): void;

    public function getActivo(): ?string ;
    public function setActivo(?string $activo): void;

    public function getUrlArchivo(): ?string ;
    public function setUrlArchivo(?string $url_archivo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;

    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;

    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;

    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    public function getEstatusId(): ?int;
    public function setEstatusId(?string $estatus_id): void;

    public function isIncludeEstatus(): ?bool;
    public function setIncludeEstatus(?string $include_estatus): void;

    public function getEstatusValor(): ?string;
    public function setEstatusValor(?string $estatus_valor): void;
}
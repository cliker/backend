<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 4:02 PM
 */

namespace App\Services\DTO\Incidencias;
use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindIncidenciasRequest extends DataTransferObject implements IFindIncidenciasRequest
{
    use TFindBaseRequest;

    protected $id = 0;
    protected $tipo_id = 0;
    protected $include_tipo = false;
    protected $fecha_inicio = '';
    protected $min_fecha_inicio = '';
    protected $max_fecha_inicio = '';
    protected $fecha_final = '';
    protected $min_fecha_final = '';
    protected $max_fecha_final = '';
    protected $empleado_id = 0;
    protected $include_empleado = false;
    protected $activo = '';
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $departamento_id = 0;
    protected $include_departamento = false;
    protected $estatus_id = 0;
    protected $include_estatus = false;
    protected $estatus_valor = '';
    protected $url_archivo = '';

    /**
     * @return int|null
     */
    public function getTipoId(): ?int
    {
        return $this->tipo_id;
    }

    /**
     * @param string|null $tipo_id
     */
    public function setTipoId(?string $tipo_id): void
    {
        $this->tipo_id = $tipo_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeTipo(): ?bool
    {
        return StringExtensions::toBoolean($this->include_tipo);
    }

    public function setIncludeTipo(?string $include_tipo): void
    {
        $this->include_tipo = $include_tipo;
    }

    /**
     * @return string|null
     */
    public function getFechaInicio(): ?string
    {
        return $this->fecha_inicio;
    }

    /**
     * @param string|null $fecha_inicio
     */
    public function setFechaInicio(?string $fecha_inicio): void
    {
        $this->fecha_inicio = $fecha_inicio;
    }

    /**
     * @return string|null
     */
    public function getMinFechaInicio(): ?string
    {
        return $this->min_fecha_inicio;
    }

    /**
     * @param string|null $min_fecha_inicio
     */
    public function setMinFechaInicio(?string $min_fecha_inicio): void
    {
        $this->min_fecha_inicio = $min_fecha_inicio;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaInicio(): ?string
    {
        return $this->max_fecha_inicio;
    }

    /**
     * @param string|null $max_fecha_inicio
     */
    public function setMaxFechaInicio(?string $max_fecha_inicio): void
    {
        $this->max_fecha_inicio = $max_fecha_inicio;
    }

    /**
     * @return string|null
     */
    public function getFechaFinal(): ?string
    {
        return $this->fecha_final;
    }

    /**
     * @param string|null $fecha_final
     */
    public function setFechaFinal(?string $fecha_final): void
    {
        $this->fecha_final = $fecha_final;
    }

    /**
     * @return string|null
     */
    public function getMinFechaFinal(): ?string
    {
        return $this->min_fecha_final;
    }

    /**
     * @param string|null $min_fecha_final
     */
    public function setMinFechaFinal(?string $min_fecha_final): void
    {
        $this->min_fecha_final = $min_fecha_final;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaFinal(): ?string
    {
        return $this->max_fecha_final;
    }

    /**
     * @param string|null $max_fecha_final
     */
    public function setMaxFechaFinal(?string $max_fecha_final): void
    {
        $this->max_fecha_final = $max_fecha_final;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpleado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empleado);
    }

    /**
     * @param string|null $include_empleado
     */
    public function setIncludeEmpleado(?string $include_empleado): void
    {
        $this->include_empleado = $include_empleado;
    }

    /**
     * @return string|null
     */
    public function getUrlArchivo(): ?string
    {
        return $this->url_archivo;
    }

    /**
     * @param string|null $url_archivo
     */
    public function setUrlArchivo(?string $url_archivo): void
    {
        $this->url_archivo = $url_archivo;
    }

    /**
     * @return string|null
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursa_id
     */
    public function setSucursalId(?string $sucursa_id): void
    {
        $this->sucursal_id = $sucursa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param string|null $include_sucursal
     */
    public function setIncludeSucursal(?string $include_empresa): void
    {
        $this->include_sucursal = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    /**
     * @param string|null $include_departamento
     */
    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return int|null
     */
    public function getEstatusId(): ?int
    {
        return $this->estatus_id;
    }

    /**
     * @param string $departamento_id
     */
    public function setEstatusId(?string $estatus_id): void
    {
        $this->estatus_id = $estatus_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstatus(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estatus);
    }

    /**
     * @param string|null $include_departamento
     */
    public function setIncludeEstatus(?string $include_estatus): void
    {
        $this->include_estatus = $include_estatus;
    }

    /**
     * @return string|null
     */
    public function getEstatusValor(): ?string
    {
        return $this->estatus_valor;
    }

    /**
     * @param string|null $estatus_valor
     */
    public function setEstatusValor(?string $estatus_valor): void
    {
        $this->estatus_valor = $estatus_valor;
    }
}
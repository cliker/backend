<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 2:29 PM
 */

namespace App\Services\DTO\Incidencias;

use App\Services\DTO\CEstatusIncidencias\ICEstatusIncidenciaResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\CTiposIncidencias\ICTipoIncidenciaResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empleados\IEmpleadoRequest;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

class IncidenciaResponse extends DataTransferObject implements IIncidenciaResponse
{
    protected $id = 0;
    protected $descripcion = '';
    protected $tipo = null;
    protected $fecha_inicio = '';
    protected $fecha_final = '';
    protected $activo = false;
    protected $empleado = null;
    protected $cliente = null;
    protected $empresa = null;
    protected $sucursal = null;
    protected $departamento = null;
    protected $estatus = null;
    protected $url_archivo = '';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return ICTipoIncidenciaResponse|null
     */
    public function getTipo(): ?ICTipoIncidenciaResponse
    {
        return $this->tipo;
    }

    /**
     * @param ICTipoIncidenciaResponse|null $tipo
     */
    public function setTipo(?ICTipoIncidenciaResponse $tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string|null
     */
    public function getFechaInicio(): ?string
    {
        return $this->fecha_inicio;
    }

    /**
     * @param string|null $fecha_inicio
     */
    public function setFechaInicio(?string $fecha_inicio)
    {
        $this->fecha_inicio = $fecha_inicio;
    }

    /**
     * @return string|null
     */
    public function getFechaFinal(): ?string
    {
        return $this->fecha_final;
    }

    /**
     * @param string|null $fecha_final
     */
    public function setFechaFinal(?string $fecha_final)
    {
        $this->fecha_final = $fecha_final;
    }

    /**
     * @return string|null
     */
    public function getUrlArchivo():? string
    {
        return $this->url_archivo;
    }

    /**
     * @param string|null $url_archivo
     */
    public function setUrlArchivo(?string $url_archivo)
    {
        $this->url_archivo = $url_archivo;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IEmpleadoRequest|null
     */
    public function getEmpleado(): ?IEmpleadoResponse
    {
        return $this->empleado;
    }

    /**
     * @param IEmpleadoResponse|null $empleado
     */
    public function setEmpleado(?IEmpleadoResponse $empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return IDepartamentoResponse|null
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param IDepartamentoResponse|null $departamento
     */
    public function setDepartamento(?IDepartamentoResponse $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return ICEstatusIncidenciaResponse|null
     */
    public function getEstatus(): ?ICEstatusIncidenciaResponse
    {
        return $this->estatus;
    }

    /**
     * @param ICEstatusIncidenciaResponse|null $estatus
     */
    public function setEstatus(?ICEstatusIncidenciaResponse $estatus)
    {
        $this->estatus = $estatus;
    }
}
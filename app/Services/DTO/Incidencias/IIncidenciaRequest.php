<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 3:21 PM
 */

namespace App\Services\DTO\Incidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IIncidenciaRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getTipoId(): ?int;
    public function setTipoId(?int $tipo_id);

    public function getFechaInicio(): ?string;
    public function setFechaInicio(?string $fecha_inicio);

    public function getFechaFinal(): ?string;
    public function setFechaFinal(?string $fecha_final);

    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?int $empleado_id);
    
    public function getUrlArchivo(): ?string;
    public function setUrlArchivo(?string $url_archivo);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?int $departamento_id);

    public function getEstatusId(): ?int;
    public function setEstatusId(?int $estatus_id);
}
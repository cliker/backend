<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/3/19
 * Time: 2:09 PM
 */

namespace App\Services\DTO\Incidencias;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CEstatusIncidencias\ICEstatusIncidenciaResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\CTiposIncidencias\ICTipoIncidenciaResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empleados\IEmpleadoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

interface IIncidenciaResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getTipo(): ?ICTipoIncidenciaResponse;
    public function setTipo(?ICTipoIncidenciaResponse $tipo);

    public function getFechaInicio(): ?string;
    public function setFechaInicio(?string $fecha_inicio);

    public function getFechaFinal(): ?string;
    public function setFechaFinal(?string $fecha_final);

    public function getEmpleado(): ?IEmpleadoResponse;
    public function setEmpleado(?IEmpleadoResponse $empleado);
    
    public function getUrlArchivo(): ?string;
    public function setUrlArchivo(?string $url_archivo);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getSucursal(): ?ISucursalResponse;
    public function setSucursal(?ISucursalResponse $sucursal);

    public function getDepartamento(): ?IDepartamentoResponse;
    public function setDepartamento(?IDepartamentoResponse $departamento);

    public function getEstatus(): ?ICEstatusIncidenciaResponse;
    public function setEstatus(?ICEstatusIncidenciaResponse $estatus);
}
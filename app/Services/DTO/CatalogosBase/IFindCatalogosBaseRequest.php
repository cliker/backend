<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:04 PM
 */

namespace App\Services\DTO\CatalogosBase;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;


interface IFindCatalogosBaseRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);
    public function getValor(): ?string;
    public function setValor(?string $valor);
    public function getActivo():? string;
    public function setActivo(?string $activo): void;
}
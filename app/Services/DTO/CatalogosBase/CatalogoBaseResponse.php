<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 28/12/18
 * Time: 12:09 PM
 */

namespace App\Services\DTO\CatalogosBase;
use App\Services\DTO\DataTransferObject;


class CatalogoBaseResponse extends DataTransferObject implements ICatalogoBaseResponse
{

    protected $id = 0;
    protected $nombre = '';
    protected $valor = '';
    protected $activo = true;


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(?string $valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }
}
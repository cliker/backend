<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 01:14 PM
 */

namespace App\Services\DTO\Usuarios;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class LayoutUsuariosRequest extends DataTransferObject implements ILayoutUsuariosRequest
{
    use TFindBaseRequest;
    protected $tipo = '';
    protected $rol_name = '';
    protected $sucursal_id;
    protected $empresa_id;
   
    /**
     * @return string
     */
    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     */
    public function setTipo(?string $tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @return string
     */
    public function getRolName(): ?string
    {
        return $this->rol_name;
    }

    /**
     * @param string $rol_name
     */
    public function setRolName(?string $rol_name): void
    {
        $this->rol_name= $rol_name;
    }

    /**
     * @return string
     */
    public function getEmpresaId(): ?string
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id= $empresa_id;
    }

    /**
     * @return string
     */
    public function getSucursalId(): ?string
    {
        return $this->sucursal_id;
    }

    /**
     * @param string $sucursal_id
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id= $sucursal_id;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 12:06 PM
 */

namespace App\Services\DTO\Usuarios;

use App\Services\DTO\Base\IFindBaseRequest;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface ILayoutUsuariosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getTipo(): ?string;
    public function setTipo(?string $nombre): void;

    public function getRolName(): ?string;
    public function setRolName(?string $rol_name): void;

    public function getEmpresaId(): ?string;
    public function setEmpresaId(?string $empresa_id): void;

    public function getSucursalId(): ?string;
    public function setSucursalId(?string $sucursal_id): void;
}
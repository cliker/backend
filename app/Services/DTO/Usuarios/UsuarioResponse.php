<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 26/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\Usuarios;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Roles\IRolResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

class UsuarioResponse extends DataTransferObject implements IUsuarioResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $titulo = '';
    protected $puesto = '';
    protected $fecha_nacimiento = '';
    protected $telefono = '';
    protected $telefono_oficina = '';
    protected $extension = '';
    protected $celular = '';
    protected $email = '';
    protected $cliente = null;
    protected $activo = true;
    protected $rol = null;
    protected $nombre_completo = '';
    protected $primer_inicio_sesion = true;
    protected $empresa = null;
    protected $sucursal = null;
    protected $departamento = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno)
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno)
    {
        $this->apellido_materno = $apellido_materno ;
    }

    /**
     * @return string
     */
    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     */
    public function setTitulo(?string $titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    /**
     * @param string $puesto
     */
    public function setPuesto(?string $puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string
     */
    public function getTelefonoOficina(): ?string
    {
        return $this->telefono_oficina;
    }

    /**
     * @param string $telefono_oficina
     */
    public function setTelefonoOficina(?string $telefono_oficina)
    {
        $this->telefono_oficina = $telefono_oficina;
    }

    /**
     * @return string
     */
    public function getExtension(): ?string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(?string $extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getCelular(): ?string
    {
        return $this->celular;
    }

    /**
     * @param string $celular
     */
    public function setCelular(?string $celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string
     */
    /*public function getPassword(): ?string
    {
        return $this->password;
    }*/

    /**
     * @param string $password
     */
    /*public function setPassword(?string $password)
    {
        $this->password = $password;
    }*/

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return IDepartamentoResponse|null
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param IDepartamentoResponse|null $departamento
     */
    public function setDepartamento(?IDepartamentoResponse $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return bool
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return bool
     */
    public function getPrimerInicioSesion(): ?bool
    {
        return $this->primer_inicio_sesion;
    }

    /**
     * @param bool $primer_inicio_sesion
     */
    public function setPrimerInicioSesion(?bool $primer_inicio_sesion)
    {
        $this->primer_inicio_sesion = $primer_inicio_sesion;
    }

    /**
     * @return IRolResponse|null
     */
    public function getRol(): ?IRolResponse
    {
        return $this->rol;
    }

    /**
     * @param IRolResponse|null $rol
     */
    public function setRol(?IRolResponse $rol)
    {
        $this->rol = $rol;
    }

    /**
     * @return string|null
     */
    public function getNombreCompleto(): ? string
    {
        return $this->nombre_completo = $this->getNombre().' '.$this->getApellidoPaterno().' '.$this->getApellidoMaterno();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:33 AM
 */

namespace App\Services\DTO\DiasFestivos;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

class DiaFestivoResponse extends DataTransferObject implements IDiaFestivoResponse
{
    protected $id = 0;
    protected $nombre = '';
    protected $fecha = '';
    protected $descripcion = '';
    protected $departamentos = null;
    protected $activo = true;
    protected $cliente = null;
    protected $empresa = null;
    protected $sucursal = null;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getfecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return array|null
     */
    public function getDepartamentos(): ?array
    {
        return $this->departamentos;
    }

    /**
     * @param array |null $departamentos
     */
    public function setDepartamentos(?array $departamentos)
    {
        $this->departamentos = $departamentos;
    }

    /**
     * @return bool
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }
}
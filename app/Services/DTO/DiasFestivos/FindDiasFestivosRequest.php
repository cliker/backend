<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 12:20 PM
 */

namespace App\Services\DTO\DiasFestivos;

use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;


class FindDiasFestivosRequest extends DataTransferObject implements IFindDiasFestivosRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $fecha = '';
    protected $max_fecha= '';
    protected $min_fecha= '';
    protected $descripcion = '';
    protected $departamento_id = 0;
    protected $include_departamentos = false;
    protected $activo = '';
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;


    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string
     */
    public function getMaxFecha(): ?string
    {
        return $this->max_fecha;
    }

    /**
     * @param string $max_fecha
     */
    public function setMaxFecha(?string $max_fecha)
    {
        $this->max_fecha = $max_fecha;
    }

    /**
     * @return string
     */
    public function getMinFecha(): ?string
    {
        return $this->min_fecha;
    }

    /**
     * @param string $min_fecha
     */
    public function setMinFecha(?string $min_fecha)
    {
        $this->min_fecha = $min_fecha;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return int|null
     */
    public function getDepartamentolId(): ?int
    {
        return StringExtensions::toInt($this->departamento_id);
    }

    /**
     * @param null|string $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamentos(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamentos);
    }

    /**
     * @param null|string $include_departamentos
     */
    public function setIncludeDepartamentos(?string $include_departamentos): void
    {
        $this->include_departamentos = $include_departamentos;
    }

    /**
     * @return bool
     */
    public function getActivo():? string
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param string|null $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }
}
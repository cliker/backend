<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:22 AM
 */

namespace App\Services\DTO\DiasFestivos;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;


interface IDiaFestivoRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getDepartamentos(): ?array;
    public function setDepartamentos(?array $departamentos);

    public function getActivo():? bool;
    public function setActivo(?bool $activo);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:25 AM
 */

namespace App\Services\DTO\DiasFestivos;

use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindDiasFestivosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getFecha(): ?string;
    public function setFecha(?string $fecha);

    public function getMaxFecha(): ?string;
    public function setMaxFecha(?string $max_fecha);

    public function getMinFecha(): ?string;
    public function setMinFecha(?string $min_fecha);

    public function getDescripcion(): ?string;
    public function setDescripcion(?string $descripcion);

    public function getDepartamentolId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;
    public function isIncludeDepartamentos(): ?bool;
    public function setIncludeDepartamentos(?string $include_departamentos): void;

    public function getActivo(): ?string;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;

    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;

    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;
}
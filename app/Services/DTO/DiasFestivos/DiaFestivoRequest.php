<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 19/12/18
 * Time: 11:49 AM
 */

namespace App\Services\DTO\DiasFestivos;
use App\Services\DTO\DataTransferObject;


class DiaFestivoRequest extends DataTransferObject implements IDiaFestivoRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $fecha = '';
    protected $descripcion = '';
    protected $departamentos = null;
    protected $activo = true;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $sucursal_id = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    /**
     * @param string|null $fecha
     */
    public function setFecha(?string $fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     */
    public function setDescripcion(?string $descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return array|null
     */
    public function getDepartamentos(): ?array
    {
        return $this->departamentos;
    }

    /**
     * @param array|null $departamentos
     */
    public function setDepartamentos(?array $departamentos)
    {
        $this->departamentos = $departamentos;
    }

    /**
     * @return bool|null
     */
    public function getActivo():? bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }
}
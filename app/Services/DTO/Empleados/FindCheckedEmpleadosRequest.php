<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 5:03 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindCheckedEmpleadosRequest extends DataTransferObject implements IFindCheckedEmpleadosRequest
{
    use TFindBaseRequest;

    protected $empleado_id = 0;
    protected $horario_id = 0;
    protected $include_horario = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $activo = '';
    protected $departamento_id = 0;
    protected $include_departamento = false;
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $min_fecha_checada = '';
    protected $max_fecha_checada = '';


    /**
     * @return int|null
     */
    public function getHorarioId(): ?int
    {
        return $this->horario_id;
    }

    /**
     * @param string|null $horario_id
     */
    public function setHorarioId(?string $horario_id): void
    {
        $this->horario_id = $horario_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeHorario(): ?bool
    {
        return StringExtensions::toBoolean($this->include_horario);
    }

    /**
     * @param string|null $include_horario
     */
    public function setIncludeHorario(?string $include_horario): void
    {
        $this->include_horario = $include_horario;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param string|null $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return string|null
     */
    public function getActivo(): ?string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string|null $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    /**
     * @param string|null $include_departamento
     */
    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return string|null
     */
    public function getMinFechaChecada(): ?string
    {
        return $this->min_fecha_checada;
    }

    /**
     * @param string|null $min_fecha_checada
     */
    public function setMinFechaChecada(?string $min_fecha_checada): void
    {
        $this->min_fecha_checada = $min_fecha_checada;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaChecada(): ?string
    {
        return $this->max_fecha_checada;
    }

    /**
     * @param string|null $max_fecha_checada
     */
    public function setMaxFechaChecada(?string $max_fecha_checada): void
    {
        $this->max_fecha_checada = $max_fecha_checada;
    }

    /**
     * @return int|null
     */
    public function getEmpleadoId(): ?int
    {
        return $this->empleado_id;
    }

    /**
     * @param string|null $empleado_id
     */
    public function setEmpleadoId(?string $empleado_id): void
    {
        $this->empleado_id = $empleado_id;
    }
}
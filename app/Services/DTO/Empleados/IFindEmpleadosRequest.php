<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 4:45 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindEmpleadosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getNombre(): ?string;
    public function setNombre(?string $nombre): void;

    public function getApellidoPaterno(): ?string;
    public function setApellidoPaterno(?string $apellido_paterno): void;

    public function getApellidoMaterno(): ?string;
    public function setApellidoMaterno(?string $apellido_materno): void;

    public function getFechaNacimiento(): ?string;
    public function setFechaNacimiento(?string $fecha_nacimiento): void;

    public function getMinFechaNacimiento(): ?string;
    public function setMinFechaNacimiento(?string $min_fecha_nacimiento): void;

    public function getMaxFechaNacimiento(): ?string;
    public function setMaxFechaNacimiento(?string $max_fecha_nacimiento): void;

    public function getSexo(): ?string;
    public function setSexo(?string $sexo): void;

    public function getEstadoId(): ?int;
    public function setEstadoId(?string $estado_id): void;

    public function isIncludeEstado(): ?bool;
    public function setIncludeEstado(?string $include_estado): void;

    public function getCiudadId(): ?int;
    public function setCiudadId(?string $ciudad_id): void;

    public function isIncludeCiudad(): ?bool;
    public function setIncludeCiudad(?string $include_ciudad): void;

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal): void;

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono): void;

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getEstadoCivilId(): ?int;
    public function setEstadoCivilId(?string $estado_civil_id): void;

    public function isIncludeEstadoCivil(): ?bool;
    public function setIncludeEstadoCivil(?string $include_estado_civil): void;

    public function getFechaIngreso(): ?string;
    public function setFechaIngreso(?string $fecha_ingreso): void;

    public function getMinFechaIngreso(): ?string;
    public function setMinFechaIngreso(?string $min_fecha_ingreso): void;

    public function getMaxFechaIngreso(): ?string;
    public function setMaxFechaIngreso(?string $max_fecha_ingreso): void;

    public function getFechaBaja(): ?string;
    public function setFechaBaja(?string $fecha_baja): void;

    public function getMinFechaBaja(): ?string;
    public function setMinFechaBaja(?string $min_fecha_baja): void;

    public function getMaxFechaBaja(): ?string;
    public function setMaxFechaBaja(?string $max_fecha_baja): void;

    public function getDiaDescanso(): ?string;
    public function setDiaDescanso(?string $dia_descanso): void;

    public function getHorarioId(): ?int;
    public function setHorarioId(?string $horario_id): void;

    public function isIncludeHorario(): ?bool;
    public function setIncludeHorario(?string $include_horario): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;

    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;

    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    /*public function getRegistrosChecador(): ?IRegistroChecadorResponse;
    public function setRegistrosChecador(?IRegistroChecadorResponse $departamento);*/

    public function getActivo(): ?string ;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;

    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function isIncludeRelojesChecador(): ?bool;
    public function setIncludeRelojesChecador(?string $include_relojes_chechador): void;

    public function getPuesto(): ?string;
    public function setPuesto(?string $puesto);

    public function getClaveInterna(): ?string;
    public function setClaveInterna(?string $clave_interna);

    public function getTieneHijos(): ?bool;
    public function setTieneHijos(?bool $tiene_hijos);
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 4:17 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface IEmpleadoRequest extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getApellidoPaterno(): ?string;
    public function setApellidoPaterno(?string $apellido_paterno);

    public function getApellidoMaterno(): ?string;
    public function setApellidoMaterno(?string $apellido_materno);

    public function getFechaNacimiento(): ?string;
    public function setFechaNacimiento(?string $fecha_nacimiento);

    public function getSexo(): ?string;
    public function setSexo(?string $sexo);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstadoId(): ?int;
    public function setEstadoId(?int $estado_id);

    public function getCiudadId(): ?int;
    public function setCiudadId(?int $ciudad_id);

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal);

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getEstadoCivilId(): ?int;
    public function setEstadoCivilId(?int $estado_civil_id);

    public function getFechaIngreso(): ?string;
    public function setFechaIngreso(?string $fecha_ingreso);

    public function getFechaBaja(): ?string;
    public function setFechaBaja(?string $fecha_baja);

    public function getDiaDescanso(): ?string;
    public function setDiaDescanso(?string $dia_descanso);

    public function getHorarioId(): ?int;
    public function setHorarioId(?int $horario_id);

    public function getSucursalId(): ?int;
    public function setSucursalId(?int $sucursal_id);

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?int $departamento_id);

    /*public function getIncidencias(): ?IIncidenciaResponse;
    public function setIncidencias(?IIncidenciaResponse $incidencias);*/

    public function getDocumentos(): ?array ;
    public function setDocumentos(?array $documentos);

    /*public function getRegistrosChecador(): ?IRegistroChecadorResponse;
    public function setRegistrosChecador(?IRegistroChecadorResponse $departamento);*/

    public function getActivo(): ?bool;
    public function setActivo(?bool $activo);

    public function getDocumentosEliminados(): ?array;
    public function setDocumentosEliminados(?array $documentos_eliminados);

    public function getClienteId(): ?int;
    public function setClienteId(?int $cliente_id);

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?int $empresa_id);

    public function getPuesto(): ?string;
    public function setPuesto(?string $puesto);

    public function getClaveInterna(): ?string;
    public function setClaveInterna(?string $clave_interna);

    public function getTieneHijos(): ?bool;
    public function setTieneHijos(?bool $tiene_hijos);

    public function getUsuarioId(): ?int;
    public function setUsuarioId(?int $usuario_id);

    public function getEmailAnterior(): ?string;
    public function setEmailAnterior(?string $email_anterior);
}
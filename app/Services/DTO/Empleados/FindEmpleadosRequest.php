<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 5:03 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\DataTransferObject;
use App\Infrastructure\StringExtensions;
use App\Services\DTO\Base\TFindBaseRequest;

class FindEmpleadosRequest extends DataTransferObject implements IFindEmpleadosRequest
{
    use TFindBaseRequest;

    protected $nombre = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $fecha_nacimiento = '';
    protected $min_fecha_nacimiento = '';
    protected $max_fecha_nacimiento = '';
    protected $sexo = '';
    protected $estado_id = 0;
    protected $include_estado = false;
    protected $ciudad_id = 0;
    protected $include_ciudad = false;
    protected $codigo_postal = '';
    protected $telefono = '';
    protected $correo = '';
    protected $estado_civil_id = 0;
    protected $include_estado_civil = false;
    protected $fecha_ingreso = '';
    protected $min_fecha_ingreso = '';
    protected $max_fecha_ingreso = '';
    protected $fecha_baja = '';
    protected $min_fecha_baja = '';
    protected $max_fecha_baja = '';
    protected $dia_descanso = '';
    protected $horario_id = 0;
    protected $include_horario = false;
    protected $sucursal_id = 0;
    protected $include_sucursal = false;
    protected $activo = '';
    protected $departamento_id = 0;
    protected $include_departamento = false;
    protected $cliente_id = 0;
    protected $include_cliente = false;
    protected $empresa_id = 0;
    protected $include_empresa = false;
    protected $include_relojes_checador = false;
    protected $puesto = '';
    protected $clave_interna= '';
    protected $tiene_hijos = false;

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string|null $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno): void
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string|null
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string|null $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno): void
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return string|null
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string|null $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento): void
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getMinFechaNacimiento(): ?string
    {
        return $this->min_fecha_nacimiento;
    }

    /**
     * @param string|null $min_fecha_nacimiento
     */
    public function setMinFechaNacimiento(?string $min_fecha_nacimiento): void
    {
        $this->min_fecha_nacimiento = $min_fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaNacimiento(): ?string
    {
        return $this->max_fecha_nacimiento;
    }

    /**
     * @param string|null $max_fecha_nacimiento
     */
    public function setMaxFechaNacimiento(?string $max_fecha_nacimiento): void
    {
        $this->max_fecha_nacimiento = $max_fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    /**
     * @param string|null $sexo
     */
    public function setSexo(?string $sexo): void
    {
        $this->sexo = $sexo;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param string|null $estado_id
     */
    public function setEstadoId(?string $estado_id): void
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstado(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estado);
    }

    /**
     * @param string|null $include_estado
     */
    public function setIncludeEstado(?string $include_estado): void
    {
        $this->include_estado = $include_estado;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return $this->ciudad_id;
    }

    /**
     * @param string|null $ciudad_id
     */
    public function setCiudadId(?string $ciudad_id): void
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCiudad(): ?bool
    {
        return StringExtensions::toBoolean($this->include_ciudad);
    }

    /**
     * @param string|null $include_ciudad
     */
    public function setIncludeCiudad(?string $include_ciudad): void
    {
        $this->include_ciudad = $include_ciudad;
    }

    /**
     * @return string|null
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string|null $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal): void
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     */
    public function setTelefono(?string $telefono): void
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo): void
    {
        $this->correo = $correo;
    }

    /**
     * @return int|null
     */
    public function getEstadoCivilId(): ?int
    {
        return $this->estado_civil_id;
    }

    /**
     * @param string|null $estado_civil_id
     */
    public function setEstadoCivilId(?string $estado_civil_id): void
    {
        $this->estado_civil_id = $estado_civil_id;
    }

    /**
     * @return string|null
     */
    public function getFechaIngreso(): ?string
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string|null $fecha_ingreso
     */
    public function setFechaIngreso(?string $fecha_ingreso): void
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string|null
     */
    public function getMinFechaIngreso(): ?string
    {
        return $this->min_fecha_ingreso;
    }

    /**
     * @param string|null $min_fecha_ingreso
     */
    public function setMinFechaIngreso(?string $min_fecha_ingreso): void
    {
        $this->min_fecha_ingreso = $min_fecha_ingreso;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaIngreso(): ?string
    {
        return $this->max_fecha_ingreso;
    }

    /**
     * @param string|null $max_fecha_ingreso
     */
    public function setMaxFechaIngreso(?string $max_fecha_ingreso): void
    {
        $this->max_fecha_ingreso = $max_fecha_ingreso;
    }

    /**
     * @return string|null
     */
    public function getFechaBaja(): ?string
    {
        return $this->fecha_baja;
    }

    /**
     * @param string|null $fecha_baja
     */
    public function setFechaBaja(?string $fecha_baja): void
    {
        $this->fecha_baja = $fecha_baja;
    }

    /**
     * @return string|null
     */
    public function getMinFechaBaja(): ?string
    {
        return $this->min_fecha_baja;
    }

    /**
     * @param string|null $min_fecha_baja
     */
    public function setMinFechaBaja(?string $min_fecha_baja): void
    {
        $this->min_fecha_baja = $min_fecha_baja;
    }

    /**
     * @return string|null
     */
    public function getMaxFechaBaja(): ?string
    {
        return $this->max_fecha_baja;
    }

    /**
     * @param string|null $max_fecha_baja
     */
    public function setMaxFechaBaja(?string $max_fecha_baja): void
    {
        $this->max_fecha_baja = $max_fecha_baja;
    }

    /**
     * @return string|null
     */
    public function getDiaDescanso(): ?string
    {
        return $this->dia_descanso;
    }

    /**
     * @param string|null $dia_descanso
     */
    public function setDiaDescanso(?string $dia_descanso): void
    {
        $this->dia_descanso = $dia_descanso;
    }

    /**
     * @return int|null
     */
    public function getHorarioId(): ?int
    {
        return $this->horario_id;
    }

    /**
     * @param string|null $horario_id
     */
    public function setHorarioId(?string $horario_id): void
    {
        $this->horario_id = $horario_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeHorario(): ?bool
    {
        return StringExtensions::toBoolean($this->include_horario);
    }

    /**
     * @param string|null $include_horario
     */
    public function setIncludeHorario(?string $include_horario): void
    {
        $this->include_horario = $include_horario;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param string|null $sucursal
     */
    public function setSucursalId(?string $sucursal_id): void
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeSucursal(): ?bool
    {
        return StringExtensions::toBoolean($this->include_sucursal);
    }

    /**
     * @param string|null $include_sucursal
     */
    public function setIncludeSucursal(?string $include_sucursal): void
    {
        $this->include_sucursal = $include_sucursal;
    }

    /**
     * @return string|null
     */
    public function getActivo(): ?string
    {
        return $this->activo;
    }

    /**
     * @param string|null $activo
     */
    public function setActivo(?string $activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param string|null $departamento_id
     */
    public function setDepartamentoId(?string $departamento_id): void
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeDepartamento(): ?bool
    {
        return StringExtensions::toBoolean($this->include_departamento);
    }

    /**
     * @param string|null $include_departamento
     */
    public function setIncludeDepartamento(?string $include_departamento): void
    {
        $this->include_departamento = $include_departamento;
    }

    /**
     * @return int
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string $cliente_id
     */
    public function setClienteId(?string $cliente_id): void
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeCliente(): ?bool
    {
        return StringExtensions::toBoolean($this->include_cliente);
    }

    /**
     * @param string|null $include_cliente
     */
    public function setIncludeCliente(?string $include_cliente): void
    {
        $this->include_cliente = $include_cliente;
    }

    /**
     * @return int
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string $empresa_id
     */
    public function setEmpresaId(?string $empresa_id): void
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEmpresa(): ?bool
    {
        return StringExtensions::toBoolean($this->include_empresa);
    }

    /**
     * @param string|null $include_empresa
     */
    public function setIncludeEmpresa(?string $include_empresa): void
    {
        $this->include_empresa = $include_empresa;
    }

    /**
     * @return bool|null
     */
    public function isIncludeRelojesChecador(): ?bool
    {
        return StringExtensions::toBoolean($this->include_relojes_checador);
    }

    /**
     * @param string|null $include_relojes_chechador
     */
    public function setIncludeRelojesChecador(?string $include_relojes_chechador): void
    {
        $this->include_relojes_checador = $include_relojes_chechador;
    }

    /**
     * @return bool|null
     */
    public function isIncludeEstadoCivil(): ?bool
    {
        return StringExtensions::toBoolean($this->include_estado_civil);
    }

    /**
     * @param string|null $include_estado_civil
     */
    public function setIncludeEstadoCivil(?string $include_estado_civil): void
    {
        $this->include_estado_civil = $include_estado_civil;
    }

    /**
     * @return string|null
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    /**
     * @param string|null $puesto
     */
    public function setPuesto(?string $puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string|null
     */
    public function getClaveInterna(): ?string
    {
        return $this->clave_interna;
    }

    /**
     * @param string|null $clave_interna
     */
    public function setClaveInterna(?string $clave_interna)
    {
        $this->clave_interna = $clave_interna;
    }

    /**
     * @return bool|null
     */
    public function getTieneHijos(): ?bool
    {
        return $this->tiene_hijos;
    }

    /**
     * @param bool|null $tiene_hijos
     */
    public function setTieneHijos(?bool $tiene_hijos)
    {
        $this->tiene_hijos = $tiene_hijos;
    }
}
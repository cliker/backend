<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 2:33 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;

interface ICreateEmpleadoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getClave():?int;
    public function setClave(?int $clave);
}
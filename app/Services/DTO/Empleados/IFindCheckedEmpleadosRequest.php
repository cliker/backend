<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 4:45 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\Base\IFindBaseRequest;

interface IFindCheckedEmpleadosRequest extends DataTransferObjectInterface, IFindBaseRequest
{
    public function getEmpleadoId(): ?int;
    public function setEmpleadoId(?string $empleado_id): void;

    public function getHorarioId(): ?int;
    public function setHorarioId(?string $horario_id): void;

    public function isIncludeHorario(): ?bool;
    public function setIncludeHorario(?string $include_horario): void;

    public function getSucursalId(): ?int;
    public function setSucursalId(?string $sucursal_id): void;

    public function isIncludeSucursal(): ?bool;
    public function setIncludeSucursal(?string $include_sucursal): void;

    public function getDepartamentoId(): ?int;
    public function setDepartamentoId(?string $departamento_id): void;

    public function isIncludeDepartamento(): ?bool;
    public function setIncludeDepartamento(?string $include_departamento): void;

    public function getActivo(): ?string ;
    public function setActivo(?string $activo): void;

    public function getClienteId(): ?int;
    public function setClienteId(?string $cliente_id): void;

    public function isIncludeCliente(): ?bool;
    public function setIncludeCliente(?string $include_cliente): void;

    public function getEmpresaId(): ?int;
    public function setEmpresaId(?string $empresa_id): void;

    public function isIncludeEmpresa(): ?bool;
    public function setIncludeEmpresa(?string $include_empresa): void;

    public function getMinFechaChecada(): ?string;
    public function setMinFechaChecada(?string $min_fecha_checada): void;

    public function getMaxFechaChecada(): ?string;
    public function setMaxFechaChecada(?string $max_fecha_checada): void;

}
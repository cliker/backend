<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 4:26 PM
 */

namespace App\Services\DTO\Empleados;
use App\Services\DTO\DataTransferObject;

class EmpleadoRequest extends DataTransferObject implements IEmpleadoRequest
{
    protected $id = 0;
    protected $nombre = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $fecha_nacimiento = '';
    protected $sexo = '';
    protected $direccion = '';
    protected $estado_id = 0;
    protected $ciudad_id = 0;
    protected $codigo_postal = '';
    protected $telefono = '';
    protected $correo = '';
    protected $estado_civil_id = 0;
    protected $fecha_ingreso = '';
    protected $fecha_baja = '';
    protected $dia_descanso = '';
    protected $horario_id = 0;
    protected $sucursal_id = 0;
    protected $documentos = null;
    protected $activo = false;
    protected $departamento_id = 0;
    protected $documentos_eliminados = null;
    protected $cliente_id = 0;
    protected $empresa_id = 0;
    protected $puesto = '';
    protected $clave_interna= '';
    protected $tiene_hijos = false;
    protected $usuario_id= 0;
    protected $email_anterior='';

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string|null $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno)
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string|null
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string|null $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno)
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return string|null
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string|null $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    /**
     * @param string|null $sexo
     */
    public function setSexo(?string $sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return int|null
     */
    public function getEstadoId(): ?int
    {
        return $this->estado_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setEstadoId(?int $estado_id)
    {
        $this->estado_id = $estado_id;
    }

    /**
     * @return int|null
     */
    public function getCiudadId(): ?int
    {
        return $this->ciudad_id;
    }

    /**
     * @param int|null $ciudad_id
     */
    public function setCiudadId(?int $ciudad_id)
    {
        $this->ciudad_id = $ciudad_id;
    }

    /**
     * @return string|null
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string|null $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return int|null
     */
    public function getEstadoCivilId(): ?int
    {
        return $this->estado_civil_id;
    }

    /**
     * @param int|null $estado_civil_id
     */
    public function setEstadoCivilId(?int $estado_civil_id)
    {
        $this->estado_civil_id = $estado_civil_id;
    }

    /**
     * @return string|null
     */
    public function getFechaIngreso(): ?string
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string|null $fecha_ingreso
     */
    public function setFechaIngreso(?string $fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string|null
     */
    public function getFechaBaja(): ?string
    {
        return $this->fecha_baja;
    }

    /**
     * @param string|null $fecha_baja
     */
    public function setFechaBaja(?string $fecha_baja)
    {
        $this->fecha_baja = $fecha_baja;
    }

    /**
     * @return string|null
     */
    public function getDiaDescanso(): ?string
    {
        return $this->dia_descanso;
    }

    /**
     * @param string|null $dia_descanso
     */
    public function setDiaDescanso(?string $dia_descanso)
    {
        $this->dia_descanso = $dia_descanso;
    }

    /**
     * @return int|null
     */
    public function getHorarioId(): ?int
    {
        return $this->horario_id;
    }

    /**
     * @param int|null $horario_id
     */
    public function setHorarioId(?int $horario_id)
    {
        $this->horario_id = $horario_id;
    }

    /**
     * @return int|null
     */
    public function getSucursalId(): ?int
    {
        return $this->sucursal_id;
    }

    /**
     * @param int|null $sucursal_id
     */
    public function setSucursalId(?int $sucursal_id)
    {
        $this->sucursal_id = $sucursal_id;
    }

    /**
     * @return array|null
     */
    public function getDocumentos(): ?array
    {
        return $this->documentos;
    }

    /**
     * @param array|null $documentos
     */
    public function setDocumentos(?array $documentos)
    {
        $this->documentos = $documentos;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return int|null
     */
    public function getDepartamentoId(): ?int
    {
        return $this->departamento_id;
    }

    /**
     * @param int|null $departamento_id
     */
    public function setDepartamentoId(?int $departamento_id)
    {
        $this->departamento_id = $departamento_id;
    }

    /**
     * @return array|null
     */
    public function getDocumentosEliminados(): ?array
    {
        return $this->documentos_eliminados;
    }

    /**
     * @param array|null $documentos_eliminados
     */
    public function setDocumentosEliminados(?array $documentos_eliminados)
    {
        $this->documentos_eliminados = $documentos_eliminados;
    }

    /**
     * @return int|null
     */
    public function getClienteId(): ?int
    {
        return $this->cliente_id;
    }

    /**
     * @param string|null $cliente_id
     */
    public function setClienteId(?int $cliente_id)
    {
        $this->cliente_id = $cliente_id;
    }

    /**
     * @return int|null
     */
    public function getEmpresaId(): ?int
    {
        return $this->empresa_id;
    }

    /**
     * @param string|null $empresa_id
     */
    public function setEmpresaId(?int $empresa_id)
    {
        $this->empresa_id = $empresa_id;
    }

    /**
     * @return string|null
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    /**
     * @param string|null $puesto
     */
    public function setPuesto(?string $puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string|null
     */
    public function getClaveInterna(): ?string
    {
        return $this->clave_interna;
    }

    /**
     * @param string|null $clave_interna
     */
    public function setClaveInterna(?string $clave_interna)
    {
        $this->clave_interna = $clave_interna;
    }

    /**
     * @return bool|null
     */
    public function getTieneHijos(): ?bool
    {
        return $this->tiene_hijos;
    }

    /**
     * @param bool|null $tiene_hijos
     */
    public function setTieneHijos(?bool $tiene_hijos)
    {
        $this->tiene_hijos = $tiene_hijos;
    }

    /**
     * @return int|null
     */
    public function getUsuarioId(): ?int
    {
        return $this->usuario_id;
    }

    /**
     * @param int|null $estado_id
     */
    public function setUsuarioId(?int $usuario_id)
    {
        $this->usuario_id = $usuario_id;
    }

    /**
     * @return string|null
     */
    public function getEmailAnterior(): ?string
    {
        return $this->email_anterior;
    }

    /**
     * @param string|null $email_anterior
     */
    public function setEmailAnterior(?string $email_anterior)
    {
        $this->email_anterior = $email_anterior;
    }
}
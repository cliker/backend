<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 8/8/19
 * Time: 2:35 PM
 */

namespace App\Services\DTO\Empleados;
use Aedart\DTO\DataTransferObject;

class CreateEmpleadoResponse extends DataTransferObject implements ICreateEmpleadoResponse
{
    protected $id = 0;
    protected $clave = 0;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getClave():?int
    {
        return $this->clave;
    }

    /**
     * @param int|null $clave
     */
    public function setClave(?int $clave)
    {
        $this->clave = $clave;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/5/19
 * Time: 12:06 PM
 */
namespace App\Services\DTO\Empleados;
use Aedart\DTO\Contracts\DataTransferObject as DataTransferObjectInterface;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\CEstadosCiviles\ICEstadoCivilResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Documentos\IDocumentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Horarios\IHorarioResponse;
use App\Services\DTO\Incidencias\IIncidenciaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

interface IEmpleadoResponse extends DataTransferObjectInterface
{
    public function getId(): ?int;
    public function setId(?int $id);

    public function getClave(): ?int;
    public function setClave(?int $id);

    public function getNombre(): ?string;
    public function setNombre(?string $nombre);

    public function getApellidoPaterno(): ?string;
    public function setApellidoPaterno(?string $apellido_paterno);

    public function getApellidoMaterno(): ?string;
    public function setApellidoMaterno(?string $apellido_materno);

    public function getFechaNacimiento(): ?string;
    public function setFechaNacimiento(?string $fecha_nacimiento);

    public function getSexo(): ?string;
    public function setSexo(?string $sexo);

    public function getDireccion(): ?string;
    public function setDireccion(?string $direccion);

    public function getEstado(): ?ICEstadoResponse;
    public function setEstado(?ICEstadoResponse $estado);

    public function getCiudad(): ?ICCiudadResponse;
    public function setCiudad(?ICCiudadResponse $ciudad);

    public function getCodigoPostal(): ?string;
    public function setCodigoPostal(?string $codigo_postal);

    public function getTelefono(): ?string;
    public function setTelefono(?string $telefono);

    public function getCorreo(): ?string;
    public function setCorreo(?string $correo);

    public function getEstadoCivil(): ?ICEstadoCivilResponse;
    public function setEstadoCivil(?ICEstadoCivilResponse $estado_civil);

    public function getFechaIngreso(): ?string;
    public function setFechaIngreso(?string $fecha_ingreso);

    public function getFechaBaja(): ?string;
    public function setFechaBaja(?string $fecha_baja);

    public function getDiaDescanso(): ?string;
    public function setDiaDescanso(?string $dia_descanso);

    public function getHorario(): ?IHorarioResponse;
    public function setHorario(?IHorarioResponse $horario);

    public function getSucursal(): ?ISucursalResponse;
    public function setSucursal(?ISucursalResponse $sucursal);

    public function getDepartamento(): ?IDepartamentoResponse;
    public function setDepartamento(?IDepartamentoResponse $departamento);

    public function getIncidencias(): ?array;
    public function setIncidencias(?array $incidencias);

    public function getDocumentos(): ?array;
    public function setDocumentos(?array $documentos);

    /*public function getRegistrosChecador(): ?IRegistroChecadorResponse;
    public function setRegistrosChecador(?IRegistroChecadorResponse $departamento);*/

    public function getActivo(): ?bool;
    public function setActivo(?bool $activo);

    public function getCliente(): ?IClienteResponse;
    public function setCliente(?IClienteResponse $cliente);

    public function getEmpresa(): ?IEmpresaResponse;
    public function setEmpresa(?IEmpresaResponse $empresa);

    public function getRelojesChecador(): ?array;
    public function setRelojesChecador(?array $relojes_checador);

    public function getPuesto(): ?string;
    public function setPuesto(?string $puesto);

    public function getClaveInterna(): ?string;
    public function setClaveInterna(?string $clave_interna);

    public function getTieneHijos(): ?bool;
    public function setTieneHijos(?bool $tiene_hijos);
}
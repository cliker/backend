<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/8/19
 * Time: 3:11 PM
 */

namespace App\Services\DTO\Empleados;
use App\Services\DTO\CCiudades\ICCiudadResponse;
use App\Services\DTO\CEstados\ICEstadoResponse;
use App\Services\DTO\CEstadosCiviles\ICEstadoCivilResponse;
use App\Services\DTO\Clientes\IClienteResponse;
use App\Services\DTO\DataTransferObject;
use App\Services\DTO\Departamentos\IDepartamentoResponse;
use App\Services\DTO\Documentos\IDocumentoResponse;
use App\Services\DTO\Empresas\IEmpresaResponse;
use App\Services\DTO\Horarios\IHorarioResponse;
use App\Services\DTO\Incidencias\IIncidenciaResponse;
use App\Services\DTO\Sucursales\ISucursalResponse;

class EmpleadoResponse extends DataTransferObject implements IEmpleadoResponse
{
    protected $id = 0;
    protected $clave = 0;
    protected $nombre = '';
    protected $apellido_paterno = '';
    protected $apellido_materno = '';
    protected $fecha_nacimiento = '';
    protected $sexo = '';
    protected $direccion = '';
    protected $estado = null;
    protected $ciudad = null;
    protected $codigo_postal = '';
    protected $telefono = '';
    protected $correo = '';
    protected $estado_civil = null;
    protected $fecha_ingreso = '';
    protected $fecha_baja = '';
    protected $dia_descanso = '';
    protected $horario = null;
    protected $sucursal = null;
    protected $incidencias = null;
    protected $documentos = null;
    protected $activo = false;
    protected $departamento = null;
    protected $cliente = null;
    protected $empresa = null;
    protected $relojes_checador = null;
    protected $puesto = '';
    protected $clave_interna= '';
    protected $tiene_hijos = false;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getClave(): ?int
    {
        return $this->clave;
    }

    /**
     * @param int|null $clave
     */
    public function setClave(?int $clave)
    {
        $this->clave = $clave;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string|null $nombre
     */
    public function setNombre(?string $nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getApellidoPaterno(): ?string
    {
        return $this->apellido_paterno;
    }

    /**
     * @param string|null $apellido_paterno
     */
    public function setApellidoPaterno(?string $apellido_paterno)
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return string|null
     */
    public function getApellidoMaterno(): ?string
    {
        return $this->apellido_materno;
    }

    /**
     * @param string|null $apellido_materno
     */
    public function setApellidoMaterno(?string $apellido_materno)
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return string|null
     */
    public function getFechaNacimiento(): ?string
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param string|null $fecha_nacimiento
     */
    public function setFechaNacimiento(?string $fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return string|null
     */
    public function getSexo(): ?string
    {
        return $this->sexo;
    }

    /**
     * @param string|null $sexo
     */
    public function setSexo(?string $sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return string|null
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param string|null $direccion
     */
    public function setDireccion(?string $direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return string|null
     */
    public function getEstado(): ?ICEstadoResponse
    {
        return $this->estado;
    }

    /**
     * @param ICEstadoResponse|null $estado
     */
    public function setEstado(?ICEstadoResponse $estado)
    {
        $this->estado = $estado;
    }

    /**
     * @return string|null
     */
    public function getCiudad(): ?ICCiudadResponse
    {
        return $this->ciudad;
    }

    /**
     * @param ICCiudadResponse|null $ciudad
     */
    public function setCiudad(?ICCiudadResponse $ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return string|null
     */
    public function getCodigoPostal(): ?string
    {
        return $this->codigo_postal;
    }

    /**
     * @param string|null $codigo_postal
     */
    public function setCodigoPostal(?string $codigo_postal)
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return string|null
     */
    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    /**
     * @param string|null $telefono
     */
    public function setTelefono(?string $telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return string|null
     */
    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    /**
     * @param string|null $correo
     */
    public function setCorreo(?string $correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string|null
     */
    public function getEstadoCivil(): ?ICEstadoCivilResponse
    {
        return $this->estado_civil;
    }

    /**
     * @param string|null $estado_civil
     */
    public function setEstadoCivil(?ICEstadoCivilResponse $estado_civil)
    {
        $this->estado_civil = $estado_civil;
    }

    /**
     * @return string|null
     */
    public function getFechaIngreso(): ?string
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param string|null $fecha_ingreso
     */
    public function setFechaIngreso(?string $fecha_ingreso)
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return string|null
     */
    public function getFechaBaja(): ?string
    {
        return $this->fecha_baja;
    }

    /**
     * @param string|null $fecha_baja
     */
    public function setFechaBaja(?string $fecha_baja)
    {
        $this->fecha_baja = $fecha_baja;
    }

    /**
     * @return string|null
     */
    public function getDiaDescanso(): ?string
    {
        return $this->dia_descanso;
    }

    /**
     * @param string|null $dia_descanso
     */
    public function setDiaDescanso(?string $dia_descanso)
    {
        $this->dia_descanso = $dia_descanso;
    }

    /**
     * @return IHorarioResponse|null
     */
    public function getHorario(): ?IHorarioResponse
    {
        return $this->horario;
    }

    /**
     * @param IHorarioResponse|null $horario
     */
    public function setHorario(?IHorarioResponse $horario)
    {
        $this->horario = $horario;
    }

    /**
     * @return ISucursalResponse|null
     */
    public function getSucursal(): ?ISucursalResponse
    {
        return $this->sucursal;
    }

    /**
     * @param ISucursalResponse|null $sucursal
     */
    public function setSucursal(?ISucursalResponse $sucursal)
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return array|null
     */
    public function getIncidencias(): ?array
    {
        return $this->incidencias;
    }

    /**
     * @param array|null $incidencias
     */
    public function setIncidencias(?array $incidencias)
    {
        $this->incidencias = $incidencias;
    }

    /**
     * @return array|null
     */
    public function getDocumentos(): ?array
    {
        return $this->documentos;
    }

    /**
     * @param array|null $documentos
     */
    public function setDocumentos(?array $documentos)
    {
        $this->documentos = $documentos;
    }

    /**
     * @return bool|null
     */
    public function getActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool|null $activo
     */
    public function setActivo(?bool $activo)
    {
        $this->activo = $activo;
    }

    /**
     * @return IDepartamentoResponse|null
     */
    public function getDepartamento(): ?IDepartamentoResponse
    {
        return $this->departamento;
    }

    /**
     * @param IDepartamentoResponse|null $activo
     */
    public function setDepartamento(?IDepartamentoResponse $departamento)
    {
        $this->departamento = $departamento;
    }

    /**
     * @return IClienteResponse|null
     */
    public function getCliente(): ?IClienteResponse
    {
        return $this->cliente;
    }

    /**
     * @param IClienteResponse|null $cliente
     */
    public function setCliente(?IClienteResponse $cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return IEmpresaResponse|null
     */
    public function getEmpresa(): ?IEmpresaResponse
    {
        return $this->empresa;
    }

    /**
     * @param IEmpresaResponse|null $empresa
     */
    public function setEmpresa(?IEmpresaResponse $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
     * @return array|null
     */
    public function getRelojesChecador(): ?array
    {
        return $this->relojes_checador;
    }

    /**
     * @param array|null $relojes_checador
     */
    public function setRelojesChecador(?array $relojes_checador)
    {
        $this->relojes_checador = $relojes_checador;
    }

    /**
     * @return string|null
     */
    public function getPuesto(): ?string
    {
        return $this->puesto;
    }
    
    /**
     * @param string|null $puesto
     */
    public function setPuesto(?string $puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return string|null
     */
    public function getClaveInterna(): ?string
    {
        return $this->clave_interna;
    }

    /**
     * @param string|null $clave_interna
     */
    public function setClaveInterna(?string $clave_interna)
    {
        $this->clave_interna = $clave_interna;
    }

    /**
     * @return bool|null
     */
    public function getTieneHijos(): ?bool
    {
        return $this->tiene_hijos;
    }

    /**
     * @param bool|null $tiene_hijos
     */
    public function setTieneHijos(?bool $tiene_hijos)
    {
        $this->tiene_hijos = $tiene_hijos;
    }
}
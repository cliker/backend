<?php
namespace App\Infrastructure;

use Exception;
use function file_exists;
use function Functional\concat;
use function is_file;

class ResourceExtensions
{
    public static function getUrl($resourceName, $data = array(), $path = null, $asUrl = null, $validate = false){
        $answer = "";

        try{       $answer =  app()->makeWith("paths.$resourceName", $data);
            $answer = self::resolvePath($path, $answer, $asUrl, $validate);
        }
        catch(Exception $exception){
            throw $exception;
        }

        return $answer;
    }

    private static function resolvePath($path = null, $answer = null, $asUrl = null, $validate = false){
        $answer=  !is_null($path) ? concat($answer, $path) : $answer;
        $file = public_path($answer);
        $response = "";

        if($validate){
            if(!empty($path) && (is_file($file) && file_exists($file))){
                $response = $asUrl  ? asset($answer) : $file;
            }
        }
        else{
            $response = $asUrl  ? asset($answer) : $file;
        }

        return $response;
    }
}
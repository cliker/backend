<?php


namespace App\Infrastructure;


use Exception;
use ReflectionClass;

class ClassExtensions
{
    public static function resolveEnumClass($tipo){
        try{
            $class = new ReflectionClass("App\\Domain\\".studly_case("e_".$tipo));
            return $class->getName();
        }
        catch (Exception $exception){
            return $tipo;
        }
    }

    public static function inverseResolveEnumClass($class){
        return strtolower(snake_case(str_replace_first("E", "", class_basename($class))));
    }

    public static function getPrivateProperty($class, $property){
        $myClassReflection = new \ReflectionClass(get_class($class));
        $mappings = $myClassReflection->getProperty($property);
        $mappings->setAccessible(true);
        return  $mappings->getValue($class);

    }

    public static function getAvailableContants($class){
        $reflectionClass = new ReflectionClass($class);
        return $reflectionClass->getConstants();
    }
}
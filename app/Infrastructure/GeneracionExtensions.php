<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 17/09/18
 * Time: 12:04
 */

namespace App\Infrastructure;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Implement\GeneracionQuery;
use App\DataAccess\Queries\Implement\RadiacionSolarMensualQuery;
use App\Domain\DivisionPolitica;
use App\Domain\Especificacion;
use App\Domain\EspecificacionPanel;
use App\Domain\Generacion;
use App\Domain\GeneracionSistema;
use App\Domain\Producto;
use App\Domain\RadiacionSolarMensual;
use Doctrine\Common\Collections\ArrayCollection;

class GeneracionExtensions
{
    /* @var $unitOfWork IUnitOfWork*/
    protected static $unitOfWork;

    private static function init()
    {
        self::$unitOfWork = resolve(IUnitOfWork::class);
    }

    /**
     * @param $divisionPoliticaId
     * @param $productoId
     * @return ArrayCollection
     */
    public static function obtenerGeneracionSistema($divisionPoliticaId, $productoId, $cantidadPaneles)
    {
        self::init();
        $producto = self::obtenerProductoPorId($productoId);
        ProxyExtensions::load($producto->getEspecificacion());

        /** @var Generacion $generacion */
        $generacion = self::obtenerGeneracionPorDivisionPolitica($divisionPoliticaId);

        $radiacionesSolaresMensuales = self::obtenerRadiacionesSolaresMensualesPorGeneracion($generacion->getId());

        $generacionesKwhPanelesDias = self::obtenerGeneracionKwhPanelDia(
            $radiacionesSolaresMensuales, $producto->getEspecificacion(), $cantidadPaneles);

        return $generacionesKwhPanelesDias;
    }

    /**
     * @param array $radiacionesSolaresMensuales
     * @param Especificacion $especificacion
     * @return ArrayCollection
     */
    private static function obtenerGeneracionKwhPanelDia(array $radiacionesSolaresMensuales, $especificacion, $cantidadPaneles) {
        $generacionesKwhPanelesDias = new ArrayCollection();

        /** @var EspecificacionPanel $especificacionPanel */
        $especificacionPanel        = $especificacion->getEspecificacionPanel();

        /** @var RadiacionSolarMensual $radiacionSolarMensual */
        foreach ($radiacionesSolaresMensuales as $radiacionSolarMensual) {
            $generacionKwhPanelDia = new GeneracionSistema();
            $generacionKwhPanelDia->setMes($radiacionSolarMensual->getMes());
            $generacionKwhPanelDia->setGeneracionPanel(
                ($radiacionSolarMensual->getDistribucion() * $especificacionPanel->getPmax() / 1000) * $cantidadPaneles);
            $dias = DateExtensions::getDaysByMonth($radiacionSolarMensual->getMes()->getNumeroMes());
            $generacionKwhPanelDia->setGeneracionMensual($dias * $generacionKwhPanelDia->getGeneracionPanel());

            $generacionesKwhPanelesDias->add($generacionKwhPanelDia);
        }

        return $generacionesKwhPanelesDias;
    }

    /**
     * @param $productoId
     * @return Producto
     */
    private static function obtenerProductoPorId($productoId) {
        /** @var Producto $producto */
        $producto = self::$unitOfWork
            ->getEntityManager()
            ->getRepository(Producto::class)
            ->find($productoId);

        return $producto;
    }

    /**
     * @param int $divisionPoliticaId
     * @return Generacion
     */
    private static function obtenerGeneracionPorDivisionPolitica(int $divisionPoliticaId) {
        $generacionQuery = new GeneracionQuery(self::$unitOfWork);
        $generacionQuery->init();
        $generacionQuery->withDivisionPoliticaId($divisionPoliticaId);
        $generacionQuery->includeRadiacionSolarMensual(true);
        $generacionQuery->sort("id", "asc");
        $generaciones = $generacionQuery->execute();
        $generacion = reset($generaciones);
        return $generacion;
    }

    /**
     * @param int $generacionId
     * @return array
     */
    private static function obtenerRadiacionesSolaresMensualesPorGeneracion(int $generacionId) {
        $radiacionSolarMensualQuery = new RadiacionSolarMensualQuery(self::$unitOfWork);
        $radiacionSolarMensualQuery->init();
        $radiacionSolarMensualQuery->withGeneracionId($generacionId);
        $radiacionSolarMensualQuery->includeMes(true);
        $radiacionSolarMensualQuery->sort("id", "asc");
        $radiacionesSolaresMensuales = $radiacionSolarMensualQuery->execute();
        return $radiacionesSolaresMensuales;
    }
}
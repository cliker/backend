<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/31/18
 * Time: 3:55 PM
 */

namespace App\Infrastructure;


class NumberExtensions
{
    ///////////////////////////////////////////////////////////
    ///////////////////////// NUMERIC /////////////////////////
    ///////////////////////////////////////////////////////////

    public static function isNumeric($numericValue)
    {
        return is_numeric($numericValue);
    }

    public static function isNumericInteger($numericIntegerValue)
    {
        return self::isNumeric($numericIntegerValue) && ctype_digit($numericIntegerValue);
    }

    ///////////////////////////////////////////////////////////
    ///////////////////////// INTEGER /////////////////////////
    ///////////////////////////////////////////////////////////

    public static function isInteger($integerValue)
    {
        return is_int($integerValue);
    }

    public static function isPositiveInteger($integerValue)
    {
        return self::isInteger($integerValue) && $integerValue > 0;
    }

    ///////////////////////////////////////////////////////////
    ////////////////////////// FLOAT /////////////////////////
    ///////////////////////////////////////////////////////////

    public static function isFloat($floatValue)
    {
        return is_float($floatValue);
    }

    public static function isPositiveFloat($floatValue)
    {
        return self::isFloat($floatValue) && $floatValue > 0.0;
    }

    ///////////////////////////////////////////////////////////
    ////////////////////////// ROUNDUP /////////////////////////
    ///////////////////////////////////////////////////////////

    public static function roundUp($value, $places)
    {
        $mult = pow(10, abs($places));
        return $places < 0 ?
        ceil($value / $mult) * $mult :
        ceil($value * $mult) / $mult;
    }
}
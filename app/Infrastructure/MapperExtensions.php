<?php


namespace App\Infrastructure;


use App\Services\Mapper\Interfaces\IBaseMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Configuration\MappingInterface;

class MapperExtensions
{

    /**
     * @param $mapperConfigSource
     * @param AutoMapperConfigInterface $mapperConfigDestination
     * @return AutoMapperConfigInterface
     */
    public static function addConfig($mapperConfigSource, AutoMapperConfigInterface  $mapperConfigDestination){
        if(is_array($mapperConfigSource)){
            foreach ($mapperConfigSource as $mapperConfig){
                $mapperConfigDestination = self::registerMapping($mapperConfig, $mapperConfigDestination);
            }
        }
        else if($mapperConfigSource instanceof AutoMapperConfigInterface) {
            $mapperConfigDestination = self::registerMapping($mapperConfigSource, $mapperConfigDestination);
        }
        else if($mapperConfigSource instanceof IBaseMapper){
            $mapperConfigDestination = self::registerMapping($mapperConfigSource->getMapper()->getConfiguration(), $mapperConfigDestination);
        }
        else if($mapperConfigSource instanceof AutoMapperInterface){
            $mapperConfigDestination = self::registerMapping($mapperConfigSource->getConfiguration(), $mapperConfigDestination);
        }
        return $mapperConfigDestination;
    }


    /**
     * @param $mapperConfig
     * @param AutoMapperConfigInterface $mapperConfigDestination
     * @return AutoMapperConfigInterface
     */
    private static function registerMapping(AutoMapperConfigInterface $mapperConfig, AutoMapperConfigInterface $mapperConfigDestination){
        $mappings = ClassExtensions::getPrivateProperty($mapperConfig, "mappings");
        foreach ($mappings as $mapping) {
            /*@var $mapping MappingInterface*/
            $mapperConfigDestination->registerMapping($mapping->getSourceClassName(), $mapping->getDestinationClassName());
        }
        return $mapperConfigDestination;
    }

    /**
     * @param $mapperConfig
     * @return AutoMapperConfigInterface
     */
    public static function registerMappingsForProxies(AutoMapperConfigInterface $mapperConfig){
        $mappings = ClassExtensions::getPrivateProperty($mapperConfig, "mappings");
        foreach ($mappings as $mapping) {
            /*@var $mapping MappingInterface*/
            $doctrineProxiesDomainNamespace = "DoctrineProxies\__CG__\\".$mapping->getSourceClassName();
            $mapperConfig->registerMapping($doctrineProxiesDomainNamespace, $mapping->getDestinationClassName())
                ->copyFrom($mapping->getSourceClassName(), $mapping->getDestinationClassName());
        }

      return $mapperConfig;

    }
}
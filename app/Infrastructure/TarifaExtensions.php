<?php
/**
 * Created by PhpStorm.
 * Usuario: karimy chable
 * Date: 13/02/2018
 * Time: 10:20 AM
 */

namespace App\Infrastructure;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Queries\Implement\LineaConsumoEnergiaQuery;
use App\Domain\LineaConsumoEnergia;
use App\Domain\Tarifa;
use App\Infrastructure\StringExtensions;
use DateTime;

class TarifaExtensions
{
    const MONTHS_BY_YEARS = 12;
    const PERIODS_OF_TWO_MONTHS_BY_YEARS = 6;

    const DAYS_IN_ONE_MONTH = array(28, 29, 30, 31);
    const DAYS_IN_TWO_MONTHS = array(59, 60, 61, 62);

    const MONTHS_WITHIN_SUMMER  = array(4, 5, 6, 7, 8, 9);
    const MONTHS_OUTSIDE_SUMMER = array(10, 11, 12, 1, 2, 3);

    /**
     * @param DateTime $date
     * @return bool
     */
    public static function isDateWithinSummer($date)
    {
        $mes = (int) $date->format('m');
        $isWithinSummer = in_array($mes, self::MONTHS_WITHIN_SUMMER);
        return $isWithinSummer;
    }

    /**
     * @param DateTime $date
     * @return bool
     */
    public static function isDateOutsideSummer($date)
    {
        $mes = (int) $date->format('m');
        $isOutsideSummer = in_array($mes, self::MONTHS_OUTSIDE_SUMMER);
        return $isOutsideSummer;
    }

    public static function makeBimonthlyToMonthly($bimonthlyArray)
    {
        $monthlyArray = [];

        /** @var LineaConsumoEnergia $bimonthlyItem */
        foreach ($bimonthlyArray as $bimonthlyItem) {
            $diff = (int) $bimonthlyItem->getFechaInicial()->diff($bimonthlyItem->getFechaFinal())->format('%a');
            $pivot = clone $bimonthlyItem;

            if (in_array($diff, self::DAYS_IN_TWO_MONTHS)) {
                $pivotDate = clone $bimonthlyItem->getFechaInicial();
                $diffPivote = StringExtensions::toInt($diff/2);
                $pivotDate->modify('+ ' . $diffPivote . ' days');

                $bimonthlyItem->setFechaFinal($pivotDate);
                $pivot        ->setFechaInicial($pivotDate);

                $bimonthlyItem->setConsumoKwh($bimonthlyItem->getConsumoKwh()/2);
                $pivot        ->setConsumoKwh($pivot->getConsumoKwh()/2);

                array_push($monthlyArray, $bimonthlyItem, $pivot);
            }
        }

        return $monthlyArray;
    }

    /**
     * @param Tarifa $rate
     * @param $lines
     * @return boolean
     */
    public static function isDAC($rate, $lines)
    {
        $limite  = $rate->getLimiteAnual();
        $consumo = 0;
        $esDAC   = false;

        /** @var LineaConsumoEnergia $line */
        foreach ($lines as $line) {
            $consumo += $line->getConsumoKwh();

            if ($consumo >= $limite) {
                $esDAC = true;
                break;
            }
        }

        return $esDAC;
    }

    /**
     * @param $lines
     * @return int|mixed
     */
    public static function getAnnualConsumption($lines)
    {
        $consumption = 0;
        /** @var LineaConsumoEnergia $line */
        foreach ($lines as $line) {
            $consumption += $line->getConsumoKwh();
        }
        return $consumption;
    }

    /**
     * @param boolean $esBimestral
     * @param int $consumoEnergiaContactoId
     * @param string $fechaInicio
     * @return array|mixed
     */
    public static function getTwelveLinesByDate($esBimestral, $consumoEnergiaContactoId, $fechaInicio)
    {
        $unitOfWork = resolve(IUnitOfWork::class);
        $elementos = ($esBimestral) ? self::PERIODS_OF_TWO_MONTHS_BY_YEARS : self::MONTHS_BY_YEARS;

        $lineaConsumoEnergiaQuery = new LineaConsumoEnergiaQuery($unitOfWork);
        $lineaConsumoEnergiaQuery->init();
        $lineaConsumoEnergiaQuery->withConsumoEnergiaContactoId($consumoEnergiaContactoId);
        $lineaConsumoEnergiaQuery->maxFechaInicial($fechaInicio);
        $lineaConsumoEnergiaQuery->sort("id", "asc");
        $lineaConsumoEnergiaQuery->paginate(true, $elementos, 1);
        $lineasConsumosEnergias = $lineaConsumoEnergiaQuery->execute();

        if ($esBimestral) {
            $lineasConsumosEnergias = self::makeBimonthlyToMonthly($lineasConsumosEnergias);
        }

        return $lineasConsumosEnergias;
    }
}

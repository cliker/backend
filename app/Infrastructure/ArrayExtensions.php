<?php
/**
 * Created by PhpStorm.
 * Usuario: erichuerta
 * Date: 16/03/18
 * Time: 16:52
 */

namespace App\Infrastructure;

use function array_combine;
use function array_keys;
use function array_values;

class ArrayExtensions
{
    const INCLUDE_VALUE  = true;
    const EXCLUDE_VALUE  = false;

    const MULTIPLE_ZERO  = 0;
    const MULTIPLE_ONE   = 1;
    const REMAINDER_ZERO = 0;

    public static function isNullOrEmpty($arrayValue)
    {
        return $arrayValue == null || count($arrayValue) == 0;
    }

    public static function isNotNullOrEmpty($arrayValue)
    {
        return !self::isNullOrEmpty($arrayValue);
    }

    public static function search($needle, $haystack)
    {
        if (is_array($needle)) {
            return !array_diff($needle, $haystack);
        } else if (is_string($needle)) {
            return in_array($needle, $haystack);
        }
    }

    public static function isMultidimensional($array)
    {
        return isset($array[0]);
    }

    public static function mergeAssociative(...$array)
    {

        $merge = [];
        foreach ($array as $arrayElement) {
            $merge = $merge + $arrayElement;
        }
        return $merge;
    }

    public static function toObject($array)
    {
        return (object)$array;
    }

    public static function lowerThan($array, $value, $includeValue = false)
    {
        $filteredArray = array_filter($array, function ($elem) use ($value, $includeValue) {
            return ($includeValue) ? $elem <= $value : $elem = $value;
        });

        return $filteredArray;
    }

    /**
     * Array wi
     * @param $value
     * @param $array
     * @return float|int
     */
    public static function getMinMultiple($value, $array)
    {
        sort($array);
        $multiplo = self::MULTIPLE_ZERO;
        if (in_array($value, $array)) {
            $multiplo = self::MULTIPLE_ONE;
        } else {
            $nuevoArreglo = self::lowerThan($array, $value / 2,
                self::INCLUDE_VALUE);
            for ($i = count($nuevoArreglo) - 1; $i >= 0; $i--) {
                if ($value % $nuevoArreglo[$i] == self::REMAINDER_ZERO) {
                    $multiplo = $value / $nuevoArreglo[$i];
                    break;
                }
            }
        }
        return $multiplo;
    }

    public static function cloneArrayWithoutSomeKeys($keys, $array)
    {
        $arr = array_filter($array, function($k) use ($keys) {
            return !in_array($k, $keys);
        }, ARRAY_FILTER_USE_KEY);

        return array_values($arr);
    }

    public static function cloneArray($array){
        $arrayNew = array();
        foreach ($array as $k => $v) {
            $arrayNew[$k] = clone $v;
        }

        return $arrayNew;
    }
}
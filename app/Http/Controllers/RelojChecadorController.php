<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IRelojChecadorService;
use App\Services\Validations\Interfaces\IRelojChecadorValidator;
use Illuminate\Http\Request;

class RelojChecadorController extends Controller
{
    protected $relojChecadorService;
    protected $relojChecadorValidator;

    public function __construct(
        IRelojChecadorService $relojChecadorService,
        IRelojChecadorValidator $relojChecadorValidator
    )
    {
        $this->relojChecadorService = $relojChecadorService;
        $this->relojChecadorValidator = $relojChecadorValidator;
    }

    public function find(Request $request)
    {
        $this->relojChecadorValidator->validateFind($request);
        return $this->relojChecadorService->find($request);
    }

    public function get($id)
    {
        $this->relojChecadorValidator->validateGet($id);
        return $this->relojChecadorService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->relojChecadorValidator->validateStore($request);
        return $this->relojChecadorService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->relojChecadorValidator->validateUpdate($request);
        return $this->relojChecadorService->update($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy(Request $request)
    {
        $this->relojChecadorValidator->eliminar($request);
        return $this->relojChecadorService->delete($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function  drop($id)
    {
        $this->relojChecadorValidator->validateDrop($id);
        return $this->relojChecadorService->drop($id);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     */
    public function removeEmployee(Request $request){
        $this->relojChecadorValidator->validateRemoveEmployee($request);
        return $this->relojChecadorService->removeEmployee($request);
    }
}
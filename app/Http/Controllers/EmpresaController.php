<?php
namespace App\Http\Controllers;

use App\Domain\Empresa;
use App\Services\Interfaces\IEmpresaService;
use App\Services\Validations\Interfaces\IEmpresaValidator;
use Illuminate\Http\Request;

class EmpresaController extends Controller
{
    protected $empresaService;
    protected $empresaValidator;

    public function __construct(
        IEmpresaService $empresaService,
        IEmpresaValidator $empresaValidator
    )
    {
        $this->empresaService = $empresaService;
        $this->empresaValidator = $empresaValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empresa::class, $request]); */
        $this->empresaValidator->validateFind($request);
        return $this->empresaService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [Empresa::class, $id]); */
        $this->empresaValidator->validateGet($id);
        return $this->empresaService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empresa::class, $request]); */
        $this->empresaValidator->validateStore($request);
        return $this->empresaService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empresa::class, $request]); */
        $this->empresaValidator->validateUpdate($request);
        return $this->empresaService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize',  [Empresa::class, $id]); */
        $this->empresaValidator->validateDelete($id);
        return $this->empresaService->delete($id);
    }
}
<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IHistoricoEmpleadoService;
use App\Services\Validations\Interfaces\IHistoricoEmpleadoValidator;
use Illuminate\Http\Request;

class HistoricoEmpleadoController extends Controller
{
    protected $historicoService;
    protected $historicoValidator;

    public function __construct(
        IHistoricoEmpleadoService $historicoService,
        IHistoricoEmpleadoValidator $historicoValidator
    )
    {
        $this->historicoService = $historicoService;
        $this->historicoValidator = $historicoValidator;
    }

    public function find(Request $request)
    {
        $this->historicoValidator->validateFind($request);
        return $this->historicoService->find($request);
    }

    public function get($id)
    {
        $this->historicoValidator->validateGet($id);
        return $this->historicoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->historicoValidator->validateStore($request);
        return $this->historicoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->historicoValidator->validateUpdate($request);
        return $this->historicoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->historicoValidator->validateDelete($id);
        return $this->historicoService->delete($id);
    }
}
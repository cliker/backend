<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICTipoIncidenciaService;
use App\Services\Validations\Interfaces\ICTipoIncidenciaValidator;
use Illuminate\Http\Request;

class CTipoIncidenciaController extends Controller
{
    protected $tipoIncidenciaService;
    protected $tipoIncidenciaValidator;

    public function __construct(
        ICTipoIncidenciaService $tipoIncidenciaService,
        ICTipoIncidenciaValidator $tipoIncidenciaValidator
    )
    {
        $this->tipoIncidenciaService = $tipoIncidenciaService;
        $this->tipoIncidenciaValidator = $tipoIncidenciaValidator;
    }

    public function find(Request $request)
    {
        $this->tipoIncidenciaValidator->validateFind($request);
        return $this->tipoIncidenciaService->find($request);
    }

    public function get($id)
    {
        $this->tipoIncidenciaValidator->validateGet($id);
        return $this->tipoIncidenciaService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->tipoIncidenciaValidator->validateStore($request);
        return $this->tipoIncidenciaService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->tipoIncidenciaValidator->validateUpdate($request);
        return $this->tipoIncidenciaService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->tipoIncidenciaValidator->validateDelete($id);
        return $this->tipoIncidenciaService->delete($id);
    }
}
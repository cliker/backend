<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IConfiguracionService;
use App\Services\Validations\Interfaces\IConfiguracionValidator;
use Illuminate\Http\Request;

class ConfiguracionController extends Controller
{
    protected $configuracionService;
    protected $configuracionValidator;

    public function __construct(
        IConfiguracionService $configuracionService,
        IConfiguracionValidator $configuracionValidator
    )
    {
        $this->configuracionService = $configuracionService;
        $this->configuracionValidator= $configuracionValidator;
    }

    public function find(Request $request)
    {
        $this->configuracionValidator->validateFind($request);
        return $this->configuracionService->find($request);
    }

    public function get($id)
    {
        $this->configuracionValidator->validateGet($id);
        return $this->configuracionService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->configuracionValidator->validateStore($request);
        return $this->configuracionService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->configuracionValidator->validateUpdate($request);
        return $this->configuracionService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->configuracionValidator->validateDelete($id);
        return $this->configuracionService->delete($id);
    }
}
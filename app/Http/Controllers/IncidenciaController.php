<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IIncidenciaService;
use App\Services\Validations\Interfaces\IIncidenciaValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class IncidenciaController extends Controller
{
    protected $incidenciaService;
    protected $incidenciaValidator;

    public function __construct(
        IIncidenciaService $incidenciaService,
        IIncidenciaValidator $incidenciaValidator
    )
    {
        $this->incidenciaService = $incidenciaService;
        $this->incidenciaValidator = $incidenciaValidator;
    }

    public function find(Request $request)
    {
        $this->incidenciaValidator->validateFind($request);
        return $this->incidenciaService->find($request);
    }

    public function get($id)
    {
        $this->incidenciaValidator->validateGet($id);
        return $this->incidenciaService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->incidenciaValidator->validateStore($request);
        return $this->incidenciaService->create($request);
    }

    public function download(Request $request){
        $archivo = storage_path()."/incidencias/".$request->url_archivo;
 
        return response()->download($archivo);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function upload_documento(Request $request)
    {
        $this->incidenciaValidator->upload_documento($request);
        return $this->incidenciaService->documento($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->incidenciaValidator->validateUpdate($request);
        return $this->incidenciaService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->incidenciaValidator->validateDelete($id);
        return $this->incidenciaService->delete($id);
    }
}
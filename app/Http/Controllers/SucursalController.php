<?php
namespace App\Http\Controllers;

use App\Domain\Sucursal;
use App\Services\Interfaces\ISucursalService;
use App\Services\Validations\Interfaces\ISucursalValidator;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    protected $sucursalService;
    protected $sucursalValidator;

    public function __construct(
        ISucursalService $sucursalService,
        ISucursalValidator $sucursalValidator
    )
    {
        $this->sucursalService = $sucursalService;
        $this->sucursalValidator = $sucursalValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Sucursal::class, $request]); */
        $this->sucursalValidator->validateFind($request);
        return $this->sucursalService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [Sucursal::class, $id]); */
        $this->sucursalValidator->validateGet($id);
        return $this->sucursalService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Sucursal::class, $request]); */
        $this->sucursalValidator->validateStore($request);
        return $this->sucursalService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Sucursal::class, $request]); */
        $this->sucursalValidator->validateUpdate($request);
        return $this->sucursalService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize',  [Sucursal::class, $id]); */
        $this->sucursalValidator->validateDelete($id);
        return $this->sucursalService->delete($id);
    }
}
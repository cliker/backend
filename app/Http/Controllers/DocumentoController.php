<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IDocumentoService;
use App\Services\Validations\Interfaces\IDocumentoValidator;
use Illuminate\Http\Request;

class DocumentoController extends Controller
{
    protected $documentoService;
    protected $documentoValidator;

    public function __construct(
        IDocumentoService $documentoService,
        IDocumentoValidator $documentoValidator
    )
    {
        $this->documentoService = $documentoService;
        $this->documentoValidator = $documentoValidator;
    }

    public function find(Request $request)
    {
        $this->documentoValidator->validateFind($request);
        return $this->documentoService->find($request);
    }

    public function get($id)
    {
        $this->documentoValidator->validateGet($id);
        return $this->documentoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->documentoValidator->validateStore($request);
        return $this->documentoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->documentoValidator->validateUpdate($request);
        return $this->documentoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->documentoValidator->validateDelete($id);
        return $this->documentoService->delete($id);
    }
}
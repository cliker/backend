<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IRegistroChecadorService;
use App\Services\Validations\Interfaces\IRegistroChecadorValidator;
use Illuminate\Http\Request;

class RegistroChecadorController extends Controller
{
    protected $registroChecadorService;
    protected $registroChecadorValidator;

    public function __construct(
        IRegistroChecadorService $registroChecadorService,
        IRegistroChecadorValidator $registroChecadorValidator
    )
    {
        $this->registroChecadorService = $registroChecadorService;
        $this->registroChecadorValidator = $registroChecadorValidator;
    }

    public function find(Request $request)
    {
        $this->registroChecadorValidator->validateFind($request);
        return $this->registroChecadorService->find($request);
    }

    public function get($id)
    {
        $this->registroChecadorValidator->validateGet($id);
        return $this->registroChecadorService->get($id);
    }

    public function getLastIdAsp($empresa_id)
    {
        return $this->registroChecadorService->getLastIdAsp($empresa_id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->registroChecadorValidator->validateStore($request);
        return $this->registroChecadorService->create($request);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function storeFromAsp(Request $request)
    {
        $this->registroChecadorValidator->validateStoreFromAsp($request);
        return $this->registroChecadorService->createFromAsp($request,"historico");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->registroChecadorValidator->validateUpdate($request);
        return $this->registroChecadorService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->registroChecadorValidator->validateDelete($id);
        return $this->registroChecadorService->delete($id);
    }

    public function updateChecadas(){
        $this->registroChecadorService->updateChecadas();
    }

    public function historico(Request $request){
        $this->registroChecadorValidator->empresa($request->id);
        return $this->registroChecadorService->historico($request->id);
    }
}
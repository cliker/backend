<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IArchivoService;
use App\Services\Validations\Interfaces\IArchivoValidator;
use Illuminate\Http\Request;

class ArchivoController extends Controller
{
    protected $archivoService;
    protected $archivoValidator;

    public function __construct(
        IArchivoService $archivoService,
        IArchivoValidator $archivoValidator
    )
    {
        $this->archivoService = $archivoService;
        $this->archivoValidator = $archivoValidator;
    }

    public function find(Request $request)
    {
        return $this->archivoService->find($request);
    }

    public function get($id)
    {
        return $this->archivoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        return $this->archivoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        return $this->archivoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        return $this->archivoService->delete($id);
    }

    public function documentoEmpleadoStore(Request $request){
        $this->archivoValidator->validateDocumentoEmpleadoStore($request);
        return $this->archivoService->documentoEmpleadoStore($request);

    }

    public function documentoEmpleadoUpdate(Request $request){
        $this->archivoValidator->validateDocumentoEmpleadoUpdate($request);
        return $this->archivoService->documentoEmpleadoUpdate($request);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function documentoEmpleadoDelete($id){

        $this->archivoValidator->validateDocumentoEmpleadoDelete($id);
        return $this->archivoService->documentoEmpleadoDelete($id);

    }
}
<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IDetalleHorarioService;
use App\Services\Validations\Interfaces\IDetalleHorarioValidator;
use Illuminate\Http\Request;

class DetalleHorarioController extends Controller
{
    protected $detalleHorarioService;
    protected $detalleHorarioValidator;

    public function __construct(
        IDetalleHorarioService $detalleHorarioService,
        IDetalleHorarioValidator $detalleHorarioValidator
    )
    {
        $this->detalleHorarioService = $detalleHorarioService;
        $this->detalleHorarioValidator = $detalleHorarioValidator;
    }

    public function find(Request $request)
    {
        $this->detalleHorarioValidator->validateFind($request);
        return $this->detalleHorarioService->find($request);
    }

    public function get($id)
    {
        $this->detalleHorarioValidator->validateGet($id);
        return $this->detalleHorarioService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->detalleHorarioValidator->validateStore($request);
        return $this->detalleHorarioService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->detalleHorarioValidator->validateUpdate($request);
        return $this->detalleHorarioService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->detalleHorarioValidator->validateDelete($id);
        return $this->detalleHorarioService->delete($id);
    }
}
<?php
namespace App\Http\Controllers;

use App\Domain\Cliente;
use App\Services\Interfaces\IClienteService;
use App\Services\Validations\Interfaces\IClienteValidator;
use Illuminate\Http\Request;

class ClienteController extends Controller
{

    protected $clienteService;
    protected $clienteValidator;

    public function __construct(
        IClienteService $clienteService,
        IClienteValidator $clienteValidator
    )
    {
        $this->clienteService = $clienteService;
        $this->clienteValidator = $clienteValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize', [Cliente::class, $request]); */
        $this->clienteValidator->validateFind($request);
        return $this->clienteService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize', [Cliente::class, $id]); */
        $this->clienteValidator->validateGet($id);
        return $this->clienteService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize', [Cliente::class, $request]); */
        $this->clienteValidator->validateStore($request);
        return $this->clienteService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize', [Cliente::class, $request]); */
        $this->clienteValidator->validateUpdate($request);
        return $this->clienteService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize', [Cliente::class, $id]); */
        $this->clienteValidator->validateDelete($id);
        return $this->clienteService->delete($id);
    }
}
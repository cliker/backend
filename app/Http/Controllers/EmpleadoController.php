<?php
namespace App\Http\Controllers;

use App\Domain\Empleado;
use App\Services\Interfaces\IEmpleadoService;
use App\Services\Interfaces\IImportarDatosService;
use App\Services\Validations\Interfaces\IEmpleadoValidator;
use Illuminate\Http\Request;


class EmpleadoController extends Controller
{
    protected $empleadoService;
    protected $empleadoValidator;
    protected $importarDatosService;
    public function __construct(
        IEmpleadoService $empleadoService,
        IEmpleadoValidator $empleadoValidator,
        IImportarDatosService $importarDatosService
    )
    {
        $this->empleadoService = $empleadoService;
        $this->empleadoValidator = $empleadoValidator;
        $this->importarDatosService = $importarDatosService;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empleado::class, $request]); */
        $this->empleadoValidator->validateFind($request);
        return $this->empleadoService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [Empleado::class, $id]); */
        $this->empleadoValidator->validateGet($id);
        return $this->empleadoService->get($id);
    }

    public function dashboard(Request $request){
        $validacion = $this->empleadoValidator->dashboard($request);
        return $this->empleadoService->dashboard($request);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empleado::class, $request]); */
        $this->empleadoValidator->validateStore($request);
  
        if($this->empleadoService->creaUsuario($request)){
            return $this->empleadoService->create($request);
        }
        else{
            $response = array('status' => false, 'message' => "EL correo ya se ha registrado anteriormente, intentar con otro");
            return response()->json($response, 402);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Empleado::class, $request]); */
        $this->empleadoValidator->validateUpdate($request);
        return $this->empleadoService->update($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function destroy(Request $request)
    {
        /* $this->authorize('getAuthorize',  [Empleado::class, $id]); */
        $this->empleadoValidator->eliminar($request);
        return $this->empleadoService->delete($request);
    }

    public function findChecked(Request $request){
        $this->empleadoValidator->checadas($request);
        return $this->empleadoService->findChecked($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importar(Request $request){
        $this->empleadoValidator->importarDatos($request);
        $respuesta = $this->importarDatosService->importarEmpleados($request);
        if($respuesta == true){
            return response()->json(['message' => 'Datos guardados correctamente'], 202);
        }
    }

    public function reingreso(Request $request)
    {
        /* $this->authorize('getAuthorize',  [Empleado::class, $id]); */
        $this->empleadoValidator->eliminar($request);
        return $this->empleadoService->reingreso($request);
    }

    public function asignacion(Request $request){
        $this->empleadoValidator->asignacion($request);
        return $this->empleadoService->asignacion($request);
    }

    public function rotacion(Request $request){
        $this->empleadoValidator->dashboard($request);
        return $this->empleadoService->rotacion($request);
    }

    public function incidencias(Request $request){
        $this->empleadoValidator->incidencias($request);
        return $this->empleadoService->incidencias($request);
    }
}
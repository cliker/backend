<?php
namespace App\Http\Controllers;

use App\Domain\Departamento;
use App\Services\Interfaces\IDepartamentoService;
use App\Services\Validations\Interfaces\IDepartamentoValidator;
use Illuminate\Http\Request;

class DepartamentoController extends Controller
{
    protected $departamentoService;
    protected $departamentoValidator;

    public function __construct(
        IDepartamentoService $departamentoService,
        IDepartamentoValidator $departamentoValidator
    )
    {
        $this->departamentoService = $departamentoService;
        $this->departamentoValidator = $departamentoValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Departamento::class, $request]); */
        $this->departamentoValidator->validateFind($request);
        return $this->departamentoService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [Departamento::class, $id]); */
        $this->departamentoValidator->validateGet($id);
        return $this->departamentoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Departamento::class, $request]); */
        $this->departamentoValidator->validateStore($request);
        return $this->departamentoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Departamento::class, $request]); */
        $this->departamentoValidator->validateUpdate($request);
        return $this->departamentoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize',  [Departamento::class, $id]); */
        $this->departamentoValidator->validateDelete($id);
        return $this->departamentoService->delete($id);
    }
}
<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IReporteService;
use Illuminate\Http\Request;

use App\Services\Interfaces\IEmpleadoService;
use App\Services\Interfaces\IIncidenciaService;
use App\Services\Validations\Interfaces\IIncidenciaValidator;


class ReporteController extends Controller
{
    protected $asistenciaService;
    protected $reporteService;
    /* protected $incidenciaValidator; */
    protected $incidenciaService;

    public function __construct(
        IEmpleadoService $empleadoService,
        IIncidenciaService $incidenciaService,
        IReporteService $reporteService
        
    )
    {
        $this->empleadoService = $empleadoService;
        $this->incidenciaService = $incidenciaService;
        $this->reporteService = $reporteService;
        /* $this->incidenciaValidator = $incidenciaValidator; */
    }

    public function asistencia(Request $request)
    {
        $registros = $this->empleadoService->findChecked($request);
        return $this->reporteService->asistencia($registros);
    }

    public function incidencias(Request $request){
        $registros = $this->incidenciaService->find($request);
        return $this->reporteService->incidencias($registros);
    }
}
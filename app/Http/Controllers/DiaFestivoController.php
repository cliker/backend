<?php
namespace App\Http\Controllers;

use App\Domain\DiaFestivo;
use App\Services\Interfaces\IDiaFestivoService;
use App\Services\Validations\Interfaces\IDiaFestivoValidator;
use Illuminate\Http\Request;

class DiaFestivoController extends Controller
{
    protected $diaFestivoService;
    protected $diaFestivoValidator;

    public function __construct(
        IDiaFestivoService $diaFestivoService,
        IDiaFestivoValidator $diaFestivoValidator
    )
    {
        $this->diaFestivoService = $diaFestivoService;
        $this->diaFestivoValidator = $diaFestivoValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [DiaFestivo::class, $request]); */
        $this->diaFestivoValidator->validateFind($request);
        return $this->diaFestivoService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [DiaFestivo::class, $id]); */
        $this->diaFestivoValidator->validateGet($id);
        return $this->diaFestivoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [DiaFestivo::class, $request]); */
        $this->diaFestivoValidator->validateStore($request);
        return $this->diaFestivoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
       /*  $this->authorize('findAuthorize',  [DiaFestivo::class, $request]); */
        $this->diaFestivoValidator->validateUpdate($request);
        return $this->diaFestivoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize',  [DiaFestivo::class, $id]); */
        $this->diaFestivoValidator->validateDelete($id);
        return $this->diaFestivoService->delete($id);
    }
}
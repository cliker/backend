<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICCiudadService;
use App\Services\Validations\Interfaces\ICCiudadValidator;
use Illuminate\Http\Request;

class CCiudadController extends Controller
{
    protected $ciudadService;
    protected $ciudadValidator;

    public function __construct(
        ICCiudadService $ciudadService,
        ICCiudadValidator $ciudadValidator
    )
    {
        $this->ciudadService = $ciudadService;
        $this->ciudadValidator = $ciudadValidator;
    }

    public function find(Request $request)
    {
        $this->ciudadValidator->validateFind($request);
        return $this->ciudadService->find($request);
    }

    public function get($id)
    {
        $this->ciudadValidator->validateGet($id);
        return $this->ciudadService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->ciudadValidator->validateStore($request);
        return $this->ciudadService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->ciudadValidator->validateUpdate($request);
        return $this->ciudadService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->ciudadValidator->validateDelete($id);
        return $this->ciudadService->delete($id);
    }
}
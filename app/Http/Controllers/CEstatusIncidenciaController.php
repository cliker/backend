<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICEstatusIncidenciaService;
use App\Services\Validations\Interfaces\ICEstatusIncidenciaValidator;
use Illuminate\Http\Request;

class CEstatusIncidenciaController extends Controller
{
    protected $estatusIncidenciaService;
    protected $estatusIncidenciaValidator;

    public function __construct(
        ICEstatusIncidenciaService $estatusIncidenciaService,
        ICEstatusIncidenciaValidator $estatusIncidenciaValidator
    )
    {
        $this->estatusIncidenciaService = $estatusIncidenciaService;
        $this->estatusIncidenciaValidator = $estatusIncidenciaValidator;
    }

    public function find(Request $request)
    {
        $this->estatusIncidenciaValidator->validateFind($request);
        return $this->estatusIncidenciaService->find($request);
    }

    public function get($id)
    {
        $this->estatusIncidenciaValidator->validateGet($id);
        return $this->estatusIncidenciaService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->estatusIncidenciaValidator->validateStore($request);
        return $this->estatusIncidenciaService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->estatusIncidenciaValidator->validateUpdate($request);
        return $this->estatusIncidenciaService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->estatusIncidenciaValidator->validateDelete($id);
        return $this->estatusIncidenciaService->delete($id);
    }
}
<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICTipoDocumentoService;
use App\Services\Validations\Interfaces\ICTipoDocumentoValidator;
use Illuminate\Http\Request;

class CTipoDocumentoController extends Controller
{
    protected $tipoDocumentoService;
    protected $tipoDocumentoValidator;

    public function __construct(
        ICTipoDocumentoService $tipoDocumentoService,
        ICTipoDocumentoValidator $tipoDocumentoValidator
    )
    {
        $this->tipoDocumentoService = $tipoDocumentoService;
        $this->tipoDocumentoValidator = $tipoDocumentoValidator;
    }

    public function find(Request $request)
    {
        $this->tipoDocumentoValidator->validateFind($request);
        return $this->tipoDocumentoService->find($request);
    }

    public function get($id)
    {
        $this->tipoDocumentoValidator->validateGet($id);
        return $this->tipoDocumentoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->tipoDocumentoValidator->validateStore($request);
        return $this->tipoDocumentoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->tipoDocumentoValidator->validateUpdate($request);
        return $this->tipoDocumentoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->tipoDocumentoValidator->validateDelete($id);
        return $this->tipoDocumentoService->delete($id);
    }
}
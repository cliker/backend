<?php
namespace App\Http\Controllers;

use App\Domain\Horario;
use App\Services\Interfaces\IHorarioService;
use App\Services\Validations\Interfaces\IHorarioValidator;
use Illuminate\Http\Request;

class HorarioController extends Controller
{
    protected $horarioService;
    protected $horarioValidator;

    public function __construct(
        IHorarioService $horarioService,
        IHorarioValidator $horarioValidator
    )
    {
        $this->horarioService = $horarioService;
        $this->horarioValidator = $horarioValidator;
    }

    public function find(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Horario::class, $request]); */
        $this->horarioValidator->validateFind($request);
        return $this->horarioService->find($request);
    }

    public function get($id)
    {
        /* $this->authorize('getAuthorize',  [Horario::class, $id]); */
        $this->horarioValidator->validateGet($id);
        return $this->horarioService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Horario::class, $request]); */
        $this->horarioValidator->validateStore($request);
        return $this->horarioService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        /* $this->authorize('findAuthorize',  [Horario::class, $request]); */
        $this->horarioValidator->validateUpdate($request);
        return $this->horarioService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        /* $this->authorize('getAuthorize',  [Horario::class, $id]); */
        $this->horarioValidator->validateDelete($id);
        return $this->horarioService->delete($id);
    }
}
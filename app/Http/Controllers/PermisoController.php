<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\IPermisoService;
use App\Services\Validations\Interfaces\IPermisoValidator;
use Illuminate\Http\Request;

class PermisoController extends Controller
{
    protected $permisoService;
    protected $permisoValidator;

    public function __construct(
        IPermisoService $permisoService,
        IPermisoValidator $permisoValidator
    )
    {
        $this->permisoService = $permisoService;
        $this->permisoValidator = $permisoValidator;
    }

    public function find(Request $request)
    {
        $this->permisoValidator->validateFind($request);
        return $this->permisoService->find($request);
    }

    public function get($id)
    {
        $this->permisoValidator->validateGet($id);
        return $this->permisoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->permisoValidator->validateStore($request);
        return $this->permisoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->permisoValidator->validateUpdate($request);
        return $this->permisoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->permisoValidator->validateDelete($id);
        return $this->permisoService->delete($id);
    }
}
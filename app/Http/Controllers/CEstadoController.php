<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICEstadoService;
use App\Services\Validations\Interfaces\ICEstadoValidator;
use Illuminate\Http\Request;

class CEstadoController extends Controller
{
    protected $estadoService;
    protected $estadoValidator;

    public function __construct(
        ICEstadoService $estadoService,
        ICEstadoValidator $estadoValidator
    )
    {
        $this->estadoService = $estadoService;
        $this->estadoValidator = $estadoValidator;
    }

    public function find(Request $request)
    {
        $this->estadoValidator->validateFind($request);
        return $this->estadoService->find($request);
    }

    public function get($id)
    {
        $this->estadoValidator->validateGet($id);
        return $this->estadoService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->estadoValidator->validateStore($request);
        return $this->estadoService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->estadoValidator->validateUpdate($request);
        return $this->estadoService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->estadoValidator->validateDelete($id);
        return $this->estadoService->delete($id);
    }
}
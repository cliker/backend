<?php
namespace App\Http\Controllers;

use App\Services\Interfaces\ICEstadoCivilService;
use App\Services\Validations\Interfaces\ICEstadoCivilValidator;
use Illuminate\Http\Request;

class CEstadoCivilController extends Controller
{
    protected $estadoCivilService;
    protected $estadoCivilValidator;

    public function __construct(
        ICEstadoCivilService $estadoCivilService,
        ICEstadoCivilValidator $estadoCivilValidator
    )
    {
        $this->estadoCivilService = $estadoCivilService;
        $this->estadoCivilValidator = $estadoCivilValidator;
    }

    public function find(Request $request)
    {
        $this->estadoCivilValidator->validateFind($request);
        return $this->estadoCivilService->find($request);
    }

    public function get($id)
    {
        $this->estadoCivilValidator->validateGet($id);
        return $this->estadoCivilService->get($id);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $this->estadoCivilValidator->validateStore($request);
        return $this->estadoCivilService->create($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request)
    {
        $this->estadoCivilValidator->validateUpdate($request);
        return $this->estadoCivilService->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->estadoCivilValidator->validateDelete($id);
        return $this->estadoCivilService->delete($id);
    }
}
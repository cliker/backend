<?php

namespace App\Providers;

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Configs\UnitOfWork;
use App\DataAccess\Queries\Implement\CEstadoCivilQuery;
use App\DataAccess\Queries\Implement\CEstadoQuery;
use App\DataAccess\Queries\Implement\CCiudadQuery;
use App\DataAccess\Queries\Implement\CEstatusIncidenciaQuery;
use App\DataAccess\Queries\Implement\ClienteQuery;
use App\DataAccess\Queries\Implement\ConfiguracionQuery;
use App\DataAccess\Queries\Implement\CTipoDocumentoQuery;
use App\DataAccess\Queries\Implement\CTipoIncidenciaQuery;
use App\DataAccess\Queries\Implement\DepartamentoQuery;
use App\DataAccess\Queries\Implement\DetalleHorarioQuery;
use App\DataAccess\Queries\Implement\DiaFestivoQuery;
use App\DataAccess\Queries\Implement\DocumentoQuery;
use App\DataAccess\Queries\Implement\EmpleadoQuery;
use App\DataAccess\Queries\Implement\EmpresaQuery;
use App\DataAccess\Queries\Implement\HistoricoEmpleadoQuery;
use App\DataAccess\Queries\Implement\HorarioQuery;
use App\DataAccess\Queries\Implement\IncidenciaQuery;
use App\DataAccess\Queries\Implement\PermisoQuery;
use App\DataAccess\Queries\Implement\RegistroChecadorQuery;
use App\DataAccess\Queries\Implement\RelojChecadorQuery;
use App\DataAccess\Queries\Implement\RolQuery;
use App\DataAccess\Queries\Implement\SucursalQuery;
use App\DataAccess\Queries\Implement\UsuarioQuery;
use App\DataAccess\Queries\Interfaces\ICEstadoCivilQuery;
use App\DataAccess\Queries\Interfaces\ICEstadoQuery;
use App\DataAccess\Queries\Interfaces\ICCiudadQuery;
use App\DataAccess\Queries\Interfaces\ICEstatusIncidenciaQuery;
use App\DataAccess\Queries\Interfaces\IClienteQuery;
use App\DataAccess\Queries\Interfaces\IConfiguracionQuery;
use App\DataAccess\Queries\Interfaces\ICTipoDocumentoQuery;
use App\DataAccess\Queries\Interfaces\ICTipoIncidenciaQuery;
use App\DataAccess\Queries\Interfaces\IDepartamentoQuery;
use App\DataAccess\Queries\Interfaces\IDetalleHorarioQuery;
use App\DataAccess\Queries\Interfaces\IDiaFestivoQuery;
use App\DataAccess\Queries\Interfaces\IDocumentoQuery;
use App\DataAccess\Queries\Interfaces\IEmpleadoQuery;
use App\DataAccess\Queries\Interfaces\IEmpresaQuery;
use App\DataAccess\Queries\Interfaces\IHistoricoEmpleadoQuery;
use App\DataAccess\Queries\Interfaces\IHorarioQuery;
use App\DataAccess\Queries\Interfaces\IIncidenciaQuery;
use App\DataAccess\Queries\Interfaces\IPermisoQuery;
use App\DataAccess\Queries\Interfaces\IRegistroChecadorQuery;
use App\DataAccess\Queries\Interfaces\IRelojChecadorQuery;
use App\DataAccess\Queries\Interfaces\IRolQuery;
use App\DataAccess\Queries\Interfaces\ISucursalQuery;
use App\DataAccess\Queries\Interfaces\IUsuarioQuery;
use App\DataAccess\Repositories\Implement\CEstadoCivilRepository;
use App\DataAccess\Repositories\Implement\CEstadoRepository;
use App\DataAccess\Repositories\Implement\CCiudadRepository;
use App\DataAccess\Repositories\Implement\CEstatusIncidenciaRepository;
use App\DataAccess\Repositories\Implement\ClienteRepository;
use App\DataAccess\Repositories\Implement\ConfiguracionRepository;
use App\DataAccess\Repositories\Implement\CTipoDocumentoRepository;
use App\DataAccess\Repositories\Implement\CTipoIncidenciaRepository;
use App\DataAccess\Repositories\Implement\DepartamentoRepository;
use App\DataAccess\Repositories\Implement\DetalleHorarioRepository;
use App\DataAccess\Repositories\Implement\DiaFestivoRepository;
use App\DataAccess\Repositories\Implement\DocumentoRepository;
use App\DataAccess\Repositories\Implement\EmpleadoRepository;
use App\DataAccess\Repositories\Implement\EmpresaRepository;
use App\DataAccess\Repositories\Implement\HistoricoEmpleadoRepository;
use App\DataAccess\Repositories\Implement\HorarioRepository;
use App\DataAccess\Repositories\Implement\IncidenciaRepository;
use App\DataAccess\Repositories\Implement\PermisoRepository;
use App\DataAccess\Repositories\Implement\RegistroChecadorRepository;
use App\DataAccess\Repositories\Implement\RelojChecadorRepository;
use App\DataAccess\Repositories\Implement\RolRepository;
use App\DataAccess\Repositories\Implement\SucursalRepository;
use App\DataAccess\Repositories\Implement\UsuarioRepository;
use App\DataAccess\Repositories\Interfaces\ICEstadoCivilRepository;
use App\DataAccess\Repositories\Interfaces\ICEstadoRepository;
use App\DataAccess\Repositories\Interfaces\ICCiudadRepository;
use App\DataAccess\Repositories\Interfaces\ICEstatusIncidenciaRepository;
use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\DataAccess\Repositories\Interfaces\IConfiguracionRepository;
use App\DataAccess\Repositories\Interfaces\ICTipoDocumentoRepository;
use App\DataAccess\Repositories\Interfaces\ICTipoIncidenciaRepository;
use App\DataAccess\Repositories\Interfaces\IDepartamentoRepository;
use App\DataAccess\Repositories\Interfaces\IDetalleHorarioRepository;
use App\DataAccess\Repositories\Interfaces\IDiaFestivoRepository;
use App\DataAccess\Repositories\Interfaces\IDocumentoRepository;
use App\DataAccess\Repositories\Interfaces\IEmpleadoRepository;
use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\DataAccess\Repositories\Interfaces\IHistoricoEmpleadoRepository;
use App\DataAccess\Repositories\Interfaces\IHorarioRepository;
use App\DataAccess\Repositories\Interfaces\IIncidenciaRepository;
use App\DataAccess\Repositories\Interfaces\IPermisoRepository;
use App\DataAccess\Repositories\Interfaces\IRegistroChecadorRepository;
use App\DataAccess\Repositories\Interfaces\IRelojChecadorRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\DataAccess\Repositories\Interfaces\ISucursalRepository;
use App\DataAccess\Repositories\Interfaces\IUsuarioRepository;
use App\Services\Implement\ArchivoService;
use App\Services\Implement\AuthService;
use App\Services\Implement\CEstadoCivilService;
use App\Services\Implement\CEstadoService;
use App\Services\Implement\CCiudadService;
use App\Services\Implement\CEstatusIncidenciaService;
use App\Services\Implement\ClienteService;
use App\Services\Implement\ConfiguracionService;
use App\Services\Implement\CTipoDocumentoService;
use App\Services\Implement\CTipoIncidenciaService;
use App\Services\Implement\DepartamentoService;
use App\Services\Implement\DetalleHorarioService;
use App\Services\Implement\DiaFestivoService;
use App\Services\Implement\DocumentoService;
use App\Services\Implement\EmpleadoService;
use App\Services\Implement\EmpresaService;
use App\Services\Implement\ExportarDatosService;
use App\Services\Implement\HistoricoEmpleadoService;
use App\Services\Implement\HorarioService;
use App\Services\Implement\ImportarDatosService;
use App\Services\Implement\IncidenciaService;
use App\Services\Implement\PermisoService;
use App\Services\Implement\RegistroChecadorService;
use App\Services\Implement\RelojChecadorService;
use App\Services\Implement\ReporteService;
use App\Services\Implement\RolService;
use App\Services\Implement\SucursalService;
use App\Services\Implement\UsuarioService;
use App\Services\Interfaces\IArchivoService;
use App\Services\Interfaces\IAuthService;
use App\Services\Interfaces\ICEstadoCivilService;
use App\Services\Interfaces\ICEstadoService;
use App\Services\Interfaces\ICCiudadService;
use App\Services\Interfaces\ICEstatusIncidenciaService;
use App\Services\Interfaces\IClienteService;
use App\Services\Interfaces\IConfiguracionService;
use App\Services\Interfaces\ICTipoDocumentoService;
use App\Services\Interfaces\ICTipoIncidenciaService;
use App\Services\Interfaces\IDepartamentoService;
use App\Services\Interfaces\IDetalleHorarioService;
use App\Services\Interfaces\IDiaFestivoService;
use App\Services\Interfaces\IDocumentoService;
use App\Services\Interfaces\IEmpleadoService;
use App\Services\Interfaces\IEmpresaService;
use App\Services\Interfaces\IExportarDatosService;
use App\Services\Interfaces\IHistoricoEmpleadoService;
use App\Services\Interfaces\IHorarioService;
use App\Services\Interfaces\IImportarDatosService;
use App\Services\Interfaces\IIncidenciaService;
use App\Services\Interfaces\IPermisoService;
use App\Services\Interfaces\IRegistroChecadorService;
use App\Services\Interfaces\IRelojChecadorService;
use App\Services\Interfaces\IReporteService;
use App\Services\Interfaces\IRolService;
use App\Services\Interfaces\ISucursalService;
use App\Services\Interfaces\IUsuarioService;
use App\Services\Mapper\Implement\CEstadoCivilMapper;
use App\Services\Mapper\Implement\CEstadoMapper;
use App\Services\Mapper\Implement\CCiudadMapper;
use App\Services\Mapper\Implement\CEstatusIncidenciaMapper;
use App\Services\Mapper\Implement\ClienteMapper;
use App\Services\Mapper\Implement\ConfiguracionMapper;
use App\Services\Mapper\Implement\CTipoDocumentoMapper;
use App\Services\Mapper\Implement\CTipoIncidenciaMapper;
use App\Services\Mapper\Implement\DepartamentoMapper;
use App\Services\Mapper\Implement\DetalleHorarioMapper;
use App\Services\Mapper\Implement\DiaFestivoMapper;
use App\Services\Mapper\Implement\DocumentoMapper;
use App\Services\Mapper\Implement\EmpleadoMapper;
use App\Services\Mapper\Implement\EmpresaMapper;
use App\Services\Mapper\Implement\HistoricoEmpleadoMapper;
use App\Services\Mapper\Implement\HorarioMapper;
use App\Services\Mapper\Implement\IncidenciaMapper;
use App\Services\Mapper\Implement\PermisoMapper;
use App\Services\Mapper\Implement\RegistroChecadorMapper;
use App\Services\Mapper\Implement\RelojChecadorMapper;
use App\Services\Mapper\Implement\RolMapper;
use App\Services\Mapper\Implement\SucursalMapper;
use App\Services\Mapper\Implement\UsuarioMapper;
use App\Services\Mapper\Interfaces\ICEstadoCivilMapper;
use App\Services\Mapper\Interfaces\ICEstadoMapper;
use App\Services\Mapper\Interfaces\ICCiudadMapper;
use App\Services\Mapper\Interfaces\ICEstatusIncidenciaMapper;
use App\Services\Mapper\Interfaces\IClienteMapper;
use App\Services\Mapper\Interfaces\IConfiguracionMapper;
use App\Services\Mapper\Interfaces\ICTipoDocumentoMapper;
use App\Services\Mapper\Interfaces\ICTipoIncidenciaMapper;
use App\Services\Mapper\Interfaces\IDepartamentoMapper;
use App\Services\Mapper\Interfaces\IDetalleHorarioMapper;
use App\Services\Mapper\Interfaces\IDiaFestivoMapper;
use App\Services\Mapper\Interfaces\IDocumentoMapper;
use App\Services\Mapper\Interfaces\IEmpleadoMapper;
use App\Services\Mapper\Interfaces\IEmpresaMapper;
use App\Services\Mapper\Interfaces\IHistoricoEmpleadoMapper;
use App\Services\Mapper\Interfaces\IHorarioMapper;
use App\Services\Mapper\Interfaces\IIncidenciaMapper;
use App\Services\Mapper\Interfaces\IPermisoMapper;
//use Illuminate\Support\Facades\Schema;
use App\Services\Mapper\Interfaces\IRegistroChecadorMapper;
use App\Services\Mapper\Interfaces\IRelojChecadorMapper;
use App\Services\Mapper\Interfaces\IRolMapper;
use App\Services\Mapper\Interfaces\ISucursalMapper;
use App\Services\Mapper\Interfaces\IUsuarioMapper;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IUnitOfWork::class, UnitOfWork::class);

        //Estados
        $this->app->bind(ICEstadoService::class, CEstadoService::class);
        $this->app->bind(ICEstadoQuery::class, CEstadoQuery::class);
        $this->app->bind(ICEstadoRepository::class, CEstadoRepository::class);
        $this->app->bind(ICEstadoMapper::class, CEstadoMapper::class);

        //Ciudades
        $this->app->bind(ICCiudadService::class, CCiudadService::class);
        $this->app->bind(ICCiudadQuery::class, CCiudadQuery::class);
        $this->app->bind(ICCiudadRepository::class, CCiudadRepository::class);
        $this->app->bind(ICCiudadMapper::class, CCiudadMapper::class);

        //Empresas
        $this->app->bind(IClienteService::class, ClienteService::class);
        $this->app->bind(IClienteQuery::class, ClienteQuery::class);
        $this->app->bind(IClienteRepository::class, ClienteRepository::class);
        $this->app->bind(IClienteMapper::class, ClienteMapper::class);

        //Sucursales
        $this->app->bind(ISucursalService::class, SucursalService::class);
        $this->app->bind(ISucursalQuery::class, SucursalQuery::class);
        $this->app->bind(ISucursalRepository::class, SucursalRepository::class);
        $this->app->bind(ISucursalMapper::class, SucursalMapper::class);

        //Departamentos
        $this->app->bind(IDepartamentoService::class, DepartamentoService::class);
        $this->app->bind(IDepartamentoQuery::class, DepartamentoQuery::class);
        $this->app->bind(IDepartamentoRepository::class, DepartamentoRepository::class);
        $this->app->bind(IDepartamentoMapper::class, DepartamentoMapper::class);

        //Dias Festivos
        $this->app->bind(IDiaFestivoService::class, DiaFestivoService::class);
        $this->app->bind(IDiaFestivoQuery::class, DiaFestivoQuery::class);
        $this->app->bind(IDiaFestivoRepository::class, DiaFestivoRepository::class);
        $this->app->bind(IDiaFestivoMapper::class, DiaFestivoMapper::class);

        //Reloj Checador
        $this->app->bind(IRelojChecadorService::class, RelojChecadorService::class);
        $this->app->bind(IRelojChecadorQuery::class, RelojChecadorQuery::class);
        $this->app->bind(IRelojChecadorRepository::class, RelojChecadorRepository::class);
        $this->app->bind(IRelojChecadorMapper::class, RelojChecadorMapper::class);

        //Permisos
        $this->app->bind(IPermisoService::class, PermisoService::class);
        $this->app->bind(IPermisoQuery::class, PermisoQuery::class);
        $this->app->bind(IPermisoRepository::class, PermisoRepository::class);
        $this->app->bind(IPermisoMapper::class, PermisoMapper::class);

        //Roles
        $this->app->bind(IRolService::class, RolService::class);
        $this->app->bind(IRolQuery::class, RolQuery::class);
        $this->app->bind(IRolRepository::class, RolRepository::class);
        $this->app->bind(IRolMapper::class, RolMapper::class);

        //Usuarios
        $this->app->bind(IUsuarioService::class, UsuarioService::class);
        $this->app->bind(IAuthService::class, AuthService::class);
        $this->app->bind(IUsuarioQuery::class, UsuarioQuery::class);
        $this->app->bind(IUsuarioRepository::class, UsuarioRepository::class);
        $this->app->bind(IUsuarioMapper::class, UsuarioMapper::class);

        //Tipos Incidencias
        $this->app->bind(ICTipoIncidenciaService::class, CTipoIncidenciaService::class);
        $this->app->bind(ICTipoIncidenciaQuery::class, CTipoIncidenciaQuery::class);
        $this->app->bind(ICTipoIncidenciaRepository::class, CTipoIncidenciaRepository::class);
        $this->app->bind(ICTipoIncidenciaMapper::class, CTipoIncidenciaMapper::class);

        //Empresas
        $this->app->bind(IEmpresaService::class, EmpresaService::class);
        $this->app->bind(IEmpresaQuery::class, EmpresaQuery::class);
        $this->app->bind(IEmpresaRepository::class, EmpresaRepository::class);
        $this->app->bind(IEmpresaMapper::class, EmpresaMapper::class);

        //Tipos Documentos
        $this->app->bind(ICTipoDocumentoService::class, CTipoDocumentoService::class);
        $this->app->bind(ICTipoDocumentoQuery::class, CTipoDocumentoQuery::class);
        $this->app->bind(ICTipoDocumentoRepository::class, CTipoDocumentoRepository::class);
        $this->app->bind(ICTipoDocumentoMapper::class, CTipoDocumentoMapper::class);

        //Detalles Horarios
        $this->app->bind(IDetalleHorarioService::class, DetalleHorarioService::class);
        $this->app->bind(IDetalleHorarioQuery::class, DetalleHorarioQuery::class);
        $this->app->bind(IDetalleHorarioRepository::class, DetalleHorarioRepository::class);
        $this->app->bind(IDetalleHorarioMapper::class, DetalleHorarioMapper::class);

        //Horarios
        $this->app->bind(IHorarioService::class, HorarioService::class);
        $this->app->bind(IHorarioQuery::class, HorarioQuery::class);
        $this->app->bind(IHorarioRepository::class, HorarioRepository::class);
        $this->app->bind(IHorarioMapper::class, HorarioMapper::class);

        //Incidencias
        $this->app->bind(IIncidenciaService::class, IncidenciaService::class);
        $this->app->bind(IIncidenciaQuery::class, IncidenciaQuery::class);
        $this->app->bind(IIncidenciaRepository::class, IncidenciaRepository::class);
        $this->app->bind(IIncidenciaMapper::class, IncidenciaMapper::class);

        //Documentos
        $this->app->bind(IDocumentoService::class, DocumentoService::class);
        $this->app->bind(IDocumentoQuery::class, DocumentoQuery::class);
        $this->app->bind(IDocumentoRepository::class, DocumentoRepository::class);
        $this->app->bind(IDocumentoMapper::class, DocumentoMapper::class);

        //Empleados
        $this->app->bind(IEmpleadoService::class, EmpleadoService::class);
        $this->app->bind(IEmpleadoQuery::class, EmpleadoQuery::class);
        $this->app->bind(IEmpleadoRepository::class, EmpleadoRepository::class);
        $this->app->bind(IEmpleadoMapper::class, EmpleadoMapper::class);

        //Archivos
        $this->app->bind(IArchivoService::class, ArchivoService::class);

        //Registros checador
        $this->app->bind(IRegistroChecadorService::class, RegistroChecadorService::class);
        $this->app->bind(IRegistroChecadorQuery::class, RegistroChecadorQuery::class);
        $this->app->bind(IRegistroChecadorRepository::class, RegistroChecadorRepository::class);
        $this->app->bind(IRegistroChecadorMapper::class, RegistroChecadorMapper::class);

        //Estado civil
        $this->app->bind(ICEstadoCivilService::class, CEstadoCivilService::class);
        $this->app->bind(ICEstadoCivilQuery::class, CEstadoCivilQuery::class);
        $this->app->bind(ICEstadoCivilRepository::class, CEstadoCivilRepository::class);
        $this->app->bind(ICEstadoCivilMapper::class, CEstadoCivilMapper::class);

        //Estatus incidencias
        $this->app->bind(ICEstatusIncidenciaService::class, CEstatusIncidenciaService::class);
        $this->app->bind(ICEstatusIncidenciaQuery::class, CEstatusIncidenciaQuery::class);
        $this->app->bind(ICEstatusIncidenciaRepository::class, CEstatusIncidenciaRepository::class);
        $this->app->bind(ICEstatusIncidenciaMapper::class, CEstatusIncidenciaMapper::class);

        //Reportes
        $this->app->bind(IExportarDatosService::class, ExportarDatosService::class);
        $this->app->bind(IReporteService::class, ReporteService::class);

        //Importaciones
        $this->app->bind(IImportarDatosService::class, ImportarDatosService::class);

        //Configuraciones
        $this->app->bind(IConfiguracionService::class, ConfiguracionService::class);
        $this->app->bind(IConfiguracionQuery::class, ConfiguracionQuery::class);
        $this->app->bind(IConfiguracionRepository::class, ConfiguracionRepository::class);
        $this->app->bind(IConfiguracionMapper::class, ConfiguracionMapper::class);

        //Historico Empleado
        $this->app->bind(IHistoricoEmpleadoService::class, HistoricoEmpleadoService::class);
        $this->app->bind(IHistoricoEmpleadoQuery::class, HistoricoEmpleadoQuery::class);
        $this->app->bind(IHistoricoEmpleadoRepository::class, HistoricoEmpleadoRepository::class);
        $this->app->bind(IHistoricoEmpleadoMapper::class, HistoricoEmpleadoMapper::class);
    }   
}

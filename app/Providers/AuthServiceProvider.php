<?php

namespace App\Providers;

use App\Domain\Candidato;
use App\Domain\Cliente;
use App\Domain\ClienteRh;
use App\Domain\Departamento;
use App\Domain\DiaFestivo;
use App\Domain\Empleado;
use App\Domain\Empresa;
use App\Domain\Factura;
use App\Domain\Horario;
use App\Domain\PuestoRh;
use App\Domain\RelojChecador;
use App\Domain\Servicio;
use App\Domain\Sucursal;
use App\Domain\Usuario;
use App\Policies\BuscadorPolicy;
use App\Policies\CandidatoPolicy;
use App\Policies\ClientePolicy;
use App\Policies\ClienteRhPolicy;
use App\Policies\DepartamentoPolicy;
use App\Policies\DiaFestivoPolicy;
use App\Policies\EmpleadoPolicy;
use App\Policies\EmpresaPolicy;
use App\Policies\FacturaPolicy;
use App\Policies\HorarioPolicy;
use App\Policies\ServicioPolicy;
use App\Policies\PuestoRhPolicy;
use App\Policies\SucursalPolicy;
use App\Policies\UsuarioPolicy;
use App\Services\DTO\Buscador\BuscadorRequest;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Usuario::class => UsuarioPolicy::class,
        Cliente::class => ClientePolicy::class,
        Empresa::class => EmpresaPolicy::class,
        Sucursal::class => SucursalPolicy::class,
        Departamento::class => DepartamentoPolicy::class,
        Empleado::class => EmpleadoPolicy::class,
        Horario::class => HorarioPolicy::class,
        DiaFestivo::class => DiaFestivoPolicy::class,
        RelojChecador::class => RelojChecador::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
    }
}

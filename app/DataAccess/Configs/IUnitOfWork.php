<?php
/**
 * Created by PhpStorm.
 * Usuario: jorgerodriguez
 * Date: 1/10/18
 * Time: 1:07 PM
 */

namespace App\DataAccess\Configs;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;

interface IUnitOfWork
{
    public function beginTransaction();
    public function commit();
    public function rollback();
    /**
     * @return EntityManager
     */
    public function getEntityManager();

    public function insert($entity);
    public function update($entity);
    public function remove($entity);

    /**
     * @return QueryBuilder
     */
    public function queryable();
}
<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IIncidenciaQuery;
use App\Domain\Incidencia;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class IncidenciaQuery implements IIncidenciaQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("incidencia")
            ->from(Incidencia::class, 'incidencia')
            ->where('incidencia.id != :id')
            ->setParameter('id', 0);
    }

    public function withTipoId($tipoId)
    {
        if(NumberExtensions::isPositiveInteger($tipoId)){
            $this->queryable->andWhere('incidencia.tipo = :tipoId')->setParameter('tipoId', $tipoId);
        }
    }

    public function includeTipo($includeTipo)
    {
        if($includeTipo){
            $this->queryable->leftJoin('incidencia.tipo','tipo')->addSelect('tipo');
        }
    }

    public function withFechaInicio($fechaInicio)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaInicio))
            $this->queryable->andWhere('incidencia.fecha_inicio = :fechaInicio')->setParameter('fechaInicio', $fechaInicio);
    }

    public function withMinFechaInicio($minFechaInicio)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaInicio))
            $this->queryable->andWhere('incidencia.fecha_inicio >= :minFechaInicio')->setParameter('minFechaInicio', $minFechaInicio);
    }

    public function withMaxFechaInicio($maxFechaInicio)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaInicio))
            $this->queryable->andWhere('incidencia.fecha_inicio <= :maxFechaInicio')->setParameter('maxFechaInicio', $maxFechaInicio);
    }

    public function withFechaFinal($fechaFinal)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaFinal))
            $this->queryable->andWhere('incidencia.fecha_final = :fechaFinal')->setParameter('fechaFinal', $fechaFinal);
    }

    public function withMinFechaFinal($minFechaFinal)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaFinal))
            $this->queryable->andWhere('incidencia.fecha_final >= :minFechaFinal')->setParameter('minFechaFinal', $minFechaFinal);
    }

    public function withMaxFechaFinal($maxFechaFinal)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaFinal))
            $this->queryable->andWhere('incidencia.fecha_final <= :maxFechaFinal')->setParameter('maxFechaFinal', $maxFechaFinal);
    }

    public function withEmpleadoId($empleadoId)
    {
        if(NumberExtensions::isPositiveInteger($empleadoId)){
            $this->queryable->andWhere('incidencia.empleado = :empleadoId')->setParameter('empleadoId', $empleadoId);
        }
    }

    public function includeEmpleado($includeEmpleado)
    {
        if($includeEmpleado){
            $this->queryable->leftJoin('incidencia.empleado','empleado')->addSelect('empleado');
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere('incidencia.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('incidencia.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('incidencia.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa){
            $this->queryable->leftJoin('incidencia.empresa', 'empresa')->addSelect('empresa');
        }
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('incidencia.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal){
            $this->queryable->leftJoin('incidencia.sucursal', 'sucursal')->addSelect('sucursal');
        }
    }

    public function withSucursalId($sucursalId)
    {
        if (NumberExtensions::isPositiveInteger($sucursalId)) {
            $this->queryable->andWhere('incidencia.sucursal = :sucursalId')->setParameter('sucursalId', $sucursalId);
        }
    }

    public function includeDepartamento($includeDepartamento)
    {
        if ($includeDepartamento){
            $this->queryable->leftJoin('incidencia.departamento', 'departamento')->addSelect('departamento');
        }
    }

    public function withDepartamentoId($departamentoId)
    {
        if (NumberExtensions::isPositiveInteger($departamentoId)) {
            $this->queryable->andWhere('incidencia.departamento = :departamentoId')->setParameter('departamentoId', $departamentoId);
        }
    }

    public function withEstatusId($estatusId)
    {
        if (NumberExtensions::isPositiveInteger($estatusId)) {
            $this->queryable->andWhere('incidencia.estatus = :estatusId')->setParameter('estatusId', $estatusId);
        }
    }

    public function includeEstatus($includeEstatus)
    {
        if ($includeEstatus){
            $this->queryable->leftJoin('incidencia.estatus', 'estatus')->addSelect('estatus');
        }
    }

    public function withEstatusValor($estatusValor)
    {
        if (StringExtensions::isNotNullOrEmpty($estatusValor)) {
            $this->includeEstatus(true);
            $this->queryable->andWhere('estatus.valor = :estatusValor')->setParameter('estatusValor', $estatusValor);
        }
    }

    public function withUrlArchivo($url_archivo){
        if (StringExtensions::isNotNullOrEmpty($url_archivo))
            $this->queryable->andWhere('incidencia.url_archivo = :url_archivo')->setParameter('url_archivo', $url_archivo);
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(incidencia.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('incidencia.'.$sortBy.'', $sort);
    }
}


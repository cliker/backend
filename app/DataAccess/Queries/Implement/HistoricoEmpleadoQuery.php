<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IHistoricoEmpleadoQuery;
use App\Domain\HistoricoEmpleado;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class HistoricoEmpleadoQuery implements IHistoricoEmpleadoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("historicoEmpleado")
            ->from(HistoricoEmpleado::class, 'historicoEmpleado')
            ->where('historicoEmpleado.id != :id')
            ->setParameter('id', 0);
    }

    public function withAccion($accion)
    {
        if(StringExtensions::isNotNullOrEmpty($accion)){
            $this->queryable->andWhere('LOWER(historicoEmpleado.accion) = LOWER(:accion)')
                ->setParameter('accion', $accion);
        }
    }

    public function withFecha($fecha)
    {
        if(StringExtensions::isNotNullOrEmpty($fecha)){
            $this->queryable->andWhere('historicoEmpleado.fecha = :fecha')
                ->setParameter('fecha', $fecha);
        }
    }

    public function withMaxFecha($maxFecha)
    {
        if(StringExtensions::isNotNullOrEmpty($maxFecha)){
            $this->queryable->andWhere('historicoEmpleado.fecha <= :maxFecha')
                ->setParameter('maxFecha', $maxFecha);
        }
    }

    public function withMinFecha($minFecha)
    {
        if(StringExtensions::isNotNullOrEmpty($minFecha)){
            $this->queryable->andWhere('historicoEmpleado.fecha >= :minFecha')
                ->setParameter('minFecha', $minFecha);
        }
    }

    public function withActivo($includeEmpleado)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('diaFestivo.activo = :activo')
                ->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }
    
    public function withEmpleadoId($empleadoId)
    {
        if(StringExtensions::isNotNullOrEmpty($empleadoId)){
            if(NumberExtensions::isPositiveInteger($cliente_id)){
                $this->queryable->andWhere("historicoEmpleado.empleado = :empleado_id")
                ->setParameter("empleado_id",$empleadoId);
            }
        }
    }

    public function includeEmpleado($includeEmpleado)
    {
        if($includeEmpleado){
            $this->queryable->leftJoin('historicoEmpleado.empleado','empleado')->addSelect('empleado');
        }
    }

    public function withResponsableId($responsableId)
    {
        if(StringExtensions::isNotNullOrEmpty($responsableId)){
            if(NumberExtensions::isPositiveInteger($cliente_id)){
                $this->queryable->andWhere("historicoEmpleado.responsable = :responsable_id")
                ->setParameter("responsable_id",$responsableId);
            }
        }
    }

    public function includeResponsable($includeResponsable)
    {
        if($includeResponsable){
            $this->queryable->leftJoin('historicoEmpleado.responsable','responsable')->addSelect('responsable');
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(historicoEmpleado.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('historicoEmpleado.'.$sortBy.'', $sort);
    }
}


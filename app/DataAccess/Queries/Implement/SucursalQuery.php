<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ISucursalQuery;
use App\Domain\Sucursal;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class SucursalQuery implements ISucursalQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("sucursal")
            ->from(Sucursal::class, "sucursal")
            ->where("sucursal.id != :id")
            ->setParameter("id", 0);
    }

    public function withNombre($nombre)
    {
        if(StringExtensions::isNotNullOrEmpty($nombre)){
            $this->queryable->andWhere("LOWER(sucursal.nombre) LIKE LOWER(:nombre)")
                ->setParameter("nombre", "%".$nombre."%");
        }
    }

    public function withDireccion($direccion)
    {
        if(StringExtensions::isNotNullOrEmpty($direccion)){
            $this->queryable->andWhere("LOWER(sucursal.direccion) LIKE LOWER(:direccion)")
                ->setParameter("direccion", "%".$direccion."%");
        }
    }

    public function withCodigoPostal($codigo_postal)
    {
        if(StringExtensions::isNotNullOrEmpty($codigo_postal)){
            $this->queryable->andWhere("LOWER(sucursal.codigo_postal) = LOWER(:codigo_postal)")
                ->setParameter("codigo_postal", $codigo_postal);
        }
    }

    public function withDescripcion($descripcion)
    {
        if(StringExtensions::isNotNullOrEmpty($descripcion)){
            $this->queryable->andWhere("LOWER(sucursal.descripcion) LIKE LOWER(:descripcion)")
                ->setParameter("descripcion", "%".$descripcion."%");
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere("sucursal.activo = :activo")
                ->setParameter("activo", StringExtensions::toBoolean($activo));
        }
    }

    public function withEstadoId($estado_id)
    {
        if(NumberExtensions::isPositiveInteger($estado_id)){
            $this->queryable->andWhere("sucursal.estado = :estado_id")
                ->setParameter("estado_id", $estado_id);
        }
    }

    public function includeEstado($includeEstado)
    {
        if($includeEstado){
            $this->queryable->leftJoin("sucursal.estado","estado")->addSelect("estado");
        }
    }

    public function withCiudadId($ciudad_id)
    {
        if(NumberExtensions::isPositiveInteger($ciudad_id)){
            $this->queryable->andWhere("sucursal.ciudad = :ciudad_id")
                ->setParameter("ciudad_id", $ciudad_id);
        }
    }

    public function includeCiudad($includeCiudad)
    {
        if($includeCiudad){
            $this->queryable->leftJoin("sucursal.ciudad","ciudad")->addSelect("ciudad");
        }
    }

    public function withEmpresaId($empresa_id)
    {
        if(NumberExtensions::isPositiveInteger($empresa_id)){
            $this->queryable->andWhere("sucursal.empresa = :empresa_id")
                ->setParameter("empresa_id", $empresa_id);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if($includeEmpresa){
            $this->queryable->leftJoin("sucursal.empresa","empresa")->addSelect("empresa");
        }
    }

    public function withDiaFestivoId($diaFestivo_id)
    {
        if(NumberExtensions::isPositiveInteger($diaFestivo_id)){
            $this->queryable->andWhere("sucursal.dias_festivos = :dias_festivos")
                ->setParameter("dias_festivos", $diaFestivo_id);
        }
    }

    public function includeDiaFestivo($includeDiaFestivo)
    {
        if($includeDiaFestivo){
            $this->queryable->leftJoin("sucursal.dias_festivos","dias_festivos")->addSelect("dias_festivos");
        }
    }


    function withClienteId($cliente_id)
    {
        if(NumberExtensions::isPositiveInteger($cliente_id)){
            $this->queryable->andWhere("sucursal.cliente = :cliente_id")
                ->setParameter("cliente_id",$cliente_id);
        }
    }

    function includeCliente($includeCliente)
    {
        if($includeCliente){
            $this->queryable->leftJoin("sucursal.cliente","cliente")->addSelect("cliente");
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(sucursal.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy("sucursal.".$sortBy."", $sort);
    }
}


<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IConfiguracionQuery;
use App\Domain\Configuracion;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class ConfiguracionQuery implements IConfiguracionQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("configuracion")
            ->from(Configuracion::class, 'configuracion')
            ->where('configuracion.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(configuracion.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withClave($clave)
    {
        if (StringExtensions::isNotNullOrEmpty($clave))
            $this->queryable->andWhere('configuracion.clave = :clave')->setParameter('clave', '%'.$clave.'%');
    }

    public function withValor($valor)
    {
        if (StringExtensions::isNotNullOrEmpty($valor))
            $this->queryable->andWhere('LOWER(configuracion.valor) LIKE LOWER(:valor)')->setParameter('valor', '%'.$valor.'%');
    }

    public function withDescripcion($descripcion)
    {
        if (StringExtensions::isNotNullOrEmpty($descripcion))
            $this->queryable->andWhere('LOWER(configuracion.descripcion) LIKE LOWER(:descripcion)')->setParameter('descripcion', '%'.$descripcion.'%');
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('configuracion.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withSucursalId($sucursal_id)
    {
        if(StringExtensions::isNotNullOrEmpty($sucursal_id)){

            $this->queryable->andWhere('configuracion.sucursal = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id);
        }
    }

    public function includeSucursal($includeSucursal)
    {
        if($includeSucursal){
            $this->queryable->leftJoin('configuracion.sucursal','sucursal')->addSelect('sucursal');
        }
    }

    public function withEmpresaId($empresa_id)
    {
        if(StringExtensions::isNotNullOrEmpty($empresa_id)){
            $this->queryable->andWhere('configuracion.empresa = :empresa_id')
                ->setParameter('empresa_id', $empresa_id);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if($includeEmpresa){
            $this->queryable->leftJoin('configuracion.empresa','empresa')->addSelect('empresa');
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(configuracion.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('configuracion.'.$sortBy.'', $sort);
    }
}


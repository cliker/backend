<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICTipoDocumentoQuery;
use App\Domain\CTipoDocumento;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CTipoDocumentoQuery extends CatalogoBaseQuery implements ICTipoDocumentoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CTipoDocumento::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId))
            $this->queryable->andWhere('entity.empresa = :empresaId')->setParameter('empresaId', $empresaId);
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('entity.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('entity.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa)
            $this->queryable->leftJoin('entity.empresa', 'empresa')->addSelect('empresa');
    }
}


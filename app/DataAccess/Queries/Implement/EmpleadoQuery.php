<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IEmpleadoQuery;
use App\Domain\Empleado;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class EmpleadoQuery implements IEmpleadoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("empleado")
            ->from(Empleado::class, 'empleado')
            ->where('empleado.id != :id')
            ->setParameter('id', 0);
    }


    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(empleado.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withApellidoPaterno($apellidoPaterno)
    {
        if (StringExtensions::isNotNullOrEmpty($apellidoPaterno))
            $this->queryable->andWhere('LOWER(empleado.apellido_paterno) LIKE LOWER(:apellidoPaterno)')->setParameter('apellidoPaterno', '%'.$apellidoPaterno.'%');
    }

    public function withApellidoMaterno($apellidoMaterno)
    {
        if (StringExtensions::isNotNullOrEmpty($apellidoMaterno))
            $this->queryable->andWhere('LOWER(empleado.apellido_materno) LIKE LOWER(:apellidoMaterno)')->setParameter('apellidoMaterno', '%'.$apellidoMaterno.'%');
    }

    public function withFechaNacimiento($fechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaNacimiento))
            $this->queryable->andWhere('empleado.fecha_nacimiento = :fechaNacimiento')->setParameter('fechaNacimiento', $fechaNacimiento);
    }

    public function withMinFechaNacimiento($minFechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaNacimiento))
            $this->queryable->andWhere('empleado.fecha_nacimiento >= :minFechaNacimiento')->setParameter('minFechaNacimiento', $minFechaNacimiento);
    }

    public function withMaxFechaNacimiento($maxFechaNacimiento)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaNacimiento))
            $this->queryable->andWhere('empleado.fecha_nacimiento <= :maxFechaNacimiento')->setParameter('maxFechaNacimiento', $maxFechaNacimiento);
    }

    public function withClaveInterna($clave_interna)
    {
        if (StringExtensions::isNotNullOrEmpty($clave_interna))
            $this->queryable->andWhere('empleado.clave_interna = :clave_interna')->setParameter('clave_interna', $clave_interna);
    }

    public function withTieneHijos($tiene_hijos)
    {
        if (StringExtensions::isNotNullOrEmpty($tiene_hijos))
            $this->queryable->andWhere('empleado.tiene_hijos = :tiene_hijos')->setParameter('tiene_hijos', $tiene_hijos);
    }

    public function withPuesto($puesto)
    {
        if (StringExtensions::isNotNullOrEmpty($puesto))
            $this->queryable->andWhere('empleado.puesto = :puesto')->setParameter('puesto', '%'.$puesto.'%');
    }
    
    public function withSexo($sexo)
    {
        if (StringExtensions::isNotNullOrEmpty($sexo))
            $this->queryable->andWhere('empleado.sexo = :sexo')->setParameter('sexo', $sexo);
    }

    public function withEstadoId($estadoId)
    {
        if (NumberExtensions::isPositiveInteger($estadoId))
            $this->queryable->andWhere('empleado.estado = :estadoId')->setParameter('estadoId', $estadoId);
    }

    public function includeEstado($includeEstado)
    {
        if ($includeEstado)
            $this->queryable->leftJoin('empleado.estado', 'estado')->addSelect('estado');
    }

    public function withCiudadId($ciudadId)
    {
        if (NumberExtensions::isPositiveInteger($ciudadId))
            $this->queryable->andWhere('empleado.ciudad = :ciudadId')->setParameter('ciudadId', $ciudadId);
    }

    public function includeCiudad($includeCiudad)
    {
        if ($includeCiudad)
            $this->queryable->leftJoin('empleado.ciudad', 'ciudad')->addSelect('ciudad');
    }

    public function withCodigoPostal($codigoPostal)
    {
        if (StringExtensions::isNotNullOrEmpty($codigoPostal))
            $this->queryable->andWhere('LOWER(empleado.codigo_postal) LIKE LOWER(:codigoPostal)')->setParameter('codigoPostal', '%'.$codigoPostal.'%');
    }

    public function withTelefono($telefono)
    {
        if (StringExtensions::isNotNullOrEmpty($telefono))
            $this->queryable->andWhere('LOWER(empleado.telefono) LIKE LOWER(:telefono)')->setParameter('telefono', '%'.$telefono.'%');
    }

    public function withCorreo($correo)
    {
        if (StringExtensions::isNotNullOrEmpty($correo))
            $this->queryable->andWhere('LOWER(empleado.correo) LIKE LOWER(:correo)')->setParameter('correo', '%'.$correo.'%');
    }

    public function withEstadoCivilId($estadoCivilId)
    {
        if (StringExtensions::isNotNullOrEmpty($estadoCivilId))
            $this->queryable->andWhere('empleado.estado_civil = :estadoCivilId')->setParameter('estadoCivilId', $estadoCivilId);
    }

    public function includeEstadoCivil($includeEstadoCivil)
    {
        $this->queryable->leftJoin('empleado.estado_civil', 'estado_civil')->addSelect('estado_civil');
    }

    public function withFechaIngreso($fechaIngreso)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaIngreso))
            $this->queryable->andWhere('empleado.fecha_ingreso = :fechaIngreso')->setParameter('fechaIngreso', $fechaIngreso);
    }

    public function withMinFechaIngreso($minFechaIngreso)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaIngreso))
            $this->queryable->andWhere('empleado.fecha_ingreso >= :minFechaIngreso')->setParameter('minFechaIngreso', $minFechaIngreso);
    }

    public function withMaxFechaIngreso($maxFechaIngreso)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaIngreso))
            $this->queryable->andWhere('empleado.fecha_ingreso <= :maxFechaIngreso')->setParameter('maxFechaIngreso', $maxFechaIngreso);
    }

    public function withFechaBaja($fechaBaja)
    {
        if (StringExtensions::isNotNullOrEmpty($fechaBaja))
            $this->queryable->andWhere('empleado.fecha_baja = :fechaBaja')->setParameter('fechaBaja', $fechaBaja);
    }

    public function withMinFechaBaja($minFechaBaja)
    {
        if (StringExtensions::isNotNullOrEmpty($minFechaBaja))
            $this->queryable->andWhere('empleado.fecha_baja >= :minFechaBaja')->setParameter('minFechaBaja', $minFechaBaja);
    }

    public function withMaxFechaBaja($maxFechaBaja)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFechaBaja))
            $this->queryable->andWhere('empleado.fecha_baja <= :maxFechaBaja')->setParameter('maxFechaBaja', $maxFechaBaja);
    }

    public function withDiaDescanso($diaDescanso)
    {
        if (StringExtensions::isNotNullOrEmpty($diaDescanso))
            $this->queryable->andWhere('LOWER(empleado.dia_descanso) LIKE LOWER(:diaDescanso)')->setParameter('diaDescanso', '%'.$diaDescanso.'%');
    }

    public function withHorarioId($horarioId)
    {
        if (NumberExtensions::isPositiveInteger($horarioId))
            $this->queryable->andWhere('empleado.horario = :horarioId')->setParameter('horarioId', $horarioId);
    }

    public function includeHorario($includeHorario)
    {
        if ($includeHorario)
            $this->queryable->leftJoin('empleado.horario', 'horario')->addSelect('horario');
    }

    public function withSucursalId($sucursalId)
    {
        if (NumberExtensions::isPositiveInteger($sucursalId))
            $this->queryable->andWhere('empleado.sucursal = :sucursalId')->setParameter('sucursalId', $sucursalId);
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal)
            $this->queryable->leftJoin('empleado.sucursal', 'sucursal')->addSelect('sucursal');
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('empleado.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withDepartamentoId($departamentoId)
    {
        if (NumberExtensions::isPositiveInteger($departamentoId))
            $this->queryable->andWhere('empleado.departamento = :departamentoId')->setParameter('departamentoId', $departamentoId);
    }

    public function includeDepartamento($includeDepartamento)
    {
        if($includeDepartamento)
            $this->queryable->leftJoin('empleado.departamento', 'departamento')->addSelect('departamento');
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('empleado.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('empleado.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa){
            $this->queryable->leftJoin('empleado.empresa', 'empresa')->addSelect('empresa');
        }
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('empleado.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function includeRelojesChecador($includeRelojesChecador)
    {
        if ($includeRelojesChecador){
            $this->queryable->leftJoin('empleado.relojes_checador', 'relojes_checador')->addSelect('relojes_checador');
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(empleado.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('empleado.'.$sortBy.'', $sort);
    }
}


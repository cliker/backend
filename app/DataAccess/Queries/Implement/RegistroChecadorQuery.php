<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IRegistroChecadorQuery;
use App\Domain\RegistroChecador;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class RegistroChecadorQuery implements IRegistroChecadorQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("registroChecador")
            ->from(RegistroChecador::class, 'registroChecador')
            ->where('registroChecador.id != :id')
            ->setParameter('id', 0);
    }

    public function withFecha($fecha)
    {
        if (StringExtensions::isNotNullOrEmpty($fecha))
            $this->queryable->andWhere('registroChecador.fecha = :fecha')->setParameter('fecha', $fecha);
    }

    public function withMinFecha($minFecha)
    {
        if (StringExtensions::isNotNullOrEmpty($minFecha))
            $this->queryable->andWhere('registroChecador.fecha >= :minFecha')->setParameter('minFecha', $minFecha);
    }

    public function withMaxFecha($maxFecha)
    {
        if (StringExtensions::isNotNullOrEmpty($maxFecha))
            $this->queryable->andWhere('registroChecador.fecha <= :maxFecha')->setParameter('maxFecha', $maxFecha);
    }

    public function withHora($hora)
    {
        if (StringExtensions::isNotNullOrEmpty($hora))
            $this->queryable->andWhere('registroChecador.hora = :hora')->setParameter('hora', $hora);
    }

    public function withMinHora($minHora)
    {
        if (StringExtensions::isNotNullOrEmpty($minHora))
            $this->queryable->andWhere('registroChecador.hora >= :minHora')->setParameter('minHora', $minHora);
    }

    public function withMaxHora($maxHora)
    {
        if (StringExtensions::isNotNullOrEmpty($maxHora))
            $this->queryable->andWhere('registroChecador.hora <= :maxHora')->setParameter('maxHora', $maxHora);
    }

    public function withEmpleadoId($empleadoId)
    {
        if (NumberExtensions::isPositiveInteger($empleadoId))
            $this->queryable->andWhere('registroChecador.empleado = :empleadoId')->setParameter('empleadoId', $empleadoId);
    }

    public function includeEmpleado($includeEmpleado)
    {
        if ($includeEmpleado)
            $this->queryable->leftJoin('registroChecador.empleado', 'empleado')->addSelect('empleado');
    }

    public function withRelojChecadorId($relojChecadorId)
    {
        if (NumberExtensions::isPositiveInteger($relojChecadorId))
            $this->queryable->andWhere('registroChecador.reloj_checador = :relojChecadorId')->setParameter('relojChecadorId', $relojChecadorId);
    }

    public function includeRelojChecador($includeRelojChecador)
    {
        if ($includeRelojChecador)
            $this->queryable->leftJoin('registroChecador.reloj_checador', 'reloj_checador')->addSelect('reloj_checador');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('registroChecador.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente){
            $this->queryable->leftJoin('registroChecador.cliente', 'cliente')->addSelect('cliente');
        }
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('registroChecador.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa){
            $this->queryable->leftJoin('registroChecador.empresa', 'empresa')->addSelect('empresa');
        }
    }

    public function withSucursalId($sucursalId)
    {
        if (NumberExtensions::isPositiveInteger($sucursalId)) {
            $this->queryable->andWhere('registroChecador.sucursal = :sucursalId')->setParameter('sucursalId', $sucursalId);
        }
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal){
            $this->queryable->leftJoin('registroChecador.sucursal', 'sucursal')->addSelect('sucursal');
        }
    }

    public function withDepartamentoId($departamentoId)
    {
        if (NumberExtensions::isPositiveInteger($departamentoId)) {
            $this->queryable->andWhere('registroChecador.departamento = :departamentoId')->setParameter('departamentoId', $departamentoId);
        }
    }

    public function includeDepartamento($includeDepartamento)
    {
        if ($includeDepartamento){
            $this->queryable->leftJoin('registroChecador.departamento', 'departamento')->addSelect('departamento');
        }
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('registroChecador.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(registroChecador.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('registroChecador.'.$sortBy.'', $sort);
    }
}


<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IDocumentoQuery;
use App\Domain\Documento;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class DocumentoQuery implements IDocumentoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("documento")
            ->from(Documento::class, 'documento')
            ->where('documento.id != :id')
            ->setParameter('id', 0);
    }

    public function withTipoId($tipoId)
    {
        if(NumberExtensions::isPositiveInteger($tipoId)){
            $this->queryable->andWhere('documento.tipo = :tipoId')->setParameter('tipoId', $tipoId);
        }
    }

    public function includeTipo($includeTipo)
    {
        if($includeTipo){
            $this->queryable->leftJoin('documento.tipo','tipo')->addSelect('tipo');
        }
    }

    public function withEmpledoId($empleadoId)
    {
        if(NumberExtensions::isPositiveInteger($empleadoId)){
            $this->queryable->andWhere('documento.empleado = :empleadoId')->setParameter('empleadoId', $empleadoId);
        }
    }

    public function includeEmpleado($includeEmpleado)
    {
        if($includeEmpleado){
            $this->queryable->leftJoin('documento.empleado','empleado')->addSelect('empleado');
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere('documento.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(documento.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('documento.'.$sortBy.'', $sort);
    }
}


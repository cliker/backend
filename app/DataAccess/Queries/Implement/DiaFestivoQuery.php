<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IDiaFestivoQuery;
use App\Domain\DiaFestivo;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class DiaFestivoQuery implements IDiaFestivoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("diaFestivo")
            ->from(DiaFestivo::class, 'diaFestivo')
            ->where('diaFestivo.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if(StringExtensions::isNotNullOrEmpty($nombre)){
            $this->queryable->andWhere('LOWER(diaFestivo.nombre) LIKE LOWER(:nombre)')
                ->setParameter('nombre', '%'.$nombre.'%');
        }
    }

    public function withFecha($fecha)
    {
        if(StringExtensions::isNotNullOrEmpty($fecha)){
            $this->queryable->andWhere('diaFestivo.fecha = :fecha')
                ->setParameter('fecha', $fecha);
        }
    }

    public function withMaxFecha($maxFecha)
    {
        if(StringExtensions::isNotNullOrEmpty($maxFecha)){
            $this->queryable->andWhere('diaFestivo.fecha <= :maxFecha')
                ->setParameter('maxFecha', $maxFecha);
        }
    }

    public function withMinFecha($minFecha)
    {
        if(StringExtensions::isNotNullOrEmpty($minFecha)){
            $this->queryable->andWhere('diaFestivo.fecha >= :minFecha')
                ->setParameter('minFecha', $minFecha);
        }
    }

    public function withDescripcion($descripcion)
    {
        if(StringExtensions::isNotNullOrEmpty($descripcion)){
            $this->queryable->andWhere('LOWER(diaFestivo.descripcion) LIKE LOWEr(:descripcion)')
                ->setParameter('descripcion', '%'.$descripcion.'%');
        }
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('diaFestivo.activo = :activo')
                ->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withDepartamentoId($departamento_id)
    {
        if(StringExtensions::isNotNullOrEmpty($departamento_id)){
            $this->includeDepartamentos(true);
            $this->queryable->andWhere('departamentos.id = :departamento')
                ->setParameter('departamento', $departamento_id );
        }
    }

    public function includeDepartamentos($includeDepartamentos)
    {
        if($includeDepartamentos){
            $this->queryable->leftJoin('diaFestivo.departamentos','departamentos')->addSelect('departamentos');
        }
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('diaFestivo.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('diaFestivo.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa){
            $this->queryable->leftJoin('diaFestivo.empresa', 'empresa')->addSelect('empresa');
        }
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('diaFestivo.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function withSucursalId($sucursalId)
    {
        if (NumberExtensions::isPositiveInteger($sucursalId))
            $this->queryable->andWhere('diaFestivo.sucursal = :sucursalId')->setParameter('sucursalId', $sucursalId);
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal)
            $this->queryable->leftJoin('diaFestivo.sucursal', 'sucursal')->addSelect('sucursal');
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(diaFestivo.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('diaFestivo.'.$sortBy.'', $sort);
    }
}


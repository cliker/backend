<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IDepartamentoQuery;
use App\Domain\Departamento;
use App\Domain\Empresa;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class DepartamentoQuery implements IDepartamentoQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("departamento")
            ->from(Departamento::class, 'departamento')
            ->where('departamento.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if(StringExtensions::isNotNullOrEmpty($nombre)){
            $this->queryable->andWhere('LOWER(departamento.nombre) LIKE LOWER(:nombre)')
                ->setParameter('nombre', '%'.$nombre.'%');
        }
    }

    public function withEncargado($encargado)
    {
        if(StringExtensions::isNotNullOrEmpty($encargado)){
            $this->queryable->andWhere('LOWER(departamento.encargado) LIKE LOWER(:encargado)')
                ->setParameter('encargado', '%'.$encargado.'%');
        }
    }

    public function withCorreo($correo)
    {
        if(StringExtensions::isNotNullOrEmpty($correo)){
            $this->queryable->andWhere('LOWER(departamento.correo) LIKE LOWER(:correo)')
                ->setParameter('correo', '%'.$correo.'%');
        }
    }

    public function withDescripcion($descripcion)
    {
        if(StringExtensions::isNotNullOrEmpty($descripcion)){
            $this->queryable->andWhere('LOWER(departamento.descripcion) LIKE LOWER(:descripcion)')
                ->setParameter('descripcion', '%'.$descripcion.'%');
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere('departamento.activo = :activo')
                ->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withSucursalId($sucursal_id)
    {
        if(StringExtensions::isNotNullOrEmpty($sucursal_id)){

            $this->queryable->andWhere('departamento.sucursal = :sucursal_id')
                ->setParameter('sucursal_id', $sucursal_id);
        }
    }

    public function includeSucursal($includeSucursal)
    {
        if($includeSucursal){
            $this->queryable->leftJoin('departamento.sucursal','sucursal')->addSelect('sucursal');
        }
    }

    public function withEmpresaId($empresa_id)
    {
        if(StringExtensions::isNotNullOrEmpty($empresa_id)){
            $this->queryable->andWhere('departamento.empresa = :empresa_id')
                ->setParameter('empresa_id', $empresa_id);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if($includeEmpresa){
            $this->queryable->leftJoin('departamento.empresa','empresa')->addSelect('empresa');
        }
    }

    function withClienteId($clienteId)
    {
        if(NumberExtensions::isPositiveInteger($clienteId)){
            $this->queryable->andWhere("departamento.cliente = :clienteId")->setParameter("clienteId",$clienteId);
        }
    }

    function includeCliente($includeCliente)
    {
        if($includeCliente){
            $this->queryable->leftJoin("departamento.cliente","cliente")->addSelect("cliente");
        }
    }

    public function includeDiasFestivos($includeDiasFestivos)
    {
        if($includeDiasFestivos){
            $this->queryable->leftJoin('departamento.dias_festivos','dias_festivos')->addSelect('dias_festivos');
        }
    }


    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(departamento.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('departamento.'.$sortBy.'', $sort);
    }
}


<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICEstadoCivilQuery;
use App\Domain\CEstadoCivil;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CEstadoCivilQuery extends CatalogoBaseQuery implements ICEstadoCivilQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CEstadoCivil::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }
}


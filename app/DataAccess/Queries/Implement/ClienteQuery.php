<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IClienteQuery;
use App\Domain\Cliente;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class ClienteQuery implements IClienteQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("cliente")
            ->from(Cliente::class, 'cliente')
            ->where('cliente.id != :id')
            ->setParameter('id', 0);
    }
    public function withNombre($nombre)
    {
        if (StringExtensions::isNotNullOrEmpty($nombre))
            $this->queryable->andWhere('LOWER(cliente.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
    }

    public function withCorreo($correo)
    {
        if (StringExtensions::isNotNullOrEmpty($correo))
            $this->queryable->andWhere('LOWER(cliente.correo) LIKE LOWER(:correo)')->setParameter('correo', '%'.$correo.'%');
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('cliente.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withEstadoId($estadoId)
    {
        if (NumberExtensions::isPositiveInteger($estadoId))
            $this->queryable->andWhere('cliente.estado = :estadoId')->setParameter('estadoId', $estadoId);
    }

    public function includeEstado($includeEstado)
    {
        if ($includeEstado)
            $this->queryable->leftJoin('cliente.estado', 'estado')->addSelect('estado');
    }

    public function withCiudadId($ciudadId)
    {
        if (NumberExtensions::isPositiveInteger($ciudadId))
            $this->queryable->andWhere('cliente.ciudad = :ciudadId')->setParameter('ciudadId', $ciudadId);
    }

    public function includeCiudad($includeCiudad)
    {
        if ($includeCiudad)
            $this->queryable->leftJoin('cliente.ciudad', 'ciudad')->addSelect('ciudad');
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(cliente.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('cliente.'.$sortBy.'', $sort);
    }
}


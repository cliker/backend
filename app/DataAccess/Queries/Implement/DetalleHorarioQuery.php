<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IDetalleHorarioQuery;
use App\Domain\DetalleHorario;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class DetalleHorarioQuery implements IDetalleHorarioQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("detalleHorario")
            ->from(DetalleHorario::class, 'detalleHorario')
            ->where('detalleHorario.id != :id')
            ->setParameter('id', 0);
    }

    public function withDia($dia)
    {
        if (StringExtensions::isNotNullOrEmpty($dia))
            $this->queryable->andWhere('LOWER(detalleHorario.dia) LIKE LOWER(:dia)')->setParameter('dia', '%'.$dia.'%');
    }

    public function withHoraEntrada($horaEntrada)
    {
        if (StringExtensions::isNotNullOrEmpty($horaEntrada))
            $this->queryable->andWhere('detalleHorario.hora_entrada = :horaEntrada')->setParameter('horaEntrada', $horaEntrada);
    }

    public function withMinHoraEntrada($minHoraEntrada)
    {
        if (StringExtensions::isNotNullOrEmpty($minHoraEntrada))
            $this->queryable->andWhere('detalleHorario.hora_entrada >= :minHoraEntrada')->setParameter('minHoraEntrada', $minHoraEntrada);
    }

    public function withMaxHoraEntrada($maxHoraEntrada)
    {
        if (StringExtensions::isNotNullOrEmpty($maxHoraEntrada))
            $this->queryable->andWhere('detalleHorario.hora_entrada <= :maxHoraEntrada')->setParameter('maxHoraEntrada', $maxHoraEntrada);
    }

    public function withHoraSalida($horaSalida)
    {
        if (StringExtensions::isNotNullOrEmpty($horaSalida))
            $this->queryable->andWhere('detalleHorario.hora_salida = :horaSalida')->setParameter('horaSalida', $horaSalida);
    }

    public function withMinHoraSalida($minHoraSalida)
    {
        if (StringExtensions::isNotNullOrEmpty($minHoraSalida))
            $this->queryable->andWhere('detalleHorario.hora_salida >= :minHoraSalida')->setParameter('minHoraSalida', $minHoraSalida);
    }

    public function withMaxHoraSalida($maxHoraSalida)
    {
        if (StringExtensions::isNotNullOrEmpty($maxHoraSalida))
            $this->queryable->andWhere('detalleHorario.hora_salida <= :maxHoraSalida')->setParameter('maxHoraSalida', $maxHoraSalida);
    }

    public function withHorarioId($horarioId)
    {
        if (NumberExtensions::isPositiveInteger($horarioId))
            $this->queryable->andWhere('detalleHorario.horario = :horarioId')->setParameter('horarioId', $horarioId);
    }

    public function includeHorario($includeHorario)
    {
        if ($includeHorario)
            $this->queryable->leftJoin('detalleHorario.horario', 'horario')->addSelect('horario');
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('detalleHorario.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(detalleHorario.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('detalleHorario.'.$sortBy.'', $sort);
    }
}


<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IEmpresaQuery;
use App\Domain\Empresa;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class EmpresaQuery implements IEmpresaQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("empresa")
            ->from(Empresa::class, 'empresa')
            ->where('empresa.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if(StringExtensions::isNotNullOrEmpty($nombre)){
            $this->queryable->andWhere('LOWER(empresa.nombre) LIKE LOWER(:nombre)')
                ->setParameter('nombre', '%'.$nombre.'%');
        }
    }

    public function withRazonSocial($razon_social)
    {
        if(StringExtensions::isNotNullOrEmpty($razon_social)){
            $this->queryable->andWhere('LOWER(empresa.razon_social) LIKE LOWER(:razon_social)')
                ->setParameter('razon_social', '%'.$razon_social.'%');
        }
    }

    public function withRFC($rfc)
    {
        if(StringExtensions::isNotNullOrEmpty($rfc)){
            $this->queryable->andWhere('LOWER(empresa.rfc) LIKE LOWER(:rfc)')
                ->setParameter('rfc', '%'.$rfc.'%');
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere('empresa.activo = :activo')
                ->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function withEstadoId($estado_id)
    {
        if(NumberExtensions::isPositiveInteger($estado_id)){
            $this->queryable->andWhere('empresa.estado = :estado_id')
                ->setParameter('estado_id', $estado_id);
        }
    }

    public function includeEstado($includeEstado)
    {
        if($includeEstado){
            $this->queryable->leftJoin('empresa.estado','estado')->addSelect('estado');
        }
    }

    public function withCiudadId($ciudad_id)
    {
        if(NumberExtensions::isPositiveInteger($ciudad_id)){
            $this->queryable->andWhere('empresa.ciudad = :ciudad_id')
                ->setParameter('ciudad_id', $ciudad_id);
        }
    }

    public function includeCiudad($includeCiudad)
    {
        if($includeCiudad){
            $this->queryable->leftJoin('empresa.ciudad','ciudad')->addSelect('ciudad');
        }
    }

    function withClienteId($cliente_id)
    {
        if(NumberExtensions::isPositiveInteger($cliente_id)){
            $this->queryable->andWhere("empresa.cliente = :cliente_id")->setParameter("cliente_id",$cliente_id);
        }
    }

    function includeCliente($includeCliente)
    {
        if($includeCliente){
            $this->queryable->leftJoin("empresa.cliente","cliente")->addSelect("cliente");
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(empresa.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('empresa.'.$sortBy.'', $sort);
    }
}


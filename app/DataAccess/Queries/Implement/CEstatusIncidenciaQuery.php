<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICEstatusIncidenciaQuery;
use App\Domain\CEstatusIncidencia;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CEstatusIncidenciaQuery extends CatalogoBaseQuery implements ICEstatusIncidenciaQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CEstatusIncidencia::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }
}


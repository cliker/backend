<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IHorarioQuery;
use App\Domain\Horario;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class HorarioQuery implements IHorarioQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("horario")
            ->from(Horario::class, 'horario')
            ->where('horario.id != :id')
            ->setParameter('id', 0);
    }

    public function withNombre($nombre)
    {
        if(StringExtensions::isNotNullOrEmpty($nombre)){
            $this->queryable->andWhere('LOWER(horario.nombre) LIKE LOWER(:nombre)')->setParameter('nombre', '%'.$nombre.'%');
        }
    }

    public function includeDetallesHorario($includeDetallesHorario)
    {
        if($includeDetallesHorario){
            $this->queryable->leftJoin('horario.detalles_horarios','detalles_horarios')->addSelect('detalles_horarios');
        }
    }

    public function withMinutosTolerancia($minutosTolerancia)
    {
        if (NumberExtensions::isPositiveInteger($minutosTolerancia))
            $this->queryable->andWhere('horario.minutos_tolerancia = :minutosTolerancia')->setParameter('minutosTolerancia', $minutosTolerancia);
    }

    public function withMinMinutosTolerancia($minMinutosTolerancia)
    {
        if (NumberExtensions::isPositiveInteger($minMinutosTolerancia))
            $this->queryable->andWhere('horario.minutos_tolerancia >= :minMinutosTolerancia')->setParameter('minMinutosTolerancia', $minMinutosTolerancia);
    }

    public function withMaxMinutosTolerancia($maxMinutosTolerancia)
    {
        if (NumberExtensions::isPositiveInteger($maxMinutosTolerancia))
            $this->queryable->andWhere('horario.minutos_tolerancia <= :maxMinutosTolerancia')->setParameter('maxMinutosTolerancia', $maxMinutosTolerancia);
    }

    public function withDepartamentoId($departamentoId)
    {
        if(NumberExtensions::isPositiveInteger($departamentoId)){
            $this->queryable->andWhere('horario.departamento = :departamentoId')->setParameter('departamentoId', $departamentoId);
        }
    }

    public function includeDepartamento($includeDepartamento)
    {
        if($includeDepartamento){
            $this->queryable->leftJoin('horario.departamento','departamento')->addSelect('departamento');
        }
    }

    public function withActivo($activo)
    {
        if(StringExtensions::isNotNullOrEmpty($activo)){
            $this->queryable->andWhere('horario.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('horario.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('horario.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa){
            $this->queryable->leftJoin('horario.empresa', 'empresa')->addSelect('empresa');
        }
    }

    public function withEmpresaId($empresaId)
    {
        if (NumberExtensions::isPositiveInteger($empresaId)) {
            $this->queryable->andWhere('horario.empresa = :empresaId')->setParameter('empresaId', $empresaId);
        }
    }

    public function withSucursalId($sucursalId)
    {
        if (NumberExtensions::isPositiveInteger($sucursalId))
            $this->queryable->andWhere('horario.sucursal = :sucursalId')->setParameter('sucursalId', $sucursalId);
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal)
            $this->queryable->leftJoin('horario.sucursal', 'sucursal')->addSelect('sucursal');
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(horario.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('horario.'.$sortBy.'', $sort);
    }
}


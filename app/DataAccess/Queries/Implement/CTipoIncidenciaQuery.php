<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\ICTipoIncidenciaQuery;
use App\Domain\CTipoIncidencia;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class CTipoIncidenciaQuery extends CatalogoBaseQuery implements ICTipoIncidenciaQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("entity")
            ->from(CTipoIncidencia::class, 'entity')
            ->where('entity.id != :id')
            ->setParameter('id', 0);
    }
}


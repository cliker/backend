<?php
namespace App\DataAccess\Queries\Implement;

use App\DataAccess\Queries\Interfaces\IRelojChecadorQuery;
use App\Domain\RelojChecador;
use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use Doctrine\ORM\AbstractQuery;


class RelojChecadorQuery implements IRelojChecadorQuery
{
    use TBaseQuery;

    function init()
    {
        $this->queryable
            ->select("reloj")
            ->from(RelojChecador::class, 'reloj')
            ->where('reloj.id != :id')
            ->setParameter('id', 0);
    }

    public function withSerial($serial)
    {
        if(StringExtensions::isNotNullOrEmpty($serial)){
            $this->queryable->andWhere('LOWER(reloj.serial) LIKE LOWER(:serial)')
                ->setParameter('serial', '%'.$serial.'%');
        }
    }

    public function withIp($ip)
    {
        if(StringExtensions::isNotNullOrEmpty($ip)){
            $this->queryable->andWhere('LOWER(reloj.ip) LIKE LOWER(:ip)')
                ->setParameter('ip', '%'.$ip.'%');
        }
    }

    public function withEmpresa($empresa)
    {
        if(StringExtensions::isNotNullOrEmpty($empresa)){
            $this->queryable->andWhere('reloj.empresa = :empresa')
                ->setParameter('empresa', $empresa);
        }


    }

    public function withSucursal($sucursal)
    {
        if(StringExtensions::isNotNullOrEmpty($sucursal)){
            $this->queryable->andWhere('reloj.sucursal = :sucursal')
                ->setParameter('sucursal', $sucursal);
        }
    }

    public function withDepartamento($departamento)
    {
        if(StringExtensions::isNotNullOrEmpty($departamento)){
            $this->queryable->andWhere('reloj.departamento = :departamento')
                ->setParameter('departamento', $departamento);
        }
    }

    public function includeEmpresa($include_empresa)
    {
        if($include_empresa){
            $this->queryable->leftJoin('reloj.empresa','empresa')->addSelect('empresa');
        }
    }

    public function includeSucursal($include_sucursal)
    {
        if($include_sucursal){
            $this->queryable->leftJoin('reloj.sucursal','sucursal')->addSelect('sucursal');
        }
    }

    public function includeDepartamento($include_departamento)
    {
        if($include_departamento){
            $this->queryable->leftJoin('reloj.departamento','departamento')->addSelect('departamento');
        }
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->queryable->andWhere('reloj.activo = :activo')->setParameter('activo', StringExtensions::toBoolean($activo));
        }
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->queryable->leftJoin('reloj.cliente', 'cliente')->addSelect('cliente');
    }

    public function withClienteId($clienteId)
    {
        if (NumberExtensions::isPositiveInteger($clienteId)) {
            $this->queryable->andWhere('reloj.cliente = :clienteId')->setParameter('clienteId', $clienteId);
        }
    }

    public function withEmpleadoId($empleado_id)
    {
        if(StringExtensions::isNotNullOrEmpty($empleado_id)){
            $this->includeEmpleados(true);
            $this->queryable->andWhere('empleados.id = :empleado')->setParameter('empleado', $empleado_id);
        }
    }

    public function includeEmpleados($includeEmpleados)
    {
        if($includeEmpleados){
            $this->queryable->leftJoin('reloj.empleados','empleados')->addSelect('empleados');
        }
    }

    function setCountSelect()
    {
        $this->hydrationMode = AbstractQuery::HYDRATE_SINGLE_SCALAR;
        $this->queryable->select("count(reloj.id)");
    }

    function sort($sortBy, $sort)
    {
        if (StringExtensions::isNotNullOrEmpty($sort) && StringExtensions::isNotNullOrEmpty($sortBy))
            $this->queryable->addOrderBy('reloj.'.$sortBy.'', $sort);
    }


}


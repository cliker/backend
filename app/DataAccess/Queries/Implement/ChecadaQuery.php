<?php
namespace App\DataAccess\Queries\Implement;

use App\Infrastructure\NumberExtensions;
use App\Infrastructure\StringExtensions;
use mysqli;


class ChecadaQuery
{
    protected $query = 'SELECT e.nombre, e.apellido_paterno, e.apellido_materno, e.clave, e.id ';
    protected $mysqli;

    public function __construct()
    {
        $this->mysqli = new mysqli(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE'));
    }

    public function getHoras($fecha){
        $this->query .= ", 
        (SELECT MIN(rc.hora) FROM registros_checador as rc WHERE e.id=rc.empleado_id AND rc.fecha='$fecha') as hora_entrada,
        (SELECT MAX(rc.hora) FROM registros_checador as rc WHERE e.id=rc.empleado_id AND rc.fecha='$fecha') as hora_salida,
        '$fecha' as fecha_checada
        ";
    }

    public function includeHorario($includeHorario)
    {
        if ($includeHorario)
            $this->query .= ' , h.id horario_id, h.nombre nombre_horario, h.minutos_tolerancia ';
    }

    public function includeSucursal($includeSucursal)
    {
        if ($includeSucursal)
            $this->query .= ' , s.id sucursal_id, s.nombre sucursal_nombre ';
    }

    public function includeDepartamento($includeDepartamento)
    {
        if ($includeDepartamento)
            $this->query .= ' , d.id departamento_id, d.nombre departamento_nombre ';
    }

    public function includeCliente($includeCliente)
    {
        if ($includeCliente)
            $this->query .= ' , c.id cliente_id, c.nombre cliente_nombre ';
    }

    public function includeEmpresa($includeEmpresa)
    {
        if ($includeEmpresa)
            $this->query .= ' , em.id empresa_id, em.nombre empresa_nombre ';
    }


    //table y join
    public function from(){
        $this->query .= 'FROM empleados as e';
    }

    public function leftJoinHorario($includeHorario){
        if ($includeHorario){
            $this->query .= " LEFT JOIN horarios as h ON e.horario_id=h.id ";
        }
    }

    public function leftJoinSucursal($includeSucursal){
        if ($includeSucursal){
            $this->query .= " LEFT JOIN sucursal as s ON e.sucursal_id=s.id ";
        }
    }

    public function leftJoinDepartamento($includeDepartamento){
        if ($includeDepartamento){
            $this->query .= " LEFT JOIN departamento as d ON e.departamento_id=d.id ";
        }
    }

    public function leftJoinCliente($includeCliente){
        if ($includeCliente){
            $this->query .= " LEFT JOIN clientes as c ON e.cliente_id=c.id ";
        }
    }

    public function leftJoinEmpresa($includeEmpresa){
        if ($includeEmpresa){
            $this->query .= " LEFT JOIN empresa as em ON e.empresa_id=em.id ";
        }
    }


    //wheres
    public function where($empleadoId){

        if($empleadoId == 0){
            $this->query .= ' WHERE e.id != 0';
        }else{
            if($empleadoId > 0){
                $this->query .= " WHERE e.id =$empleadoId ";
            }
        }

    }

    public function whitHorarioId($horarioId){
        if (NumberExtensions::isPositiveInteger($horarioId)){
            $this->query .= " AND e.horario_id = $horarioId";
        }
    }

    public function whitSucursalId($sucursalId){
        if (NumberExtensions::isPositiveInteger($sucursalId)){
            $this->query .= " AND e.sucursal_id = $sucursalId";
        }
    }

    public function whitDepartamentoId($departamentoId){
        if (NumberExtensions::isPositiveInteger($departamentoId)){
            $this->query .= " AND e.departamento_id = $departamentoId";
        }
    }

    public function whitClienteId($clienteId){
        if (NumberExtensions::isPositiveInteger($clienteId)){
            $this->query .= " AND e.cliente_id = $clienteId";
        }
    }

    public function whitEmpresaId($empresaId){
        if (NumberExtensions::isPositiveInteger($empresaId)){
            $this->query .= " AND e.empresa_id = $empresaId";
        }
    }

    public function withActivo($activo)
    {
        if (StringExtensions::isNotNullOrEmpty($activo) ){
            $this->query .= " AND e.activo = $activo";
        }
    }


    public function execute($includeHorario, $includeSucursal, $includeDepartamento, $includeCliente, $includeEmpresa){
        $resultado = $this->mysqli->query($this->query);
        $checadas = $this->generateResults($resultado,$includeHorario, $includeSucursal, $includeDepartamento, $includeCliente, $includeEmpresa);
        return $checadas;
    }

    public function closeConnection(){
        $this->mysqli->close();
    }
    private function generateResults($resultQuery, $includeHorario, $includeSucursal, $includeDepartamento, $includeCliente, $includeEmpresa)
    {
        $checadasObj = array();
        $c = 0;
        while ($checadas = $resultQuery->fetch_assoc()) {
            $checadasObj[$c] = array(
                'id'=> $checadas['id'],
                'clave'=> $checadas['clave'],
                'nombre' => utf8_encode($checadas['nombre']." ".$checadas['apellido_paterno']." ".$checadas['apellido_materno']),
                'fecha' => $checadas['fecha_checada'],
                'dia' => $this->getNameDay($checadas['fecha_checada']),
                'hora_entrada' => $checadas['hora_entrada'],
                'hora_salida' => $checadas['hora_salida'],
                'horario' => null,
                'sucursal' => null,
                'departamento' => null,
                'cliente' => null,
                'empresa' => null,
            );

            if ($includeHorario) {
                $horario = array(
                    'id' => $checadas["horario_id"],
                    'nombre' => utf8_encode($checadas["nombre_horario"]),
                    'minutos_tolerancia' => $checadas["minutos_tolerancia"],
                    'detalles_horario' => array()
                );
                $query = "SELECT * FROM detalles_horarios WHERE  horario_id = ".$checadas['horario_id']." AND activo = true";

                if ($detalles = $this->mysqli->query($query)) {
                    while($detallesHorario = $detalles->fetch_assoc()){

                        $detalleHorario = array(
                            'id' => $detallesHorario['id'],
                            'horario_id' => $detallesHorario['horario_id'],
                            'dia' => utf8_encode($detallesHorario['dia']),
                            'hora_entrada' => $detallesHorario['hora_entrada'],
                            'hora_salida' => $detallesHorario['hora_salida']
                        );
                        array_push($horario['detalles_horario'], $detalleHorario);
                    }
                }

                $checadasObj[$c]['horario'] = $horario;

            }

            if ($includeSucursal) {
                $sucursal = array(
                    'id' => $checadas["sucursal_id"],
                    'nombre' => utf8_encode($checadas["sucursal_nombre"]),
                );
                $checadasObj[$c]['sucursal'] = $sucursal;
            }

            if ($includeDepartamento) {
                $departamento = array(
                    'id' => $checadas["departamento_id"],
                    'nombre' => utf8_encode($checadas["departamento_nombre"]),
                );
                $checadasObj[$c]['departamento'] = $departamento;
            }

            if ($includeCliente) {
                $cliente = array(
                    'id' => $checadas["cliente_id"],
                    'nombre' => utf8_encode($checadas["cliente_nombre"]),
                );
                $checadasObj[$c]['cliente'] = $cliente;
            }

            if ($includeEmpresa) {
                $empresa = array(
                    'id' => $checadas["empresa_id"],
                    'nombre' => utf8_encode($checadas["empresa_nombre"]),
                );
                $checadasObj[$c]['empresa'] = $empresa;
            }
            $c++;
        }
        $resultQuery->free();
        return $checadasObj;
    }

    private function getNameDay($fecha){
        $fechats = strtotime($fecha);
        $nameDay = '';
        switch (date('w', $fechats)){
            case 0: $nameDay = 'Domingo'; break;
            case 1: $nameDay = 'Lunes'; break;
            case 2: $nameDay = 'Martes'; break;
            case 3: $nameDay = 'Miércoles'; break;
            case 4: $nameDay = 'Jueves'; break;
            case 5: $nameDay = 'Viernes'; break;
            case 6: $nameDay = 'Sábado'; break;
        }
        return $nameDay;

    }
}


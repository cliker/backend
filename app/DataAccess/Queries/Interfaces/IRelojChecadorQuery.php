<?php
namespace App\DataAccess\Queries\Interfaces;

interface IRelojChecadorQuery extends IBaseQuery
{
    public function withSerial($serial);
    public function withIp($ip);
    public function withEmpresa($empresa);
    public function includeEmpresa($include_empresa);
    public function withSucursal($sucursal);
    public function includeSucursal($include_sucursal);
    public function withDepartamento($departamento);
    public function includeDepartamento($include_departamento);
    public function withClienteId($clienteId);
    public function includeCliente($includeCliente);
    public function withActivo($activo);
    public function withEmpleadoId($empleado_id);
    public function includeEmpleados($includeEmpleados);
}

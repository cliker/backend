<?php
namespace App\DataAccess\Queries\Interfaces;

interface IIncidenciaQuery extends IBaseQuery
{
    public function withTipoId($tipo_id);
    public function includeTipo($includeTipo);
    public function withFechaInicio($fechaInicio);
    public function withMinFechaInicio($minFechaInicio);
    public function withMaxFechaInicio($maxFechaInicio);
    public function withFechaFinal($fechaFinal);
    public function withMinFechaFinal($minFechaFinal);
    public function withMaxFechaFinal($maxFechaFinal);
    public function withEmpleadoId($empleadoId);
    public function includeEmpleado($includeEmpleado);
    public function withActivo($activo);
    public function withClienteId($clienteId);
    public function includeCliente($includeCliente);
    public function withEmpresaId($empresaId);
    public function includeEmpresa($includeEmpresa);
    public function withSucursalId($sucursalId);
    public function includeSucursal($includeSucursal);
    public function withDepartamentoId($departamentoId);
    public function includeDepartamento($includeDepartamento);
    public function withEstatusId($estatusId);
    public function includeEstatus($includeEstatus);
    public function withEstatusValor($estatusValor);
    public function withUrlArchivo($url_archivo);
}

<?php
namespace App\DataAccess\Queries\Interfaces;

interface IEmpresaQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withRazonSocial($razon_social);
    public function withRFC($rfc);
    public function withActivo($activo);
    public function withEstadoId($estado_id);
    public function includeEstado($includeEstado);
    public function withCiudadId($ciudad_id);
    public function includeCiudad($includeCiudad);
    public function withClienteId($cliente_id);
    public function includeCliente($includeCliente);
}

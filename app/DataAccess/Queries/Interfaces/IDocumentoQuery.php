<?php
namespace App\DataAccess\Queries\Interfaces;

interface IDocumentoQuery extends IBaseQuery
{
    public function withTipoId($tipoId);
    public function includeTipo($includeTipo);
    public function withEmpledoId($empleadoId);
    public function includeEmpleado($includeEmpleado);
    public function withActivo($activo);
}

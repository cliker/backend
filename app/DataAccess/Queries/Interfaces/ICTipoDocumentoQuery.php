<?php
namespace App\DataAccess\Queries\Interfaces;

interface ICTipoDocumentoQuery extends ICatalogoBaseQuery
{
    public function withEmpresaId($empresaId);
    public function includeEmpresa($includeEmpresa);
    public function includeCliente($includeCliente);
    public function withClienteId($clienteId);
}

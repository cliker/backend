<?php
namespace App\DataAccess\Queries\Interfaces;

interface IDetalleHorarioQuery extends IBaseQuery
{
    public function withDia($dia);
    public function withHoraEntrada($horaEntrada);
    public function withMinHoraEntrada($minHoraEntrada);
    public function withMaxHoraEntrada($maxHoraEntrada);
    public function withHoraSalida($horaSalida);
    public function withMinHoraSalida($minHoraSalida);
    public function withMaxHoraSalida($maxHoraSalida);
    public function withHorarioId($horarioId);
    public function includeHorario($includeHorario);
    public function withActivo($activo);
}

<?php
namespace App\DataAccess\Queries\Interfaces;

interface IDiaFestivoQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withFecha($fecha);
    public function withMinFecha($minFecha);
    public function withMaxFecha($maxFecha);
    public function withDescripcion($descripcion);
    public function withActivo($activo);
    public function withDepartamentoId($departamento_id);
    public function includeDepartamentos($includeDepartamentos);
    public function includeCliente($includeCliente);
    public function withClienteId($clienteId);
    public function includeEmpresa($includeEmpresa);
    public function withEmpresaId($empresaId);
    public function withSucursalId($sucursalId);
    public function includeSucursal($includeSucursal);
}

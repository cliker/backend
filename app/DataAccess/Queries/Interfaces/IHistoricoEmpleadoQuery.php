<?php
namespace App\DataAccess\Queries\Interfaces;

interface IHistoricoEmpleadoQuery extends IBaseQuery
{
    public function withAccion($accion);
    public function withFecha($fecha);
    public function withMinFecha($minFecha);
    public function withMaxFecha($maxFecha);
    public function withResponsableId($responsableId);
    public function includeResponsable($includeResponsable);
    public function withEmpleadoId($empleadoId);
    public function includeEmpleado($includeEmpleado);
    public function withActivo($activo);
}

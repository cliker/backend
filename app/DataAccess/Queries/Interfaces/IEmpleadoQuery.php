<?php
namespace App\DataAccess\Queries\Interfaces;

interface IEmpleadoQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withApellidoPaterno($apellidoPaterno);
    public function withApellidoMaterno($apellidoMaterno);
    public function withFechaNacimiento($fechaNacimiento);
    public function withMinFechaNacimiento($minFechaNacimiento);
    public function withMaxFechaNacimiento($maxFechaNacimiento);
    public function withSexo($sexo);
    public function withEstadoId($estadoId);
    public function includeEstado($includeEstado);
    public function withCiudadId($ciudadId);
    public function includeCiudad($includeCiudad);
    public function withCodigoPostal($codigoPostal);
    public function withTelefono($telefono);
    public function withCorreo($correo);
    public function withFechaIngreso($fechaIngreso);
    public function withMinFechaIngreso($minFechaIngreso);
    public function withMaxFechaIngreso($maxFechaIngreso);
    public function withFechaBaja($fechaBaja);
    public function withMinFechaBaja($minFechaBaja);
    public function withMaxFechaBaja($maxFechaBaja);
    public function withDiaDescanso($diaDescanso);
    public function withHorarioId($horarioId);
    public function includeHorario($includeHorario);
    public function withSucursalId($sucursalId);
    public function includeSucursal($includeSucursal);
    public function withActivo($activo);
    public function withDepartamentoId($departamentoId);
    public function includeDepartamento($includeDepartamento);
    public function includeCliente($includeCliente);
    public function withClienteId($clienteId);
    public function includeEmpresa($includeEmpresa);
    public function withEmpresaId($empresaId);
    public function includeRelojesChecador($includeRelojesChecador);
    public function withEstadoCivilId($estadoCivilId);
    public function includeEstadoCivil($includeEstadoCivil);
    public function withPuesto($pusto);
    public function withClaveInterna($clave_interna);
    public function withTieneHijos($tiene_hijos);
}

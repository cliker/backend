<?php
namespace App\DataAccess\Queries\Interfaces;

interface IConfiguracionQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withClave($clave);
    public function withValor($valor);
    public function withDescripcion($descripcion);
    public function withActivo($activo);
    public function withSucursalId($sucursal_id);
    public function includeSucursal($includeSucursal);
    public function withEmpresaId($empresa_id);
    public function includeEmpresa($includeEmpresa);
}

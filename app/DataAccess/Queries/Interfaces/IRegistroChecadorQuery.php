<?php
namespace App\DataAccess\Queries\Interfaces;

interface IRegistroChecadorQuery extends IBaseQuery
{
    public function withFecha($fecha);
    public function withMinFecha($minFecha);
    public function withMaxFecha($maxFecha);

    public function withHora($hora);
    public function withMinHora($minHora);
    public function withMaxHora($maxHora);

    public function withEmpleadoId($empleadoId);
    public function includeEmpleado($includeEmpleado);

    public function withRelojChecadorId($relojChecadorId);
    public function includeRelojChecador($includeRelojChecador);

    public function withClienteId($clienteId);
    public function includeCliente($includeCliente);
    public function withEmpresaId($empresaId);
    public function includeEmpresa($includeEmpresa);
    public function withSucursalId($sucursalId);
    public function includeSucursal($includeSucursal);
    public function withDepartamentoId($departamentoId);
    public function includeDepartamento($includeDepartamento);

    public function withActivo($activo);
}

<?php
namespace App\DataAccess\Queries\Interfaces;

interface IHorarioQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function includeDetallesHorario($includeDetallesHorario);
    public function withMinutosTolerancia($minutosTolerancia);
    public function withMinMinutosTolerancia($minMinutosTolerancia);
    public function withMaxMinutosTolerancia($maxMinutosTolerancia);
    public function withDepartamentoId($departamentoId);
    public function includeDepartamento($includeDepartamento);
    public function withActivo($activo);
    public function includeCliente($includeCliente);
    public function withClienteId($clienteId);
    public function includeEmpresa($includeEmpresa);
    public function withEmpresaId($empresaId);
    public function withSucursalId($sucursalId);
    public function includeSucursal($includeSucursal);
}

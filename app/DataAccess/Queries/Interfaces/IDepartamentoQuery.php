<?php
namespace App\DataAccess\Queries\Interfaces;

interface IDepartamentoQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withEncargado($encargado);
    public function withCorreo($correo);
    public function withDescripcion($descripcion);
    public function withActivo($activo);
    public function withSucursalId($sucursal_id);
    public function includeSucursal($includeSucursal);
    public function withEmpresaId($empresa_id);
    public function includeEmpresa($includeEmpresa);
    public function includeDiasFestivos($includeDiasFestivos);
    public function withClienteId($clienteId);
    public function includeCliente($includeCliente);
}

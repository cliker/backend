<?php
namespace App\DataAccess\Queries\Interfaces;

interface ISucursalQuery extends IBaseQuery
{
    public function withNombre($nombre);
    public function withDireccion($direccion);
    public function withCodigoPostal($codigo_postal);
    public function withDescripcion($descripcion);
    public function withActivo($activo);
    public function withEstadoId($estado_id);
    public function includeEstado($includeEstado);
    public function withCiudadId($ciudad_id);
    public function includeCiudad($includeCiudad);
    public function withEmpresaId($empresa_id);
    public function includeEmpresa($includeEmpresa);
    public function withDiaFestivoId($diaFestivo_id);
    public function includeDiaFestivo($includeDiaFestivo);
    public function withClienteId($cliente_id);
    public function includeCliente($includeCliente);
}

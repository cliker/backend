<?php
namespace App\DataAccess\Repositories\Interfaces;

interface IEmpleadoRepository extends IBaseRepository
{
    public function getByEmpresaClave($empresaId, $clave);
}

<?php
namespace App\DataAccess\Repositories\Interfaces;

interface IRegistroChecadorRepository extends IBaseRepository
{
    public function getLastIdAsp($empresa_id);
}

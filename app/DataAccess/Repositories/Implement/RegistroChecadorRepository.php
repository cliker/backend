<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IRegistroChecadorRepository;
use App\Domain\RegistroChecador;
use App\Infrastructure\ArrayExtensions;

class RegistroChecadorRepository implements IRegistroChecadorRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('registroChecador')
            ->from(RegistroChecador::class, 'registroChecador')
            ->where('registroChecador.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }

    public function getLastIdAsp($empresa_id)
    {
        $lastId = 0;
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('registroChecador')
            ->from(RegistroChecador::class, 'registroChecador')
            ->where('registroChecador.empresa = :empresa')
            ->setParameter('empresa', $empresa_id)
            ->addOrderBy('registroChecador.id_asp', 'DESC')
            ->setMaxResults(1)
            ->getQuery();
        $results = $query->getArrayResult();
        if(ArrayExtensions::isNotNullOrEmpty($results)){
            $lastId = $results[0]['id_asp'];
        }
        return $lastId;

    }
}
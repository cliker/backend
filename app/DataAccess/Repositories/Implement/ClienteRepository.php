<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\Domain\Cliente;

class ClienteRepository implements IClienteRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('cliente')
            ->from(Cliente::class, 'cliente')
            ->where('cliente.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}
<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\ICTipoIncidenciaRepository;
use App\Domain\CTipoIncidencia;

class CTipoIncidenciaRepository implements ICTipoIncidenciaRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('tipoIncidencia')
            ->from(CTipoIncidencia::class, 'tipoIncidencia')
            ->where('tipoIncidencia.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}
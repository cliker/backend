<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IHistoricoEmpleadoRepository;
use App\Domain\HistoricoEmpleado;

class HistoricoEmpleadoRepository implements IHistoricoEmpleadoRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('historicoEmpleado')
            ->from(HistoricoEmpleado::class, 'historicoEmpleado')
            ->where('historicoEmpleado.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}
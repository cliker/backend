<?php


namespace App\DataAccess\Repositories\Implement;


use App\DataAccess\Configs\IUnitOfWork;

trait TBaseRepository
{
    protected $unitOfWork;

    public function __construct(IUnitOfWork $unitOfWork)
    {
        $this->unitOfWork = $unitOfWork;
    }

    /**
     * @return IUnitOfWork
     */
    public function getUnitOfWork(): IUnitOfWork
    {
        return $this->unitOfWork;
    }

}
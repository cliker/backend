<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IEmpleadoRepository;
use App\Domain\Empleado;

class EmpleadoRepository implements IEmpleadoRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('empleado')
            ->from(Empleado::class, 'empleado')
            ->where('empleado.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }

    public function getByEmpresaClave($empresaId, $clave)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('empleado')
            ->from(Empleado::class, 'empleado')
            ->where('empleado.empresa = :empresa')
            ->setParameter('empresa', $empresaId)
            ->andWhere('empleado.clave = :clave')
            ->setParameter('clave', $clave)
            ->getQuery();

        return $query->getOneOrNullResult();
    }
}
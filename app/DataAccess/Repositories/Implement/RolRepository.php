<?php
namespace App\DataAccess\Repositories\Implement;

use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Rol;

class RolRepository implements IRolRepository
{
    use TBaseRepository;

    public function get($id)
    {
        $queryable = $this->unitOfWork->queryable();
        $query = $queryable->select('rol')
            ->from(Rol::class, 'rol')
            ->where('rol.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    public function add($entity)
    {
        $this->unitOfWork->insert($entity);
    }

    public function update($entity)
    {
        $this->unitOfWork->update($entity);
    }

    public function remove($entity)
    {
        $this->unitOfWork->remove($entity);
    }
}
<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Cliente;
use App\Infrastructure\StringExtensions;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;


class ClientePolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $clienteRepository;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        IClienteRepository $clienteRepository
    )
    {
        parent::__construct($rolRepository);
        $this->clienteRepository = $clienteRepository;
    }

    public function findAuthorize(Usuario $usuario, Request $request){
        $rolesPermitidos = ['root'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $usuario, $id){
        $rolesPermitidos = ['root'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

}

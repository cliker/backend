<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IEmpresaRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Http\Request;

class EmpresaPolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $empresaRespository;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        IEmpresaRepository $empresaRepository
    )
    {
        parent::__construct($rolRepository);
        $this->empresaRespository = $empresaRepository;
    }

    public function findAuthorize(Usuario $usuario, Request $request){
        $rolesPermitidos = ['root', 'admin'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $usuario, $id){
        $rolesPermitidos = ['root', 'admin'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }
}

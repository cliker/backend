<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IRelojChecadorRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class RelojChecadorPolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $relojChecadorRepository;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        IRelojChecadorRepository $relojChecadorRepository
    )
    {
        parent::__construct($rolRepository);
        $this->relojChecadorRepository = $relojChecadorRepository;
    }

    public function findAuthorize(Usuario $usuario, Request $request){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa', 'admin-sucursal'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $usuario, $id){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa', 'admin-sucursal'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }
}

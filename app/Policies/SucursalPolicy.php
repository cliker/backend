<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\DataAccess\Repositories\Interfaces\ISucursalRepository;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class SucursalPolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $sucursalRespository;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        ISucursalRepository $sucursalRepository
    )
    {
        parent::__construct($rolRepository);
        $this->sucursalRespository = $sucursalRepository;
    }

    public function findAuthorize(Usuario $usuario, Request $request){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $usuario, $id){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }
}

<?php

namespace App\Policies;

use App\DataAccess\Repositories\Interfaces\IDepartamentoRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartamentoPolicy extends BasePolicy
{
    use HandlesAuthorization;
    protected $departamentoRepository;
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        IRolRepository $rolRepository,
        IDepartamentoRepository $departamentoRepository
    )
    {
        parent::__construct($rolRepository);
        $this->departamentoRepository = $departamentoRepository;
    }

    public function findAuthorize(Usuario $usuario, Request $request){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa', 'admin-sucursal'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }

    public function getAuthorize(Usuario $usuario, $id){
        $rolesPermitidos = ['root', 'admin', 'admin-empresa', 'admin-sucursal'];
        $rolUsuario = $this->getRolUsuario($usuario->rol_id);

        if(StringExtensions::isNotNullOrEmpty($rolUsuario)){
            if(in_array($rolUsuario->getName(), $rolesPermitidos)){
                $this->response = true;
            }
        }

        $this->setErrorAuthorization($this->response, $this->error);
        return $this->response;
    }
}

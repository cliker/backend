<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 17/12/18
 * Time: 04:06 PM
 */

namespace App\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="estados")
 */

class CEstado extends CatalogoBase
{
    /**
     * One Estado has Many Ciudades.
     * @ORM\ManyToMany(targetEntity="CCiudad", mappedBy="estado", cascade={"all"}))
     */
    protected $ciudades;

    /**
     * @return mixed
     */
    public function getCiudades()
    {
        return $this->ciudades;
    }

    /**
     * @param mixed $ciudades
     */
    public function setCiudades($ciudades): void
    {
        $this->ciudades = $ciudades;
    }

    public function addCiudad(CCiudad $ciudad){
        if(is_null( $this->ciudades)){
            $this->ciudades = new ArrayCollection();
        }
        $this->ciudades->add($ciudad);

        if(!$this->ciudades->contains($ciudad)){
            $ciudad->setEstado($this);
            $this->ciudades->add($ciudad);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 6/28/19
 * Time: 12:37 PM
 */

namespace App\Domain;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * @ORM\Table(name="tipos_documentos")
 */


class CTipoDocumento extends CatalogoBase
{
    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\Column(type="text")
     */
    protected $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }
}
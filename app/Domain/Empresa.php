<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 17/12/18
 * Time: 04:01 PM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="empresa")
 * @ORM\HasLifecycleCallbacks
 */

class Empresa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    protected $clave_empleado_auto = 0;

    /**
     * @ORM\Column(type="string")
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $url_server_asp;


    /**
     * @ORM\Column(type="string")
     */
    protected $razon_social;

    /**
     * @ORM\Column(type="string", length= 15)
     */
    protected $rfc;

    /**
     * @ORM\Column(type="string")
     */
    protected $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="CEstado")
     * @var CEstado
     */
    protected $estado;

    /**
     * @ORM\ManyToOne(targetEntity="CCiudad")
     * @var CCiudad
     */
    protected $ciudad;

    /**
     * One Empresa has Many Sucursales
     * @ORM\OneToMany(targetEntity="Sucursal", mappedBy="empresa", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $sucursales;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $activo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    public function __construct()
    {
        $this->sucursales = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClaveEmpleadoAuto()
    {
        return $this->clave_empleado_auto;
    }

    /**
     * @param mixed $clave_empleado_auto
     */
    public function setClaveEmpleadoAuto($clave_empleado_auto): void
    {
        $this->clave_empleado_auto = $clave_empleado_auto;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getUrlServerAsp()
    {
        return $this->url_server_asp;
    }

    /**
     * @param mixed $url_server_asp
     */
    public function setUrlServerAsp($url_server_asp): void
    {
        $this->url_server_asp = $url_server_asp;
    }

    /**
     * @return mixed
     */
    public function getRazonSocial()
    {
        return $this->razon_social;
    }

    /**
     * @param mixed $razon_social
     */
    public function setRazonSocial($razon_social): void
    {
        $this->razon_social = $razon_social;
    }

    /**
     * @return mixed
     */
    public function getRFC()
    {
        return $this->rfc;
    }

    /**
     * @param mixed $rfc
     */
    public function setRFC($rfc): void
    {
        $this->rfc = $rfc;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad): void
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getSucursales()
    {
        return $this->sucursales;
    }

    /**
     * @param mixed $sucursales
     */
    public function setSucursales($sucursales)
    {
        $this->sucursales = $sucursales;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 17/12/18
 * Time: 04:01 PM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="sucursal")
 * @ORM\HasLifecycleCallbacks
 */

class Sucursal
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string")
     */
    protected $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="CEstado")
     * @var CEstado
     */
    protected $estado;

    /**
     * @ORM\ManyToOne(targetEntity="CCiudad")
     * @var CCiudad
     */
    protected $ciudad;

    /**
     * @ORM\Column(type="string", length= 10, nullable= true)
     */
    protected $codigo_postal;

    /**
     * @ORM\Column(type="text", nullable= true)
     */
    protected $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * One Sucursal has Many DiaFestivo.
     * @ORM\OneToMany(targetEntity="DiaFestivo", mappedBy="sucursal", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $dias_festivos;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $activo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    public function __construct()
    {
        $this->dias_festivos = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad): void
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getCodigoPostal()
    {
        return $this->codigo_postal;
    }

    /**
     * @param mixed $codigo_postal
     */
    public function setCodigoPosta($codigo_postal): void
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getDiasFestivos()
    {
        return $this->dias_festivos;
    }

    /**
     * @param mixed $dias_festivos
     */
    public function setDiasFestivos($dias_festivos): void
    {
        $this->dias_festivos = $dias_festivos;
    }

    public function addDiaFestivo(DiaFestivo $diaFestivo){
        $diaFestivo->setSucursal($this);
        $this->dias_festivos->add($diaFestivo);
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }


    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
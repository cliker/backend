<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 7/1/19
 * Time: 9:58 AM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="registros_checador")
 * @ORM\HasLifecycleCallbacks
 */

class RegistroChecador
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id_asp;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $fecha;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $hora;

    /**
     * Many HistoricoChecado have One Empleado.
     * @ORM\ManyToOne(targetEntity="Empleado", inversedBy="registros_checador")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     */
    protected $empleado;

    /**
     * @ORM\ManyToOne(targetEntity="RelojChecador")
     * @var RelojChecador
     */
    protected $reloj_checador;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @var Sucursal
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @var Departamento
     */
    protected $departamento;


    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdAsp()
    {
        return $this->id_asp;
    }

    /**
     * @param mixed $id
     */
    public function setIdAsp($id_asp): void
    {
        $this->id_asp = $id_asp;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha): void
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * @param mixed $hora
     */
    public function setHora($hora): void
    {
        $this->hora = $hora;
    }

    /**
     * @return mixed
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * @param mixed $empleado
     */
    public function setEmpleado($empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return mixed
     */
    public function getRelojChecador()
    {
        return $this->reloj_checador;
    }

    /**
     * @param mixed $reloj_checador
     */
    public function setRelojChecador($reloj_checador)
    {
        $this->reloj_checador = $reloj_checador;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * @param mixed $sucursal
     */
    public function setSucursal($sucursal): void
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento): void
    {
        $this->departamento = $departamento;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
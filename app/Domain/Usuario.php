<?php
/**
 * Created by PhpStorm.
 * Usuario: jorge
 * Date: 13/12/18
 * Time: 09:35 AM
 */

namespace App\Domain;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
//use function Functional\concat;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuarios")
 * @ORM\HasLifecycleCallbacks
 */

class Usuario
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $nombre;
    /**
     * @ORM\Column(type="string", length=60)
     */
    protected $apellido_paterno;

    /**
     * @ORM\Column(type="string" , length=60, nullable=true)
     */
    protected $apellido_materno;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $titulo;
    /**
     * @ORM\Column(type="string" , length=150, nullable=true)
     */
    protected $puesto;
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $telefono_oficina;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $extension;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $celular;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $fecha_nacimiento;
    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $remember_token;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @var Sucursal
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @var Departamento
     */
    protected $departamento;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $activo;

    /**
     * @ORM\ManyToOne(targetEntity="Rol")
     **/
    protected $rol;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $primer_inicio_sesion = true;

    /*One Usuario has One SesionUsuarioDoctrine.
    @ORM\OneToOne(targetEntity="SesionUsuarioDoctrine", mappedBy="usuario", cascade={"all"}, fetch="LAZY")*/
    //protected $sesion_usuario;

    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getAPellidoPaterno()
    {
        return $this->apellido_paterno;
    }

    /**
     * @param mixed $apellido_paterno
     */
    public function setApellidoPAterno($apellido_paterno): void
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return mixed
     */
    public function getApellidoMaterno()
    {
        return $this->apellido_materno;
    }

    /**
     * @param mixed $apellido_materno
     */
    public function setApellidoMaterno($apellido_materno): void
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono): void
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getTelefonoOficina()
    {
        return $this->telefono_oficina;
    }

    /**
     * @param mixed $telefono_oficina
     */
    public function setTelefonoOficina($telefono_oficina): void
    {
        $this->telefono_oficina = $telefono_oficina;
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension): void
    {
        $this->extension = $extension;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular): void
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }

    /**
     * @param mixed $remember_token
     */
    public function setRememberToken($remember_token): void
    {
        $this->remember_token = $remember_token;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @param mixed $rol
     */
    public function setRol($rol): void
    {
        $this->rol = $rol;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * @param mixed $sucursal
     */
    public function setSucursal($sucursal): void
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento): void
    {
        $this->departamento = $departamento;
    }

    /**
     * @return mixed
     */
    /*public function getSesionUsuario()
    {
        return $this->sesion_usuario;
    }*/

    /**
     * @param mixed $sesion_usuario
     */
    /*public function setSesionUsuario($sesion_usuario): void
    {
        $this->sesion_usuario = $sesion_usuario;
    }*/

    /**
     * @return mixed
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * @param mixed $puesto
     */
    public function setPuesto($puesto)
    {
        $this->puesto = $puesto;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param mixed $fecha_nacimiento
     */
    public function setFechaNacimiento($fecha_nacimiento)
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new DateTime("now");
    }

    /**
     * @return mixed
     */
    public function getNombreCompleto(): ? string
    {
        return $this->getNombre().' '.$this->getApellidoPaterno().' '.$this->getApellidoMaterno();
    }

    /**
     * @return mixed
     */
    public function getPrimerInicioSesion()
    {
        return $this->primer_inicio_sesion;
    }

    /**
     * @param mixed $primer_inicio_sesion
     */
    public function setPrimerInicioSesion($primer_inicio_sesion)
    {
        $this->primer_inicio_sesion = $primer_inicio_sesion;
    }

}
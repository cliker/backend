<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 6/28/19
 * Time: 10:55 AM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="detalles_horarios")
 * @ORM\HasLifecycleCallbacks
 *
 */

class DetalleHorario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $dia;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $hora_entrada;

    /**
     * @ORM\Column(type="time", nullable=false)
     */
    protected $hora_salida;

    /**
     * Many DetalleHorario have One Horario
     * @ORM\ManyToOne(targetEntity="Horario")
     * @ORM\JoinColumn(name="horario_id", referencedColumnName="id")
     * @var Horario
     */
    protected $horario;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDia()
    {
        return $this->dia;
    }

    /**
     * @param mixed $dia
     */
    public function setDia($dia): void
    {
        $this->dia = $dia;
    }

    /**
     * @return mixed
     */
    public function getHoraEntrada()
    {
        return $this->hora_entrada;
    }

    /**
     * @param mixed $hora_entrada
     */
    public function setHoraEntrada($hora_entrada): void
    {
        $this->hora_entrada = $hora_entrada;
    }

    /**
     * @return mixed
     */
    public function getHoraSalida()
    {
        return $this->hora_salida;
    }

    /**
     * @param mixed $hora_salida
     */
    public function setHoraSalida($hora_salida): void
    {
        $this->hora_salida = $hora_salida;
    }

    /**
     * @return Horario
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * @param Horario $horario
     */
    public function setHorario($horario): void
    {
        $this->horario = $horario;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
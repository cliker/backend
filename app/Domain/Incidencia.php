<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 6/28/19
 * Time: 12:39 PM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="incidencias")
 * @ORM\HasLifecycleCallbacks
 */


class Incidencia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $descripcion;

    /**
     * Many Incidencias have One CTipoIncidencia.
     * @ORM\ManyToOne(targetEntity="CTipoIncidencia")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     */
    protected $tipo;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $fecha_inicio;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $fecha_final;

    /**
     * Many Incidencia have One Empleado.
     * @ORM\ManyToOne(targetEntity="Empleado", inversedBy="incidencias")
     * @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
     * @var Empleado
     */
    protected $empleado;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $url_archivo;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @var Sucursal
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @var Departamento
     */
    protected $departamento;

    /**
     * @ORM\ManyToOne(targetEntity="CEstatusIncidencia")
     * @var CEstatusIncidencia
     */
    protected $estatus;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     */
    public function setDescripcion($descripcion): void
    {
        $this->descripcion = $descripcion;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo): void
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getFechaInicio()
    {
        return $this->fecha_inicio;
    }

    /**
     * @param mixed $fecha_inicio
     */
    public function setFechaInicio($fecha_inicio): void
    {
        $this->fecha_inicio = $fecha_inicio;
    }

    /**
     * @return mixed
     */
    public function getFechaFinal()
    {
        return $this->fecha_final;
    }

    /**
     * @param mixed $fecha_final
     */
    public function setFechaFinal($fecha_final): void
    {
        $this->fecha_final = $fecha_final;
    }

    /**
     * @return mixed
     */
    public function getEmpleado()
    {
        return $this->empleado;
    }

    /**
     * @param mixed $empleado
     */
    public function setEmpleado($empleado)
    {
        $this->empleado = $empleado;
    }

    /**
     * @return mixed
     */
    public function getUrlArchivo()
    {
        return $this->url_archivo;
    }

    /**
     * @param mixed $url_archivo
     */
    public function setUrlArchivo($url_archivo): void
    {
        $this->url_archivo = $url_archivo;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * @param mixed $sucursal
     */
    public function setSucursal($sucursal): void
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento): void
    {
        $this->departamento = $departamento;
    }

    /**
     * @return mixed
     */
    public function getEstatus()
    {
        return $this->estatus;
    }

    /**
     * @param mixed $estatus
     */
    public function setEstatus($estatus): void
    {
        $this->estatus = $estatus;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: des-sis14
 * Date: 6/28/19
 * Time: 2:01 PM
 */

namespace App\Domain;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Illuminate\Support\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="empleados")
 * @ORM\HasLifecycleCallbacks
 */

class Empleado
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=200, nullable=false)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="integer", length=11, nullable=false)
     */
    protected $clave;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $apellido_paterno;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $apellido_materno;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $fecha_nacimiento;

    /**
     * @ORM\Column(type="string", length=1, nullable=false)
     */
    protected $sexo;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $direccion;

    /**
     * @ORM\ManyToOne(targetEntity="CEstado")
     * @var CEstado
     */
    protected $estado;

    /**
     * @ORM\ManyToOne(targetEntity="CCiudad")
     * @var CCiudad
     */
    protected $ciudad;

    /**
     * @ORM\Column(type="string", length=10, nullable=false)
     */
    protected $codigo_postal;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $clave_interna;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $puesto;

    /**
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    protected $correo;

    /**
     * @ORM\ManyToOne(targetEntity="CEstadoCivil")
     * @var CEstadoCivil
     */
    protected $estado_civil;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    protected $fecha_ingreso;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $fecha_baja;

    /**
     * @ORM\Column(type="string",  length=100, nullable=false)
     */
    protected $dia_descanso;

    /**
     * @ORM\ManyToOne(targetEntity="Horario")
     * @var Horario
     */
    protected $horario;

    /**
     * @ORM\ManyToOne(targetEntity="Sucursal")
     * @var Sucursal
     */
    protected $sucursal;

    /**
     * @ORM\ManyToOne(targetEntity="Departamento")
     * @var Departamento
     */
    protected $departamento;

    /**
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @var Cliente
     */
    protected $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @var Empresa
     */
    protected $empresa;

    /**
     * One Empleado has Many Incidencia.
     * @ORM\OneToMany(targetEntity="Incidencia", mappedBy="empleado", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $incidencias;

    /**
     * One Empleado has Many Documentos.
     * @ORM\OneToMany(targetEntity="Documento", mappedBy="empleado", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $documentos;

    /**
     * One Empleado has Many RegistroChecador.
     * @ORM\OneToMany(targetEntity="RegistroChecador", mappedBy="empleado", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $registros_checador;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $tiene_hijos;


    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $activo;

    /**
     * One Empleado has Many RelojChecador.
     * @ORM\ManyToMany(targetEntity="RelojChecador", mappedBy="empleados", cascade={"all"}, orphanRemoval=true)
     * @var Collection
     */
    protected $relojes_checador;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $created_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updated_date;

    public function __construct()
    {
        $this->incidencias = new ArrayCollection();
        $this->documentos = new ArrayCollection();
        $this->registros_checador = new ArrayCollection();
        $this->relojes_checador = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * @param mixed $clave
     */
    public function setClave($clave): void
    {
        $this->clave = $clave;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getApellidoPaterno()
    {
        return $this->apellido_paterno;
    }

    /**
     * @param mixed $apellido_paterno
     */
    public function setApellidoPaterno($apellido_paterno): void
    {
        $this->apellido_paterno = $apellido_paterno;
    }

    /**
     * @return mixed
     */
    public function getApellidoMaterno()
    {
        return $this->apellido_paterno;
    }

    /**
     * @param mixed $apellido_materno
     */
    public function setApellidoMaterno($apellido_materno): void
    {
        $this->apellido_materno = $apellido_materno;
    }

    /**
     * @return mixed
     */
    public function getFechaNacimiento()
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param mixed $fecha_nacimiento
     */
    public function setFechaNacimiento($fecha_nacimiento): void
    {
        $this->fecha_nacimiento = $fecha_nacimiento;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo): void
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion): void
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     */
    public function setEstado($estado): void
    {
        $this->estado = $estado;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad): void
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getCodigoPostal()
    {
        return $this->codigo_postal;
    }

    /**
     * @param mixed $codigo_postal
     */
    public function setCodigoPostal($codigo_postal): void
    {
        $this->codigo_postal = $codigo_postal;
    }

    /**
     * @return mixed
     */
    public function getPuesto()
    {
        return $this->puesto;
    }

    /**
     * @param mixed $puesto
     */
    public function setPuesto($puesto): void
    {
        $this->puesto = $puesto;
    }

    /**
     * @return mixed
     */
    public function getClaveInterna()
    {
        return $this->clave_interna;
    }

    /**
     * @param mixed $clave_interna
     */
    public function setClaveInterna($clave_interna): void
    {
        $this->clave_interna = $clave_interna;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono): void
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param mixed $correo
     */
    public function setCorreo($correo): void
    {
        $this->correo = $correo;
    }

    /**
     * @return mixed
     */
    public function getEstadoCivil()
    {
        return $this->estado_civil;
    }

    /**
     * @param mixed $estado_civil
     */
    public function setEstadoCivil($estado_civil): void
    {
        $this->estado_civil = $estado_civil;
    }

    /**
     * @return mixed
     */
    public function getFechaIngreso()
    {
        return $this->fecha_ingreso;
    }

    /**
     * @param mixed $fecha_ingreso
     */
    public function setFechaIngreso($fecha_ingreso): void
    {
        $this->fecha_ingreso = $fecha_ingreso;
    }

    /**
     * @return mixed
     */
    public function getFechaBaja()
    {
        return $this->fecha_baja;
    }

    /**
     * @param mixed $fecha_baja
     */
    public function setFechaBaja($fecha_baja): void
    {
        $this->fecha_baja = $fecha_baja;
    }

    /**
     * @return mixed
     */
    public function getDiaDescanso()
    {
        return $this->dia_descanso;
    }

    /**
     * @param mixed $dia_descanso
     */
    public function setDiaDescanso($dia_descanso): void
    {
        $this->dia_descanso = $dia_descanso;
    }

    /**
     * @return mixed
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * @param mixed $horario
     */
    public function setHorario($horario): void
    {
        $this->horario = $horario;
    }

    /**
     * @return mixed
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * @param mixed $sucursal
     */
    public function setSucursal($sucursal): void
    {
        $this->sucursal = $sucursal;
    }

    /**
     * @return mixed
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * @param mixed $departamento
     */
    public function setDepartamento($departamento): void
    {
        $this->departamento = $departamento;
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getIncidencias()
    {
        return $this->incidencias;
    }

    /**
     * @param $incidencias
     */
    public function setIncidencias($incidencias): void
    {
        $this->incidencias = $incidencias;
    }

    public function addIncidencia(Incidencia $incidencia){
        $incidencia->setEmpleado($this);
        $this->incidencias->add($incidencia);
    }

    /**
     * @return ArrayCollection|Collection
     */
    public function getDocumentos()
    {
        return $this->documentos;
    }

    /**
     * @param $documentos
     */
    public function setDocumentos($documentos): void
    {
        $this->documentos = $documentos;
    }

    public function addDocumento(Documento $documento){
        $documento->setEmpleado($this);
        $this->documentos->add($documento);
    }

    /**
     * @return mixed
     */
    public function getRegistrosChecador()
    {
        return $this->registros_checador;
    }

    /**
     * @param mixed $
     */
    public function setRegistrosChecador($registros_checador): void
    {
        $this->registros_checador = $registros_checador;
    }

    public function addRegistroChecador(RegistroChecador $registro_checador){
        $registro_checador->setEmpleado($this);
        $this->registros_checador->add($registro_checador);
    }

    /**
     * @return mixed
     */
    public function getTieneHijos()
    {
        return $this->tiene_hijos;
    }

    /**
     * @param mixed $tiene_hijos
     */
    public function setTieneHijos($tiene_hijos): void
    {
        $this->tiene_hijos = $tiene_hijos;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * @param mixed $activo
     */
    public function setActivo($activo): void
    {
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getRelojesChecador()
    {
        return $this->relojes_checador;
    }

    /**
     * @param mixed $relojes_checador
     */
    public function setRelojesChecador($relojes_checador): void
    {
        $this->relojes_checador = $relojes_checador;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * @param mixed $empresa
     */
    public function setEmpresa($empresa): void
    {
        $this->empresa = $empresa;
    }

    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->created_date;
    }

    /**
     * @param mixed $created_date
     */
    public function setCreatedDate($created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return mixed
     */
    public function getUpdatedDate()
    {
        return $this->updated_date;
    }

    /**
     * @param mixed $updated_date
     */
    public function setUpdatedDate($updated_date): void
    {
        $this->updated_date = $updated_date;
    }

    /**
     * Triggered on insert
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_date = new DateTime("now");
    }

    /**
     * Triggered on update
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_date = new DateTime("now");
    }
}
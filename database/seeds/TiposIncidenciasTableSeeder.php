<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\CTipoIncidencia;
use Illuminate\Database\Seeder;

class TiposIncidenciasTableSeeder extends Seeder
{
    protected $unitOfWork;
    public function __construct(
        IUnitOfWork $unitOfWork
    ){
        $this->unitOfWork = $unitOfWork;
    }

    public function run()
    {

        $items = array(
            [
                "nombre" => "Vacaciones",
                "valor" => "tipo_incidencia_vacaciones",
            ],
            [
                "nombre" => "Permiso por enfermedad",
                "valor" => "tipo_incidencia_permiso_enfermedad",
            ],
            [
                "nombre" => "Permiso Casual",
                "valor" => "tipo_incidencia_permiso_casual",
            ],
            [
                "nombre" => "Permiso por maternidad",
                "valor" => "tipo_incidencia_permiso_maternidad",
            ],
            [
                "nombre" => "Permiso especial",
                "valor" => "tipo_incidencia_permiso_especial",
            ],
            [
                "nombre" => "Permiso por viajes de negocios",
                "valor" => "tipo_incidencia_permiso_viajes_negocios",
            ],
            [
                "nombre" => "Permiso con goce de sueldo",
                "valor" => "tipo_incidencia_permiso_con_goce_sueldo",
            ],
            [
                "nombre" => "Permiso sin goce de sueldo",
                "valor" => "tipo_incidencia_permiso_sin_goce_sueldo",
            ],
            [
                "nombre" => "Incapacidad",
                "valor" => "tipo_incidencia_por_incapacidad",
            ],
            [
                "nombre" => "Permiso urgencia",
                "valor" => "tipo_incidencia_permiso_por_urgencia",
            ],
            [
                "nombre" => "Diligencias laborales",
                "valor" => "tipo_incidencia_diligencia_laboral",
            ],
            [
                "nombre" => "Retardo justificado",
                "valor" => "tipo_incidencia_retardo_justificado",
            ],
        );

        foreach ($items as $item){
            $obj = new CTipoIncidencia();
            $obj->setNombre($item['nombre']);
            $obj->setValor($item['valor']);
            $obj->setActivo(true);
            $this->unitOfWork->insert($obj);
        }
    }
}
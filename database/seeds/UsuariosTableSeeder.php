<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\IClienteRepository;
use App\DataAccess\Repositories\Interfaces\IRolRepository;
use App\Domain\Usuario;
use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{
    protected $unitOfWork;
    protected $clienteRepository;
    protected $rolRepository;

    public function __construct(
        IUnitOfWork $unitOfWork,
        IClienteRepository $clienteRepository,
        IRolRepository $rolRepository
    ){
        $this->unitOfWork = $unitOfWork;
        $this->clienteRepository = $clienteRepository;
        $this->rolRepository = $rolRepository;
    }

    public function run()
    {
        //$cliente = $this->clienteRepository->get(1);
        $rol = $this->rolRepository->get(1);
        $usuario = new Usuario();
        $usuario->setNombre('Administrador General');
        $usuario->setApellidoPAterno('.');
        $usuario->setApellidoMaterno('');
        $usuario->setTitulo('Ingeniero');
        $usuario->setPuesto('Desarrollador');
        $usuario->setTelefono('9993381472');
        $usuario->setTelefonoOficina('9785467');
        $usuario->setExtension('111');
        $usuario->setCelular('9993381472');
        $usuario->setEmail('sistemas1@interno.com.mx');
        $usuario->setFechaNacimiento(new \DateTime('1990-07-11'));
        $usuario->setPassword(bcrypt('Pb123456*'));
        //$usuario->setCliente($cliente);
        $usuario->setActivo(true);
        $usuario->setRol($rol);
        $usuario->setPrimerInicioSesion(true);
        $this->unitOfWork->insert($usuario);
    }
}
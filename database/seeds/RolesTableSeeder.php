<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\DataAccess\Repositories\Interfaces\IPermisoRepository;
use App\Domain\Rol;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    protected $unitOfWork;
    protected $permisoRepository;
    public function __construct(
        IUnitOfWork $unitOfWork,
        IPermisoRepository $permisoRepository
    ){
        $this->unitOfWork = $unitOfWork;
        $this->permisoRepository = $permisoRepository;
    }

    public function run()
    {
        //$permiso = $this->permisoRepository->get(1);
        $rol = new Rol();
        $rol->setName('root');
        $rol->setDisplayName('Super usuario');
        $rol->setDescription('Usuario root');
        $rol->setIsDelete(false);
        //$rol->addPermission($permiso);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('admin');
        $rol->setDisplayName('Administrador');
        $rol->setDescription('Administrador');
        $rol->setIsDelete(false);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('admin-empresa');
        $rol->setDisplayName('Admin Empresa');
        $rol->setDescription('Admin Empresa');
        $rol->setIsDelete(false);
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('admin-sucursal');
        $rol->setDisplayName('Admin Sucursal');
        $rol->setIsDelete(false);
        $rol->setDescription('Admin Sucursal');
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('admin-depto');
        $rol->setDisplayName('Admin Departamento');
        $rol->setIsDelete(false);
        $rol->setDescription('Admin Departamento');
        $this->unitOfWork->insert($rol);

        $rol = new Rol();
        $rol->setName('empleado');
        $rol->setDisplayName('Empleado');
        $rol->setIsDelete(false);
        $rol->setDescription('Empleado');
        $this->unitOfWork->insert($rol);
    }
}
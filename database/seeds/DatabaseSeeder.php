<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            //Catalogos
            TiposIncidenciasTableSeeder::class,
            TiposDocumentosTableSeeder::class,
            EstatusIncidenciasTableSeeder::class,

            //General
            EstadosTableSeeder::class,
            ClientesTableSeeder::class,
            PermisosTableSeeder::class,
            RolesTableSeeder::class,
            UsuariosTableSeeder::class,

            //Nomina Dummy

            //RH Dummy
        ]);
    }
}

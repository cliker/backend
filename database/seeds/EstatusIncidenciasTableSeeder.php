<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\CEstatusIncidencia;
use App\Domain\CEstadoCivil;
use Illuminate\Database\Seeder;

class EstatusIncidenciasTableSeeder extends Seeder
{
    protected $unitOfWork;
    public function __construct(
        IUnitOfWork $unitOfWork
    ){
        $this->unitOfWork = $unitOfWork;
    }

    public function run()
    {

        $items = array(
            [
                "nombre" => "Pendiente",
                "valor" => "estatus_pendiente",
            ],
            [
                "nombre" => "En revision",
                "valor" => "estatus_revision",
            ],
            [
                "nombre" => "Aprobado",
                "valor" => "estatus_aprobado",
            ],
            [
                "nombre" => "Rechazado",
                "valor" => "estatus_rechazado",
            ]
        );

        $estados_civiles = array(
            [ "nombre" => "Soltero", "valor" => "soltero", ],
            [ "nombre" => "Casado", "valor" => "casado", ]
        );

        foreach ($items as $item){
            $obj = new CEstatusIncidencia();
            $obj->setNombre($item['nombre']);
            $obj->setValor($item['valor']);
            $obj->setActivo(true);
            $this->unitOfWork->insert($obj);
        }

        foreach ($estados_civiles as $valor){
            $obj = new CEstadoCivil();
            $obj->setNombre($valor['nombre']);
            $obj->setValor($valor['valor']);
            $obj->setActivo(true);
            $this->unitOfWork->insert($obj);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jorge
 * Date: 27/12/18
 * Time: 02:51 PM
 */

use App\DataAccess\Configs\IUnitOfWork;
use App\Domain\CTipoDocumento;
use Illuminate\Database\Seeder;

class TiposDocumentosTableSeeder extends Seeder
{
    protected $unitOfWork;
    public function __construct(
        IUnitOfWork $unitOfWork
    ){
        $this->unitOfWork = $unitOfWork;
    }

    public function run()
    {

        /* $items = array(
            [
                "nombre" => "IFE",
                "valor" => "tipo_documento_ife",
            ],
            [
                "nombre" => "Acta de nacimiento",
                "valor" => "tipo_documento_acta_nacimiento",
            ],
            [
                "nombre" => "Curriculum",
                "valor" => "tipo_documento_curriculum",
            ],
            [
                "nombre" => "Comprobante de domicilio",
                "valor" => "tipo_documento_comprobante_domicilio",
            ],
            [
                "nombre" => "Titulo",
                "valor" => "tipo_documento_titulo",
            ],
            [
                "nombre" => "Cedula",
                "valor" => "tipo_documento_cedula",
            ],
        );

        foreach ($items as $item){
            $obj = new CTipoDocumento();
            $obj->setNombre($item['nombre']);
            $obj->setValor($item['valor']);
            $obj->setActivo(true);
            $this->unitOfWork->insert($obj);
        } */
    }
}
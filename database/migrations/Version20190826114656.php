<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190826114656 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE empleados_reloj_checador (reloj_checador_id INT NOT NULL, empleado_id INT NOT NULL, INDEX IDX_103532D023D818FF (reloj_checador_id), INDEX IDX_103532D0952BE730 (empleado_id), PRIMARY KEY(reloj_checador_id, empleado_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE empleados_reloj_checador ADD CONSTRAINT FK_103532D023D818FF FOREIGN KEY (reloj_checador_id) REFERENCES reloj_checador (id)');
        $this->addSql('ALTER TABLE empleados_reloj_checador ADD CONSTRAINT FK_103532D0952BE730 FOREIGN KEY (empleado_id) REFERENCES empleados (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE empleados_reloj_checador');
    }
}

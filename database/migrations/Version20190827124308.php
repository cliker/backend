<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190827124308 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE empleados ADD estado_civil_id INT DEFAULT NULL, DROP estado_civil');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C75376D93 FOREIGN KEY (estado_civil_id) REFERENCES estados_civil (id)');
        $this->addSql('CREATE INDEX IDX_9EB2266C75376D93 ON empleados (estado_civil_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266C75376D93');
        $this->addSql('DROP INDEX IDX_9EB2266C75376D93 ON empleados');
        $this->addSql('ALTER TABLE empleados ADD estado_civil VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP estado_civil_id');
    }
}

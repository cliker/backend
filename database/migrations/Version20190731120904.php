<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190731120904 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dias_festivos_departamentos (dia_festivo_id INT NOT NULL, departamento_id INT NOT NULL, INDEX IDX_BE5D2F70C6C4C2DB (dia_festivo_id), INDEX IDX_BE5D2F705A91C08D (departamento_id), PRIMARY KEY(dia_festivo_id, departamento_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dias_festivos_departamentos ADD CONSTRAINT FK_BE5D2F70C6C4C2DB FOREIGN KEY (dia_festivo_id) REFERENCES dia_festivo (id)');
        $this->addSql('ALTER TABLE dias_festivos_departamentos ADD CONSTRAINT FK_BE5D2F705A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('ALTER TABLE dia_festivo DROP FOREIGN KEY FK_687817955A91C08D');
        $this->addSql('DROP INDEX IDX_687817955A91C08D ON dia_festivo');
        $this->addSql('ALTER TABLE dia_festivo DROP departamento_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE dias_festivos_departamentos');
        $this->addSql('ALTER TABLE dia_festivo ADD departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dia_festivo ADD CONSTRAINT FK_687817955A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_687817955A91C08D ON dia_festivo (departamento_id)');
    }
}

<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20191030134905 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registros_checador ADD cliente_id INT DEFAULT NULL, ADD sucursal_id INT DEFAULT NULL, ADD departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F4313DE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F4313279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F43135A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_7B2F4313DE734E51 ON registros_checador (cliente_id)');
        $this->addSql('CREATE INDEX IDX_7B2F4313279A5D5E ON registros_checador (sucursal_id)');
        $this->addSql('CREATE INDEX IDX_7B2F43135A91C08D ON registros_checador (departamento_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F4313DE734E51');
        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F4313279A5D5E');
        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F43135A91C08D');
        $this->addSql('DROP INDEX IDX_7B2F4313DE734E51 ON registros_checador');
        $this->addSql('DROP INDEX IDX_7B2F4313279A5D5E ON registros_checador');
        $this->addSql('DROP INDEX IDX_7B2F43135A91C08D ON registros_checador');
        $this->addSql('ALTER TABLE registros_checador DROP cliente_id, DROP sucursal_id, DROP departamento_id');
    }
}

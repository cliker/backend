<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190711165556 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios ADD empresa_id INT DEFAULT NULL, ADD sucursal_id INT DEFAULT NULL, ADD departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F2521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F2279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE usuarios ADD CONSTRAINT FK_EF687F25A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_EF687F2521E1991 ON usuarios (empresa_id)');
        $this->addSql('CREATE INDEX IDX_EF687F2279A5D5E ON usuarios (sucursal_id)');
        $this->addSql('CREATE INDEX IDX_EF687F25A91C08D ON usuarios (departamento_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F2521E1991');
        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F2279A5D5E');
        $this->addSql('ALTER TABLE usuarios DROP FOREIGN KEY FK_EF687F25A91C08D');
        $this->addSql('DROP INDEX IDX_EF687F2521E1991 ON usuarios');
        $this->addSql('DROP INDEX IDX_EF687F2279A5D5E ON usuarios');
        $this->addSql('DROP INDEX IDX_EF687F25A91C08D ON usuarios');
        $this->addSql('ALTER TABLE usuarios DROP empresa_id, DROP sucursal_id, DROP departamento_id');
    }
}

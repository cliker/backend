<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190709155352 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dia_festivo DROP FOREIGN KEY FK_68781795279A5D5E');
        $this->addSql('DROP INDEX IDX_68781795279A5D5E ON dia_festivo');
        $this->addSql('ALTER TABLE dia_festivo CHANGE sucursal_id departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dia_festivo ADD CONSTRAINT FK_687817955A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_687817955A91C08D ON dia_festivo (departamento_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE dia_festivo DROP FOREIGN KEY FK_687817955A91C08D');
        $this->addSql('DROP INDEX IDX_687817955A91C08D ON dia_festivo');
        $this->addSql('ALTER TABLE dia_festivo CHANGE departamento_id sucursal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE dia_festivo ADD CONSTRAINT FK_68781795279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('CREATE INDEX IDX_68781795279A5D5E ON dia_festivo (sucursal_id)');
    }
}

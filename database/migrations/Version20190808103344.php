<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190808103344 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registros_checador ADD empresa_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F4313521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_7B2F4313521E1991 ON registros_checador (empresa_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F4313521E1991');
        $this->addSql('DROP INDEX IDX_7B2F4313521E1991 ON registros_checador');
        $this->addSql('ALTER TABLE registros_checador DROP empresa_id');
    }
}

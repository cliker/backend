<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190828120821 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672EC0FD2F1');
        $this->addSql('DROP INDEX IDX_FFC7C672EC0FD2F1 ON incidencias');
        $this->addSql('ALTER TABLE incidencias CHANGE estatus_incidencium_id estatus_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C67259BAB351 FOREIGN KEY (estatus_id) REFERENCES estatus_incidencias (id)');
        $this->addSql('CREATE INDEX IDX_FFC7C67259BAB351 ON incidencias (estatus_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C67259BAB351');
        $this->addSql('DROP INDEX IDX_FFC7C67259BAB351 ON incidencias');
        $this->addSql('ALTER TABLE incidencias CHANGE estatus_id estatus_incidencium_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672EC0FD2F1 FOREIGN KEY (estatus_incidencium_id) REFERENCES estatus_incidencias (id)');
        $this->addSql('CREATE INDEX IDX_FFC7C672EC0FD2F1 ON incidencias (estatus_incidencium_id)');
    }
}

<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190705115344 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE departamento DROP FOREIGN KEY FK_40E497EB5A91C08D');
        $this->addSql('DROP INDEX IDX_40E497EB5A91C08D ON departamento');
        $this->addSql('ALTER TABLE departamento CHANGE departamento_id empresa_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE departamento ADD CONSTRAINT FK_40E497EB521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_40E497EB521E1991 ON departamento (empresa_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE departamento DROP FOREIGN KEY FK_40E497EB521E1991');
        $this->addSql('DROP INDEX IDX_40E497EB521E1991 ON departamento');
        $this->addSql('ALTER TABLE departamento CHANGE empresa_id departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE departamento ADD CONSTRAINT FK_40E497EB5A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_40E497EB5A91C08D ON departamento (departamento_id)');
    }
}

<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190701102106 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dia_festivo (id INT AUTO_INCREMENT NOT NULL, sucursal_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, descripcion LONGTEXT DEFAULT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_68781795279A5D5E (sucursal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departamento (id INT AUTO_INCREMENT NOT NULL, sucursal_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, encargado VARCHAR(255) NOT NULL, descripcion LONGTEXT DEFAULT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_40E497EB279A5D5E (sucursal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE incidencias (id INT AUTO_INCREMENT NOT NULL, tipo_id INT DEFAULT NULL, empleado_id INT DEFAULT NULL, descripcion LONGTEXT NOT NULL, fecha_inicio DATE NOT NULL, fecha_final DATE NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_FFC7C672A9276E6C (tipo_id), INDEX IDX_FFC7C672952BE730 (empleado_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empresa (id INT AUTO_INCREMENT NOT NULL, estado_id INT DEFAULT NULL, ciudad_id INT DEFAULT NULL, usuario_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, razon_social VARCHAR(255) NOT NULL, rfc VARCHAR(15) NOT NULL, direccion VARCHAR(255) NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_B8D75A509F5A440B (estado_id), INDEX IDX_B8D75A50E8608214 (ciudad_id), INDEX IDX_B8D75A50DB38439E (usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE documentos (id INT AUTO_INCREMENT NOT NULL, tipo_id INT DEFAULT NULL, empleado_id INT DEFAULT NULL, nombre VARCHAR(200) NOT NULL, descripcion LONGTEXT DEFAULT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_1EB82936A9276E6C (tipo_id), INDEX IDX_1EB82936952BE730 (empleado_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE empleados (id INT AUTO_INCREMENT NOT NULL, estado_id INT DEFAULT NULL, ciudad_id INT DEFAULT NULL, horario_id INT DEFAULT NULL, sucursal_id INT DEFAULT NULL, departamento_id INT DEFAULT NULL, nombre VARCHAR(200) NOT NULL, apellido_paterno VARCHAR(255) NOT NULL, apellido_materno VARCHAR(255) NOT NULL, fecha_nacimiento DATE NOT NULL, sexo VARCHAR(1) NOT NULL, direccion VARCHAR(255) NOT NULL, codigo_postal VARCHAR(10) NOT NULL, telefono VARCHAR(20) DEFAULT NULL, correo VARCHAR(150) DEFAULT NULL, estado_civil VARCHAR(255) DEFAULT NULL, fecha_ingreso DATE NOT NULL, fecha_baja DATE DEFAULT NULL, dia_descanso VARCHAR(100) NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_9EB2266C9F5A440B (estado_id), INDEX IDX_9EB2266CE8608214 (ciudad_id), INDEX IDX_9EB2266C4959F1BA (horario_id), INDEX IDX_9EB2266C279A5D5E (sucursal_id), INDEX IDX_9EB2266C5A91C08D (departamento_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sucursal (id INT AUTO_INCREMENT NOT NULL, estado_id INT DEFAULT NULL, ciudad_id INT DEFAULT NULL, empresa_id INT DEFAULT NULL, nombre VARCHAR(255) NOT NULL, direccion VARCHAR(255) NOT NULL, codigo_postal VARCHAR(10) DEFAULT NULL, descripcion LONGTEXT DEFAULT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_E99C6D569F5A440B (estado_id), INDEX IDX_E99C6D56E8608214 (ciudad_id), INDEX IDX_E99C6D56521E1991 (empresa_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tipos_documentos (id INT AUTO_INCREMENT NOT NULL, empresa_id INT DEFAULT NULL, nombre VARCHAR(200) NOT NULL, valor VARCHAR(200) DEFAULT NULL, activo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_55EC61CF2E892728 (valor), INDEX IDX_55EC61CF521E1991 (empresa_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tipos_incidencias (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(200) NOT NULL, valor VARCHAR(200) DEFAULT NULL, activo TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_3BEDD8822E892728 (valor), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registros_checador (id INT AUTO_INCREMENT NOT NULL, empleado_id INT DEFAULT NULL, reloj_checador_id INT DEFAULT NULL, fecha DATE NOT NULL, hora TIME NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_7B2F4313952BE730 (empleado_id), INDEX IDX_7B2F431323D818FF (reloj_checador_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detalles_horarios (id INT AUTO_INCREMENT NOT NULL, horario_id INT DEFAULT NULL, dia VARCHAR(100) NOT NULL, hora_entrada TIME NOT NULL, hora_salida TIME NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_EFE72744959F1BA (horario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE horarios (id INT AUTO_INCREMENT NOT NULL, departamento_id INT DEFAULT NULL, nombre VARCHAR(200) NOT NULL, descripcion LONGTEXT DEFAULT NULL, min_tolerancia INT NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_5433650A5A91C08D (departamento_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reloj_checador (id INT AUTO_INCREMENT NOT NULL, empresa_id INT DEFAULT NULL, sucursal_id INT DEFAULT NULL, departamento_id INT DEFAULT NULL, serial VARCHAR(255) NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_BC809C47521E1991 (empresa_id), INDEX IDX_BC809C47279A5D5E (sucursal_id), INDEX IDX_BC809C475A91C08D (departamento_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dia_festivo ADD CONSTRAINT FK_68781795279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE departamento ADD CONSTRAINT FK_40E497EB279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672A9276E6C FOREIGN KEY (tipo_id) REFERENCES tipos_incidencias (id)');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672952BE730 FOREIGN KEY (empleado_id) REFERENCES empleados (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A509F5A440B FOREIGN KEY (estado_id) REFERENCES estados (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A50E8608214 FOREIGN KEY (ciudad_id) REFERENCES ciudades (id)');
        $this->addSql('ALTER TABLE empresa ADD CONSTRAINT FK_B8D75A50DB38439E FOREIGN KEY (usuario_id) REFERENCES usuarios (id)');
        $this->addSql('ALTER TABLE documentos ADD CONSTRAINT FK_1EB82936A9276E6C FOREIGN KEY (tipo_id) REFERENCES tipos_documentos (id)');
        $this->addSql('ALTER TABLE documentos ADD CONSTRAINT FK_1EB82936952BE730 FOREIGN KEY (empleado_id) REFERENCES empleados (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C9F5A440B FOREIGN KEY (estado_id) REFERENCES estados (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266CE8608214 FOREIGN KEY (ciudad_id) REFERENCES ciudades (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C4959F1BA FOREIGN KEY (horario_id) REFERENCES horarios (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C5A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('ALTER TABLE sucursal ADD CONSTRAINT FK_E99C6D569F5A440B FOREIGN KEY (estado_id) REFERENCES estados (id)');
        $this->addSql('ALTER TABLE sucursal ADD CONSTRAINT FK_E99C6D56E8608214 FOREIGN KEY (ciudad_id) REFERENCES ciudades (id)');
        $this->addSql('ALTER TABLE sucursal ADD CONSTRAINT FK_E99C6D56521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE tipos_documentos ADD CONSTRAINT FK_55EC61CF521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F4313952BE730 FOREIGN KEY (empleado_id) REFERENCES empleados (id)');
        $this->addSql('ALTER TABLE registros_checador ADD CONSTRAINT FK_7B2F431323D818FF FOREIGN KEY (reloj_checador_id) REFERENCES reloj_checador (id)');
        $this->addSql('ALTER TABLE detalles_horarios ADD CONSTRAINT FK_EFE72744959F1BA FOREIGN KEY (horario_id) REFERENCES horarios (id)');
        $this->addSql('ALTER TABLE horarios ADD CONSTRAINT FK_5433650A5A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('ALTER TABLE reloj_checador ADD CONSTRAINT FK_BC809C47521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE reloj_checador ADD CONSTRAINT FK_BC809C47279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE reloj_checador ADD CONSTRAINT FK_BC809C475A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266C5A91C08D');
        $this->addSql('ALTER TABLE horarios DROP FOREIGN KEY FK_5433650A5A91C08D');
        $this->addSql('ALTER TABLE reloj_checador DROP FOREIGN KEY FK_BC809C475A91C08D');
        $this->addSql('ALTER TABLE sucursal DROP FOREIGN KEY FK_E99C6D56521E1991');
        $this->addSql('ALTER TABLE tipos_documentos DROP FOREIGN KEY FK_55EC61CF521E1991');
        $this->addSql('ALTER TABLE reloj_checador DROP FOREIGN KEY FK_BC809C47521E1991');
        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672952BE730');
        $this->addSql('ALTER TABLE documentos DROP FOREIGN KEY FK_1EB82936952BE730');
        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F4313952BE730');
        $this->addSql('ALTER TABLE dia_festivo DROP FOREIGN KEY FK_68781795279A5D5E');
        $this->addSql('ALTER TABLE departamento DROP FOREIGN KEY FK_40E497EB279A5D5E');
        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266C279A5D5E');
        $this->addSql('ALTER TABLE reloj_checador DROP FOREIGN KEY FK_BC809C47279A5D5E');
        $this->addSql('ALTER TABLE documentos DROP FOREIGN KEY FK_1EB82936A9276E6C');
        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672A9276E6C');
        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266C4959F1BA');
        $this->addSql('ALTER TABLE detalles_horarios DROP FOREIGN KEY FK_EFE72744959F1BA');
        $this->addSql('ALTER TABLE registros_checador DROP FOREIGN KEY FK_7B2F431323D818FF');
        $this->addSql('DROP TABLE dia_festivo');
        $this->addSql('DROP TABLE departamento');
        $this->addSql('DROP TABLE incidencias');
        $this->addSql('DROP TABLE empresa');
        $this->addSql('DROP TABLE documentos');
        $this->addSql('DROP TABLE empleados');
        $this->addSql('DROP TABLE sucursal');
        $this->addSql('DROP TABLE tipos_documentos');
        $this->addSql('DROP TABLE tipos_incidencias');
        $this->addSql('DROP TABLE registros_checador');
        $this->addSql('DROP TABLE detalles_horarios');
        $this->addSql('DROP TABLE horarios');
        $this->addSql('DROP TABLE reloj_checador');
    }
}

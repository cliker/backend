<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190726114313 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reloj_checador ADD cliente_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reloj_checador ADD CONSTRAINT FK_BC809C47DE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('CREATE INDEX IDX_BC809C47DE734E51 ON reloj_checador (cliente_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reloj_checador DROP FOREIGN KEY FK_BC809C47DE734E51');
        $this->addSql('DROP INDEX IDX_BC809C47DE734E51 ON reloj_checador');
        $this->addSql('ALTER TABLE reloj_checador DROP cliente_id');
    }
}

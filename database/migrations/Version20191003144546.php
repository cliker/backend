<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20191003144546 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE historico_empleados (id INT AUTO_INCREMENT NOT NULL, empleado_id INT DEFAULT NULL, responsable_id INT DEFAULT NULL, fecha DATE NOT NULL, accion VARCHAR(50) NOT NULL, activo TINYINT(1) NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, INDEX IDX_A301E623952BE730 (empleado_id), INDEX IDX_A301E62353C59D72 (responsable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE historico_empleados ADD CONSTRAINT FK_A301E623952BE730 FOREIGN KEY (empleado_id) REFERENCES empleados (id)');
        $this->addSql('ALTER TABLE historico_empleados ADD CONSTRAINT FK_A301E62353C59D72 FOREIGN KEY (responsable_id) REFERENCES usuarios (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE historico_empleados');
    }
}

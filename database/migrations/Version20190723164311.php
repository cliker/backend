<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190723164311 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE departamento ADD cliente_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE departamento ADD CONSTRAINT FK_40E497EBDE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('CREATE INDEX IDX_40E497EBDE734E51 ON departamento (cliente_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE departamento DROP FOREIGN KEY FK_40E497EBDE734E51');
        $this->addSql('DROP INDEX IDX_40E497EBDE734E51 ON departamento');
        $this->addSql('ALTER TABLE departamento DROP cliente_id');
    }
}

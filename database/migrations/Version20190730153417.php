<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190730153417 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horarios ADD cliente_id INT DEFAULT NULL, ADD empresa_id INT DEFAULT NULL, ADD sucursal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE horarios ADD CONSTRAINT FK_5433650ADE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('ALTER TABLE horarios ADD CONSTRAINT FK_5433650A521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE horarios ADD CONSTRAINT FK_5433650A279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('CREATE INDEX IDX_5433650ADE734E51 ON horarios (cliente_id)');
        $this->addSql('CREATE INDEX IDX_5433650A521E1991 ON horarios (empresa_id)');
        $this->addSql('CREATE INDEX IDX_5433650A279A5D5E ON horarios (sucursal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE horarios DROP FOREIGN KEY FK_5433650ADE734E51');
        $this->addSql('ALTER TABLE horarios DROP FOREIGN KEY FK_5433650A521E1991');
        $this->addSql('ALTER TABLE horarios DROP FOREIGN KEY FK_5433650A279A5D5E');
        $this->addSql('DROP INDEX IDX_5433650ADE734E51 ON horarios');
        $this->addSql('DROP INDEX IDX_5433650A521E1991 ON horarios');
        $this->addSql('DROP INDEX IDX_5433650A279A5D5E ON horarios');
        $this->addSql('ALTER TABLE horarios DROP cliente_id, DROP empresa_id, DROP sucursal_id');
    }
}

<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190801160123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tipos_documentos ADD cliente_id INT DEFAULT NULL, ADD descripcion LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE tipos_documentos ADD CONSTRAINT FK_55EC61CFDE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('CREATE INDEX IDX_55EC61CFDE734E51 ON tipos_documentos (cliente_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE tipos_documentos DROP FOREIGN KEY FK_55EC61CFDE734E51');
        $this->addSql('DROP INDEX IDX_55EC61CFDE734E51 ON tipos_documentos');
        $this->addSql('ALTER TABLE tipos_documentos DROP cliente_id, DROP descripcion');
    }
}

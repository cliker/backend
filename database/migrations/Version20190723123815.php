<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190723123815 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sucursal ADD cliente_id INT DEFAULT NULL, DROP updated_date');
        $this->addSql('ALTER TABLE sucursal ADD CONSTRAINT FK_E99C6D56DE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('CREATE INDEX IDX_E99C6D56DE734E51 ON sucursal (cliente_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sucursal DROP FOREIGN KEY FK_E99C6D56DE734E51');
        $this->addSql('DROP INDEX IDX_E99C6D56DE734E51 ON sucursal');
        $this->addSql('ALTER TABLE sucursal ADD updated_date DATETIME DEFAULT NULL, DROP cliente_id');
    }
}

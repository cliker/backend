<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190805162947 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias ADD cliente_id INT DEFAULT NULL, ADD empresa_id INT DEFAULT NULL, ADD sucursal_id INT DEFAULT NULL, ADD departamento_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672DE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C672279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('ALTER TABLE incidencias ADD CONSTRAINT FK_FFC7C6725A91C08D FOREIGN KEY (departamento_id) REFERENCES departamento (id)');
        $this->addSql('CREATE INDEX IDX_FFC7C672DE734E51 ON incidencias (cliente_id)');
        $this->addSql('CREATE INDEX IDX_FFC7C672521E1991 ON incidencias (empresa_id)');
        $this->addSql('CREATE INDEX IDX_FFC7C672279A5D5E ON incidencias (sucursal_id)');
        $this->addSql('CREATE INDEX IDX_FFC7C6725A91C08D ON incidencias (departamento_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672DE734E51');
        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672521E1991');
        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C672279A5D5E');
        $this->addSql('ALTER TABLE incidencias DROP FOREIGN KEY FK_FFC7C6725A91C08D');
        $this->addSql('DROP INDEX IDX_FFC7C672DE734E51 ON incidencias');
        $this->addSql('DROP INDEX IDX_FFC7C672521E1991 ON incidencias');
        $this->addSql('DROP INDEX IDX_FFC7C672279A5D5E ON incidencias');
        $this->addSql('DROP INDEX IDX_FFC7C6725A91C08D ON incidencias');
        $this->addSql('ALTER TABLE incidencias DROP cliente_id, DROP empresa_id, DROP sucursal_id, DROP departamento_id');
    }
}

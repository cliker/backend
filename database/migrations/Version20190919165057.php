<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190919165057 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias ADD url_archivo LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE tipos_documentos CHANGE descripcion descripcion LONGTEXT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE incidencias DROP url_archivo');
        $this->addSql('ALTER TABLE tipos_documentos CHANGE descripcion descripcion LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}

<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190730144305 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE empleados ADD cliente_id INT DEFAULT NULL, ADD empresa_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266CDE734E51 FOREIGN KEY (cliente_id) REFERENCES clientes (id)');
        $this->addSql('ALTER TABLE empleados ADD CONSTRAINT FK_9EB2266C521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('CREATE INDEX IDX_9EB2266CDE734E51 ON empleados (cliente_id)');
        $this->addSql('CREATE INDEX IDX_9EB2266C521E1991 ON empleados (empresa_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266CDE734E51');
        $this->addSql('ALTER TABLE empleados DROP FOREIGN KEY FK_9EB2266C521E1991');
        $this->addSql('DROP INDEX IDX_9EB2266CDE734E51 ON empleados');
        $this->addSql('DROP INDEX IDX_9EB2266C521E1991 ON empleados');
        $this->addSql('ALTER TABLE empleados DROP cliente_id, DROP empresa_id');
    }
}

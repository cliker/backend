<?php

namespace Database\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema as Schema;

class Version20190925145725 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE configuraciones ADD empresa_id INT DEFAULT NULL, ADD sucursal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE configuraciones ADD CONSTRAINT FK_E6493EE5521E1991 FOREIGN KEY (empresa_id) REFERENCES empresa (id)');
        $this->addSql('ALTER TABLE configuraciones ADD CONSTRAINT FK_E6493EE5279A5D5E FOREIGN KEY (sucursal_id) REFERENCES sucursal (id)');
        $this->addSql('CREATE INDEX IDX_E6493EE5521E1991 ON configuraciones (empresa_id)');
        $this->addSql('CREATE INDEX IDX_E6493EE5279A5D5E ON configuraciones (sucursal_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE configuraciones DROP FOREIGN KEY FK_E6493EE5521E1991');
        $this->addSql('ALTER TABLE configuraciones DROP FOREIGN KEY FK_E6493EE5279A5D5E');
        $this->addSql('DROP INDEX IDX_E6493EE5521E1991 ON configuraciones');
        $this->addSql('DROP INDEX IDX_E6493EE5279A5D5E ON configuraciones');
        $this->addSql('ALTER TABLE configuraciones DROP empresa_id, DROP sucursal_id');
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function () {
    return redirect("https://pbmg.com.mx/");
});

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'AuthController@login');

});
Route::get('registro/update', 'RegistroChecadorController@updateChecadas');

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('/logout', 'AuthController@logout');

    Route::group(['prefix' => 'catalogo'], function () {
        Route::group(['prefix' => 'estados'], function () {
            Route::get('/find', 'CEstadoController@find');
            Route::get('/{id}', 'CEstadoController@get');
            Route::post('/', 'CEstadoController@store');
            Route::put('/', 'CEstadoController@update');
            Route::delete('/{id?}', 'CEstadoController@destroy');
        });

        Route::group(['prefix' => 'ciudades'], function () {
            Route::get('/find', 'CCiudadController@find');
            Route::get('/{id}', 'CCiudadController@get');
            Route::post('/', 'CCiudadController@store');
            Route::put('/', 'CCiudadController@update');
            Route::delete('/{id?}', 'CCiudadController@destroy');
        });

        Route::group(['prefix' => 'tipos-incidencias'], function () {
            Route::get('/find', 'CTipoIncidenciaController@find');
            Route::get('/{id}', 'CTipoIncidenciaController@get');
            Route::post('/', 'CTipoIncidenciaController@store');
            Route::put('/', 'CTipoIncidenciaController@update');
            Route::delete('/{id?}', 'CTipoIncidenciaController@destroy');
        });

        Route::group(['prefix' => 'tipos-documentos'], function () {
            Route::get('/find', 'CTipoDocumentoController@find');
            Route::get('/{id}', 'CTipoDocumentoController@get');
            Route::post('/', 'CTipoDocumentoController@store');
            Route::put('/', 'CTipoDocumentoController@update');
            Route::delete('/{id?}', 'CTipoDocumentoController@destroy');
        });

        Route::group(['prefix' => 'estados-civiles'], function () {
            Route::get('/find', 'CEstadoCivilController@find');
            Route::get('/{id}', 'CEstadoCivilController@get');
            Route::post('/', 'CEstadoCivilController@store');
            Route::put('/', 'CEstadoCivilController@update');
            Route::delete('/{id?}', 'CEstadoCivilController@destroy');
        });

        Route::group(['prefix' => 'estatus-incidencias'], function () {
            Route::get('/find', 'CEstatusIncidenciaController@find');
            Route::get('/{id}', 'CEstatusIncidenciaController@get');
            Route::post('/', 'CEstatusIncidenciaController@store');
            Route::put('/', 'CEstatusIncidenciaController@update');
            Route::delete('/{id?}', 'CEstatusIncidenciaController@destroy');
        });
    });

    Route::group(['prefix' => 'clientes'], function () {
        Route::get('/find', 'ClienteController@find');
        Route::get('/{id}', 'ClienteController@get');
        Route::post('/', 'ClienteController@store');
        Route::put('/', 'ClienteController@update');
        Route::delete('/{id?}', 'ClienteController@destroy');
    });

    Route::group(['prefix' => 'permisos'], function () {
        Route::get('/find', 'PermisoController@find');
        Route::get('/{id}', 'PermisoController@get');
        Route::post('/', 'PermisoController@store');
        Route::put('/', 'PermisoController@update');
        Route::delete('/{id?}', 'PermisoController@destroy');
    });

    Route::group(['prefix' => 'roles'], function () {
        Route::get('/find', 'RolController@find');
        Route::get('/{id}', 'RolController@get');
        Route::post('/', 'RolController@store');
        Route::put('/', 'RolController@update');
        Route::delete('/{id?}', 'RolController@destroy');
    });

    Route::group(['prefix' => 'usuarios'], function () {
        Route::get('/layout', 'UsuarioController@exportLayout');
        Route::get('/find', 'UsuarioController@find');
        Route::get('/{id}', 'UsuarioController@get');
        Route::post('/', 'UsuarioController@store');
        Route::put('/', 'UsuarioController@update');
        Route::put('/password', 'UsuarioController@password');
        Route::delete('/{id?}', 'UsuarioController@destroy');
    });

    Route::group(['prefix' => 'empresas'], function () {
        Route::get('/find', 'EmpresaController@find');
        Route::get('/{id}', 'EmpresaController@get');
        Route::post('/', 'EmpresaController@store');
        Route::put('/', 'EmpresaController@update');
        Route::delete('/{id?}', 'EmpresaController@destroy');
    });

    Route::group(['prefix' => 'sucursales'], function () {
        Route::get('/find', 'SucursalController@find');
        Route::get('/{id}', 'SucursalController@get');
        Route::post('/', 'SucursalController@store');
        Route::put('/', 'SucursalController@update');
        Route::delete('/{id?}', 'SucursalController@destroy');
    });

    Route::group(['prefix' => 'departamentos'], function () {
        Route::get('/find', 'DepartamentoController@find');
        Route::get('/{id}', 'DepartamentoController@get');
        Route::post('/', 'DepartamentoController@store');
        Route::put('/', 'DepartamentoController@update');
        Route::delete('/{id?}', 'DepartamentoController@destroy');
    });

    Route::group(['prefix' => 'dias-festivos'], function () {
        Route::get('/find', 'DiaFestivoController@find');
        Route::get('/{id}', 'DiaFestivoController@get');
        Route::post('/', 'DiaFestivoController@store');
        Route::put('/', 'DiaFestivoController@update');
        Route::delete('/{id?}', 'DiaFestivoController@destroy');
    });

    Route::group(['prefix' => 'reloj-checador'], function () {
        Route::get('/find', 'RelojChecadorController@find');
        Route::get('/{id}', 'RelojChecadorController@get');
        Route::post('/', 'RelojChecadorController@store');
        Route::put('/remover-empleado', 'RelojChecadorController@removeEmployee');
        Route::put('/', 'RelojChecadorController@update');
        Route::post('/delete', 'RelojChecadorController@destroy');
        Route::delete('/drop/{id?}', 'RelojChecadorController@drop');
    });

    Route::group(['prefix' => 'detalles-horarios'], function () {
        Route::get('/find', 'DetalleHorarioController@find');
        Route::get('/{id}', 'DetalleHorarioController@get');
        Route::post('/', 'DetalleHorarioController@store');
        Route::put('/', 'DetalleHorarioController@update');
        Route::delete('/{id?}', 'DetalleHorarioController@destroy');
    });

    Route::group(['prefix' => 'horarios'], function () {
        Route::get('/find', 'HorarioController@find');
        Route::get('/{id}', 'HorarioController@get');
        Route::post('/', 'HorarioController@store');
        Route::put('/', 'HorarioController@update');
        Route::delete('/{id?}', 'HorarioController@destroy');
    });

    Route::group(['prefix' => 'incidencias'], function () {
        Route::get('/find', 'IncidenciaController@find');
        Route::get('/{id}', 'IncidenciaController@get');
        Route::post('/', 'IncidenciaController@store');
        Route::post('/download', 'IncidenciaController@download');
        Route::post('/upload_documento', 'IncidenciaController@upload_documento');
        Route::put('/', 'IncidenciaController@update');
        Route::delete('/{id?}', 'IncidenciaController@destroy');
    });

    Route::group(['prefix' => 'documentos'], function () {
        Route::get('/find', 'DocumentoController@find');
        Route::get('/{id}', 'DocumentoController@get');
        Route::post('/', 'DocumentoController@store');
        Route::put('/', 'DocumentoController@update');
        Route::delete('/{id?}', 'DocumentoController@destroy');
    });

    Route::group(['prefix' => 'empleados'], function () {
        Route::get('/find', 'EmpleadoController@find');
        Route::get('/find-checked', 'EmpleadoController@findChecked');
        Route::get('/dashboard', 'EmpleadoController@dashboard');
        Route::get('/{id}', 'EmpleadoController@get');
        Route::post('/', 'EmpleadoController@store');
        Route::post('/importar', 'EmpleadoController@importar');
        Route::put('/', 'EmpleadoController@update');
        Route::post('/eliminar', 'EmpleadoController@destroy');
        Route::post('/reingreso', 'EmpleadoController@reingreso');
        Route::post('/upload-documento', 'ArchivoController@documentoEmpleadoStore');
        Route::post('/update-documento', 'ArchivoController@documentoEmpleadoUpdate');
        Route::delete('/documento/{id?}', 'ArchivoController@documentoEmpleadoDelete');
        Route::post('/asignacion', 'EmpleadoController@asignacion');
    });

    Route::group(['prefix' => 'registros-checadores'], function () {
        Route::get('/find', 'RegistroChecadorController@find');
        Route::get('/historico', 'RegistroChecadorController@historico');
        Route::get('last-id/{empresa_id}', 'RegistroChecadorController@getLastIdAsp');
        Route::get('/{id}', 'RegistroChecadorController@get');
        Route::post('/', 'RegistroChecadorController@store');
        Route::post('create-asp/', 'RegistroChecadorController@storeFromAsp');
        Route::put('/', 'RegistroChecadorController@update');
        Route::delete('/{id?}', 'RegistroChecadorController@destroy');
    });

    Route::group(['prefix' => 'configuraciones'], function () {
        Route::get('/find', 'ConfiguracionController@find');
        Route::get('/{id}', 'ConfiguracionController@get');
        Route::post('/', 'ConfiguracionController@store');
        Route::put('/', 'ConfiguracionController@update');
        Route::delete('/{id?}', 'ConfiguracionController@destroy');
    });

    Route::group(['prefix' => 'reportes'], function () {
        Route::get('/asistencia', 'ReporteController@asistencia');
        Route::get('/incidencias', 'ReporteController@incidencias');
    });

    Route::group(['prefix' => 'historico_empleado'], function () {
        Route::get('/find', 'HistoricoEmpleadoController@find');
        Route::get('/{id}', 'HistoricoEmpleadoController@get');
        Route::post('/', 'HistoricoEmpleadoController@store');
        Route::put('/', 'HistoricoEmpleadoController@update');
        Route::delete('/{id?}', 'HistoricoEmpleadoController@destroy');
    });

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/rotacion', 'EmpleadoController@rotacion');
        Route::get('/incidencias', 'EmpleadoController@incidencias');
    });

});
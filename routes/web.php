<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("https://pbmg.com.mx/");
});

Route::get('documentos/{folder}/{slug}', [
    'as'         => 'images.show',
    'uses'       => 'FilesController@show',
    'middleware' => 'auth:api',
]);

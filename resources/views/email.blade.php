<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Notificación</title>
</head>
<body>
    <div>
        <div class="container">
            <form>
                {{ csrf_field() }}
                <body>
                <h2> {{ $titulo }}</h2>
                    {{ $contenido }}
                </body>
            </form>
        </div>
    </div>
</body>
</html>